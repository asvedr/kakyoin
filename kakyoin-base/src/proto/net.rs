use mddd::macros::auto_impl;

use crate::entities::common::DynFut;
use crate::entities::errors::NetError;
use crate::entities::net::HttpResp;

#[auto_impl(link, dyn)]
pub trait IHttpClient: Send + Sync {
    fn get(&self, base_url: &str, path: &str) -> DynFut<Result<HttpResp, NetError>>;
    fn post(&self, base_url: &str, path: &str, data: Vec<u8>)
        -> DynFut<Result<HttpResp, NetError>>;
}
