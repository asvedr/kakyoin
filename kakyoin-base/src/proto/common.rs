use std::io;
use std::path::{Path, PathBuf};

use mddd::macros::auto_impl;
use rsa::{RsaPrivateKey, RsaPublicKey};

use crate::entities::errors::CryptoErr;
use crate::entities::fs::FileMeta;
use crate::entities::log::Level;

#[auto_impl(link, dyn)]
pub trait ILogger: Send + Sync {
    fn log(&self, level: Level, msg: &str);
}

#[auto_impl(link, dyn)]
pub trait IFS: Send + Sync {
    fn exec_dir(&self) -> io::Result<PathBuf>;
    fn create_dir(&self, path: &Path) -> io::Result<()>;
    fn read_file(&self, path: &Path) -> io::Result<Vec<u8>>;
    fn write_file(&self, path: &Path, data: &[u8]) -> io::Result<()>;
    fn append_file(&self, path: &Path, data: &[u8]) -> io::Result<()>;
    fn get_block(&self, path: &Path, block_id: u64) -> io::Result<Vec<u8>>;
    fn get_meta(&self, path: &Path) -> io::Result<FileMeta>;
}

#[auto_impl(link, dyn)]
pub trait ICryptoOps: Send + Sync {
    fn gen_priv_key(&self) -> Result<RsaPrivateKey, CryptoErr>;
    fn encrypt(&self, key: &RsaPublicKey, data: &[u8]) -> Result<Vec<u8>, CryptoErr>;
    fn decrypt(&self, key: &RsaPrivateKey, data: &[u8]) -> Result<Vec<u8>, CryptoErr>;
    fn sign_len(&self) -> usize;
    fn sign(&self, key: &RsaPrivateKey, data: &[u8]) -> Result<Vec<u8>, CryptoErr>;
    fn check_signature(
        &self,
        key: &RsaPublicKey,
        data: &[u8],
        signature: &[u8],
    ) -> Result<bool, CryptoErr>;
    fn get_key_code(&self, key: &RsaPublicKey) -> String;
}
