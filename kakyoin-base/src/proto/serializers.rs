use crate::entities::api_msg::{ApiMsg, ApiMsgPacked};
use crate::entities::errors::{PackerError, SerializerError};
use mddd::macros::auto_impl;
use rsa::{RsaPrivateKey, RsaPublicKey};

#[auto_impl(link, dyn)]
pub trait IApiMsgPacker: Send + Sync {
    fn pack(
        &self,
        msg: ApiMsg,
        enc_key: Option<&RsaPublicKey>,
        sig_key: &RsaPrivateKey,
    ) -> ApiMsgPacked;

    fn unpack(
        &self,
        packed: &ApiMsgPacked,
        check_key: Option<&RsaPublicKey>,
        dec_key: Option<&RsaPrivateKey>,
    ) -> Result<ApiMsg, PackerError>;
}

pub trait IStrSerializer<Obj>: Send + Sync {
    fn serialize(&self, obj: &Obj) -> String;
    fn deserialize(&self, txt: &str) -> Result<Obj, SerializerError>;
}
