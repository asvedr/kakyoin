use rsa::{RsaPrivateKey, RsaPublicKey};

use serdebin::{from_bytes, to_bytes};
use uuid::Uuid;

use crate::entities::api_msg::{ApiMsg, ApiMsgPacked};
use crate::entities::errors::PackerError;
use crate::proto::common::ICryptoOps;
use crate::proto::serializers::{IApiMsgPacker, IStrSerializer};

pub struct ApiMsgPacker<COps: ICryptoOps, KeySer: IStrSerializer<RsaPublicKey>> {
    crypto: COps,
    key_ser: KeySer,
}

impl<COps: ICryptoOps, KeySer: IStrSerializer<RsaPublicKey>> ApiMsgPacker<COps, KeySer> {
    pub fn new(crypto: COps, key_ser: KeySer) -> Self {
        Self { crypto, key_ser }
    }

    #[inline]
    fn encrypt(&self, msg_id: Uuid, key: &RsaPublicKey, json: Vec<u8>) -> Vec<u8> {
        match self.crypto.encrypt(key, &json) {
            Ok(val) => val,
            Err(err) => panic!(
                "Can not encrypt message(msg_id={:?}) (err={:?})",
                msg_id, err,
            ),
        }
    }

    #[inline]
    fn sign(&self, msg_id: Uuid, key: &RsaPrivateKey, data: &[u8]) -> Vec<u8> {
        match self.crypto.sign(key, data) {
            Ok(val) => val,
            Err(err) => panic!("Can not sign message(msg_id={:?}) (err={:?})", msg_id, err,),
        }
    }

    fn validate(&self, key: &RsaPublicKey, data: &[u8], sig: &[u8]) -> Result<(), PackerError> {
        let is_valid = self.crypto.check_signature(key, data, sig)?;
        if is_valid {
            Ok(())
        } else {
            Err(PackerError::SigMissmatch)
        }
    }
}

impl<COps: ICryptoOps, KeySer: IStrSerializer<RsaPublicKey>> IApiMsgPacker
    for ApiMsgPacker<COps, KeySer>
{
    fn pack(
        &self,
        msg: ApiMsg,
        enc_key: Option<&RsaPublicKey>,
        sig_key: &RsaPrivateKey,
    ) -> ApiMsgPacked {
        let msg_id = msg.header.id;
        let receiver = msg.header.receiver;
        let mut data = match to_bytes(msg) {
            Ok(val) => val,
            Err(err) => panic!("Can not serialize msg(msg_id={}), (err={:?})", msg_id, err),
        };
        let mut key_code = String::new();
        if let Some(key) = enc_key {
            data = self.encrypt(msg_id, key, data);
            key_code = self.crypto.get_key_code(key);
        }
        let signature = self.sign(msg_id, sig_key, &data);
        ApiMsgPacked {
            id: msg_id,
            receiver,
            key_code,
            data,
            signature,
        }
    }

    fn unpack(
        &self,
        packed: &ApiMsgPacked,
        check_key: Option<&RsaPublicKey>,
        dec_key: Option<&RsaPrivateKey>,
    ) -> Result<ApiMsg, PackerError> {
        if let Some(key) = check_key {
            self.validate(key, &packed.data, &packed.signature)?
        }
        let msg: ApiMsg = if let Some(key) = dec_key {
            let dec = self.crypto.decrypt(key, &packed.data)?;
            from_bytes(&dec)?.0
        } else {
            from_bytes(&packed.data)?.0
        };
        if check_key.is_none() {
            let key = self
                .key_ser
                .deserialize(&msg.header.sender_pub_key)
                .map_err(|_| PackerError::InvalidKeyInMessage)?;
            self.validate(&key, &packed.data, &packed.signature)?;
        }
        Ok(msg)
    }
}
