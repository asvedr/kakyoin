use std::str::FromStr;
use std::time::Duration;

use hyper::client::{Client, HttpConnector};
use hyper::{Body, Method, Request, Uri};
use tokio::time::timeout;

use crate::entities::common::DynFut;
use crate::entities::errors::NetError;
use crate::entities::net::HttpResp;
use crate::proto::net::IHttpClient;

pub struct HttpClient {
    timeout: Duration,
    client: Client<HttpConnector>,
}

impl HttpClient {
    pub fn new(timeout: u64) -> Self {
        Self {
            timeout: Duration::from_secs(timeout),
            client: Client::new(),
        }
    }
}

impl IHttpClient for HttpClient {
    fn get(&self, base_url: &str, path: &str) -> DynFut<Result<HttpResp, NetError>> {
        let url = format!("http://{}/{}", base_url, path);
        let uri = match Uri::from_str(&url) {
            Ok(val) => val,
            _ => return Box::pin(async { Err(NetError::InvalidUrl) }),
        };
        let get = self.client.get(uri);
        let get = timeout(self.timeout, get);
        let fut = async move {
            let resp = get
                .await
                .map_err(|_| NetError::Timeout)?
                .map_err(|_| NetError::ConnectionFailed)?;
            let code = resp.status().as_u16();
            let body = hyper::body::to_bytes(resp)
                .await
                .map_err(|_| NetError::CanNotReadBody)?
                .to_vec();
            Ok(HttpResp { code, body })
        };
        Box::pin(fut)
    }

    fn post(
        &self,
        base_url: &str,
        path: &str,
        data: Vec<u8>,
    ) -> DynFut<Result<HttpResp, NetError>> {
        let url = format!("http://{}/{}", base_url, path);
        let uri = match Uri::from_str(&url) {
            Ok(val) => val,
            _ => return Box::pin(async { Err(NetError::InvalidUrl) }),
        };
        let req = Request::builder()
            .method(Method::POST)
            .uri(uri)
            .body(Body::from(data))
            .expect("post request");
        let post = self.client.request(req);
        let post = timeout(self.timeout, post);
        let fut = async move {
            let resp = post
                .await
                .map_err(|_| NetError::Timeout)?
                .map_err(|_| NetError::ConnectionFailed)?;
            let code = resp.status().as_u16();
            let body = hyper::body::to_bytes(resp)
                .await
                .map_err(|_| NetError::CanNotReadBody)?
                .to_vec();
            Ok(HttpResp { code, body })
        };
        Box::pin(fut)
    }
}
