use std::io::Write;
use std::sync::Mutex;

use crate::entities::log::Level;
use crate::proto::common::ILogger;

pub struct Logger {
    pub level: Level,
    pub prefix_gen: Box<dyn Fn() -> String>,
    pub output: Mutex<Box<dyn Write>>,
}

unsafe impl Send for Logger {}
unsafe impl Sync for Logger {}

impl Logger {
    pub fn new(level: Level, prefix_gen: Box<dyn Fn() -> String>, output: Box<dyn Write>) -> Self {
        Self {
            level,
            prefix_gen,
            output: Mutex::new(output),
        }
    }

    pub fn mock() -> Self {
        struct Nowhere;

        impl Write for Nowhere {
            fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
                Ok(buf.len())
            }

            fn flush(&mut self) -> std::io::Result<()> {
                Ok(())
            }
        }

        Self {
            level: Level::Err,
            prefix_gen: Box::new(|| "".to_string()),
            output: Mutex::new(Box::new(Nowhere)),
        }
    }
}

impl ILogger for Logger {
    fn log(&self, level: Level, msg: &str) {
        if level > self.level {
            return;
        }
        let mut output = self.output.lock().expect("log mutex failed");
        let _ = writeln!(output, "[{:?}][{}] {}", level, (self.prefix_gen)(), msg,);
    }
}
