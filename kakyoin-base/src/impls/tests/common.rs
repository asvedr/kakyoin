use std::path::Path;

use mddd::macros::singleton;
use rsa::{RsaPrivateKey, RsaPublicKey};

use crate::entities::log::Level;
use crate::impls::fs::FS;
use crate::impls::serializers::rsa_key;
use crate::proto::common::{ILogger, IFS};
use crate::proto::serializers::IStrSerializer;

#[derive(Default)]
pub struct MockLogger;

impl ILogger for MockLogger {
    fn log(&self, _level: Level, _msg: &str) {}
}

#[singleton]
pub fn priv_key_1() -> RsaPrivateKey {
    let data = FS.read_file(Path::new("test_data/priv_key1.txt")).unwrap();
    let text = String::from_utf8(data).unwrap();
    rsa_key::Serializer.deserialize(&text).unwrap()
}

#[singleton]
pub fn priv_key_2() -> RsaPrivateKey {
    let data = FS.read_file(Path::new("test_data/priv_key2.txt")).unwrap();
    let text = String::from_utf8(data).unwrap();
    rsa_key::Serializer.deserialize(&text).unwrap()
}

#[singleton]
pub fn pub_key_1() -> RsaPublicKey {
    let data = FS.read_file(Path::new("test_data/pub_key1.txt")).unwrap();
    let text = String::from_utf8(data).unwrap();
    rsa_key::Serializer.deserialize(&text).unwrap()
}

#[singleton]
pub fn pub_key_2() -> RsaPublicKey {
    let data = FS.read_file(Path::new("test_data/pub_key2.txt")).unwrap();
    let text = String::from_utf8(data).unwrap();
    rsa_key::Serializer.deserialize(&text).unwrap()
}
