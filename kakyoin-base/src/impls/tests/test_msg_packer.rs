use crate::entities::api_msg::{ApiMsg, ApiMsgBody, ApiMsgBodyText, ApiMsgHeader};
use crate::entities::errors::PackerError;
use crate::impls::crypto_ops::CryptoOps;
use crate::impls::msg_packer::ApiMsgPacker;
use crate::impls::serializers::rsa_key;
use crate::impls::tests::common::{priv_key_1, priv_key_2, pub_key_1, pub_key_2, MockLogger};
use crate::proto::serializers::{IApiMsgPacker, IStrSerializer};
use uuid::Uuid;

fn packer() -> Box<dyn IApiMsgPacker> {
    Box::new(ApiMsgPacker::new(
        CryptoOps::new(MockLogger),
        rsa_key::Serializer,
    ))
}

fn msg() -> ApiMsg {
    let sender_pub_key = rsa_key::Serializer.serialize(pub_key_2());
    ApiMsg {
        header: ApiMsgHeader {
            id: Uuid::NAMESPACE_URL,
            sender: Uuid::NAMESPACE_X500,
            sender_pub_key,
            receiver: Uuid::NAMESPACE_OID,
            receiver_key_code: vec![1, 2, 3, 4],
            ts: 0,
        },
        body: ApiMsgBody::Text(Box::new(ApiMsgBodyText {
            chat: Some(Uuid::NAMESPACE_X500),
            text: "Hello world".to_string(),
        })),
    }
}

#[test]
fn test_pack_unpack_ok() {
    let packed = packer().pack(msg(), Some(pub_key_1()), priv_key_2());
    let unpacked = packer()
        .unpack(&packed, Some(pub_key_2()), Some(priv_key_1()))
        .unwrap();
    assert_eq!(unpacked, msg())
}

#[test]
fn test_pack_unpack_invalid_sig() {
    let packed = packer().pack(msg(), Some(pub_key_1()), priv_key_2());
    let err = packer()
        .unpack(&packed, Some(pub_key_1()), Some(priv_key_1()))
        .unwrap_err();
    assert_eq!(err, PackerError::SigMissmatch)
}

#[test]
fn test_pack_unpack_can_not_decrypt() {
    let packed = packer().pack(msg(), Some(pub_key_1()), priv_key_2());
    let err = packer()
        .unpack(&packed, Some(pub_key_2()), Some(priv_key_2()))
        .unwrap_err();
    assert_eq!(err, PackerError::CanNotDecrypt)
}

#[test]
fn test_pack_unpack_malformed() {
    let packed = packer().pack(msg(), Some(pub_key_1()), priv_key_2());
    let err = packer().unpack(&packed, None, None).unwrap_err();
    assert_eq!(err, PackerError::InvalidBinData)
}
