use crate::entities::errors::CryptoErr;
use crate::impls::crypto_ops::CryptoOps;
use crate::impls::tests::common::{priv_key_1, priv_key_2, pub_key_1, pub_key_2, MockLogger};
use crate::proto::common::ICryptoOps;

fn ops() -> CryptoOps<MockLogger> {
    CryptoOps::new(MockLogger)
}

#[test]
fn test_encrypt_decrypt_ok() {
    let message: &[u8] = b"hello world!?!?!";
    let enc = ops().encrypt(pub_key_1(), message).unwrap();
    let dec = ops().decrypt(priv_key_1(), &enc).unwrap();
    assert_eq!(message, dec);
}

#[test]
fn test_encrypt_decrypt_err() {
    let message = b"hello world!?!?!";
    let enc = ops().encrypt(pub_key_1(), message).unwrap();
    let err = ops().decrypt(priv_key_2(), &enc).unwrap_err();
    assert_eq!(err, CryptoErr::CanNotDecrypt);
}

#[test]
fn test_sign_verify_ok() {
    let message: &[u8] = b"hello world!?!?!";
    let sig = ops().sign(priv_key_1(), message).unwrap();
    let checked = ops().check_signature(pub_key_1(), message, &sig).unwrap();
    assert!(checked);
}

#[test]
fn test_sign_verify_err() {
    let message: &[u8] = b"hello world!?!?!";
    let sig = ops().sign(priv_key_1(), message).unwrap();
    let checked = ops().check_signature(pub_key_2(), message, &sig).unwrap();
    assert!(!checked);
}

#[test]
fn test_get_code() {
    let code1 = ops().get_key_code(pub_key_1());
    let code2 = ops().get_key_code(pub_key_2());
    let code3 = ops().get_key_code(pub_key_1());
    assert_eq!(code1, code3);
    assert_ne!(code1, code2);
}
