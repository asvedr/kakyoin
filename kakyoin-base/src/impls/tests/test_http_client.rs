use crate::impls::http_client::HttpClient;
use crate::proto::net::IHttpClient;

#[tokio::test]
async fn test_ping_google() {
    let client = HttpClient::new(5);
    let resp = client.get("google.com", "").await.unwrap();
    assert_eq!(resp.code, 301);
}
