use crate::entities::contact::{ClientContact, ServerContact};
use crate::impls::serializers::{client_contact, rsa_key, server_contact};
use crate::impls::tests::common::{priv_key_1, pub_key_1};
use crate::proto::serializers::IStrSerializer;
use rsa::{RsaPrivateKey, RsaPublicKey};
use uuid::Uuid;

#[test]
fn test_server_contact() {
    let ser = server_contact::Serializer;
    let contact = ServerContact::new("127.0.0.1:8080".to_string(), pub_key_1());
    let text = ser.serialize(&contact);
    assert_eq!(ser.deserialize(&text).unwrap(), contact)
}

#[test]
fn test_client_contact() {
    let ser = client_contact::Serializer;
    let contact = ClientContact::new(Uuid::NAMESPACE_DNS, pub_key_1());
    let text = ser.serialize(&contact);
    assert_eq!(ser.deserialize(&text).unwrap(), contact)
}

#[test]
fn test_priv_key() {
    let ser = rsa_key::Serializer;
    let key = priv_key_1();
    let text = ser.serialize(key);
    let des: RsaPrivateKey = ser.deserialize(&text).unwrap();
    assert_eq!(&des, key)
}

#[test]
fn test_pub_key() {
    let ser = rsa_key::Serializer;
    let key = pub_key_1();
    let text = ser.serialize(key);
    let des: RsaPublicKey = ser.deserialize(&text).unwrap();
    assert_eq!(&des, key)
}
