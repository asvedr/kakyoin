use base64::{engine::general_purpose::STANDARD as B64, Engine as _};
use rand::rngs::OsRng;
use rand::thread_rng;
use rsa::traits::{PublicKeyParts, SignatureScheme};
use rsa::{Oaep, Pkcs1v15Sign, RsaPrivateKey, RsaPublicKey};
use sha2::{Digest, Sha256};

use crate::entities::errors::CryptoErr;
use crate::entities::log;
use crate::proto::common::{ICryptoOps, ILogger};

const ENC_CHUNK_SIZE: usize = 425;
const DEC_CHUNK_SIZE: usize = 512;

type CryptoRng = OsRng;

pub struct CryptoOps<L: ILogger> {
    logger: L,
}

impl<L: ILogger> CryptoOps<L> {
    pub fn new(logger: L) -> Self {
        Self { logger }
    }
}

fn padding() -> Oaep {
    Oaep::new::<Sha256>()
}

fn chunkify(src: &[u8], chunk_len: usize) -> Vec<&[u8]> {
    let limit = src.len() / chunk_len;
    let mut result = Vec::new();
    for i in 0..limit {
        let chunk = &src[i * chunk_len..(i + 1) * chunk_len];
        result.push(chunk);
    }
    if limit * chunk_len < src.len() {
        result.push(&src[limit * chunk_len..]);
    }
    result
}

impl<L: ILogger> ICryptoOps for CryptoOps<L> {
    fn gen_priv_key(&self) -> Result<RsaPrivateKey, CryptoErr> {
        let err = match RsaPrivateKey::new(&mut CryptoRng::default(), 4096) {
            Ok(val) => return Ok(val),
            Err(val) => val,
        };
        self.logger
            .log(log::Level::Err, &format!("Key generation error: {:?}", err));
        Err(CryptoErr::CanNotGenerateKey)
    }

    fn encrypt(&self, key: &RsaPublicKey, data: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        let mut result = Vec::new();
        for chunk in chunkify(data, ENC_CHUNK_SIZE) {
            match key.encrypt(&mut CryptoRng::default(), padding(), chunk) {
                Ok(mut val) => result.append(&mut val),
                Err(err) => {
                    self.logger
                        .log(log::Level::Err, &format!("Encryption error: {:?}", err));
                    return Err(CryptoErr::CanNotEncrypt);
                }
            };
        }
        Ok(result)
    }

    fn decrypt(&self, key: &RsaPrivateKey, data: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        let mut result = Vec::new();
        for chunk in chunkify(data, DEC_CHUNK_SIZE) {
            match key.decrypt(padding(), chunk) {
                Ok(mut val) => result.append(&mut val),
                Err(_) => return Err(CryptoErr::CanNotDecrypt),
            }
        }
        Ok(result)
    }

    fn sign_len(&self) -> usize {
        // sha256 len
        512
    }

    fn sign(&self, key: &RsaPrivateKey, data: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        let scheme = Pkcs1v15Sign::new::<Sha256>();
        let mut hasher = Sha256::new();
        hasher.update(data);
        let hashed = hasher.finalize();
        let mut rng = thread_rng();
        scheme
            .sign(Some(&mut rng), key, &hashed)
            .map_err(|_| CryptoErr::CanNotSign)
    }

    fn check_signature(
        &self,
        key: &RsaPublicKey,
        data: &[u8],
        signature: &[u8],
    ) -> Result<bool, CryptoErr> {
        let scheme = Pkcs1v15Sign::new::<Sha256>();
        let mut hasher = Sha256::new();
        hasher.update(data);
        let hashed = hasher.finalize();
        match scheme.verify(key, &hashed, signature) {
            Ok(()) => Ok(true),
            Err(rsa::Error::Verification) | Err(rsa::Error::Pkcs1(_)) => Ok(false),
            Err(err) => {
                let msg = format!("Verifier failed: {:?}", err);
                self.logger.log(log::Level::Err, &msg);
                Err(CryptoErr::CanNotValidate)
            }
        }
    }

    fn get_key_code(&self, key: &RsaPublicKey) -> String {
        let mut hasher = sha2::Sha256::new();
        hasher.update(key.n().to_bytes_le());
        hasher.update(key.e().to_bytes_le());
        let res = hasher.finalize();
        B64.encode(res)
    }
}
