use crate::entities::fs::FileMeta;
use std::env::current_exe;
use std::fs::{metadata, File, OpenOptions, create_dir_all};
use std::io;
use std::io::{Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};

use crate::proto::common::IFS;

pub struct FS;

const BLOCK_SIZE: u64 = 1024 * 10;

impl IFS for FS {
    fn exec_dir(&self) -> io::Result<PathBuf> {
        let exe_path = current_exe()?;
        if let Some(val) = exe_path.parent() {
            Ok(val.to_path_buf())
        } else {
            Ok(".".into())
        }
    }

    fn create_dir(&self, path: &Path) -> io::Result<()> {
        if path.is_dir() {
            return Ok(())
        }
        create_dir_all(path)
    }

    fn read_file(&self, path: &Path) -> io::Result<Vec<u8>> {
        let mut file = File::open(path)?;
        let mut buf = Vec::new();
        file.read_to_end(&mut buf)?;
        Ok(buf)
    }

    fn write_file(&self, path: &Path, data: &[u8]) -> io::Result<()> {
        File::create(path)?.write_all(data)
    }

    fn append_file(&self, path: &Path, data: &[u8]) -> io::Result<()> {
        if !path.exists() {
            return self.write_file(path, data);
        }
        let mut file = OpenOptions::new().write(true).append(true).open(path)?;
        file.seek(SeekFrom::End(0))?;
        file.write_all(data)
    }

    fn get_block(&self, path: &Path, block_id: u64) -> io::Result<Vec<u8>> {
        let mut file = File::open(path)?;
        let max_len = file.metadata()?.len();
        let begin = block_id * BLOCK_SIZE;
        let len = BLOCK_SIZE.min(max_len - begin);
        file.seek(SeekFrom::Start(begin))?;
        let mut buf = vec![0; len as usize];
        file.read_exact(&mut buf)?;
        Ok(buf)
    }

    fn get_meta(&self, path: &Path) -> io::Result<FileMeta> {
        let size = metadata(path)?.len();
        let mut blocks = size / BLOCK_SIZE;
        if blocks * BLOCK_SIZE < size {
            blocks += 1
        }
        Ok(FileMeta { blocks, size })
    }
}
