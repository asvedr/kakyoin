use base64::{engine::general_purpose::STANDARD as B64, Engine as _};

use crate::entities::contact::ServerContact;
use crate::entities::errors::SerializerError;
use crate::impls::serializers::common::{de_bts, de_str, ser_bts};
use crate::proto::serializers::IStrSerializer;

#[derive(Copy, Clone)]
pub struct Serializer;

impl IStrSerializer<ServerContact> for Serializer {
    fn serialize(&self, obj: &ServerContact) -> String {
        let mut result = Vec::new();
        ser_bts(&mut result, obj.addr.as_bytes());
        ser_bts(&mut result, &obj.n);
        ser_bts(&mut result, &obj.e);
        B64.encode(result)
    }

    fn deserialize(&self, txt: &str) -> Result<ServerContact, SerializerError> {
        let bts = B64.decode(txt)?;
        let (addr, bts) = de_str(&bts)?;
        let (n, bts) = de_bts(bts)?;
        let (e, _) = de_bts(bts)?;
        Ok(ServerContact {
            addr,
            n: n.to_vec(),
            e: e.to_vec(),
        })
    }
}
