use rsa::pkcs1::{
    DecodeRsaPrivateKey, DecodeRsaPublicKey, EncodeRsaPrivateKey, EncodeRsaPublicKey, LineEnding,
};
use rsa::{RsaPrivateKey, RsaPublicKey};

use crate::entities::errors::SerializerError;
use crate::proto::serializers::IStrSerializer;

#[derive(Clone, Copy)]
pub struct Serializer;

struct Schema {
    start: &'static str,
    end: &'static str,
    len: usize,
}

const PRIV_KEY_SCHEMA: Schema = Schema {
    start: "-----BEGIN RSA PRIVATE KEY-----",
    end: "-----END RSA PRIVATE KEY-----\n",
    len: 64,
};

const PUB_KEY_SCHEMA: Schema = Schema {
    start: "-----BEGIN RSA PUBLIC KEY-----",
    end: "-----END RSA PUBLIC KEY-----\n",
    len: 64,
};

fn compress(key: &str, schema: &Schema) -> String {
    key.trim_start_matches(schema.start)
        .trim_end_matches(schema.end)
        .replace('\n', "")
}

fn decompress(key: &str, schema: &Schema) -> String {
    let mut lines = vec![schema.start];
    let k_len = key.len();
    let limit = k_len / schema.len;
    for i in 0..limit {
        let begin = schema.len * i;
        let end = schema.len * (i + 1);
        lines.push(&key[begin..end]);
    }
    if limit < k_len {
        lines.push(&key[schema.len * limit..])
    }
    lines.push(schema.end);
    lines.join("\n")
}

impl IStrSerializer<RsaPrivateKey> for Serializer {
    fn serialize(&self, key: &RsaPrivateKey) -> String {
        let zeroze = key.to_pkcs1_pem(LineEnding::LF).unwrap();
        compress(zeroze.as_ref(), &PRIV_KEY_SCHEMA)
    }

    fn deserialize(&self, bts: &str) -> Result<RsaPrivateKey, SerializerError> {
        let bts = decompress(bts, &PRIV_KEY_SCHEMA);
        RsaPrivateKey::from_pkcs1_pem(&bts).map_err(|_| SerializerError::InvalidData)
    }
}

impl IStrSerializer<RsaPublicKey> for Serializer {
    fn serialize(&self, key: &RsaPublicKey) -> String {
        let text = key.to_pkcs1_pem(LineEnding::LF).unwrap();
        compress(&text, &PUB_KEY_SCHEMA)
    }

    fn deserialize(&self, bts: &str) -> Result<RsaPublicKey, SerializerError> {
        let bts = decompress(bts, &PUB_KEY_SCHEMA);
        RsaPublicKey::from_pkcs1_pem(&bts).map_err(|_| SerializerError::InvalidData)
    }
}
