use crate::entities::errors::SerializerError;

type LenType = u16;

#[inline]
fn ser_len(acc: &mut Vec<u8>, len: usize) {
    acc.extend_from_slice(&(len as LenType).to_le_bytes())
}

#[inline]
fn de_len(acc: &[u8]) -> Result<(usize, &[u8]), SerializerError> {
    let mut buf = (0 as LenType).to_le_bytes();
    if acc.len() < buf.len() {
        return Err(SerializerError::UnexpectedEOF);
    }
    let buf_len = buf.len();
    buf.copy_from_slice(&acc[..buf_len]);
    let len = LenType::from_le_bytes(buf) as usize;
    Ok((len, &acc[buf.len()..]))
}

pub fn ser_bts(acc: &mut Vec<u8>, bts: &[u8]) {
    ser_len(acc, bts.len());
    acc.extend_from_slice(bts);
}

pub fn de_bts(acc: &[u8]) -> Result<(&[u8], &[u8]), SerializerError> {
    let (len, acc) = de_len(acc)?;
    if acc.len() < len {
        return Err(SerializerError::UnexpectedEOF);
    }
    Ok((&acc[..len], &acc[len..]))
}

pub fn de_str(acc: &[u8]) -> Result<(String, &[u8]), SerializerError> {
    let (bts, rest) = de_bts(acc)?;
    Ok((String::from_utf8(bts.to_vec())?, rest))
}
