use base64::{engine::general_purpose::STANDARD as B64, Engine as _};
use uuid::Uuid;

use crate::entities::contact::ClientContact;
use crate::entities::errors::SerializerError;
use crate::impls::serializers::common::{de_bts, ser_bts};
use crate::proto::serializers::IStrSerializer;

#[derive(Copy, Clone)]
pub struct Serializer;

impl IStrSerializer<ClientContact> for Serializer {
    fn serialize(&self, obj: &ClientContact) -> String {
        let mut result = Vec::new();
        ser_bts(&mut result, &obj.id.to_bytes_le());
        ser_bts(&mut result, &obj.n);
        ser_bts(&mut result, &obj.e);
        B64.encode(result)
    }

    fn deserialize(&self, txt: &str) -> Result<ClientContact, SerializerError> {
        let bts = B64.decode(txt)?;
        let (id_bts, bts) = de_bts(&bts)?;
        let id = Uuid::from_bytes_le(
            id_bts
                .try_into()
                .map_err(|_| SerializerError::InvalidData)?,
        );
        let (n, bts) = de_bts(bts)?;
        let (e, _) = de_bts(bts)?;
        Ok(ClientContact {
            id,
            n: n.to_vec(),
            e: e.to_vec(),
        })
    }
}
