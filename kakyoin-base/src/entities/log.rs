use std::str::FromStr;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq, Copy, Clone)]
pub enum Level {
    Err,
    Warn,
    Debug,
}

impl FromStr for Level {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, ()> {
        Ok(match &s.to_lowercase()[..] {
            "debug" => Self::Debug,
            "warn" => Self::Warn,
            "err" => Self::Err,
            _ => return Err(()),
        })
    }
}
