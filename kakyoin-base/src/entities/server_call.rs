use std::collections::HashMap;

use rsa::RsaPublicKey;
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

use crate::entities::api_msg::{ApiMsgPacked, ApiMsgStatus};

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub enum ServerCall {
    SendMessages {
        token: Vec<u8>,
        msgs: Vec<ApiMsgPacked>,
    },
    FetchMessages {
        token: Vec<u8>,
    },
    GetStatuses {
        token: Vec<u8>,
        ids: Vec<Uuid>,
    },
    MarkReceived {
        token: Vec<u8>,
        msgs: Vec<Uuid>,
    },
    GetToken {
        // request_time: u64,
        user_id: Uuid,
        pub_key: RsaPublicKey,
    },
    ReqMetaHash {
        token: Vec<u8>,
        users: Vec<(Uuid, String)>,
    },
    ReqMeta {
        token: Vec<u8>,
        user: Uuid,
        key_code: String,
    },
    PostMyMeta {
        token: Vec<u8>,
        meta: Box<ApiUserMeta>,
    },
}

#[derive(Deserialize, Serialize)]
pub enum ServerResponse {
    Nothing,
    Messages {
        msgs: Vec<ApiMsgPacked>,
    },
    Statuses {
        map: HashMap<Uuid, ApiMsgStatus>,
    },
    Token {
        token: Vec<u8>,
        expired_at: u64,
    },
    MetaHashes {
        users: Vec<((Uuid, String), String)>,
    },
    UserMeta {
        meta: Box<ApiUserMeta>,
    },
}

#[derive(Clone, Default, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ApiUserMeta {
    pub name: String,
    pub description: String,
    pub photo: String,
    pub hash: String,
}
