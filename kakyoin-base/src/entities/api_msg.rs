use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

use crate::entities::contact::ClientContact;

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ApiMsg {
    pub header: ApiMsgHeader,
    pub body: ApiMsgBody,
}

#[derive(Deserialize, Serialize, Copy, Clone, Debug, Eq, PartialEq)]
pub enum ApiMsgStatus {
    Received,
    NotReceived,
    NotFound,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct ApiMsgPacked {
    pub receiver: Uuid,
    pub id: Uuid,
    // receiver key code
    pub key_code: String,
    pub data: Vec<u8>,
    pub signature: Vec<u8>,
}

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ApiMsgHeader {
    pub id: Uuid,
    pub sender: Uuid,
    pub sender_pub_key: String,
    pub receiver: Uuid,
    pub receiver_key_code: Vec<u8>,
    pub ts: u64,
}

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub enum ApiMsgBody {
    Nothing,
    Text(Box<ApiMsgBodyText>),
    DeclareFile(Box<ApiMsgBodyDeclareFile>),
    ReqFilePart(Box<ApiMsgBodyReqFilePart>),
    SendFilePart(Box<ApiMsgBodySendFilePart>),
    AddToChat(Box<ApiMsgBodyAddToChat>),
    // ReqMeta, //(Box<ApiMsgBodyReqMeta>),
    // SendMeta(Box<ApiUserMeta>),
    // GetMetaHash, //(Box<ApiMsgBodyGetMetaHash>),
    // ConfirmReceived(Box<ApiMsgBodyConfirmReceived>),
}

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ApiMsgBodyText {
    pub chat: Option<Uuid>,
    pub text: String,
}

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ApiMsgBodyDeclareFile {
    pub chat: Option<Uuid>,
    pub file_id: Uuid,
    pub name: String,
    pub size: u64,
    pub parts: u64,
}

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ApiMsgBodyReqFilePart {
    pub file_id: Uuid,
    pub part: u64,
}

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ApiMsgBodySendFilePart {
    pub file_id: Uuid,
    pub part: u64,
    pub data: Vec<u8>,
}

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ApiMsgBodyAddToChat {
    pub chat_id: Uuid,
    pub chat_name: String,
    pub other_participants: Vec<ClientContact>,
}
