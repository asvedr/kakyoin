use rsa::traits::PublicKeyParts;
use rsa::{BigUint, RsaPublicKey};
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone, Deserialize, Serialize, Debug, Eq, PartialEq)]
pub struct ClientContact {
    pub id: Uuid,
    pub(crate) n: Vec<u8>,
    pub(crate) e: Vec<u8>,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct ServerContact {
    pub addr: String,
    pub(crate) n: Vec<u8>,
    pub(crate) e: Vec<u8>,
}

#[inline]
fn make_key(n: &[u8], e: &[u8]) -> Result<RsaPublicKey, String> {
    let n = BigUint::from_bytes_le(n);
    let e = BigUint::from_bytes_le(e);
    RsaPublicKey::new(n, e).map_err(|err| err.to_string())
}

impl ServerContact {
    pub fn new(addr: String, key: &RsaPublicKey) -> Self {
        Self {
            addr,
            n: key.n().to_bytes_le(),
            e: key.e().to_bytes_le(),
        }
    }

    pub fn get_key(&self) -> Result<RsaPublicKey, String> {
        make_key(&self.n, &self.e)
    }
}

impl ClientContact {
    pub fn new(id: Uuid, key: &RsaPublicKey) -> Self {
        Self {
            id,
            n: key.n().to_bytes_le(),
            e: key.e().to_bytes_le(),
        }
    }

    pub fn get_key(&self) -> Result<RsaPublicKey, String> {
        make_key(&self.n, &self.e)
    }
}
