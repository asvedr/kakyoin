use std::future::Future;
use std::pin::Pin;

pub type DynFut<T> = Pin<Box<dyn Future<Output = T> + Send + 'static>>;
pub type DynFutRes<T, E> = DynFut<Result<T, E>>;
