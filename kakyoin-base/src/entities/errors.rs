use mddd::macros::EnumAutoFrom;
use std::string::FromUtf8Error;

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum CryptoErr {
    CanNotGenerateKey,
    CanNotDecrypt,
    CanNotEncrypt,
    CanNotValidate,
    CanNotSign,
}

#[derive(Clone, Debug, EnumAutoFrom, Eq, PartialEq)]
pub enum PackerError {
    SigMissmatch,
    #[accept(CryptoErr)]
    CanNotDecrypt,
    InvalidBodyType,
    #[accept(serde_json::Error)]
    #[accept(base64::DecodeError)]
    #[accept(FromUtf8Error)]
    InvalidJson,
    #[accept(serdebin::SerializerError)]
    InvalidBinData,
    InvalidKeyInMessage,
}

#[derive(Debug, EnumAutoFrom)]
pub enum SerializerError {
    UnexpectedEOF,
    #[accept(base64::DecodeError)]
    #[accept(serde_json::Error)]
    #[accept(FromUtf8Error)]
    InvalidData,
    Custom(String),
    Crypto(CryptoErr),
}

#[derive(Clone, Debug, PartialEq)]
pub enum NetError {
    InvalidUrl,
    ConnectionFailed,
    CanNotReadBody,
    Timeout,
}
