import os
import subprocess as sp
from typing import Optional
from urllib.parse import urljoin

import requests
from pydantic import BaseModel


class Me(BaseModel):
    uuid: str
    priv_key: str
    pub_key: str
    key_code: str
    name: str
    description: str
    pic: str


class UserShort(BaseModel):
    uuid: str
    name: str
    pic: str
    key_code: str


class Server(BaseModel):
    addr: str
    key_code: str
    status: str


class MsgDecFile(BaseModel):
    name: str
    size: int
    user_id: Optional[str]
    file_id: str


class Msg(BaseModel):
    id: int
    uuid: str
    user: Optional[str]
    text: str
    file: Optional[MsgDecFile]
    read: bool
    sent: bool
    received: bool


class ChatShort(BaseModel):
    id: str
    name: str
    last_msg: str
    new: bool


class ChatParticipant(BaseModel):
    uuid: str
    name: str
    pic: str
    key_code: str


class ChatFull(BaseModel):
    id: str
    name: str
    participants: list[ChatParticipant]


class DownloadStatus(BaseModel):
    progress: float
    completed: bool
    failed: bool


class ResponseNot200(Exception):
    def __init__(self, status: int, data: bytes):
        self.status = status
        self.data = data

    def __str__(self):
        return f"<Unexpected response({self.status}): {self.data}>"

    __repr__ = __str__


class Client:
    def __init__(self, dbname: str, port: int, prefix: str):
        os.environ['kakyoin_db_file'] = str(dbname)
        os.environ['kakyoin_api_server_port'] = str(port)
        os.environ['kakyoin_log_level'] = 'debug'
        os.environ['kakyoin_log_prefix'] = prefix
        os.environ['kakyoin_report_atex'] = 't'
        self.port = port
        self.proc = sp.Popen(['./data/client'])
        self.auth_token = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.proc.kill()
        self.proc.wait(2)

    def init(self, name: str, key: str = None, pwd: str = None) -> None:
        js = {'name': name, 'description': 'Test'}
        if key:
            js['priv_key'] = key
        if pwd:
            js['password'] = pwd
        self._request('post', '/api/init/', js=js)

    def check_init(self) -> str:
        return self._request('get', '/api/check-init/')['status']

    def login(self, pwd: str) -> str:
        resp = self._request('post', '/api/login/', js={'password': pwd})
        token = resp['token']
        self.auth_token = token
        return token

    def add_server(self, contact: str) -> None:
        self._request('post', '/api/servers/', js={'data': contact})

    def del_server(self, addr: str) -> None:
        self._request('delete', f'/api/servers/?addr={addr}')

    def get_servers(self) -> list[Server]:
        return [
            Server(**item)
            for item in self._request('get', '/api/servers/')['data']
        ]

    def get_me(self) -> Me:
        return Me(**self._request('get', '/api/settings/me/'))

    def patch_me(
        self,
        uuid: str = None,
        priv_key: str = None,
        name: str = None,
        description: str = None,
        pic_path: str = None,
    ) -> None:
        js = {}
        if uuid:
            js['uuid'] = uuid
        if priv_key:
            js['priv_key'] = priv_key
        if name:
            js["name"] = name
        if description:
            js["description"] = description
        if pic_path:
            js["pic_path"] = pic_path
        self._request(
            'patch',
            '/api/settings/me/',
            js=js,
        )

    def get_contact(self) -> str:
        return self._request('get', '/api/settings/contact/')['data']

    def connect(self, data: str) -> None:
        self._request(
            'post',
            '/api/users/connect/',
            js={'contact': data},
        )

    def get_all_users(self) -> list[UserShort]:
        return [
            UserShort(**item)
            for item in self._request('get', '/api/users/list/')['data']
        ]

    def create_private_text_msg(
        self,
        user: str,
        text: str,
    ) -> None:
        self._request(
            'post',
            '/api/msgs/text/',
            js={"chat": f'U{user}', "text": text},
        )

    def create_private_file_msg(
        self,
        user: str,
        *,
        path: str = None,
        data: str = None,
    ) -> None:
        if bool(path) == bool(data):
            raise Exception("You must user 'path' XOR 'data'")
        js = {"chat": f'U{user}', "name": 'new-file'}
        if path:
            js["path"] = path
        if data:
            js["data"] = data
        self._request('post', '/api/msg/file/', js=js)

    def create_chat_text_msg(
        self,
        chat: str,
        text: str,
    ) -> None:
        self._request(
            'post',
            '/api/msgs/text/',
            js={"chat": chat, "text": text},
        )

    def get_private_msgs(self, user: str, last_msg: int = None) -> list[Msg]:
        path = f"/api/chats/msgs/?chat=U{user}"
        if last_msg is not None:
            path += f'&last_msg={last_msg}'
        return [
            Msg(**item)
            for item in self._request('get', path)['data']
        ]

    def get_chat_msgs(self, chat: str, last_msg: int = None) -> list[Msg]:
        path = f"/api/chats/msgs/?chat={chat}"
        if last_msg is not None:
            path += f'&last_msg={last_msg}'
        return [
            Msg(**item)
            for item in self._request('get', path)['data']
        ]

    def mark_read(self, msgs: list[str]) -> None:
        self._request('post', '/api/msgs/mark-read/', js={'msgs': msgs})

    def trigger_periodic(self, pname: str) -> None:
        self._request('post', f"/api/tasks/trigger-periodic/?name={pname}")

    def create_or_update_chat(self, name: str, users: list[str], chat_id: str = None) -> None:
        js = {"users": users, "name": name}
        if chat_id:
            js['chat_id'] = chat_id
        self._request('post', '/api/chats/', js=js)

    def get_chats(self) -> list[ChatShort]:
        return [
            ChatShort(**item)
            for item in self._request('get', '/api/chats/')['data']
        ]

    def get_chat_details(self, uid: str) -> ChatFull:
        return ChatFull(
            **self._request('get', f"/api/chats/info/?id={uid}")
        )

    def download(self, path: str, msg_id: str, chat: str = None, user: str = None) -> int:
        assert chat or user
        if user:
            chat = f'U{user}'
        resp = self._request(
            'post',
            '/api/files/download/',
            js={"chat": chat, "msg_id": msg_id, "path": path}
        )
        return resp['task_id']

    def check_download_status(self, task_id: int, user_id: str, file_id: str) -> DownloadStatus:
        query = f"task_id={task_id}&user_id={user_id}&file_id={file_id}"
        resp = self._request('get', f"/api/files/get-status/?{query}")
        return DownloadStatus(**resp)

    def _request(self, method: str, path: str, js: dict = None) -> dict:
        params = {
            'method': method,
            'url': urljoin(f'http://localhost:{self.port}/', path),
        }
        if js:
            params['json'] = js
        if self.auth_token:
            params['headers'] = {'Authorization': self.auth_token}
        resp = requests.request(**params)
        if resp.status_code != 200:
            print(f'REQUEST: {method} {path} {js}')
            raise ResponseNot200(
                resp.status_code,
                resp.content,
            )
        return resp.json()
