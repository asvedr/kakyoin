import time
from typing import Callable


def wait_until(
    check: Callable[[], bool],
    timeout: int,
    update: Callable[[], None] = None,
) -> None:
    if update:
        update()
    start = time.time()
    while time.time() - start < timeout:
        if check():
            return
        if update:
            update()
        time.sleep(1)
    raise Exception("Timeout exceed")
