import time

from client import Client
from server import Server

# port=8000
INVALID_CONTACT = 'DgAxMjcuMC4wLjE6ODAwMAACo/9mFbMyeR2RZXEMx4fckmJCNEUy569qP/C6eC3VFNJ4vQX+CxRzDSSxNbzkP1A0SlLP4QMtP3vdRngYgUrxR1nmE8ieBghU54ChU+Mdc8bkVCthzKGQcn/5aUE0uRIVKQOxTeMJ9dizGtJVCLvbylpkZawL7vXFWwyxK/sDXQky18TbM9tiRp3bUgXuaGE+3mlVQcHAWqvKqfgdXbrxti6+t3jbGyPJQzF4/4oTdCfU2uyLhpWDQoMB9eYDX9AdVJm3e2quvQIzXhgWJm2xvSQhYBThK6GUnVAAe4v02vgAPaFj8Y6xTLc+5gJKVes99OcgPV7OJ9GbvLsy4ez70kGt8UVNCE8WNSBi2718fNCOGgFC3nMUBJJuA1cfJXLAdZQINVE5dKSRuxS1Z6IXMEszFVFICmhOp3YqnfFXmTASM4UuaEqBFNGLmTpd60TpX50nixQ1uiiEzpzetNa/FCUliOXlovMnE0KukIIWW20dmWzuu9X/e3M3L9NacByU6wOvuotBkVmd0FAi57BtzVHuPIRaPD5unTl+c1iJKdfsC5ZMjFsazAH70IrnkuJB8q/sbifCCraSS+zwcAVYxbgqWg9rmNnSavao3zRWot0yNuMLVSjLujsB9NZNp+1S8i+TW/CUxf0/kzahA/AdVuCz26w38y9/SLvcwONxSqUDAAEAAQ=='


def test_alice_check_server(server: Server, alice: Client, alice_inited: None):
    print('Alice:', alice.check_init())
    alice.add_server(server.contact)
    alice.add_server(INVALID_CONTACT)
    print("Alice: server added")
    time.sleep(20)
    servers = alice.get_servers()
    assert len(servers) == 2
    by_addr = {s.addr: s for s in servers}
    assert by_addr['127.0.0.1:8080'].status == 'Valid'
    assert by_addr['127.0.0.1:8000'].status == 'Invalid'

    alice.del_server("'127.0.0.1:8000'")
    servers = alice.get_servers()
    assert len(servers) == 1
    by_addr = {s.addr: s for s in servers}
    assert by_addr['127.0.0.1:8080'].status == 'Valid'
