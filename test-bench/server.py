import os
import subprocess as sp


class Server:
    def __init__(self, port: int, contact: str):
        os.environ['kakyoin_port'] = str(port)
        os.environ['kakyoin_priv_key_path'] = './data/server-priv.key'
        os.environ['kakyoin_log_prefix'] = 'server,'
        os.environ['kakyoin_log_level'] = 'debug'
        self.contact = contact
        self.proc = sp.Popen(['./data/server', 'run'])

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.proc.kill()
        self.proc.wait(2)
