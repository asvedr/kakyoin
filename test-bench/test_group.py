import time

from client import Client
from server import Server
from utils import wait_until


def contact_all(server: Server, clients: list[Client]):
    for client in clients:
        client.add_server(server.contact)
    for client_a in clients:
        for client_b in clients:
            if client_a is client_b:
                continue
            client_a.connect(client_b.get_contact())


def test_create_chat_3(
    server: Server,
    alice: Client,
    bob: Client,
    alex: Client,
    triple_inited: None,
) -> None:
    contact_all(server, [alice, bob, alex])
    alice.create_or_update_chat(
        'My duudes',
        [bob.get_me().uuid, alex.get_me().uuid],
    )
    wait_until(
        check=lambda: any(c.id.startswith('C') for c in bob.get_chats()),
        update=lambda: bob.trigger_periodic('p_fetch_msgs'),
        timeout=15
    )
    wait_until(
        check=lambda: any(c.id.startswith('C') for c in alex.get_chats()),
        update=lambda: alex.trigger_periodic('p_fetch_msgs'),
        timeout=15,
    )
    chat_id = [
        chat.id
        for chat in bob.get_chats()
        if chat.id.startswith('C')
    ][0]

    alice_id = alice.get_me().uuid
    bob_id = bob.get_me().uuid
    alex_id = alex.get_me().uuid

    chat = bob.get_chat_details(chat_id)
    assert chat.name == 'My duudes'
    assert {p.uuid for p in chat.participants} == {alice_id, alex_id}
    chat = alex.get_chat_details(chat_id)
    assert chat.name == 'My duudes'
    assert {p.uuid for p in chat.participants} == {alice_id, bob_id}


def test_create_chat_2_and_invite(
    server: Server,
    alice: Client,
    bob: Client,
    alex: Client,
    triple_inited: None,
):
    contact_all(server, [alice, bob, alex])
    alice.create_or_update_chat('My duudes', [bob.get_me().uuid])
    wait_until(
        update=lambda: bob.trigger_periodic('p_fetch_msgs'),
        check=lambda: any(c.id.startswith('C') for c in bob.get_chats()),
        timeout=15,
    )
    chat_id = [
        chat.id
        for chat in bob.get_chats()
        if chat.id.startswith('C')
    ][0]

    chat = bob.get_chat_details(chat_id)
    assert len(chat.participants) == 1
    assert chat.participants[0].uuid == alice.get_me().uuid

    bob.create_or_update_chat('My duudes', [alex.get_me().uuid], chat_id=chat_id)

    wait_until(
        update=lambda: alex.trigger_periodic('p_fetch_msgs') or alice.trigger_periodic('p_fetch_msgs'),
        check=lambda: any(c.id.startswith('C') for c in alex.get_chats()),
        timeout=15
    )

    chat = alex.get_chat_details(chat_id)
    assert len(chat.participants) == 2
    assert {p.uuid for p in chat.participants} == {alice.get_me().uuid, bob.get_me().uuid}

    chat = bob.get_chat_details(chat_id)
    assert len(chat.participants) == 2
    assert {p.uuid for p in chat.participants} == {alice.get_me().uuid, alex.get_me().uuid}

    time.sleep(3)

    chat = alice.get_chat_details(chat_id)
    assert len(chat.participants) == 2
    assert {p.uuid for p in chat.participants} == {bob.get_me().uuid, alex.get_me().uuid}


def test_msg_in_chat(
    server: Server,
    alice: Client,
    bob: Client,
    alex: Client,
    triple_inited: None,
):
    contact_all(server, [alice, bob, alex])
    alice.create_or_update_chat(
        'My duudes',
        [bob.get_me().uuid, alex.get_me().uuid],
    )
    wait_until(
        update=lambda: bob.trigger_periodic('p_fetch_msgs'),
        check=lambda: any(c.id.startswith('C') for c in bob.get_chats()),
        timeout=15,
    )
    wait_until(
        update=lambda: alex.trigger_periodic('p_fetch_msgs'),
        check=lambda: any(c.id.startswith('C') for c in alex.get_chats()),
        timeout=15,
    )
    chat_id = [
        chat.id
        for chat in bob.get_chats()
        if chat.id.startswith('C')
    ][0]
    alice.create_chat_text_msg(chat_id, "Hello Bob and Alex")

    wait_until(
        update=lambda: bob.trigger_periodic('p_fetch_msgs'),
        check=lambda: len(bob.get_chat_msgs(chat_id)) > 0,
        timeout=15,
    )
    wait_until(
        update=lambda: alex.trigger_periodic('p_fetch_msgs'),
        check=lambda: len(alex.get_chat_msgs(chat_id)) > 0,
        timeout=15,
    )
