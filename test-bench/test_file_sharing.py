import os
from typing import Generator
import base64

import pytest

from client import Client
from server import Server
from utils import wait_until


def try_remove(path: str) -> None:
    try:
        os.remove(path)
    except Exception:
        pass


@pytest.fixture()
def file_to_write() -> Generator[str, None, None]:
    path = 'downloaded.txt'
    try_remove(path)
    yield path
    try_remove(path)


@pytest.fixture()
def clear_uploads_dir() -> Generator[None, None, None]:
    path = "uploads"
    try_remove(path)
    yield
    try_remove(path)


def test_share_file_path(
    server: Server,
    alice: Client,
    bob: Client,
    both_inited: None,
    file_to_write: str,
) -> None:
    alice.add_server(server.contact)
    bob.add_server(server.contact)
    alice.connect(bob.get_contact())
    bob.connect(alice.get_contact())
    alice.create_private_file_msg(
        bob.get_me().uuid,
        path='./data/file_to_send.txt'
    )
    alice_id = alice.get_me().uuid
    wait_until(
        check=lambda: len(bob.get_private_msgs(alice_id, None)) > 0,
        timeout=15,
    )
    dec_msg = bob.get_private_msgs(alice_id, None)[0]
    assert dec_msg.user == alice_id
    assert dec_msg.text == 'new-file'
    assert dec_msg.file
    assert dec_msg.file.name == 'new-file'
    assert dec_msg.file.size == 2626

    file_id = dec_msg.file.file_id

    task_id = bob.download(path=file_to_write, msg_id=dec_msg.uuid, user=alice_id)

    def check() -> bool:
        status = bob.check_download_status(task_id=task_id, user_id=alice_id, file_id=file_id)
        print(f'PROGRESS: {status.progress}')
        if status.failed:
            raise Exception("downloading failed")
        return status.completed

    wait_until(check=check, timeout=30)

    with open(file_to_write) as h:
        got = h.read()

    with open('data/file_to_send.txt') as h:
        expected = h.read()

    assert expected == got


def test_share_file_blob(
    server: Server,
    alice: Client,
    bob: Client,
    both_inited: None,
    file_to_write: str,
    clear_uploads_dir: None,
) -> None:
    alice.add_server(server.contact)
    bob.add_server(server.contact)
    alice.connect(bob.get_contact())
    bob.connect(alice.get_contact())
    with open('./data/file_to_send.txt', 'rb') as h:
        data = base64.encodebytes(h.read()).decode()
    alice.create_private_file_msg(bob.get_me().uuid, data=data)
    alice_id = alice.get_me().uuid
    wait_until(
        check=lambda: len(bob.get_private_msgs(alice_id, None)) > 0,
        timeout=15,
    )
    dec_msg = bob.get_private_msgs(alice_id, None)[0]
    assert dec_msg.user == alice_id
    assert dec_msg.text == 'new-file'
    assert dec_msg.file
    assert dec_msg.file.name == 'new-file'
    assert dec_msg.file.size == 2626

    file_id = dec_msg.file.file_id

    task_id = bob.download(path=file_to_write, msg_id=dec_msg.uuid, user=alice_id)

    def check() -> bool:
        status = bob.check_download_status(task_id=task_id, user_id=alice_id, file_id=file_id)
        print(f'PROGRESS: {status.progress}')
        if status.failed:
            raise Exception("downloading failed")
        return status.completed

    wait_until(check=check, timeout=30)

    with open(file_to_write) as h:
        got = h.read()

    with open('data/file_to_send.txt') as h:
        expected = h.read()

    assert expected == got
