from client import Client, ResponseNot200


def test_got_403(alice: Client, alice_inited_with_pwd: str):
    try:
        alice.get_me()
    except ResponseNot200 as err:
        assert err.status == 403
    else:
        assert False


def test_login_with_invalid_pwd(alice: Client, alice_inited_with_pwd: str):
    try:
        alice.login(alice_inited_with_pwd + '123123')
    except ResponseNot200 as err:
        assert err.status == 403
    else:
        assert False


def test_valid_resp_after_login(alice: Client, alice_inited_with_pwd: str):
    alice.login(alice_inited_with_pwd)
    me = alice.get_me()
    assert me.name == 'Alice'
