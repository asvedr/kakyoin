#!/bin/sh
rm server
rm client

mkdir -p data
cd ../kakyoin-server/
cargo build --release
cp target/release/kakyoin-server ../test-bench/data/server
cd ../test-bench/

cd ../kakyoin-client/
cargo build --release
cp target/release/kakyoin-client ../test-bench/data/client
cd ../test-bench/
