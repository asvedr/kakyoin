from client import Client
from server import Server
from utils import wait_until

# Workflow:
#   Alice    Server     Bob
#     |  send   |        |
#     |   msg   |        |
#     |-------->|        |
#     |         | fetch  |
#     |         |<-------|
#     |         |        |
#     |         | report |
#     |         | recv   |
#     |         |<-------|
#     | fetch   |        |
#     |  recv   |        |
#     | report  |        |
#     |-------->|        |

def test_alice_send_msg_to_bob(server: Server, alice: Client, bob: Client, both_inited: None):
    alice.add_server(server.contact)
    bob.add_server(server.contact)
    alice.connect(bob.get_contact())
    bob.connect(alice.get_contact())
    alice_id = alice.get_me().uuid
    bob_id = bob.get_me().uuid
    alice.create_private_text_msg(bob_id, 'Hello Bob')
    msgs = alice.get_private_msgs(bob_id)
    assert len(msgs) == 1
    assert msgs[0].text == 'Hello Bob'
    assert msgs[0].user is None
    assert msgs[0].read
    assert not msgs[0].sent
    assert not msgs[0].received
    wait_until(
        check=lambda: len(bob.get_private_msgs(alice_id)) == 1,
        update=lambda: bob.trigger_periodic('p_fetch_msgs'),
        timeout=15
    )
    msgs = bob.get_private_msgs(alice_id)
    assert msgs[0].text == 'Hello Bob'
    assert msgs[0].user == alice_id
    assert not msgs[0].read
    assert msgs[0].sent
    assert msgs[0].received

    bob.mark_read([msgs[0].uuid])

    msgs = bob.get_private_msgs(alice_id)
    assert msgs[0].text == 'Hello Bob'
    assert msgs[0].user == alice_id
    assert msgs[0].read
    assert msgs[0].sent
    assert msgs[0].received
    wait_until(
        check=lambda: bool(alice.get_private_msgs(bob_id)[0].received),
        update=alice.trigger_periodic('p_send_msgs'),
        timeout=15
    )
