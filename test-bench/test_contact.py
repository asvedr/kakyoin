from client import Client
from server import Server


def test_alice_and_bob_see_server(server: Server, alice: Client, bob: Client, both_inited: None):
    print('Alice:', alice.check_init())
    print('Bob:', bob.check_init())
    alice.add_server(server.contact)
    bob.add_server(server.contact)
    assert len(alice.get_servers()) == 1
    assert len(bob.get_servers()) == 1


def test_alice_and_bob_connect_each_other(alice: Client, bob: Client, both_inited: None):
    print('Alice:', alice.check_init())
    print('Bob:', bob.check_init())
    alice_id = alice.get_me().uuid
    bob_id = bob.get_me().uuid
    alice.connect(bob.get_contact())
    bob.connect(alice.get_contact())
    alice_contacts = alice.get_all_users()
    assert len(alice_contacts) == 1
    assert alice_contacts[0].uuid == bob_id
    bob_contacts = bob.get_all_users()
    assert len(bob_contacts) == 1
    assert bob_contacts[0].uuid == alice_id
