import base64
import os

import pytest
from PIL import Image

from client import Client
from server import Server
from utils import wait_until


DELTA = 11


def check_eq(img1: Image.Image, img2: Image.Image) -> None:
    assert img1.size == img2.size, f'different size {img1.size} != {img2.size}'
    px1 = img1.load()
    px2 = img2.load()
    for x in range(img1.width):
        for y in range(img1.height):
            r1, g1, b1 = px1[x, y]
            r2, g2, b2 = px2[x, y]
            assert abs(r1 - r2) <= DELTA, f'coord {x},{y} r_delta={abs(r1 - r2)}'
            assert abs(g1 - g2) <= DELTA, f'coord {x},{y} g_delta={abs(g1 - g2)}'
            assert abs(b1 - b2) <= DELTA, f'coord {x},{y} b_delta={abs(b1 - b2)}'


@pytest.fixture()
def outfile() -> str:
    path = 'out.webp'
    try:
        os.remove(path)
    except:
        pass
    yield path
    try:
        os.remove(path)
    except:
        pass


def test_upload_pic_and_get_it_back(alice: Client, alice_inited: None, outfile: str):
    alice.patch_me(pic_path='./data/pic.png')
    me = alice.get_me()
    decoded = base64.decodebytes(me.pic.encode())
    with open(outfile, 'wb') as h:
        h.write(decoded)
    expected = Image.open('./data/pic.png')
    got = Image.open(outfile)
    check_eq(expected, got)


def test_upload_download_meta(
    server: Server,
    alice: Client,
    bob: Client,
    both_inited: None,
    outfile: str,
):
    alice.connect(bob.get_contact())
    bob.connect(alice.get_contact())
    alice.patch_me(pic_path='./data/pic.png')
    alice.add_server(server.contact)
    bob.add_server(server.contact)

    wait_until(
        check=lambda: bob.get_all_users()[0].name == 'Alice',
        timeout=30,
    )

    user = bob.get_all_users()[0]
    assert user.pic
    decoded = base64.decodebytes(user.pic.encode())
    with open(outfile, 'wb') as h:
        h.write(decoded)
    got = Image.open(outfile)
    expected = Image.open('./data/pic.png')
    check_eq(expected, got)

