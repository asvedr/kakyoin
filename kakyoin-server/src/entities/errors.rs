use kakyoin_base::entities::errors::{CryptoErr, NetError, SerializerError};
use mddd::macros::EnumAutoFrom;

use std::string::FromUtf8Error;

#[derive(Debug, EnumAutoFrom)]
pub enum AddrGetterError {
    NetError(NetError),
    InvalidBody,
    AllFailed,
}

pub type StateRepoResult<T> = Result<T, StateRepoError>;

#[derive(Debug, Eq, PartialEq)]
pub enum StateRepoError {
    NotFound,
    LimitReached,
    IdCollision,
}

#[allow(clippy::upper_case_acronyms)]
#[derive(EnumAutoFrom)]
pub enum ApiUCError {
    UserNotFound,
    SRE(StateRepoError),
}

#[derive(Debug, EnumAutoFrom, Eq, PartialEq)]
pub enum TokenMakerError {
    #[accept(CryptoErr)]
    CanNotDecrypt,
    #[accept(FromUtf8Error)]
    #[accept(uuid::Error)]
    InvalidToken,
    TokenExpired,
    TokenNotMatches,
    UserNotFound,
    UserBanned,
}

#[derive(Debug)]
pub struct ApiError {
    pub status: u16,
    pub code: &'static str,
    pub details: Option<String>,
}

impl From<ApiUCError> for ApiError {
    fn from(value: ApiUCError) -> Self {
        match value {
            // ApiUCError::UserBanned => Self::banned(),
            ApiUCError::UserNotFound => Self::user_not_found(),
            _ => Self::make_500("INTERNAL"),
            // ApiUCError::SRE(_) => {}
        }
    }
}

impl ApiError {
    // pub fn not_found(details: Option<String>) -> Self {
    //     Self {
    //         status: 404,
    //         code: "NOT_FOUND",
    //         details,
    //     }
    // }

    pub fn banned() -> Self {
        Self::make_400("BANNED")
    }

    pub fn can_not_read_request() -> Self {
        Self::make_400("CAN_NOT_READ_REQUEST")
    }

    // pub fn can_not_parse_request() -> Self {
    //     Self::make_400("CAN_NOT_PARSE_REQUEST")
    // }

    // pub fn invalid_message_len() -> Self {
    //     Self::make_400("INVALID_MESSAGE_LEN")
    // }

    // pub fn invalid_body() -> Self {
    //     Self::make_400("INVALID_BODY")
    // }

    // pub fn can_not_handle(tp: u32) -> Self {
    //     Self {
    //         status: 400,
    //         code: "CAN_NOT_HANDLE",
    //         details: Some(format!("{}", tp)),
    //     }
    // }

    pub fn invalid_token() -> Self {
        Self {
            status: 403,
            code: "INVALID_TOKEN",
            details: None,
        }
    }

    // pub fn msg_chat_not_found() -> Self {
    //     Self::make_400("MSG_CHAT_NOT_FOUND")
    // }

    fn make_400(code: &'static str) -> Self {
        Self {
            status: 400,
            code,
            details: None,
        }
    }

    fn user_not_found() -> Self {
        Self {
            status: 404,
            code: "USER_NOT_FOUND",
            details: None,
        }
    }

    pub fn make_500(code: &'static str) -> Self {
        Self {
            status: 500,
            code,
            details: None,
        }
    }
}

impl From<serdebin::SerializerError> for ApiError {
    fn from(_: serdebin::SerializerError) -> Self {
        Self::make_500("SERIALIZATION_ERROR")
    }
}

impl From<CryptoErr> for ApiError {
    fn from(value: CryptoErr) -> Self {
        match value {
            CryptoErr::CanNotGenerateKey => Self::make_500("CAN_NOT_GEN_KEY"),
            CryptoErr::CanNotDecrypt => Self::make_400("CAN_NOT_DECRYPT"),
            CryptoErr::CanNotEncrypt => Self::make_400("CAN_NOT_ENCRYPT"),
            CryptoErr::CanNotValidate => Self::make_500("CAN_NOT_VALIDATE_SIGNATURE"),
            CryptoErr::CanNotSign => Self::make_500("CAN_NOT_SIGN"),
        }
    }
}

impl From<SerializerError> for ApiError {
    fn from(value: SerializerError) -> Self {
        Self {
            status: 500,
            code: "SERIALIZER_ERROR",
            details: Some(format!("{:?}", value)),
        }
    }
}
