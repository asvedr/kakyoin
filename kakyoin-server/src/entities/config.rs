use kakyoin_base::entities::log::Level;
use mddd::macros::IConfig;

#[allow(clippy::manual_non_exhaustive)]
#[derive(IConfig)]
pub struct Config {
    #[default(4)]
    pub max_threads: usize,
    #[default(Level::Err)]
    pub log_level: Level,
    #[default("".to_string())]
    pub log_prefix: String,
    pub priv_key_path: Option<String>,
    #[default(10123)]
    pub port: u16,
    #[default(5)]
    pub timeout: u64,
    #[default(10_000)]
    pub max_users: usize,
    #[default(60 * 60 * 24 * 5)]
    pub max_user_inactive_time: u64,
    #[default(60 * 60 * 24 * 3)]
    pub max_msg_lifetime: u64,
    #[default(60 * 60 * 11)]
    pub gc_user_interval: u64,
    #[default(60 * 60 * 9)]
    pub gc_msg_interval: u64,
    #[prefix("kakyoin_")]
    __meta__: (),
}
