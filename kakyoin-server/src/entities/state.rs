use std::sync::Arc;

use kakyoin_base::entities::api_msg::ApiMsgPacked;
use kakyoin_base::entities::server_call::ApiUserMeta;
use rsa::RsaPublicKey;
use uuid::Uuid;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct User {
    pub id: Uuid,
    pub banned: bool,
    pub last_active_ts: u64,
    pub key_code: String,
    pub key: Arc<RsaPublicKey>,
    pub token: String,
    pub token_expired: u64,
    pub meta: Arc<ApiUserMeta>,
}

#[derive(Debug, Clone)]
pub struct Msg {
    pub id: Uuid,
    pub created_at: u64,
    pub sender_id: Uuid,
    pub sender_key_code: String,
    pub packed: Arc<ApiMsgPacked>,
}

#[derive(Debug, Eq, PartialEq)]
pub struct MsgTableStats {
    pub new_msg_count: usize,
    pub received_count: usize,
}

#[derive(Debug, Eq, PartialEq)]
pub struct UserTableStats {
    pub user_count: usize,
}
