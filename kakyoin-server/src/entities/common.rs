use rsa::RsaPublicKey;
use std::sync::Arc;
use uuid::Uuid;

#[derive(Debug)]
pub struct DerefToken {
    pub uid: Uuid,
    pub key_code: String,
    pub pub_key: Arc<RsaPublicKey>,
}
