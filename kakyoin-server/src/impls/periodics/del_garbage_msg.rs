use crate::entities::config::Config;
use atex::{ITaskController, ITaskExecutor, Task, TaskStatus};
use kakyoin_base::entities::common::DynFut;

use crate::proto::local::IState;

pub struct Periodic<S: IState> {
    interval: u64,
    state: S,
}

impl<S: IState> Periodic<S> {
    pub fn new(config: &Config, state: S) -> Self {
        Self {
            interval: config.gc_msg_interval,
            state,
        }
    }
}

impl<S: IState + 'static> ITaskExecutor for Periodic<S> {
    type Error = ();

    fn name(&self) -> &'static str {
        "del_garbage_msg"
    }

    fn periodic_interval(&self) -> Option<u64> {
        Some(self.interval)
    }

    fn execute(&self, _: Box<dyn ITaskController>, _: Task) -> DynFut<Result<TaskStatus, ()>> {
        let lock = self.state.user();
        Box::pin(async move {
            lock.write().await.del_garbage();
            Ok(TaskStatus::Completed)
        })
    }
}
