use atex::ITaskManager;
use kakyoin_base::entities::common::DynFut;
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;
use std::io::{Error, ErrorKind};

use crate::proto::common::IService;

pub struct PeriodicService {
    pub logger: &'static dyn ILogger,
    pub executor: &'static dyn ITaskManager,
}

unsafe impl Send for PeriodicService {}
unsafe impl Sync for PeriodicService {}

impl IService for PeriodicService {
    fn name(&self) -> &str {
        "tasks"
    }

    fn run(&mut self) -> DynFut<Result<(), Error>> {
        let fut = self.executor.run();
        let logger = self.logger;
        Box::pin(async move {
            logger.log(Level::Debug, "task runner launched");
            fut.await.map_err(|err| {
                let msg = format!("{:?}", err);
                Error::new(ErrorKind::Other, msg)
            })
        })
    }
}
