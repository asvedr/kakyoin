use std::collections::HashMap;

use uuid::Uuid;

use crate::entities::errors::{StateRepoError, StateRepoResult};
use crate::entities::state::{User, UserTableStats};
use crate::proto::local::IUserTable;
use crate::utils::time::get_now;

pub struct UserTable {
    data: HashMap<Vec<u8>, User>,
    max_len: usize,
    max_inactive_time: u64,
}

impl UserTable {
    pub fn new(max_len: usize, max_inactive_time: u64) -> Self {
        Self {
            data: Default::default(),
            max_len,
            max_inactive_time,
        }
    }
}

#[inline]
fn make_id(id: &Uuid, code: &str) -> Vec<u8> {
    let mut bts = id.to_bytes_le().to_vec();
    bts.extend_from_slice(code.as_bytes());
    bts
}

impl IUserTable for UserTable {
    fn get_by_id(&self, id: &Uuid, code: &str) -> StateRepoResult<&User> {
        let id = make_id(id, code);
        match self.data.get(&id) {
            Some(val) => Ok(val),
            None => Err(StateRepoError::NotFound),
        }
    }

    fn get_mut_by_id(&mut self, id: &Uuid, code: &str) -> StateRepoResult<&mut User> {
        let id = make_id(id, code);
        match self.data.get_mut(&id) {
            Some(val) => Ok(val),
            None => Err(StateRepoError::NotFound),
        }
    }

    fn get_stats(&self) -> UserTableStats {
        UserTableStats {
            user_count: self.data.len(),
        }
    }

    fn add_user(&mut self, user: User) -> StateRepoResult<()> {
        self.del_garbage();
        if self.data.len() >= self.max_len {
            return Err(StateRepoError::LimitReached);
        }
        let id = make_id(&user.id, &user.key_code);
        if self.data.contains_key(&id) {
            return Err(StateRepoError::IdCollision);
        }
        let id = make_id(&user.id, &user.key_code);
        self.data.insert(id, user);
        Ok(())
    }

    fn update_last_active_ts(&mut self, id: &Uuid, code: &str) {
        let id = make_id(id, code);
        if let Some(usr) = self.data.get_mut(&id) {
            usr.last_active_ts = get_now();
        }
    }

    fn set_ban(&mut self, id: &Uuid, code: &str, banned: bool) {
        let id = make_id(id, code);
        if let Some(usr) = self.data.get_mut(&id) {
            usr.banned = banned;
        }
    }

    fn del_garbage(&mut self) {
        let threshold = get_now() - self.max_inactive_time;
        self.data.retain(|_, usr| usr.last_active_ts > threshold);
    }
}
