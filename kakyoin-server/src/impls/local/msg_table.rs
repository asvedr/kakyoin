use kakyoin_base::entities::api_msg::{ApiMsgPacked, ApiMsgStatus};
use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::sync::Arc;
use uuid::Uuid;

use crate::entities::errors::{StateRepoError, StateRepoResult};
use crate::entities::state::{Msg, MsgTableStats};
use crate::proto::local::IMsgTable;
use crate::utils::time::get_now;

pub struct MsgTable {
    new_msgs: BTreeMap<Uuid, Msg>,
    received_msgs: BTreeMap<Uuid, ReceivedMsg>,
    index_by_receiver: BTreeMap<Uuid, BTreeSet<Uuid>>,
    max_inactive_time: u64,
}

struct ReceivedMsg {
    sender_id: Uuid,
    sender_key: String,
    ts: u64,
}

impl MsgTable {
    pub fn new(max_inactive_time: u64) -> Self {
        Self {
            new_msgs: Default::default(),
            received_msgs: Default::default(),
            index_by_receiver: Default::default(),
            max_inactive_time,
        }
    }

    fn msg_status(&self, id: &Uuid, sender_id: &Uuid, key: &str) -> ApiMsgStatus {
        if let Some(msg) = self.received_msgs.get(id) {
            return if msg.sender_id == *sender_id && msg.sender_key == key {
                ApiMsgStatus::Received
            } else {
                ApiMsgStatus::NotFound
            };
        }
        if let Some(msg) = self.new_msgs.get(id) {
            if msg.sender_id == *sender_id && msg.sender_key_code == key {
                return ApiMsgStatus::NotReceived;
            }
        }
        ApiMsgStatus::NotFound
    }

    fn remove_from_index_by_receiver(&mut self, user_id: &Uuid, msg_id: &Uuid) {
        let id_list = match self.index_by_receiver.get_mut(user_id) {
            Some(val) => val,
            _ => return,
        };
        id_list.remove(msg_id);
    }

    fn add_to_index_by_receiver(&mut self, user_id: &Uuid, msg_id: &Uuid) {
        if let Some(id_list) = self.index_by_receiver.get_mut(user_id) {
            id_list.insert(*msg_id);
            return;
        }
        let mut set = BTreeSet::new();
        set.insert(*msg_id);
        self.index_by_receiver.insert(*user_id, set);
    }
}

impl IMsgTable for MsgTable {
    fn get_by_id(&self, id: &Uuid) -> StateRepoResult<&Msg> {
        match self.new_msgs.get(id) {
            None => Err(StateRepoError::NotFound),
            Some(val) => Ok(val),
        }
    }

    fn check_statuses(
        &self,
        sender_id: &Uuid,
        sender_key: &str,
        id_list: &[Uuid],
    ) -> HashMap<Uuid, ApiMsgStatus> {
        let mut result = HashMap::new();
        for id in id_list {
            let status = self.msg_status(id, sender_id, sender_key);
            result.insert(*id, status);
        }
        result
    }

    fn get_new_for_user(&self, user_id: &Uuid, user_key: &str) -> Vec<Arc<ApiMsgPacked>> {
        let id_list = match self.index_by_receiver.get(user_id) {
            Some(val) => val,
            _ => return Default::default(),
        };
        let mut result = Vec::with_capacity(id_list.len());
        for id in id_list {
            let msg = self.new_msgs.get(id).unwrap();
            if msg.packed.key_code == user_key {
                result.push(msg.packed.clone());
            }
        }
        result
    }

    fn check_unknown(&self, id_list: &[Uuid]) -> Vec<Uuid> {
        let mut result = Vec::new();
        for id in id_list {
            if !(self.new_msgs.contains_key(id) || self.received_msgs.contains_key(id)) {
                result.push(*id)
            }
        }
        result
    }

    fn get_stats(&self) -> MsgTableStats {
        MsgTableStats {
            new_msg_count: self.new_msgs.len(),
            received_count: self.received_msgs.len(),
        }
    }

    fn move_to_received(&mut self, id: &Uuid) -> StateRepoResult<()> {
        let msg = match self.new_msgs.remove(id) {
            Some(msg) => msg,
            None => return Err(StateRepoError::NotFound),
        };
        self.remove_from_index_by_receiver(&msg.packed.receiver, id);
        self.received_msgs.insert(
            *id,
            ReceivedMsg {
                sender_id: msg.sender_id,
                sender_key: msg.sender_key_code,
                ts: get_now(),
            },
        );
        Ok(())
    }

    fn create_new(
        &mut self,
        sender_id: Uuid,
        sender_code: String,
        packed: Arc<ApiMsgPacked>,
    ) -> StateRepoResult<bool> {
        let id = packed.id;
        if let Some(msg) = self.new_msgs.get(&id) {
            return if msg.sender_id == sender_id {
                Ok(false)
            } else {
                Err(StateRepoError::IdCollision)
            };
        }
        if let Some(msg) = self.received_msgs.get(&id) {
            return if msg.sender_id == sender_id {
                Ok(false)
            } else {
                Err(StateRepoError::IdCollision)
            };
        }
        let created_at = get_now();
        self.add_to_index_by_receiver(&packed.receiver, &id);
        let msg = Msg {
            id,
            created_at,
            sender_id,
            sender_key_code: sender_code,
            packed,
        };
        self.new_msgs.insert(id, msg);
        Ok(true)
    }

    fn del_garbage(&mut self) {
        let threshold = get_now() - self.max_inactive_time;
        self.new_msgs.retain(|_, msg| msg.created_at > threshold);
        for msgs in self.index_by_receiver.values_mut() {
            msgs.retain(|id| self.new_msgs.contains_key(id));
        }
        self.received_msgs.retain(|_, msg| msg.ts > threshold)
    }
}
