use std::sync::Arc;
use tokio::sync::RwLock;

use crate::entities::config::Config;
use crate::impls::local::msg_table::MsgTable;
use crate::impls::local::user_table::UserTable;
use crate::proto::local::{IMsgTable, IState, IUserTable};

pub struct State {
    user: Arc<RwLock<Box<dyn IUserTable>>>,
    msg: Arc<RwLock<Box<dyn IMsgTable>>>,
}

impl State {
    pub fn new(config: &Config) -> Self {
        let user = UserTable::new(config.max_users, config.max_user_inactive_time);
        let msg = MsgTable::new(config.max_msg_lifetime);
        Self {
            user: Arc::new(RwLock::new(Box::new(user))),
            msg: Arc::new(RwLock::new(Box::new(msg))),
        }
    }
}

impl Clone for State {
    fn clone(&self) -> Self {
        Self {
            user: self.user.clone(),
            msg: self.msg.clone(),
        }
    }
}

impl IState for State {
    fn user(&self) -> Arc<RwLock<Box<dyn IUserTable>>> {
        self.user.clone()
    }

    fn msg(&self) -> Arc<RwLock<Box<dyn IMsgTable>>> {
        self.msg.clone()
    }
}
