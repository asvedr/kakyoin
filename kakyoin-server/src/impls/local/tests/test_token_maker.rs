use kakyoin_base::impls::crypto_ops::CryptoOps;
use mddd::traits::IConfig;

use uuid::Uuid;

use crate::entities::config::Config;

use crate::impls::local::state::State;
use crate::impls::local::token_maker::TokenMaker;
use crate::proto::local::ITokenMaker;
use crate::utils::tests::keys::{priv_key_1, pub_key_2};
use crate::utils::tests::logger::MockLogger;

fn maker() -> Box<dyn ITokenMaker> {
    let config = Config::build().unwrap();
    let state = State::new(&config);
    Box::new(TokenMaker::new(
        state,
        CryptoOps::new(MockLogger),
        MockLogger,
        priv_key_1().clone(),
    ))
}

#[tokio::test]
async fn test_gen_deref_token_ok() {
    let m = maker();
    let (token, _) = m
        .make_token(&Uuid::NAMESPACE_OID, pub_key_2().clone())
        .await
        .unwrap();
    let deref = m.deref_token(token).await.unwrap();
    assert_eq!(deref.uid, Uuid::NAMESPACE_OID);
    assert_eq!(deref.pub_key, *pub_key_2());
}

#[tokio::test]
async fn test_gen_deref_token_err() {
    let m = maker();
    let (mut token, _) = m
        .make_token(&Uuid::NAMESPACE_OID, pub_key_2().clone())
        .await
        .unwrap();
    token[0] = !token[0];
    token[1] = !token[1];
    token[2] = !token[2];
    // no fail
    m.deref_token(token).await.unwrap_err();
}
