use mddd::traits::IConfig;
use std::mem;
use std::sync::Arc;
use tokio::sync::RwLock;
use uuid::Uuid;

use crate::entities::config::Config;
use crate::entities::errors::StateRepoError;
use crate::entities::state::User;
use crate::impls::local::state::State;
use crate::proto::local::{IState, IUserTable};
use crate::utils::tests::keys::pub_key_1;
use crate::utils::time::get_now;

async fn repo_guard() -> Arc<RwLock<Box<dyn IUserTable>>> {
    let guard = State::new(&Config::build().unwrap()).user();
    let mut repo = guard.write().await;
    repo.add_user(User {
        id: Uuid::NAMESPACE_OID,
        banned: false,
        last_active_ts: 0,
        key_code: "abc".to_string(),
        key: pub_key_1().clone(),
        token: "token".to_string(),
        token_expired: 123,
        meta: Arc::new(Default::default()),
    })
    .unwrap();
    mem::drop(repo);
    guard
}

#[tokio::test]
async fn test_get_by_id_err() {
    let guard = repo_guard().await;
    let repo = guard.read().await;
    assert_eq!(
        repo.get_by_id(&Uuid::NAMESPACE_OID, "cde"),
        Err(StateRepoError::NotFound),
    )
}

#[tokio::test]
async fn test_get_by_id_ok() {
    let guard = repo_guard().await;
    let repo = guard.read().await;
    let user = repo.get_by_id(&Uuid::NAMESPACE_OID, "abc").unwrap();
    assert_eq!(user.token, "token")
}

#[tokio::test]
async fn test_get_stats() {
    let guard = repo_guard().await;
    let repo = guard.read().await;
    assert_eq!(repo.get_stats().user_count, 1)
}

#[tokio::test]
async fn test_update_last_active_ts() {
    let guard = repo_guard().await;
    let mut repo = guard.write().await;
    let ts1 = repo
        .get_by_id(&Uuid::NAMESPACE_OID, "abc")
        .unwrap()
        .last_active_ts;
    repo.update_last_active_ts(&Uuid::NAMESPACE_OID, "abc");
    let ts2 = repo
        .get_by_id(&Uuid::NAMESPACE_OID, "abc")
        .unwrap()
        .last_active_ts;
    assert!(ts2 > ts1);
}

#[tokio::test]
async fn test_set_ban() {
    let guard = repo_guard().await;
    let mut repo = guard.write().await;
    assert!(!repo.get_by_id(&Uuid::NAMESPACE_OID, "abc").unwrap().banned);
    repo.set_ban(&Uuid::NAMESPACE_OID, "abc", true);
    assert!(repo.get_by_id(&Uuid::NAMESPACE_OID, "abc").unwrap().banned);
}

#[tokio::test]
async fn test_del_garbage() {
    let guard = repo_guard().await;
    let mut repo = guard.write().await;
    repo.get_mut_by_id(&Uuid::NAMESPACE_OID, "abc")
        .unwrap()
        .last_active_ts = get_now();
    repo.del_garbage();
    assert!(repo.get_mut_by_id(&Uuid::NAMESPACE_OID, "abc").is_ok());

    repo.get_mut_by_id(&Uuid::NAMESPACE_OID, "abc")
        .unwrap()
        .last_active_ts = 0;
    repo.del_garbage();
    assert!(repo.get_mut_by_id(&Uuid::NAMESPACE_OID, "abc").is_err());
}
