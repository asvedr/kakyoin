use kakyoin_base::entities::api_msg::{ApiMsgPacked, ApiMsgStatus};
use std::mem;
use std::sync::Arc;

use mddd::traits::IConfig;
use tokio::sync::RwLock;
use uuid::Uuid;

use crate::entities::config::Config;
use crate::entities::errors::StateRepoError;
use crate::entities::state::MsgTableStats;
use crate::impls::local::state::State;
use crate::proto::local::{IMsgTable, IState};

async fn repo_guard() -> Arc<RwLock<Box<dyn IMsgTable>>> {
    let state = State::new(&Config::build().unwrap());
    let guard = state.msg();
    let mut repo = guard.write().await;
    repo.create_new(
        Uuid::NAMESPACE_OID,
        "abc".to_string(),
        Arc::new(ApiMsgPacked {
            receiver: Uuid::NAMESPACE_URL,
            id: Uuid::NAMESPACE_DNS,
            key_code: "cde".to_string(),
            data: vec![1, 2, 3],
            signature: vec![4, 5, 6],
        }),
    )
    .unwrap();
    mem::drop(repo);
    guard
}

#[tokio::test]
async fn test_get_by_id() {
    let guard = repo_guard().await;
    let repo = guard.read().await;
    assert_eq!(
        repo.get_by_id(&Uuid::NAMESPACE_OID).unwrap_err(),
        StateRepoError::NotFound,
    );
    let msg = repo.get_by_id(&Uuid::NAMESPACE_DNS).unwrap();
    assert_eq!(msg.packed.data, &[1, 2, 3]);
}

#[tokio::test]
async fn test_check_statuses() {
    let guard = repo_guard().await;
    let mut repo = guard.write().await;
    let map = repo.check_statuses(
        &Uuid::NAMESPACE_OID,
        "abc",
        &[Uuid::NAMESPACE_DNS, Uuid::NAMESPACE_OID],
    );
    assert_eq!(map.len(), 2);
    assert_eq!(
        *map.get(&Uuid::NAMESPACE_DNS).unwrap(),
        ApiMsgStatus::NotReceived
    );
    assert_eq!(
        *map.get(&Uuid::NAMESPACE_OID).unwrap(),
        ApiMsgStatus::NotFound
    );

    let _ = repo.move_to_received(&Uuid::NAMESPACE_DNS);

    let map = repo.check_statuses(
        &Uuid::NAMESPACE_OID,
        "abc",
        &[Uuid::NAMESPACE_DNS, Uuid::NAMESPACE_OID],
    );
    assert_eq!(map.len(), 2);
    assert_eq!(
        *map.get(&Uuid::NAMESPACE_DNS).unwrap(),
        ApiMsgStatus::Received
    );
    assert_eq!(
        *map.get(&Uuid::NAMESPACE_OID).unwrap(),
        ApiMsgStatus::NotFound
    );
}

#[tokio::test]
async fn test_get_new_for_user() {
    let guard = repo_guard().await;
    let mut repo = guard.write().await;
    let msgs = repo.get_new_for_user(&Uuid::NAMESPACE_URL, "cde");
    assert_eq!(msgs.len(), 1);
    assert_eq!(msgs[0].id, Uuid::NAMESPACE_DNS);
    assert_eq!(msgs[0].data, vec![1, 2, 3]);

    let msgs = repo.get_new_for_user(&Uuid::NAMESPACE_OID, "abc");
    assert!(msgs.is_empty());

    let _ = repo.move_to_received(&Uuid::NAMESPACE_DNS);

    let msgs = repo.get_new_for_user(&Uuid::NAMESPACE_URL, "cde");
    assert!(msgs.is_empty());
}

#[tokio::test]
async fn test_check_unknown() {
    let guard = repo_guard().await;
    let mut repo = guard.write().await;
    let ids = repo.check_unknown(&[Uuid::NAMESPACE_DNS, Uuid::NAMESPACE_OID]);
    assert_eq!(ids, &[Uuid::NAMESPACE_OID]);

    let _ = repo.move_to_received(&Uuid::NAMESPACE_DNS);

    let ids = repo.check_unknown(&[Uuid::NAMESPACE_DNS, Uuid::NAMESPACE_OID]);
    assert_eq!(ids, &[Uuid::NAMESPACE_OID]);
}

#[tokio::test]
async fn test_get_stats() {
    let guard = repo_guard().await;
    let mut repo = guard.write().await;
    assert_eq!(
        repo.get_stats(),
        MsgTableStats {
            new_msg_count: 1,
            received_count: 0
        }
    );

    let _ = repo.move_to_received(&Uuid::NAMESPACE_DNS);

    assert_eq!(
        repo.get_stats(),
        MsgTableStats {
            new_msg_count: 0,
            received_count: 1
        }
    );
}
