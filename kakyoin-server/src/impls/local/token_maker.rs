use crate::entities::common::DerefToken;
use crate::entities::errors::{StateRepoError, TokenMakerError};
use crate::entities::state::User;
use crate::proto::local::{IState, ITokenMaker};
use crate::utils::time::get_now;
use kakyoin_base::entities::common::DynFut;

use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::{ICryptoOps, ILogger};
use rsa::{RsaPrivateKey, RsaPublicKey};
use std::str::FromStr;
use std::sync::Arc;
use uuid::Uuid;

const LIFETIME: u64 = 60 * 10;

pub struct TokenMaker<S: IState, CO: ICryptoOps, L: ILogger> {
    ctx: Arc<Ctx<S, CO, L>>,
}

impl<S: IState, CO: ICryptoOps, L: ILogger> Clone for TokenMaker<S, CO, L> {
    fn clone(&self) -> Self {
        Self {
            ctx: self.ctx.clone(),
        }
    }
}

struct Ctx<S: IState, CO: ICryptoOps, L: ILogger> {
    state: S,
    crypto_ops: CO,
    logger: L,
    my_priv_key: Arc<RsaPrivateKey>,
    my_pub_key: RsaPublicKey,
}

fn gen_suffix() -> String {
    Uuid::new_v4().to_string()
}

impl<S: IState, CO: ICryptoOps, L: ILogger> TokenMaker<S, CO, L> {
    pub fn new(state: S, crypto_ops: CO, logger: L, my_priv_key: Arc<RsaPrivateKey>) -> Self {
        let my_pub_key = my_priv_key.to_public_key();
        let ctx = Arc::new(Ctx {
            state,
            crypto_ops,
            logger,
            my_pub_key,
            my_priv_key,
        });
        Self { ctx }
    }

    async fn a_make_token(
        self,
        user_id: Uuid,
        pub_key: Arc<RsaPublicKey>,
    ) -> Option<(Vec<u8>, u64)> {
        let (code, suffix, ts) = self.gen_or_get_suffix(user_id, pub_key).await?;
        let raw = format!("{}:{}:{}", user_id, code, suffix,);
        let token = self
            .ctx
            .crypto_ops
            .encrypt(&self.ctx.my_pub_key, raw.as_bytes())
            .expect("Can not encrypt token");
        Some((token, ts))
    }

    async fn a_deref_token(self, token: Vec<u8>) -> Result<DerefToken, TokenMakerError> {
        let raw_vec_token = self.ctx.crypto_ops.decrypt(&self.ctx.my_priv_key, &token)?;
        let str_token = String::from_utf8(raw_vec_token)?;
        let split = str_token.split(':').collect::<Vec<_>>();
        let (user_id, code, suffix) = match &split as &[&str] {
            &[id, code, suff] => (Uuid::from_str(id)?, code, suff),
            _ => return Err(TokenMakerError::InvalidToken),
        };
        let repo_lock = self.ctx.state.user();
        let repo = repo_lock.read().await;
        let user = match repo.get_by_id(&user_id, code) {
            Ok(val) => val,
            Err(StateRepoError::NotFound) => return Err(TokenMakerError::UserNotFound),
            Err(e) => {
                self.ctx
                    .logger
                    .log(Level::Err, &format!("token_maker::user_repo_err: {:?}", e));
                return Err(TokenMakerError::InvalidToken);
            }
        };
        if user.token != suffix {
            return Err(TokenMakerError::TokenNotMatches);
        }
        if user.token_expired < get_now() {
            return Err(TokenMakerError::TokenExpired);
        }
        if user.banned {
            return Err(TokenMakerError::UserBanned);
        }
        Ok(DerefToken {
            uid: user.id,
            key_code: user.key_code.clone(),
            pub_key: user.key.clone(),
        })
    }

    async fn gen_or_get_suffix(
        &self,
        user_id: Uuid,
        pub_key: Arc<RsaPublicKey>,
    ) -> Option<(String, String, u64)> {
        let code = self.ctx.crypto_ops.get_key_code(&pub_key);
        let repo_lock = self.ctx.state.user();
        let mut repo = repo_lock.write().await;
        let now = get_now();
        match repo.get_mut_by_id(&user_id, &code) {
            Ok(user) if user.token_expired > now => {
                return Some((code, user.token.clone(), user.token_expired))
            }
            Ok(user) => {
                user.token = gen_suffix();
                user.token_expired = now + LIFETIME;
                return Some((code, user.token.clone(), user.token_expired));
            }
            Err(StateRepoError::NotFound) => (),
            Err(err) => {
                self.ctx.logger.log(
                    Level::Err,
                    &format!("Can not get user for making token: {:?}", err),
                );
                return None;
            }
        }
        let user = User {
            id: user_id,
            banned: false,
            last_active_ts: now,
            key_code: code.clone(),
            key: pub_key,
            token: gen_suffix(),
            token_expired: now + LIFETIME,
            meta: Arc::new(Default::default()),
        };
        let token = user.token.clone();
        let expired = user.token_expired;
        if let Err(err) = repo.add_user(user) {
            self.ctx.logger.log(
                Level::Err,
                &format!("get token :: user repo failed: {:?}", err),
            );
            return None;
        };
        Some((code, token, expired))
    }
}

impl<S: IState + 'static, CO: ICryptoOps + 'static, L: ILogger + 'static> ITokenMaker
    for TokenMaker<S, CO, L>
{
    fn make_token(
        &self,
        user_id: &Uuid,
        pub_key: Arc<RsaPublicKey>,
    ) -> DynFut<Option<(Vec<u8>, u64)>> {
        Box::pin(self.clone().a_make_token(*user_id, pub_key))
    }

    fn deref_token(&self, token: Vec<u8>) -> DynFut<Result<DerefToken, TokenMakerError>> {
        Box::pin(self.clone().a_deref_token(token))
    }
}
