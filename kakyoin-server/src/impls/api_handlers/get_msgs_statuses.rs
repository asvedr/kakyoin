use crate::entities::errors::ApiError;
use crate::proto::common::IAsyncScript;
use crate::proto::local::ITokenMaker;
use crate::proto::net::IApiHandler;
use kakyoin_base::entities::api_msg::ApiMsgStatus;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::server_call::{ServerCall, ServerResponse};
use kakyoin_base::proto::common::{ICryptoOps, ILogger};
use serdebin::to_bytes;
use std::collections::HashMap;
use std::sync::Arc;
use uuid::Uuid;

pub struct GetMsgsStatusesHandler<
    TM: ITokenMaker,
    CO: ICryptoOps,
    Sc: IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>>,
    L: ILogger,
> {
    ctx: Arc<Ctx<TM, CO, Sc, L>>,
}

#[allow(dead_code)]
struct Ctx<
    TM: ITokenMaker,
    CO: ICryptoOps,
    Sc: IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>>,
    L: ILogger,
> {
    token_maker: TM,
    crypto_ops: CO,
    script: Sc,
    logger: L,
}

impl<
        TM: ITokenMaker + 'static,
        CO: ICryptoOps + 'static,
        Sc: IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>> + 'static,
        L: ILogger + 'static,
    > GetMsgsStatusesHandler<TM, CO, Sc, L>
{
    pub fn new(token_maker: TM, crypto_ops: CO, script: Sc, logger: L) -> Self {
        let ctx = Arc::new(Ctx {
            token_maker,
            crypto_ops,
            script,
            logger,
        });
        Self { ctx }
    }

    async fn a_handle(self, token: Vec<u8>, ids: Vec<Uuid>) -> Result<Vec<u8>, ApiError> {
        let deref = self
            .ctx
            .token_maker
            .deref_token(token)
            .await
            .map_err(|_| ApiError::invalid_token())?;
        let map = self
            .ctx
            .script
            .execute(deref.uid, deref.key_code, ids)
            .await?;
        let bts = self.ctx.crypto_ops.encrypt(
            &deref.pub_key,
            &to_bytes(&ServerResponse::Statuses { map })?,
        )?;
        Ok(bts)
    }
}

impl<
        TM: ITokenMaker + 'static,
        CO: ICryptoOps + 'static,
        Sc: IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>> + 'static,
        L: ILogger + 'static,
    > IApiHandler for GetMsgsStatusesHandler<TM, CO, Sc, L>
{
    fn name(&self) -> &str {
        "get msgs statuses"
    }

    fn match_msg(&self, call: &ServerCall) -> bool {
        matches!(call, ServerCall::GetStatuses { .. })
    }

    fn handle(&self, call: ServerCall) -> DynFutRes<Vec<u8>, ApiError> {
        let (token, ids) = match call {
            ServerCall::GetStatuses { token, ids } => (token, ids),
            _ => unreachable!(),
        };
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_handle(token, ids),
        )
    }
}
