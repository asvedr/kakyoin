use crate::entities::errors::ApiError;
use crate::proto::local::ITokenMaker;
use crate::proto::net::IApiHandler;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::server_call::{ServerCall, ServerResponse};
use kakyoin_base::proto::common::ICryptoOps;
use rsa::RsaPublicKey;
use serdebin::to_bytes;
use std::sync::Arc;
use uuid::Uuid;

#[derive(Clone)]
pub struct GetTokenHandler<TM: ITokenMaker, CO: ICryptoOps> {
    token_maker: TM,
    crypto_ops: CO,
}

impl<TM: ITokenMaker + 'static, CO: ICryptoOps + 'static> GetTokenHandler<TM, CO> {
    pub fn new(token_maker: TM, crypto_ops: CO) -> Self {
        Self {
            token_maker,
            crypto_ops,
        }
    }

    async fn a_handle(self, id: Uuid, pub_key: Arc<RsaPublicKey>) -> Result<Vec<u8>, ApiError> {
        let res = self.token_maker.make_token(&id, pub_key.clone()).await;
        let (token, expired_at) = match res {
            None => return Err(ApiError::banned()),
            Some(val) => val,
        };
        let resp = ServerResponse::Token { token, expired_at };
        let bts = self.crypto_ops.encrypt(&pub_key, &to_bytes(resp)?)?;
        Ok(bts)
    }
}

impl<TM: ITokenMaker + Clone + 'static, CO: ICryptoOps + Clone + 'static> IApiHandler
    for GetTokenHandler<TM, CO>
{
    fn name(&self) -> &str {
        "get_token"
    }

    fn match_msg(&self, call: &ServerCall) -> bool {
        matches!(call, ServerCall::GetToken { .. })
    }

    fn handle(&self, call: ServerCall) -> DynFutRes<Vec<u8>, ApiError> {
        let (id, key) = match call {
            ServerCall::GetToken { user_id, pub_key } => (user_id, pub_key),
            _ => unreachable!(),
        };
        let key = Arc::new(key);
        Box::pin(self.clone().a_handle(id, key))
    }
}
