use crate::entities::errors::ApiError;
use crate::proto::common::IAsyncScript;
use crate::proto::local::ITokenMaker;
use crate::proto::net::IApiHandler;
use kakyoin_base::entities::api_msg::{ApiMsgPacked, ApiMsgStatus};
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::log::Level;
use kakyoin_base::entities::server_call::{ServerCall, ServerResponse};
use kakyoin_base::proto::common::{ICryptoOps, ILogger};
use serdebin::to_bytes;
use std::collections::HashMap;
use std::sync::Arc;
use uuid::Uuid;

pub struct AddMsgsHandler<
    TM: ITokenMaker,
    CO: ICryptoOps,
    AddSc: IAsyncScript<Request = Vec<Arc<ApiMsgPacked>>, Response = ()>,
    StatSc: IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>>,
    L: ILogger,
> {
    ctx: Arc<Ctx<TM, CO, AddSc, StatSc, L>>,
}

struct Ctx<
    TM: ITokenMaker,
    CO: ICryptoOps,
    AddSc: IAsyncScript<Request = Vec<Arc<ApiMsgPacked>>, Response = ()>,
    StatSc: IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>>,
    L: ILogger,
> {
    token_maker: TM,
    crypto_ops: CO,
    add_script: AddSc,
    get_status_script: StatSc,
    logger: L,
}

impl<
        TM: ITokenMaker + 'static,
        CO: ICryptoOps + 'static,
        AddSc: IAsyncScript<Request = Vec<Arc<ApiMsgPacked>>, Response = ()>,
        StatSc: IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>>,
        L: ILogger + 'static,
    > AddMsgsHandler<TM, CO, AddSc, StatSc, L>
{
    pub fn new(
        token_maker: TM,
        crypto_ops: CO,
        add_script: AddSc,
        get_status_script: StatSc,
        logger: L,
    ) -> Self {
        let ctx = Arc::new(Ctx {
            token_maker,
            crypto_ops,
            add_script,
            get_status_script,
            logger,
        });
        Self { ctx }
    }

    async fn a_handle(self, token: Vec<u8>, msgs: Vec<ApiMsgPacked>) -> Result<Vec<u8>, ApiError> {
        let deref_token = match self.ctx.token_maker.deref_token(token).await {
            Ok(val) => val,
            Err(err) => {
                self.ctx
                    .logger
                    .log(Level::Debug, &format!("Add msgs::invalid error: {:?}", err));
                return Err(ApiError::invalid_token());
            }
        };
        let id_list = msgs.iter().map(|m| m.id).collect::<Vec<_>>();
        let msgs = msgs.into_iter().map(Arc::new).collect::<Vec<_>>();
        self.ctx
            .add_script
            .execute(deref_token.uid, deref_token.key_code.clone(), msgs)
            .await?;
        let map = self
            .ctx
            .get_status_script
            .execute(deref_token.uid, deref_token.key_code, id_list)
            .await?;
        let bts = self.ctx.crypto_ops.encrypt(
            &deref_token.pub_key,
            &to_bytes(&ServerResponse::Statuses { map })?,
        )?;
        // let bts = self
        //     .ctx
        //     .crypto_ops
        //     .encrypt(&deref_token.pub_key, &to_bytes(&ServerResponse::Nothing)?)?;
        Ok(bts)
    }
}

impl<
        TM: ITokenMaker + 'static,
        CO: ICryptoOps + 'static,
        AddSc: IAsyncScript<Request = Vec<Arc<ApiMsgPacked>>, Response = ()> + 'static,
        StatSc: IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>> + 'static,
        L: ILogger + 'static,
    > IApiHandler for AddMsgsHandler<TM, CO, AddSc, StatSc, L>
{
    fn name(&self) -> &str {
        "add_msgs"
    }

    fn match_msg(&self, call: &ServerCall) -> bool {
        matches!(call, ServerCall::SendMessages { .. })
    }

    fn handle(&self, call: ServerCall) -> DynFutRes<Vec<u8>, ApiError> {
        let (token, msgs) = match call {
            ServerCall::SendMessages { token, msgs } => (token, msgs),
            _ => unreachable!(),
        };
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_handle(token, msgs),
        )
    }
}
