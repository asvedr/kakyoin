use crate::entities::errors::ApiError;
use crate::proto::common::IAsyncScript;
use crate::proto::local::ITokenMaker;
use crate::proto::net::IApiHandler;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::log::Level;
use kakyoin_base::entities::server_call::{ApiUserMeta, ServerCall, ServerResponse};
use kakyoin_base::proto::common::{ICryptoOps, ILogger};
use serdebin::to_bytes;
use std::sync::Arc;
use uuid::Uuid;

pub struct ReqMetaHandler<
    TM: ITokenMaker,
    CO: ICryptoOps,
    Sc: IAsyncScript<Request = (Uuid, String), Response = Box<ApiUserMeta>>,
    L: ILogger,
> {
    ctx: Arc<Ctx<TM, CO, Sc, L>>,
}

struct Ctx<
    TM: ITokenMaker,
    CO: ICryptoOps,
    Sc: IAsyncScript<Request = (Uuid, String), Response = Box<ApiUserMeta>>,
    L: ILogger,
> {
    token_maker: TM,
    crypto_ops: CO,
    script: Sc,
    logger: L,
}

impl<
        TM: ITokenMaker,
        CO: ICryptoOps,
        Sc: IAsyncScript<Request = (Uuid, String), Response = Box<ApiUserMeta>>,
        L: ILogger,
    > ReqMetaHandler<TM, CO, Sc, L>
{
    pub fn new(token_maker: TM, crypto_ops: CO, script: Sc, logger: L) -> Self {
        let ctx = Arc::new(Ctx {
            token_maker,
            crypto_ops,
            script,
            logger,
        });
        Self { ctx }
    }

    async fn a_handle(
        self,
        token: Vec<u8>,
        user: Uuid,
        key_code: String,
    ) -> Result<Vec<u8>, ApiError> {
        let deref_token = match self.ctx.token_maker.deref_token(token).await {
            Ok(val) => val,
            Err(err) => {
                self.ctx.logger.log(
                    Level::Debug,
                    &format!("Post my meta::invalid error: {:?}", err),
                );
                return Err(ApiError::invalid_token());
            }
        };
        let meta = self
            .ctx
            .script
            .execute(deref_token.uid, deref_token.key_code, (user, key_code))
            .await?;
        let resp = ServerResponse::UserMeta { meta };
        let bts = self
            .ctx
            .crypto_ops
            .encrypt(&deref_token.pub_key, &to_bytes(&resp)?)?;
        Ok(bts)
    }
}

impl<
        TM: ITokenMaker + 'static,
        CO: ICryptoOps + 'static,
        Sc: IAsyncScript<Request = (Uuid, String), Response = Box<ApiUserMeta>> + 'static,
        L: ILogger + 'static,
    > IApiHandler for ReqMetaHandler<TM, CO, Sc, L>
{
    fn name(&self) -> &str {
        "mark_received"
    }

    fn match_msg(&self, call: &ServerCall) -> bool {
        matches!(call, ServerCall::ReqMeta { .. })
    }

    fn handle(&self, call: ServerCall) -> DynFutRes<Vec<u8>, ApiError> {
        let (token, user, key_code) = match call {
            ServerCall::ReqMeta {
                token,
                user,
                key_code,
            } => (token, user, key_code),
            _ => unreachable!(),
        };
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_handle(token, user, key_code),
        )
    }
}
