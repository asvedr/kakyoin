use crate::entities::errors::ApiError;
use crate::proto::common::IAsyncScript;
use crate::proto::local::ITokenMaker;
use crate::proto::net::IApiHandler;
use kakyoin_base::entities::api_msg::ApiMsgPacked;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::log::Level;
use kakyoin_base::entities::server_call::{ServerCall, ServerResponse};
use kakyoin_base::proto::common::{ICryptoOps, ILogger};
use serdebin::to_bytes;

use std::sync::Arc;

pub struct FetchMsgsHandler<
    TM: ITokenMaker,
    CO: ICryptoOps,
    Sc: IAsyncScript<Request = (), Response = Vec<Arc<ApiMsgPacked>>>,
    L: ILogger,
> {
    ctx: Arc<Ctx<TM, CO, Sc, L>>,
}

struct Ctx<
    TM: ITokenMaker,
    CO: ICryptoOps,
    Sc: IAsyncScript<Request = (), Response = Vec<Arc<ApiMsgPacked>>>,
    L: ILogger,
> {
    token_maker: TM,
    crypto_ops: CO,
    script: Sc,
    logger: L,
}

impl<
        TM: ITokenMaker + 'static,
        CO: ICryptoOps + 'static,
        Sc: IAsyncScript<Request = (), Response = Vec<Arc<ApiMsgPacked>>> + 'static,
        L: ILogger + 'static,
    > FetchMsgsHandler<TM, CO, Sc, L>
{
    pub fn new(token_maker: TM, crypto_ops: CO, script: Sc, logger: L) -> Self {
        let ctx = Arc::new(Ctx {
            token_maker,
            crypto_ops,
            script,
            logger,
        });
        Self { ctx }
    }

    async fn a_handle(self, token: Vec<u8>) -> Result<Vec<u8>, ApiError> {
        let deref_token = match self.ctx.token_maker.deref_token(token).await {
            Ok(val) => val,
            Err(err) => {
                self.ctx.logger.log(
                    Level::Debug,
                    &format!("Fetch msgs::invalid error: {:?}", err),
                );
                return Err(ApiError::invalid_token());
            }
        };
        let msgs = self
            .ctx
            .script
            .execute(deref_token.uid, deref_token.key_code.clone(), ())
            .await?
            .into_iter()
            .map(|ptr| (*ptr).clone())
            .collect::<Vec<ApiMsgPacked>>();
        let bts = self.ctx.crypto_ops.encrypt(
            &deref_token.pub_key,
            &to_bytes(&ServerResponse::Messages { msgs })?,
        )?;
        Ok(bts)
    }
}

impl<
        TM: ITokenMaker + 'static,
        CO: ICryptoOps + 'static,
        Sc: IAsyncScript<Request = (), Response = Vec<Arc<ApiMsgPacked>>> + 'static,
        L: ILogger + 'static,
    > IApiHandler for FetchMsgsHandler<TM, CO, Sc, L>
{
    fn name(&self) -> &str {
        "fetch_msgs"
    }

    fn match_msg(&self, call: &ServerCall) -> bool {
        matches!(call, ServerCall::FetchMessages { .. })
    }

    fn handle(&self, call: ServerCall) -> DynFutRes<Vec<u8>, ApiError> {
        let token = match call {
            ServerCall::FetchMessages { token } => token,
            _ => unreachable!(),
        };
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_handle(token),
        )
    }
}
