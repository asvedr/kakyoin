use futures_util::future::join_all;
use kakyoin_base::entities::common::DynFut;
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;

use crate::entities::errors::AddrGetterError;
use crate::proto::net::IAddrGetter;

pub struct MapAddrGetter<L: ILogger, AG: IAddrGetter> {
    pub logger: L,
    pub getters: Vec<AG>,
}

async fn deal_with<L: ILogger>(
    fut: DynFut<Result<String, AddrGetterError>>,
    name: String,
    logger: L,
) -> Option<String> {
    let err = match fut.await {
        Ok(val) => return Some(val),
        Err(err) => err,
    };
    let msg = format!("Can not get addr from {}: {:?}", name, err);
    logger.log(Level::Warn, &msg);
    None
}

#[inline]
fn reduce(results: Vec<Option<String>>) -> Result<String, AddrGetterError> {
    results
        .into_iter()
        .flatten()
        .next()
        .ok_or(AddrGetterError::AllFailed)
}

impl<L: ILogger + Clone + 'static, AG: IAddrGetter> IAddrGetter for MapAddrGetter<L, AG> {
    fn name(&self) -> String {
        "map_getter".to_string()
    }

    fn get_my_addr(&self) -> DynFut<Result<String, AddrGetterError>> {
        let futs = self
            .getters
            .iter()
            .map(|g| deal_with(g.get_my_addr(), g.name(), self.logger.clone()))
            .collect::<Vec<_>>();
        Box::pin(async move { reduce(join_all(futs).await) })
    }
}
