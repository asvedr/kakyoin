use std::convert::Infallible;
use std::io;
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::Arc;

use hyper::body::HttpBody;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Request, Response, Server};
use kakyoin_base::entities::common::DynFut;
use kakyoin_base::entities::log::Level;
use kakyoin_base::entities::server_call::ServerCall;
use kakyoin_base::proto::common::{ICryptoOps, ILogger};
use rsa::RsaPrivateKey;
use serdebin::from_bytes;

use crate::entities::errors::ApiError;
use crate::proto::common::IService;
use crate::proto::net::IApiHandler;

pub struct BaseServer<L: ILogger, CO: ICryptoOps, H: IApiHandler + 'static> {
    inner: Arc<ServerInner<L, CO, H>>,
}

struct ServerInner<L: ILogger, CO: ICryptoOps, H: IApiHandler + 'static> {
    addr: SocketAddr,
    service_name: &'static str,
    logger: L,
    crypto_ops: CO,
    my_key: Arc<RsaPrivateKey>,

    handlers: &'static [H],
}

impl<L: ILogger, CO: ICryptoOps + 'static, H: IApiHandler + 'static> Clone
    for BaseServer<L, CO, H>
{
    fn clone(&self) -> Self {
        Self {
            inner: self.inner.clone(),
        }
    }
}

impl<L: ILogger + 'static, CO: ICryptoOps + 'static, H: IApiHandler + 'static>
    BaseServer<L, CO, H>
{
    pub fn new(
        service_name: &'static str,
        crypto_ops: CO,
        logger: L,
        handlers: &'static [H],
        addr: &str,
        my_key: Arc<RsaPrivateKey>,
    ) -> Self {
        Self {
            inner: Arc::new(ServerInner {
                addr: SocketAddr::from_str(addr).unwrap(),
                service_name,
                logger,
                crypto_ops,
                my_key,
                handlers,
            }),
        }
    }

    async fn a_run(self) -> Result<(), io::Error> {
        self.inner.logger.log(Level::Debug, "Server launched");
        let arc = self.inner.clone();
        self.inner.run(arc).await
    }
}

pub fn make_response(status: u16, body: Body) -> Response<Body> {
    match Response::builder().status(status).body(body) {
        Ok(val) => val,
        Err(err) => panic!("Can not build response: {:?}", err),
    }
}

pub async fn read_request(mut req: Request<Body>) -> Result<Vec<u8>, ApiError> {
    Ok(match req.data().await {
        None => Vec::new(),
        Some(bts_res) => bts_res
            .map_err(|_| ApiError::can_not_read_request())?
            .to_vec(),
    })
}

impl<L: ILogger + 'static, CO: ICryptoOps + 'static, H: IApiHandler + 'static>
    ServerInner<L, CO, H>
{
    async fn run(&self, arc: Arc<Self>) -> Result<(), io::Error> {
        let addr = self.addr;

        let maker = make_service_fn(move |_cnn| {
            let arc = arc.clone();
            let f = service_fn(move |req| {
                let arc = arc.clone();
                async move { arc.handle(req).await }
            });
            async move { Ok::<_, Infallible>(f) }
        });

        let msg = format!("run {:?} on: {:?}", self.service_name, addr);
        self.logger.log(Level::Debug, &msg);
        let server = Server::bind(&addr).serve(maker);
        if let Err(err) = server.await {
            let msg = format!("{} failed: {:?}", self.service_name, err);
            self.logger.log(Level::Err, &msg);
            return Err(io::Error::new(io::ErrorKind::Other, err));
        }
        Ok(())
    }

    async fn handle_wrapped(&self, req: Request<Body>) -> Result<Vec<u8>, ApiError> {
        let call = self.read_req(req).await?;
        let opt_handler = self.handlers.iter().find(|h| h.match_msg(&call));
        if let Some(handler) = opt_handler {
            handler.handle(call).await
        } else {
            Err(ApiError {
                status: 404,
                code: "Handler not found",
                details: None,
            })
        }
    }

    async fn read_req(&self, req: Request<Body>) -> Result<ServerCall, ApiError> {
        let bts = read_request(req).await?;
        let decrypted = self.crypto_ops.decrypt(&self.my_key, &bts)?;
        let (call, _) = from_bytes(&decrypted)?;
        Ok(call)
    }

    async fn handle(&self, req: Request<Body>) -> Result<Response<Body>, Infallible> {
        let res = self.handle_wrapped(req).await;
        let err = match res {
            Ok(val) => {
                // self.logger.log(Level::Debug, &format!("{} => OK", meta));
                return Ok(make_response(200, Body::from(val)));
            }
            Err(val) => val,
        };
        let js = format!(
            r#"{op}"code": {code:?}, "details": {det:?}{cl}"#,
            op = '{',
            cl = '}',
            code = err.code,
            det = err.details.as_ref().map(|s| -> &str { s }).unwrap_or(""),
        );
        self.logger.log(Level::Debug, &js);
        Ok(make_response(err.status, Body::from(js)))
    }
}

impl<L: ILogger + 'static, CO: ICryptoOps + 'static, H: IApiHandler + 'static> IService
    for BaseServer<L, CO, H>
{
    fn name(&self) -> &str {
        self.inner.service_name
    }

    fn run(&mut self) -> DynFut<Result<(), io::Error>> {
        Box::pin(self.clone().a_run())
    }
}
