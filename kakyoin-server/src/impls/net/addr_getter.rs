use kakyoin_base::entities::common::DynFut;
use kakyoin_base::entities::errors::NetError;
use kakyoin_base::entities::net::HttpResp;
use kakyoin_base::proto::net::IHttpClient;

use crate::entities::errors::AddrGetterError;
use crate::proto::net::IAddrGetter;

pub struct AddrGetter<C: IHttpClient> {
    pub addr: String,
    pub client: C,
}

async fn resolve(get: DynFut<Result<HttpResp, NetError>>) -> Result<String, AddrGetterError> {
    let resp = get.await?;
    if resp.code != 200 {
        return Err(AddrGetterError::InvalidBody);
    }
    let text = String::from_utf8(resp.body).map_err(|_| AddrGetterError::InvalidBody)?;
    Ok(text.trim().to_string())
}

impl<C: IHttpClient> IAddrGetter for AddrGetter<C> {
    fn name(&self) -> String {
        format!("url({})", self.addr)
    }

    fn get_my_addr(&self) -> DynFut<Result<String, AddrGetterError>> {
        Box::pin(resolve(self.client.get(&self.addr, "")))
    }
}
