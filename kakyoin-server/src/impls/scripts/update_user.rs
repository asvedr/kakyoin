use crate::entities::errors::ApiUCError;

use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;
use crate::utils::time::get_now;

use kakyoin_base::entities::common::DynFutRes;

use uuid::Uuid;

#[derive(Clone)]
pub struct UpdateUserSc<S: IState> {
    state: S,
}

impl<S: IState> UpdateUserSc<S> {
    pub fn new(state: S) -> Self {
        Self { state }
    }

    async fn a_execute(self, id: Uuid, code: String) -> Result<(), ApiUCError> {
        let user_repo_lock = self.state.user();
        let mut user_repo = user_repo_lock.write().await;
        let user = user_repo.get_mut_by_id(&id, &code)?;
        user.last_active_ts = get_now();
        Ok(())
    }
}

impl<S: IState + Clone + 'static> IAsyncScript for UpdateUserSc<S> {
    type Request = ();
    type Response = ();

    fn execute(&self, id: Uuid, code: String, _: ()) -> DynFutRes<(), ApiUCError> {
        Box::pin(self.clone().a_execute(id, code))
    }
}
