use crate::entities::errors::ApiUCError;
use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;

use kakyoin_base::entities::common::DynFutRes;

use uuid::Uuid;

#[derive(Clone)]
pub struct MarkReceivedSc<S: IState, UU: IAsyncScript<Request = (), Response = ()>> {
    state: S,
    update_user: UU,
}

impl<S: IState + 'static, UU: IAsyncScript<Request = (), Response = ()> + 'static>
    MarkReceivedSc<S, UU>
{
    pub fn new(state: S, update_user: UU) -> Self {
        Self { state, update_user }
    }

    async fn a_execute(
        self,
        user_id: Uuid,
        code: String,
        id_list: Vec<Uuid>,
    ) -> Result<(), ApiUCError> {
        self.update_user.execute(user_id, code.clone(), ()).await?;
        let repo_lock = self.state.msg();
        let mut repo = repo_lock.write().await;
        for msg_id in id_list {
            let msg = match repo.get_by_id(&msg_id) {
                Ok(val) => &val.packed,
                _ => continue,
            };
            if msg.receiver != user_id || msg.key_code != code {
                continue;
            }
            repo.move_to_received(&msg_id)?;
        }
        Ok(())
    }
}

impl<
        S: IState + Clone + 'static,
        UU: IAsyncScript<Request = (), Response = ()> + Clone + 'static,
    > IAsyncScript for MarkReceivedSc<S, UU>
{
    type Request = Vec<Uuid>;
    type Response = ();

    fn execute(
        &self,
        id: Uuid,
        code: String,
        id_list: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError> {
        Box::pin(self.clone().a_execute(id, code, id_list))
    }
}
