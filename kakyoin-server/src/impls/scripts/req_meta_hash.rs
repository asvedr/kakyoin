use crate::entities::errors::{ApiUCError, StateRepoError};
use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;
use kakyoin_base::entities::common::DynFutRes;

use uuid::Uuid;

#[derive(Clone)]
pub struct ReqMetaHashSc<S: IState> {
    state: S,
}

impl<S: IState + 'static> ReqMetaHashSc<S> {
    pub fn new(state: S) -> Self {
        Self { state }
    }

    async fn a_execute(
        self,
        users: Vec<(Uuid, String)>,
    ) -> Result<Vec<((Uuid, String), String)>, ApiUCError> {
        let lock = self.state.user();
        let mut result = Vec::new();
        let repo = lock.read().await;
        for (id, code) in users {
            let user = match repo.get_by_id(&id, &code) {
                Ok(user) => user,
                Err(StateRepoError::NotFound) => continue,
                Err(err) => return Err(err.into()),
            };
            let key = (id, code);
            let val = user.meta.hash.clone();
            result.push((key, val));
        }
        Ok(result)
    }
}

impl<S: IState + Clone + 'static> IAsyncScript for ReqMetaHashSc<S> {
    type Request = Vec<(Uuid, String)>;
    type Response = Vec<((Uuid, String), String)>;

    fn execute(
        &self,
        _: Uuid,
        _: String,
        users: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError> {
        Box::pin(self.clone().a_execute(users))
    }
}
