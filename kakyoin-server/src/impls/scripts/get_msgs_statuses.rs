use crate::entities::errors::ApiUCError;
use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;
use kakyoin_base::entities::api_msg::ApiMsgStatus;
use kakyoin_base::entities::common::DynFutRes;
use std::collections::HashMap;
use uuid::Uuid;

#[derive(Clone)]
pub struct GetMsgsStatusesSc<S: IState, UU: IAsyncScript<Request = (), Response = ()>> {
    state: S,
    update_user: UU,
}

impl<S: IState + 'static, UU: IAsyncScript<Request = (), Response = ()> + 'static>
    GetMsgsStatusesSc<S, UU>
{
    pub fn new(state: S, update_user: UU) -> Self {
        Self { state, update_user }
    }

    async fn a_execute(
        self,
        user_id: Uuid,
        code: String,
        req: Vec<Uuid>,
    ) -> Result<HashMap<Uuid, ApiMsgStatus>, ApiUCError> {
        self.update_user.execute(user_id, code.clone(), ()).await?;
        let repo_lock = self.state.msg();
        let repo = repo_lock.read().await;
        let result = repo.check_statuses(&user_id, &code, &req);
        Ok(result)
    }
}

impl<
        S: IState + Clone + 'static,
        UU: IAsyncScript<Request = (), Response = ()> + Clone + 'static,
    > IAsyncScript for GetMsgsStatusesSc<S, UU>
{
    type Request = Vec<Uuid>;
    type Response = HashMap<Uuid, ApiMsgStatus>;

    fn execute(
        &self,
        id: Uuid,
        code: String,
        req: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError> {
        Box::pin(self.clone().a_execute(id, code, req))
    }
}
