use crate::entities::errors::ApiUCError;
use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::server_call::ApiUserMeta;
use uuid::Uuid;

#[derive(Clone)]
pub struct PostMyMetaSc<S: IState> {
    state: S,
}

impl<S: IState + 'static> PostMyMetaSc<S> {
    pub fn new(state: S) -> Self {
        Self { state }
    }

    async fn a_execute(
        self,
        id: Uuid,
        code: String,
        meta: Box<ApiUserMeta>,
    ) -> Result<(), ApiUCError> {
        let lock = self.state.user();
        let mut repo = lock.write().await;
        let user = repo.get_mut_by_id(&id, &code)?;
        user.meta = meta.into();
        Ok(())
    }
}

impl<S: IState + Clone + 'static> IAsyncScript for PostMyMetaSc<S> {
    type Request = Box<ApiUserMeta>;
    type Response = ();

    fn execute(
        &self,
        id: Uuid,
        code: String,
        req: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError> {
        Box::pin(self.clone().a_execute(id, code, req))
    }
}
