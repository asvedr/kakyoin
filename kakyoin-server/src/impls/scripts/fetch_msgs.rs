use crate::entities::errors::ApiUCError;
use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;
use kakyoin_base::entities::api_msg::ApiMsgPacked;
use kakyoin_base::entities::common::DynFutRes;

use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;
use std::sync::Arc;
use uuid::Uuid;

#[derive(Clone)]
pub struct FetchMsgsSc<L: ILogger, S: IState, UU: IAsyncScript<Request = (), Response = ()>> {
    logger: L,
    state: S,
    update_user: UU,
}

impl<
        L: ILogger + 'static,
        S: IState + 'static,
        UU: IAsyncScript<Request = (), Response = ()> + 'static,
    > FetchMsgsSc<L, S, UU>
{
    pub fn new(logger: L, state: S, update_user: UU) -> Self {
        Self {
            logger,
            state,
            update_user,
        }
    }

    async fn a_execute(
        self,
        user_id: Uuid,
        code: String,
    ) -> Result<Vec<Arc<ApiMsgPacked>>, ApiUCError> {
        self.update_user.execute(user_id, code.clone(), ()).await?;
        let repo_lock = self.state.msg();
        let repo = repo_lock.read().await;
        let result = repo.get_new_for_user(&user_id, &code);
        self.logger.log(
            Level::Debug,
            &format!("Fetch msgs. User: {}, msgs: {}", user_id, result.len()),
        );
        Ok(result)
    }
}

impl<
        L: ILogger + Clone + 'static,
        S: IState + Clone + 'static,
        UU: IAsyncScript<Request = (), Response = ()> + Clone + 'static,
    > IAsyncScript for FetchMsgsSc<L, S, UU>
{
    type Request = ();
    type Response = Vec<Arc<ApiMsgPacked>>;

    fn execute(
        &self,
        id: Uuid,
        code: String,
        _: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError> {
        Box::pin(self.clone().a_execute(id, code))
    }
}
