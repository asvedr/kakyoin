use crate::entities::errors::{ApiUCError, StateRepoError};
use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::server_call::ApiUserMeta;
use uuid::Uuid;

#[derive(Clone)]
pub struct ReqMetaSc<S: IState> {
    state: S,
}

impl<S: IState + 'static> ReqMetaSc<S> {
    pub fn new(state: S) -> Self {
        Self { state }
    }

    async fn a_execute(self, id: Uuid, code: String) -> Result<Box<ApiUserMeta>, ApiUCError> {
        let lock = self.state.user();
        let repo = lock.read().await;
        match repo.get_by_id(&id, &code) {
            Ok(user) => Ok(Box::new((*user.meta).clone())),
            Err(StateRepoError::NotFound) => Err(ApiUCError::UserNotFound),
            Err(err) => Err(err.into()),
        }
    }
}

impl<S: IState + Clone + 'static> IAsyncScript for ReqMetaSc<S> {
    type Request = (Uuid, String);
    type Response = Box<ApiUserMeta>;

    fn execute(
        &self,
        _: Uuid,
        _: String,
        req: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError> {
        let (id, code) = req;
        Box::pin(self.clone().a_execute(id, code))
    }
}
