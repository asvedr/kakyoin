use kakyoin_base::entities::api_msg::ApiMsgPacked;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;

use std::sync::Arc;
use uuid::Uuid;

use crate::entities::errors::{ApiUCError, StateRepoError};
use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;

#[derive(Clone)]
pub struct AddMsgsSc<L: ILogger, S: IState, UU: IAsyncScript<Request = (), Response = ()>> {
    logger: L,
    state: S,
    update_user: UU,
}

impl<
        L: ILogger + 'static,
        S: IState + 'static,
        UU: IAsyncScript<Request = (), Response = ()> + 'static,
    > AddMsgsSc<L, S, UU>
{
    pub fn new(logger: L, state: S, update_user: UU) -> Self {
        Self {
            logger,
            state,
            update_user,
        }
    }

    async fn a_execute(
        self,
        user_id: Uuid,
        code: String,
        req: Vec<Arc<ApiMsgPacked>>,
    ) -> Result<(), ApiUCError> {
        self.update_user.execute(user_id, code.clone(), ()).await?;
        let msg_repo_lock = self.state.msg();
        let mut msg_repo = msg_repo_lock.write().await;
        for msg in req {
            let id = msg.id;
            let receiver = msg.receiver;
            match msg_repo.create_new(user_id, code.clone(), msg) {
                Err(err) => self.log_err(&id, &user_id, &code, err),
                Ok(true) => self.logger.log(
                    Level::Debug,
                    &format!("Created msg({}) {} ==> {}", id, user_id, receiver),
                ),
                _ => (),
            }
        }
        Ok(())
    }

    #[inline]
    fn log_err(&self, msg_id: &Uuid, user_id: &Uuid, code: &str, err: StateRepoError) {
        self.logger.log(
            Level::Err,
            &format!(
                "Can not add msg({}) from user({} :: {}): {:?}",
                msg_id, user_id, code, err,
            ),
        )
    }
}

impl<
        L: ILogger + Clone + 'static,
        S: IState + Clone + 'static,
        UU: IAsyncScript<Request = (), Response = ()> + Clone + 'static,
    > IAsyncScript for AddMsgsSc<L, S, UU>
{
    type Request = Vec<Arc<ApiMsgPacked>>;
    type Response = ();

    fn execute(
        &self,
        id: Uuid,
        code: String,
        req: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError> {
        Box::pin(self.clone().a_execute(id, code, req))
    }
}
