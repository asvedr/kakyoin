use std::path::Path;
use std::sync::Arc;
use std::time::SystemTime;

use crate::app::serializers::rsa_key_ser;
use kakyoin_base::impls::crypto_ops::CryptoOps;
use kakyoin_base::impls::fs::FS;
use kakyoin_base::impls::logger::Logger;
use kakyoin_base::proto::common::IFS;
use kakyoin_base::proto::serializers::IStrSerializer;
use mddd::macros::singleton;
use rsa::RsaPrivateKey;

use crate::entities::config::Config;

#[singleton(no_mutex)]
pub fn config() -> Config {
    use mddd::traits::IConfig;
    match Config::build() {
        Ok(val) => val,
        Err(err) => panic!("Can not build config: {:?}", err),
    }
}

#[singleton(no_mutex)]
pub fn logger() -> Logger {
    let prefix = config().log_prefix.clone();
    let now = move || -> String {
        let time = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        format!("{}ts={}", prefix, time)
    };
    Logger::new(
        config().log_level,
        Box::new(now),
        Box::new(std::io::stderr()),
    )
}

#[singleton(no_mutex)]
pub fn crypto_ops() -> CryptoOps<&'static Logger> {
    CryptoOps::new(logger())
}

#[singleton(no_mutex)]
pub fn fs() -> FS {
    FS
}

#[singleton(no_mutex)]
pub fn my_priv_key() -> Arc<RsaPrivateKey> {
    let path = match config().priv_key_path {
        Some(ref val) => val,
        None => panic!("Priv key not set"),
    };
    let bts = match fs().read_file(Path::new(&path)) {
        Ok(val) => val,
        Err(err) => panic!("Can not read priv key: {:?}", err),
    };
    let text = String::from_utf8(bts).expect("Invalid priv key");
    let key = rsa_key_ser().deserialize(&text).expect("Invalid priv key");
    Arc::new(key)
}

pub fn tokio_rt() -> tokio::runtime::Runtime {
    let cnf = config();
    tokio::runtime::Builder::new_multi_thread()
        .worker_threads(cnf.max_threads)
        .enable_all()
        .build()
        .expect("Can not build tokio")
}
