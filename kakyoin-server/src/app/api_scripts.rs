use kakyoin_base::entities::api_msg::{ApiMsgPacked, ApiMsgStatus};
use kakyoin_base::entities::server_call::ApiUserMeta;

use mddd::macros::singleton;
use std::collections::HashMap;
use std::sync::Arc;
use uuid::Uuid;

use crate::app::common::logger;
use crate::app::local::state;
use crate::impls::scripts::add_msgs::AddMsgsSc;
use crate::impls::scripts::fetch_msgs::FetchMsgsSc;
use crate::impls::scripts::get_msgs_statuses::GetMsgsStatusesSc;
use crate::impls::scripts::mark_received::MarkReceivedSc;
use crate::impls::scripts::post_my_meta::PostMyMetaSc;
use crate::impls::scripts::req_meta::ReqMetaSc;
use crate::impls::scripts::req_meta_hash::ReqMetaHashSc;
use crate::impls::scripts::update_user::UpdateUserSc;
use crate::proto::common::IAsyncScript;
use crate::proto::local::IState;

#[singleton(no_mutex)]
fn update_user_sc() -> UpdateUserSc<&'static dyn IState> {
    UpdateUserSc::new(state())
}

#[singleton(no_mutex)]
pub fn add_msgs_sc() -> Box<dyn IAsyncScript<Request = Vec<Arc<ApiMsgPacked>>, Response = ()>> {
    Box::new(AddMsgsSc::new(logger(), state(), update_user_sc()))
}

#[singleton(no_mutex)]
pub fn fetch_msgs_sc() -> Box<dyn IAsyncScript<Request = (), Response = Vec<Arc<ApiMsgPacked>>>> {
    Box::new(FetchMsgsSc::new(logger(), state(), update_user_sc()))
}

#[singleton(no_mutex)]
pub fn get_msg_statuses_sc(
) -> Box<dyn IAsyncScript<Request = Vec<Uuid>, Response = HashMap<Uuid, ApiMsgStatus>>> {
    Box::new(GetMsgsStatusesSc::new(state(), update_user_sc()))
}

#[singleton(no_mutex)]
pub fn mark_received_sc() -> Box<dyn IAsyncScript<Request = Vec<Uuid>, Response = ()>> {
    Box::new(MarkReceivedSc::new(state(), update_user_sc()))
}

#[singleton(no_mutex)]
pub fn post_my_meta_sc() -> Box<dyn IAsyncScript<Request = Box<ApiUserMeta>, Response = ()>> {
    Box::new(PostMyMetaSc::new(state()))
}

#[singleton(no_mutex)]
pub fn req_meta_sc() -> Box<dyn IAsyncScript<Request = (Uuid, String), Response = Box<ApiUserMeta>>>
{
    Box::new(ReqMetaSc::new(state()))
}

#[singleton(no_mutex)]
pub fn req_meta_hash_sc(
) -> Box<dyn IAsyncScript<Request = Vec<(Uuid, String)>, Response = Vec<((Uuid, String), String)>>>
{
    Box::new(ReqMetaHashSc::new(state()))
}
