use atex::{ITaskManager, TaskManagerBuilder};
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;
use mddd::macros::singleton;

use crate::app::common::{config, logger};
use crate::app::local::state;
use crate::impls::local::state::State;
use crate::impls::periodics::service::PeriodicService;
use crate::impls::periodics::{del_garbage_msg, del_garbage_user};
use crate::proto::common::IService;

#[singleton(no_mutex)]
fn del_garbage_user() -> del_garbage_user::Periodic<&'static State> {
    del_garbage_user::Periodic::new(config(), state())
}

#[singleton(no_mutex)]
fn del_garbage_msg() -> del_garbage_msg::Periodic<&'static State> {
    del_garbage_msg::Periodic::new(config(), state())
}

#[singleton(no_mutex)]
fn task_manager() -> Box<dyn ITaskManager> {
    let log = logger();
    TaskManagerBuilder::new_mem()
        .set_log_func(Box::new(|msg| log.log(Level::Err, msg)))
        .add_executor(del_garbage_user())
        .add_executor(del_garbage_msg())
        .build()
        .expect("Can not build task executor")
}

pub fn task_service() -> Box<dyn IService> {
    Box::new(PeriodicService {
        logger: logger(),
        executor: &**task_manager(),
    })
}
