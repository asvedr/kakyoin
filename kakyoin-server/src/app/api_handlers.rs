use mddd::macros::singleton;

use crate::app::api_scripts::{
    add_msgs_sc, fetch_msgs_sc, get_msg_statuses_sc, mark_received_sc, post_my_meta_sc,
    req_meta_hash_sc, req_meta_sc,
};
use crate::app::common::{crypto_ops, logger};
use crate::app::local::token_maker;
use crate::impls::api_handlers::add_msgs::AddMsgsHandler;
use crate::impls::api_handlers::fetch_msgs::FetchMsgsHandler;
use crate::impls::api_handlers::get_msgs_statuses::GetMsgsStatusesHandler;
use crate::impls::api_handlers::get_token::GetTokenHandler;
use crate::impls::api_handlers::mark_received::MarkReceivedHandler;
use crate::impls::api_handlers::post_my_meta::PostMyMetaHandler;
use crate::impls::api_handlers::req_meta::ReqMetaHandler;
use crate::impls::api_handlers::req_meta_hash::ReqMetaHashHandler;
use crate::proto::net::IApiHandler;

#[singleton(no_mutex)]
fn add_msgs() -> Box<dyn IApiHandler> {
    Box::new(AddMsgsHandler::new(
        token_maker(),
        crypto_ops(),
        &**add_msgs_sc(),
        &**get_msg_statuses_sc(),
        logger(),
    ))
}

#[singleton(no_mutex)]
fn fetch_msgs() -> Box<dyn IApiHandler> {
    Box::new(FetchMsgsHandler::new(
        token_maker(),
        crypto_ops(),
        &**fetch_msgs_sc(),
        logger(),
    ))
}

#[singleton(no_mutex)]
fn get_msgs_statuses() -> Box<dyn IApiHandler> {
    Box::new(GetMsgsStatusesHandler::new(
        token_maker(),
        crypto_ops(),
        &**get_msg_statuses_sc(),
        logger(),
    ))
}

#[singleton(no_mutex)]
fn mark_received() -> Box<dyn IApiHandler> {
    Box::new(MarkReceivedHandler::new(
        token_maker(),
        crypto_ops(),
        &**mark_received_sc(),
        logger(),
    ))
}

#[singleton(no_mutex)]
fn get_token() -> Box<dyn IApiHandler> {
    Box::new(GetTokenHandler::new(token_maker(), crypto_ops()))
}

#[singleton(no_mutex)]
fn post_my_meta() -> Box<dyn IApiHandler> {
    Box::new(PostMyMetaHandler::new(
        token_maker(),
        crypto_ops(),
        &**post_my_meta_sc(),
        logger(),
    ))
}

#[singleton(no_mutex)]
fn req_meta() -> Box<dyn IApiHandler> {
    Box::new(ReqMetaHandler::new(
        token_maker(),
        crypto_ops(),
        &**req_meta_sc(),
        logger(),
    ))
}

#[singleton(no_mutex)]
fn req_meta_hash() -> Box<dyn IApiHandler> {
    Box::new(ReqMetaHashHandler::new(
        token_maker(),
        crypto_ops(),
        &**req_meta_hash_sc(),
        logger(),
    ))
}

#[singleton(no_mutex)]
pub fn all() -> Vec<&'static dyn IApiHandler> {
    vec![
        &**get_token(),
        &**add_msgs(),
        &**fetch_msgs(),
        &**get_msgs_statuses(),
        &**mark_received(),
        &**post_my_meta(),
        &**req_meta(),
        &**req_meta_hash(),
    ]
}
