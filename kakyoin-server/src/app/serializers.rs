use kakyoin_base::impls::serializers::rsa_key;
use kakyoin_base::impls::serializers::server_contact;
use mddd::macros::singleton;

#[singleton(no_mutex)]
pub fn rsa_key_ser() -> rsa_key::Serializer {
    rsa_key::Serializer
}

#[singleton(no_mutex)]
pub fn contact_ser() -> server_contact::Serializer {
    server_contact::Serializer
}
