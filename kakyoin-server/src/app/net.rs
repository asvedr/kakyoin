use crate::app::api_handlers;
use crate::app::common::{config, crypto_ops, logger, my_priv_key};
use crate::impls::net::addr_getter::AddrGetter;
use crate::impls::net::map_addr_getter::MapAddrGetter;
use crate::impls::net::server::BaseServer;
use crate::proto::common::IService;
use crate::proto::net::IAddrGetter;
use kakyoin_base::impls::http_client::HttpClient;
use mddd::macros::singleton;

const ADDRS: &[&str] = &[
    "icanhazip.com",
    "ipinfo.io/ip",
    "ifconfig.me",
    "api.ipify.org",
    "ident.me",
    "ipecho.net/plain",
];

#[singleton(no_mutex)]
pub fn http_client() -> HttpClient {
    HttpClient::new(config().timeout)
}

#[singleton(no_mutex)]
pub fn addr_getter() -> Box<dyn IAddrGetter> {
    let mut getters = Vec::new();
    for addr in ADDRS {
        let addr = addr.to_string();
        let client = http_client();
        getters.push(AddrGetter { addr, client })
    }
    let logger = logger();
    Box::new(MapAddrGetter { logger, getters })
}

pub fn server() -> Box<dyn IService> {
    Box::new(BaseServer::new(
        "server",
        crypto_ops(),
        logger(),
        api_handlers::all(),
        &format!("0.0.0.0:{}", config().port),
        my_priv_key().clone(),
    ))
}
