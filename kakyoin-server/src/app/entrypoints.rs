use crate::app::common::{config, crypto_ops, fs};
use crate::app::net::{addr_getter, server};
use crate::app::serializers::{contact_ser, rsa_key_ser};
use crate::app::tasks::task_service;
use crate::entrypoints::gen_priv_key::GenPrivKeyUC;
use crate::entrypoints::get_contact::GetContactUC;
use crate::entrypoints::run::RunUC;

pub fn gen_key() -> GenPrivKeyUC {
    GenPrivKeyUC {
        crypto_ops: crypto_ops(),
        serializer: rsa_key_ser(),
        fs: fs(),
    }
}

pub fn get_contact() -> GetContactUC {
    GetContactUC {
        addr_getter: &**addr_getter(),
        fs: fs(),
        key_ser: rsa_key_ser(),
        crypto_ops: crypto_ops(),
        contact_ser: contact_ser(),
        config: config(),
    }
}

pub fn run() -> RunUC {
    RunUC {
        services: vec![server(), task_service()],
    }
}
