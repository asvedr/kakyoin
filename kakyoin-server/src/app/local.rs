use mddd::macros::singleton;

use crate::app::common::{config, crypto_ops, logger, my_priv_key};
use crate::impls::local::state::State;
use crate::impls::local::token_maker::TokenMaker;
use crate::proto::local::ITokenMaker;

#[singleton(no_mutex)]
pub fn state() -> State {
    State::new(config())
}

#[singleton(no_mutex)]
fn _token_maker() -> Box<dyn ITokenMaker> {
    Box::new(TokenMaker::new(
        state(),
        crypto_ops(),
        logger(),
        my_priv_key().clone(),
    ))
}

#[inline(always)]
pub fn token_maker() -> &'static dyn ITokenMaker {
    &**_token_maker()
}
