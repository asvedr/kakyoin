use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;

#[derive(Default)]
pub struct MockLogger;

impl ILogger for MockLogger {
    fn log(&self, _level: Level, _msg: &str) {}
}
