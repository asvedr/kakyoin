use std::path::Path;
use std::sync::Arc;

use mddd::macros::singleton;
use rsa::{RsaPrivateKey, RsaPublicKey};

use kakyoin_base::impls::fs::FS;
use kakyoin_base::impls::serializers::rsa_key;
use kakyoin_base::proto::common::IFS;
use kakyoin_base::proto::serializers::IStrSerializer;

#[singleton]
pub fn priv_key_1() -> Arc<RsaPrivateKey> {
    let data = FS.read_file(Path::new("test_data/priv_key1.txt")).unwrap();
    let text = String::from_utf8(data).unwrap();
    Arc::new(rsa_key::Serializer.deserialize(&text).unwrap())
}

#[singleton]
pub fn priv_key_2() -> Arc<RsaPrivateKey> {
    let data = FS.read_file(Path::new("test_data/priv_key2.txt")).unwrap();
    let text = String::from_utf8(data).unwrap();
    Arc::new(rsa_key::Serializer.deserialize(&text).unwrap())
}

#[singleton]
pub fn pub_key_1() -> Arc<RsaPublicKey> {
    let data = FS.read_file(Path::new("test_data/pub_key1.txt")).unwrap();
    let text = String::from_utf8(data).unwrap();
    Arc::new(rsa_key::Serializer.deserialize(&text).unwrap())
}

#[singleton]
pub fn pub_key_2() -> Arc<RsaPublicKey> {
    let data = FS.read_file(Path::new("test_data/pub_key2.txt")).unwrap();
    let text = String::from_utf8(data).unwrap();
    Arc::new(rsa_key::Serializer.deserialize(&text).unwrap())
}
