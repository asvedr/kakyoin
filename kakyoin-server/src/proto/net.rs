use kakyoin_base::entities::common::{DynFut, DynFutRes};
use kakyoin_base::entities::server_call::ServerCall;
use mddd::macros::auto_impl;

use crate::entities::errors::{AddrGetterError, ApiError};

#[auto_impl(link, dyn)]
pub trait IAddrGetter: Send + Sync {
    fn name(&self) -> String;
    fn get_my_addr(&self) -> DynFut<Result<String, AddrGetterError>>;
}

#[auto_impl(link, dyn)]
pub trait IApiHandler: Send + Sync {
    fn name(&self) -> &str;
    fn match_msg(&self, call: &ServerCall) -> bool;
    fn handle(&self, call: ServerCall) -> DynFutRes<Vec<u8>, ApiError>;
}
