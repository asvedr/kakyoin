use std::io;

use kakyoin_base::entities::common::{DynFut, DynFutRes};
use mddd::macros::auto_impl;

use uuid::Uuid;

use crate::entities::errors::ApiUCError;

#[auto_impl(link)]
pub trait IAsyncScript: Send + Sync {
    type Request;
    type Response;
    fn execute(
        &self,
        id: Uuid,
        code: String,
        req: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError>;
}

impl<Req, Resp> IAsyncScript for &dyn IAsyncScript<Request = Req, Response = Resp> {
    type Request = Req;
    type Response = Resp;

    fn execute(
        &self,
        id: Uuid,
        code: String,
        req: Self::Request,
    ) -> DynFutRes<Self::Response, ApiUCError> {
        (**self).execute(id, code, req)
    }
}

// #[auto_impl(link)]
pub trait IService: Send + Sync {
    fn name(&self) -> &str;
    fn run(&mut self) -> DynFut<Result<(), io::Error>>;
}
