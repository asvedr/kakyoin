use std::collections::HashMap;
use std::sync::Arc;

use crate::entities::common::DerefToken;
use kakyoin_base::entities::api_msg::{ApiMsgPacked, ApiMsgStatus};
use kakyoin_base::entities::common::DynFut;
use mddd::macros::auto_impl;
use rsa::RsaPublicKey;
use tokio::sync::RwLock;
use uuid::Uuid;

use crate::entities::errors::{StateRepoResult, TokenMakerError};
use crate::entities::state::{Msg, MsgTableStats, User, UserTableStats};

#[auto_impl(link, dyn)]
pub trait IState: Send + Sync {
    fn user(&self) -> Arc<RwLock<Box<dyn IUserTable>>>;
    fn msg(&self) -> Arc<RwLock<Box<dyn IMsgTable>>>;
}

pub trait IMsgTable: Send + Sync {
    fn get_by_id(&self, id: &Uuid) -> StateRepoResult<&Msg>;
    fn check_statuses(
        &self,
        sender_id: &Uuid,
        sender_key: &str,
        id_list: &[Uuid],
    ) -> HashMap<Uuid, ApiMsgStatus>;
    fn get_new_for_user(&self, user_id: &Uuid, user_key: &str) -> Vec<Arc<ApiMsgPacked>>;
    fn check_unknown(&self, id_list: &[Uuid]) -> Vec<Uuid>;
    fn get_stats(&self) -> MsgTableStats;
    fn move_to_received(&mut self, id: &Uuid) -> StateRepoResult<()>;
    fn create_new(
        &mut self,
        sender_id: Uuid,
        sender_code: String,
        packed: Arc<ApiMsgPacked>,
    ) -> StateRepoResult<bool>;
    fn del_garbage(&mut self);
}

pub trait IUserTable: Send + Sync {
    fn get_by_id(&self, id: &Uuid, code: &str) -> StateRepoResult<&User>;
    fn get_mut_by_id(&mut self, id: &Uuid, code: &str) -> StateRepoResult<&mut User>;
    fn get_stats(&self) -> UserTableStats;
    fn add_user(&mut self, user: User) -> StateRepoResult<()>;
    fn update_last_active_ts(&mut self, id: &Uuid, code: &str);
    fn set_ban(&mut self, id: &Uuid, code: &str, banned: bool);
    fn del_garbage(&mut self);
}

#[auto_impl(dyn)]
pub trait ITokenMaker: Send + Sync {
    fn make_token(
        &self,
        user_id: &Uuid,
        pub_key: Arc<RsaPublicKey>,
    ) -> DynFut<Option<(Vec<u8>, u64)>>;

    fn deref_token(&self, token: Vec<u8>) -> DynFut<Result<DerefToken, TokenMakerError>>;
}
