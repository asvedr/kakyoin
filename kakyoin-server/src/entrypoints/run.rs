use mddd::traits::IUseCase;
use std::mem;

use crate::app::common::tokio_rt;
use crate::proto::common::IService;

pub struct RunUC {
    pub services: Vec<Box<dyn IService>>,
}

async fn run(services: Vec<Box<dyn IService>>) {
    for mut service in services {
        tokio::task::spawn(service.run());
    }
    loop {
        tokio::task::yield_now().await;
    }
}

impl IUseCase for RunUC {
    type Request = ();
    type Error = ();

    fn short_description() -> String {
        "Run service".to_string()
    }

    fn execute(&mut self, _: Self::Request) -> Result<(), Self::Error> {
        let services = mem::take(&mut self.services);
        tokio_rt().block_on(async move { run(services).await });
        Ok(())
    }
}
