use std::path::Path;

use kakyoin_base::entities::errors::{CryptoErr, SerializerError};
use kakyoin_base::proto::common::{ICryptoOps, IFS};
use kakyoin_base::proto::serializers::IStrSerializer;
use mddd::macros::{EnumAutoFrom, IUseCaseRequest};
use mddd::traits::IUseCase;
use rsa::RsaPrivateKey;

pub struct GenPrivKeyUC {
    pub crypto_ops: &'static dyn ICryptoOps,
    pub serializer: &'static dyn IStrSerializer<RsaPrivateKey>,
    pub fs: &'static dyn IFS,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    path: String,
}

#[derive(EnumAutoFrom, Debug)]
pub enum Error {
    Crypto(CryptoErr),
    Ser(SerializerError),
    IO(std::io::Error),
}

impl IUseCase for GenPrivKeyUC {
    type Request = Request;
    type Error = Error;

    fn short_description() -> String {
        "Gen new private key".to_string()
    }

    fn execute(&mut self, request: Self::Request) -> Result<(), Self::Error> {
        let key = self.crypto_ops.gen_priv_key()?;
        let text = self.serializer.serialize(&key);
        self.fs
            .write_file(Path::new(&request.path), text.as_bytes())?;
        println!("Done");
        Ok(())
    }
}
