use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::entities::errors::SerializerError;
use kakyoin_base::proto::common::{ICryptoOps, IFS};
use kakyoin_base::proto::serializers::IStrSerializer;
use mddd::macros::{EnumAutoFrom, IUseCaseRequest};
use mddd::traits::IUseCase;
use rsa::RsaPrivateKey;
use std::path::Path;

use crate::app::common::tokio_rt;
use crate::entities::config::Config;
use crate::entities::errors::AddrGetterError;

use crate::proto::net::IAddrGetter;

#[derive(Clone)]
pub struct GetContactUC {
    pub addr_getter: &'static dyn IAddrGetter,
    pub fs: &'static dyn IFS,
    pub key_ser: &'static dyn IStrSerializer<RsaPrivateKey>,
    pub crypto_ops: &'static dyn ICryptoOps,
    pub contact_ser: &'static dyn IStrSerializer<ServerContact>,
    pub config: &'static Config,
}

#[derive(IUseCaseRequest)]
pub struct Request {
    addr: Option<String>,
}

#[derive(Debug, EnumAutoFrom)]
pub enum Error {
    IO(std::io::Error),
    AG(AddrGetterError),
    Ser(SerializerError),
    PrivKeyNotSet,
    InvalidPrivKey,
}

impl GetContactUC {
    async fn a_execute(self, req: Request) -> Result<(), Error> {
        let bts = match self.config.priv_key_path {
            None => return Err(Error::PrivKeyNotSet),
            Some(ref path) => self.fs.read_file(Path::new(path))?,
        };
        let txt = String::from_utf8(bts).map_err(|_| Error::InvalidPrivKey)?;
        let key = self.key_ser.deserialize(&txt)?;
        let ip = if let Some(addr) = req.addr {
            addr
        } else {
            self.addr_getter.get_my_addr().await?
        };
        println!("addr: {}", ip);
        let addr = self.combine_addr(ip, self.config.port);
        let pub_key = key.to_public_key();
        let code = self.crypto_ops.get_key_code(&pub_key);
        println!("port: {}", self.config.port);
        println!("key code: {}", code);
        let contact = ServerContact::new(addr, &key.to_public_key());
        let line = self.contact_ser.serialize(&contact);
        println!("contact: {}", line);
        Ok(())
    }

    fn combine_addr(&self, addr: String, path: u16) -> String {
        if addr.split(':').count() > 1 {
            // ipv6
            format!("[{}]:{}", addr, path)
        } else {
            // ipv4
            format!("{}:{}", addr, path)
        }
    }
}

impl IUseCase for GetContactUC {
    type Request = Request;
    type Error = Error;

    fn short_description() -> String {
        "Get string contact for clients".to_string()
    }

    fn execute(&mut self, req: Request) -> Result<(), Error> {
        tokio_rt().block_on(self.clone().a_execute(req))
    }
}
