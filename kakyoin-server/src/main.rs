mod app;
mod entities;
mod entrypoints;
mod impls;
mod proto;
mod utils;

fn main() {
    use crate::app::entrypoints as ep;
    use mddd::std::{LeafRunner, NodeRunner};

    NodeRunner::new("Kakyoin server app")
        .add("gen-key", LeafRunner::new(ep::gen_key))
        .add("get-contact", LeafRunner::new(ep::get_contact))
        .add("run", LeafRunner::new(ep::run))
        .run()
}
