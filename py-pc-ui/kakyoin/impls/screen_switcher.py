from typing import List, Type, Optional

from kivy.uix.boxlayout import BoxLayout

from kakyoin.entitites.common import ScreenName
from kakyoin.proto.common import IScreenSwitcher, IScreen, IUniqTaskExecutor, IApiRepo


class ScreenSwitcher(IScreenSwitcher, BoxLayout):
    def __init__(
        self,
        api: IApiRepo,
        executor: IUniqTaskExecutor,
        constructors: List[Type[IScreen]],
    ):
        BoxLayout.__init__(self)
        self._api = api
        self._executor = executor
        self._current = None
        self._constructors = {
            cnst.screen_name(): cnst
            for cnst in constructors
        }

    def get_active_screen(self) -> Optional[ScreenName]:
        return self._current

    def set_active_screen(self, screen: ScreenName) -> None:
        if self._current == screen:
            return
        self.clear_widgets()
        constructor = self._constructors[screen]
        screen = constructor(api=self._api, executor=self._executor)
        self.add_widget(screen)
