import json
from typing import Optional, List, Tuple, Dict, Awaitable

import aiohttp

from kakyoin.entitites.common import Me
from kakyoin.entitites.msg import ShortMsg
from kakyoin.proc_manager import PORT
from kakyoin.proto.common import IApiRepo


class ApiRepo(IApiRepo):
    def __init__(self):
        self.session = aiohttp.ClientSession(f"http://127.0.0.1:{PORT}/")

    async def check_init(self) -> bool:
        resp = await self._get("/api/check-init/")
        print(f'\nRESP: {resp}\n')
        return resp['status'] == 'INITED'

    async def init(self, name: str, uid: Optional[str], key: Optional[str]) -> None:
        js = {
            "name": name,
            "description": "",
        }
        if uid:
            js["uuid"] = uid
        if key:
            js["priv_key"] = key
        await self._post("/api/init/", js)

    async def get_servers(self) -> Dict[str, str]:
        resp = await self._get("/api/servers/")
        return {srv["addr"]: srv["status"] for srv in resp["data"]}

    async def add_server(self, data: str) -> None:
        await self._post("/api/servers/", {"data": data})

    async def get_me(self) -> Me:
        resp = await self._get("/api/settings/me/")
        return Me(**resp)

    async def patch_me(
        self,
        uuid: str = None,
        key: str = None,
        name: str = None,
        description: str = None,
        pic: str = None,
    ) -> None:
        params = {}
        if uuid:
            params["uuid"] = uuid
        if key:
            params["priv_key"] = key
        if name:
            params["name"] = name
        if description:
            params["description"] = description
        if pic:
            params["pic_path"] = pic
        if not params:
            return
        await self._patch("/api/settings/me/", params)

    async def regenerate(self, uuid: bool = False, key: bool = False) -> None:
        query = []
        if uuid:
            query.append("uuid=true")
        if key:
            query.append("priv_key=true")
        if not query:
            return
        await self._post(f"/api/settings/regenerate/?{'&'.join(query)}", {})

    async def get_last_msgs(self) -> List[ShortMsg]:
        resp = await self._get("/api/msgs/last-unread/")
        return [ShortMsg(**msg) for msg in resp["data"]]

    def _get(self, path: str) -> Awaitable[Optional[dict]]:
        return self._request('get', path)

    def _post(self, path: str, js: dict) -> Awaitable[Optional[dict]]:
        return self._request('post', path, js)

    def _patch(self, path: str, js: dict) -> Awaitable[Optional[dict]]:
        return self._request('patch', path, js)

    async def _request(self, method: str, path: str, js: dict = None) -> Optional[dict]:
        params = {'url': path}
        if js:
            params['data'] = json.dumps(js).encode()
        async with getattr(self.session, method)(**params) as resp:
            if resp.status != 200:
                print(f"request: {method} {path} ({js})")
                print(f"response status: {resp.status}")
                body = await resp.read()
                print(f"response data: {body}")
                raise ValueError(f"Response not 200 ({method} {path}): {resp.status}")
            return await self._parse_resp(resp)

    @staticmethod
    async def _parse_resp(resp) -> Optional[dict]:
        bts = await resp.read()
        txt = bts.decode('utf8').strip()
        if not txt:
            return None
        return json.loads(txt)
