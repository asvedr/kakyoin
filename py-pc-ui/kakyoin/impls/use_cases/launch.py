from dataclasses import dataclass

from kakyoin.entitites.common import ScreenName
from kakyoin.proto.common import IUseCase, IApiRepo, IScreenSwitcher


@dataclass
class LaunchUC(IUseCase):
    api: IApiRepo
    switcher: IScreenSwitcher

    async def execute(self) -> None:
        inited = await self.api.check_init()
        if not inited:
            self.switcher.set_active_screen(ScreenName.init_name_id_key)
            return
        servers = await self.api.get_servers()
        if len(servers) == 0:
            self.switcher.set_active_screen(ScreenName.init_srv)
            return
        self.switcher.set_active_screen(ScreenName.chat_list)
