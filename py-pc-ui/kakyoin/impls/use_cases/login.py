from dataclasses import dataclass
from typing import Optional

from kakyoin.entitites.common import ScreenName
from kakyoin.proto.common import IApiRepo, IScreenSwitcher, IUseCase


@dataclass
class LoginUC(IUseCase):
    api: IApiRepo
    switcher: IScreenSwitcher

    async def execute(self, name: str, id: Optional[str], key: Optional[str]) -> None:
        await self.api.init(name=name, uid=id, key=key)
        self.switcher.set_active_screen(ScreenName.init_srv)
