from dataclasses import dataclass
from typing import List

from kakyoin.entitites.common import ScreenName
from kakyoin.proto.common import IApiRepo, IScreenSwitcher, IUseCase


@dataclass
class AddFirstServersUC(IUseCase):
    api: IApiRepo
    switcher: IScreenSwitcher

    async def execute(self, servers: List[str]) -> None:
        for srv in servers:
            await self.api.add_server(srv)
        self.switcher.set_active_screen(ScreenName.chat_list)
