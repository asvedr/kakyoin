import time
from asyncio import AbstractEventLoop, sleep
from typing import Coroutine, Set, List, Type

from kakyoin.proto.common import IUniqTaskExecutor, IPeriodic


async def wrap(coro: Coroutine, locked: Set[str], key: str) -> None:
    try:
        await coro
    finally:
        locked.remove(key)


class UniqTaskExecutor(IUniqTaskExecutor):
    def __init__(
        self,
        loop: AbstractEventLoop,
        periodics: List[IPeriodic],
    ):
        self._locked = set()
        self._periodics = periodics
        self._loop = loop

    def spawn(self, coro: Coroutine, key: str = None) -> None:
        if not key:
            key = coro.__qualname__
        if key in self._locked:
            return
        self._locked.add(key)
        self._loop.create_task(wrap(coro, self._locked, key))

    def run_periodics(self) -> None:
        self._loop.create_task(self._periodic_main_loop())

    async def _periodic_main_loop(self) -> None:
        last_exec_time = {}
        while True:
            now = time.time()
            for periodic in self._periodics:
                name = periodic.__class__.__qualname__
                if now - last_exec_time.get(name, 0) < periodic.timeout():
                    continue
                self.spawn(periodic.execute())
                last_exec_time[name] = now
            await sleep(1)
