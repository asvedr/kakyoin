from typing import List

from kakyoin.entitites.msg import ShortMsg
from kakyoin.proto.common import IPeriodic, IApiRepo


MAX_ID_COUNT = 50


class NotifyNewMsgsPeriodic(IPeriodic):
    def __init__(self, api: IApiRepo):
        self._api = api
        self._viewed = set()
        self._first_launch = True

    def timeout(self) -> float:
        return 1.0

    async def execute(self) -> None:
        msgs = await self._api.get_last_msgs()
        if self._first_launch:
            self.first_launch(msgs)
        else:
            await self.not_first_launch(msgs)

    def first_launch(self, msgs: List[ShortMsg]) -> None:
        self._viewed = {msg.id for msg in msgs}
        self._first_launch = False

    async def not_first_launch(self, msgs: List[ShortMsg]) -> None:
        for msg in msgs:
            if msg.id in self._viewed:
                continue
            # TODO: show notif
            print(f"> NEW MSG: {msg.uuid}: {repr(msg.text)} IN {msg.chat}")
            self._viewed.add(msg.id)
        drop_count = max(len(self._viewed) - MAX_ID_COUNT, 0)
        for _ in range(drop_count):
            min_id = min(self._viewed)
            self._viewed.remove(min_id)
