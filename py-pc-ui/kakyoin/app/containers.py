import asyncio
from typing import List

from kakyoin.utils.di import Singleton
from kakyoin.impls.api_repo import ApiRepo
from kakyoin.impls.periodics.notify_new_msgs import NotifyNewMsgsPeriodic
from kakyoin.impls.screen_switcher import ScreenSwitcher
from kakyoin.impls.uniq_task_executor import UniqTaskExecutor
from kakyoin.impls.use_cases.add_first_servers import AddFirstServersUC
from kakyoin.impls.use_cases.launch import LaunchUC
from kakyoin.impls.use_cases.login import LoginUC
from kakyoin.proto.common import IPeriodic
from kakyoin.screens import screens


class Repos:
    api = Singleton(ApiRepo)


class Periodics:
    notify_new_msgs = Singleton(
        lambda: NotifyNewMsgsPeriodic(api=Repos.api.get())
    )

    @classmethod
    def all(cls) -> List[IPeriodic]:
        return [cls.notify_new_msgs.get()]


class Managers:
    uniq_executor = Singleton(
        lambda: UniqTaskExecutor(asyncio.get_event_loop(), Periodics.all())
    )
    screen = Singleton(
        lambda: ScreenSwitcher(Repos.api.get(), Managers.uniq_executor.get(), screens)
    )


class UseCases:
    launch = Singleton(
        lambda: LaunchUC(api=Repos.api.get(), switcher=Managers.screen.get())
    )
    login = Singleton(
        lambda: LoginUC(api=Repos.api.get(), switcher=Managers.screen.get())
    )
    add_first_srvs = Singleton(
        lambda: AddFirstServersUC(api=Repos.api.get(), switcher=Managers.screen.get())
    )
