from kivy.app import App


class KakyoinApp(App):
    def build(self):
        from kakyoin.app.containers import Managers

        return Managers.screen.get()
