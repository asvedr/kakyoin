import os.path
import os
import subprocess as sp
from typing import Optional

MAIN_DIR = os.path.dirname(os.path.dirname(__file__))
PATH = os.path.join(MAIN_DIR, "client.exe")
DB = os.path.join(MAIN_DIR, "storage.db")
PORT = 8085


class ProcManager:
    def __init__(self):
        self.proc: Optional[sp.Popen] = None

    def __enter__(self):
        os.environ['kakyoin_db_file'] = DB
        os.environ['kakyoin_api_server_port'] = str(PORT)
        self.proc = sp.Popen(PATH)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.proc.kill()
        self.proc.wait(5)
