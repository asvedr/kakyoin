from dataclasses import dataclass
from enum import IntEnum


class ScreenName(IntEnum):
    init_name_id_key = 1
    init_srv = 2
    chat_list = 3
    active_chat = 4
    settings = 5


@dataclass
class Me:
    uuid: str
    priv_key: str
    pub_key: str
    key_code: str
    name: str
    description: str
    pic: str
