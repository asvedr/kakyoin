from dataclasses import dataclass


@dataclass
class ShortMsg:
    id: int
    uuid: str
    user: str
    text: str
    chat: str
