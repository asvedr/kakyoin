from .add_first_servers import AddFirstServersScreen
from .init import InitScreen
from .chat_list import ChatListScreen
from .settings import SettingsScreen

screens = [InitScreen, AddFirstServersScreen, ChatListScreen, SettingsScreen]
