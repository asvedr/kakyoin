from typing import List

from kivy.uix.widget import Widget
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label

from kakyoin.entitites.common import ScreenName
from kakyoin.proto.common import IUniqTaskExecutor, IScreen, IApiRepo

ID_LAYOUT_LINES = "layout_lines"
ID_LABEL_ERR = "label_err"


class AddFirstServersScreen(Widget, IScreen):
    def __init__(
        self,
        api: IApiRepo,
        executor: IUniqTaskExecutor,
        **kwargs
    ):
        super().__init__(**kwargs)
        self.executor = executor

    @classmethod
    def screen_name(cls) -> ScreenName:
        return ScreenName.init_srv

    def add_line(self) -> None:
        container = self.ids[ID_LAYOUT_LINES]
        container.add_widget(TextInput(), len(container.children))

    def login(self) -> None:
        container = self.ids[ID_LAYOUT_LINES]
        servers = []
        for t_input in container.children:
            text = t_input.text.strip()
            if text:
                servers.append(text)
        if not servers:
            self.ids[ID_LABEL_ERR].text = "Set at least one server"
            return
        self.ids[ID_LABEL_ERR].text = ""
        coro = self.a_login(servers, self.ids[ID_LABEL_ERR])
        self.executor.spawn(coro)

    @staticmethod
    async def a_login(
        servers: List[str],
        err_label: Label,
    ):
        from kakyoin.app.containers import UseCases

        try:
            await UseCases.add_first_srvs.get().execute(servers)
        except Exception as err:
            err_label.text = str(err)
