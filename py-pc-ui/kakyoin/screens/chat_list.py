from kivy.uix.widget import Widget

from kakyoin.entitites.common import ScreenName
from kakyoin.proto.common import IUniqTaskExecutor, IScreen, IApiRepo


# from kakyoin.screens.file_dialog import *


class ChatListScreen(Widget, IScreen):
    def __init__(
        self,
        api: IApiRepo,
        executor: IUniqTaskExecutor,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self.executor = executor

    @classmethod
    def screen_name(cls) -> ScreenName:
        return ScreenName.settings
        # return ScreenName.chat_list
