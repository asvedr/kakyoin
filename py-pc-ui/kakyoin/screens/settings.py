from typing import Optional, List
import os

from kivy.uix.widget import Widget
from kivy.uix.popup import Popup

from kakyoin.entitites.common import ScreenName
from kakyoin.proto.common import IScreen, IUniqTaskExecutor, IApiRepo
from kakyoin.utils.pics import base64_to_image
from kakyoin.screens.file_dialog import LoadDialog

ID_CONTAINER_PICTURE = "container_picture"
ID_INPUT_UUID = "input_uuid"
ID_INPUT_PRIV_KEY = "input_priv_key"
ID_INPUT_NAME = "input_name"
ID_INPUT_DESCRIPTION = "input_description"
ID_LABEL_ERR = "label_err"


class SettingsScreen(Widget, IScreen):
    @classmethod
    def screen_name(cls) -> ScreenName:
        return ScreenName.chat_list
        # return ScreenName.settings

    def __init__(
        self,
        api: IApiRepo,
        executor: IUniqTaskExecutor,
        **kwargs,
    ):
        super().__init__(**kwargs)
        self._api = api
        self._executor = executor
        self._ready = False
        self._executor.spawn(self.load_data_task())
        self._popup: Optional[Popup] = None

    def is_ready(self) -> bool:
        return self._ready

    async def load_data_task(self) -> None:
        self._ready = False
        try:
            await self.update_data_task_inner()
        except Exception as err:
            self.ids[ID_LABEL_ERR].text = str(err)
        finally:
            self._ready = True

    async def update_data_task_inner(self) -> None:
        me = await self._api.get_me()
        self.ids[ID_INPUT_UUID].text = me.uuid
        self.ids[ID_INPUT_PRIV_KEY].text = me.priv_key
        self.ids[ID_INPUT_NAME].text = me.name
        self.ids[ID_INPUT_DESCRIPTION].text = me.description
        self.ids[ID_CONTAINER_PICTURE].clear_widgets()
        if me.pic:
            img = base64_to_image(me.pic)
            self.ids[ID_CONTAINER_PICTURE].add_widget(img)

    def update_picture(self) -> None:
        if not self._ready or self._popup is not None:
            return

        def callback(path: str, names: List[str]) -> None:
            if not names:
                return
            path = os.path.join(path, names[0])
            self._executor.spawn(self.load_pic_task(path))
            self.dismiss_popup()

        content = LoadDialog(load=callback, cancel=self.dismiss_popup)
        self._popup = Popup(title="Load file", content=content,
                            size_hint=(0.9, 0.9))
        self._popup.open()

    def dismiss_popup(self):
        if self._popup:
            self._popup.dismiss()
            self._popup = None

    def regenerate_keys(self) -> None:
        if not self._ready:
            return
        self._executor.spawn(self.regen_keys_task())

    def update_data(self) -> None:
        if not self._ready:
            return
        self._executor.spawn(self.patch_me_task())

    async def regen_keys_task(self) -> None:
        await self._api.regenerate()
        await self.load_data_task()

    async def patch_me_task(self) -> None:
        await self._api.patch_me(
            uuid=self.ids[ID_INPUT_UUID].text,
            key=self.ids[ID_INPUT_PRIV_KEY].text,
            name=self.ids[ID_INPUT_NAME].text,
            description=self.ids[ID_INPUT_DESCRIPTION].text,
        )
        await self.load_data_task()

    async def load_pic_task(self, path: str) -> None:
        print('MAKE PATCH')
        await self._api.patch_me(pic=path)
        print("LOAD TASK")
        await self.load_data_task()
        print("LOADED")
