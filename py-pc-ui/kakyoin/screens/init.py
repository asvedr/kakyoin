from typing import Optional

from kivy.uix.widget import Widget
from kivy.uix.label import Label

from kakyoin.entitites.common import ScreenName
from kakyoin.proto.common import IUniqTaskExecutor, IScreen, IApiRepo

ID_INPUT_NAME = "input_name"
ID_INPUT_ID = "input_id"
ID_INPUT_KEY = "input_key"
ID_LABEL_ERR = "label_err"


class InitScreen(Widget, IScreen):
    def __init__(
        self,
        api: IApiRepo,
        executor: IUniqTaskExecutor,
        **kwargs
    ):
        super().__init__(**kwargs)
        self.executor = executor

    @classmethod
    def screen_name(cls) -> ScreenName:
        return ScreenName.init_name_id_key

    def login(self) -> None:
        name = self.ids[ID_INPUT_NAME].text.strip()
        id = self.ids[ID_INPUT_ID].text.strip()
        key = self.ids[ID_INPUT_KEY].text.strip()
        print(f'do login with {name} {id} {key}')
        if not name:
            self.ids[ID_LABEL_ERR].text = "You must set name"
            return
        self.ids[ID_LABEL_ERR].text = ""
        self.executor.spawn(self.a_login(name, id, key, self.ids[ID_LABEL_ERR]))

    @staticmethod
    async def a_login(
        name: str,
        id: Optional[str],
        key: Optional[str],
        err_label: Label,
    ):
        from kakyoin.app.containers import UseCases

        try:
            await UseCases.login.get().execute(name, id, key)
        except Exception as err:
            err_label.text = str(err)
