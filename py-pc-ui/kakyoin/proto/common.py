from abc import ABC, abstractmethod
from typing import Optional, Coroutine, Dict, List

from kakyoin.entitites.common import ScreenName, Me
from kakyoin.entitites.msg import ShortMsg


class IApiRepo(ABC):
    @abstractmethod
    async def check_init(self) -> bool:
        ...

    @abstractmethod
    async def init(self, name: str, uid: Optional[str], key: Optional[str]) -> None:
        ...

    @abstractmethod
    async def get_servers(self) -> Dict[str, str]:
        ...

    @abstractmethod
    async def add_server(self, data: str) -> None:
        ...

    @abstractmethod
    async def get_me(self) -> Me:
        ...

    @abstractmethod
    async def patch_me(
        self,
        uuid: str = None,
        key: str = None,
        name: str = None,
        description: str = None,
        pic: str = None,
    ) -> None:
        ...

    @abstractmethod
    async def regenerate(self, uuid: bool = False, key: bool = False) -> None:
        ...

    @abstractmethod
    async def get_last_msgs(self) -> List[ShortMsg]:
        ...


class IScreenSwitcher:
    def get_active_screen(self) -> Optional[ScreenName]:
        raise NotImplementedError()

    def set_active_screen(self, screen: ScreenName) -> None:
        raise NotImplementedError()


class IScreen:
    @classmethod
    def screen_name(cls) -> ScreenName:
        raise NotImplementedError()

    def is_ready(self) -> bool:
        return True


class IPeriodic(ABC):
    @abstractmethod
    def timeout(self) -> float:
        ...

    @abstractmethod
    async def execute(self) -> None:
        ...


class IUniqTaskExecutor(ABC):
    @abstractmethod
    def spawn(self, coro: Coroutine, key: str = None) -> None:
        ...

    @abstractmethod
    def run_periodics(self) -> None:
        ...


class IUseCase(ABC):
    async def execute(self, *args, **kwargs) -> None:
        ...
