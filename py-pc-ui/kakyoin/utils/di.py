from typing import TypeVar, Generic, Callable

T = TypeVar('T')


class Singleton(Generic[T]):
    def __init__(self, initializer: Callable[[], T]):
        self._initializer = initializer
        self._result = None

    def get(self) -> T:
        if self._initializer is not None:
            self._result = self._initializer()
            self._initializer = None
        return self._result
