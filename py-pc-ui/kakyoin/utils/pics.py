import base64
import tempfile
from kivy.uix.image import Image


IMAGE_EXT = "webp"


def base64_to_image(image_str: str) -> Image:
    image_bytes = base64.decodebytes(image_str.encode())
    with tempfile.NamedTemporaryFile(mode='wb') as tfile:
        tfile.write(image_bytes)
        return Image(source=tfile.name)
