import asyncio
import time

from kivy.app import Builder

from kakyoin.app.app import KakyoinApp
from kakyoin.app.containers import UseCases
from kakyoin.proc_manager import ProcManager

Builder.load_file("./kvs/main.kv")
app = KakyoinApp()
with ProcManager():
    time.sleep(1)
    loop = asyncio.get_event_loop()
    loop.create_task(UseCases.launch.get().execute())
    loop.run_until_complete(app.async_run(async_lib='asyncio'))

