![alt text](./logo.png)
# !Link to documentation!
[press here](./docs/index.md)

# What is it?
Kakyoin is a decentralized encrypted messenger.

# Main features:
- All messages are signed and contacts includes public key to protect you from MITM attack.
- All messages are entrypted e2e to protect you from stealing data even if the attacker got one of servers under control.
- Clients can communicate through multiple unrelated servers to make connections more reliable.
- Server keep all the data in RAM to make process of stealing data from server even more difficult.
- File sharing.
- Group chats.

# How to run server
1. Build server from kakyoin-server dir.
```sh
cd kakyoin-server
cargo build --release
cp target/release/kakyoin-server .
```
2. Generate server priv key.
```sh
./kakyoin-server gen-key path-where-to-write
```
3. Get server contact.
```sh
kakyoin_priv_key_path=path-to-key ./kakyoin-server get-contact
```
4. Share contact to clients.
5. Run server.
```
kakyoin_priv_key_path=path-to-key ./kakyoin-server run
```

# How to run client
1. Build client from kakyoin-client dir.
```sh
cd kakyoin-client
cargo build --release
cp target/release/kakyoin-client .
```
2. Run client
```sh
./kakyoin-client
```
3. Check web interface on localhost (default port is 8080)

---

Check more details in ./kakyoin-server/README.md and ./kakyoin-client/README.md
Check more workflow schemas in ./docs/
