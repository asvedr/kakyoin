# Create and invite
### Api message
`ApiMsgBody::AddToChat {chat_id, chat_name, other_participants}`
Is used to create a new chat or invite new members 
### Group chat creation
![alt text](./assets/Create_chat.svg)
### Invite new member to chat
![alt text](./assets/Invite_to_chat.svg)
# Messages in chat
When Alice want to send a message in chat with Bob and Alex she just two copies of the message. One to Bob and one to alex.
Every message has chat_id in header. If chat_id is null then it's a private message not a group one.
### Send group chat message schema
![alt text](./assets/Group_message.svg)
