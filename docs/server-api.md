# Endpoints
Kakyoin server has only one http endpoint it's:

`POST /`

# Body format
Request body is binary encoded and encrypted struct `ServerCall` from [this file](../kakyoin-base/src/entities/server_call.rs)

Response body is binary encoded and encrypted struct `ServerResponse`  from [this file](../kakyoin-base/src/entities/server_call.rs)

### Reading request
1. decrypt via server's RSA private key
2. parse bytes into ServerCall via `serdebin::from_bytes`
### Write response
1. serialize ServerResponse via `serdebin::to_bytes`
2. encrypt via user's public RSA key

# Authentication
Most of the server handlers requires `token` for the authentication.
Client calls `ServerCall::GetToken` to get the token.
Server encrypts token with client's public key from request.
**Client do not need any kind of registration before request the token**

# ServerCalls
1. Get token for other actions:

  `request:`
  ```
  ServerCall::GetToken {
    user_id: Uuid,
    pub_key: RsaPublicKey,
  }
  ```
  `response:`
  ```
  ServerResponse::Token {
    token: Vec<u8>,
    expired_at: u64,
  }
  ```
2. Send messages from user to somebody:

  `request:`
  ```
  ServerCall::SendMessages {
    token: Vec<u8>,
    msgs: Vec<ApiMsgPacked>,
  }
  ```
  `response:`
  ```
  ServerResponse::Statuses {
    map: HashMap<Uuid, ApiMsgStatus>,
  }
  ```
3. Get new messages for user:

  `request:`
  ```
  ServerCall::FetchMessages {token: Vec<u8>}
  ```
  `response:`
  ```
  ServerResponse::Messages {
    msgs: Vec<ApiMsgPacked>,
  }
  ```
4. Get statuses of messages(send, not send, not found):

  `request:`
  ```
  ServerCall::GetStatuses {
    token: Vec<u8>,
    ids: Vec<Uuid>,
  }
  ```
  `response:`
  ```
  ServerResponse::Statuses {
    map: HashMap<Uuid, ApiMsgStatus>,
  }
  ```
5. Move messages for me from New to Received:

  `request:`
  ```
  ServerCall::MarkReceived {
    token: Vec<u8>,
    msgs: Vec<Uuid>,
  }
  ```
  `response:`
  ```
  ServerResponse::Nothing
  ```
6. Get hashes of user meta for many users:

  `request:`
  ```
  ServerCall::ReqMetaHash {
    token: Vec<u8>,
    users: Vec<(Uuid, String)>,
  }
  ```
  `response:`
  ```
  ServerResponse::MetaHashes {
    users: Vec<((Uuid, String), String)>
  }
  ```
7. Request full user meta:

  `request:`
  ```
  ServerCall::ReqMeta {
    token: Vec<u8>,
    user: Uuid,
    key_code: String,
  }
  ```
  `response:`
  ```
  ServerResponse::UserMeta {
    meta: Box<ApiUserMeta>,
  }
  ```
8. Send users meta to server:

  `request:`
  ```
  PostMyMeta {
    token: Vec<u8>,
    meta: Box<ApiUserMeta>,
  }
  ```
  `response:`
  ```
  ServerResponse::Nothing
  ```
