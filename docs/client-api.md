# Methods
## Frontend
GET /
## API
### Initialization
- GET /api/check-init/

  `Authorization header: off`

  `response:`
  ```json
  {"status": str}
  ```

- POST /api/init/

  `Authorization header: on`

  `request:`
  ```json
  {
    "uuid": opt[str],
    "priv_key": opt[str],
    "password": opt[str],
    "name": str,
    "description": str,
  }
  ```
- POST /api/login/

  `Authorization header: off`

  `request:`
  ```
  {"password": str}
  ```
  `response:`
  ```
  {"token": str}
  ```
### Settings
- GET /api/settings/me/

  `Authorization header: on`

  `response:`
  ```json
  {
    "uuid": uuid,
    "priv_key": str,
    "pub_key": str,
    "key_code": str,
    "name": str,
    "description": str,
    "pic": str,
  }
  ```
- PATCH /api/settings/me/

  `Authorization header: on`

  `request:`
  ```json
  {
    "uuid": opt[uuid],
    "priv_key": opt[str],
    "name": opt[str],
    "description": opt[str],
    "pic_path": opt[str],
  }
  ```
- POST /api/settings/regenerate/?[uuid=true][&priv_key=true]

  `Authorization header: on`

  Response just status=200.
- GET /api/settings/contact/

  `Authorization header: on`

  `response:`
  ```json
  {"data": "<my-contact>"}
  ```
### Servers
- GET /api/servers/

  `Authorization header: on`

  `response:`
  ```json
  {
    "data": [
      {
        "addr": str,
        "key_code": str,
        "status": str,
      }
    ]
  }
  ```
- POST /api/servers/

  `Authorization header: on`
  
  `request:`
  ```json
  {"data": base64}
  ```
- DELETE /api/servers/?addr='str'

  `Authorization header: on`
### Chats
- GET /api/chats/

  `Authorization header: on`

  `response:`
  ```json
  {
    "data": [
      {
        "id": str,
        "name": str,
        "last_msg": str,
        "new": bool,
      }
    ]
  }
  ```
- GET /api/chats/info/?id=str

  `Authorization header: on`

  `response:`
  ```json
  {
    "id": str,
    "name": str,
    "participants": [UserShort, ...]
  }
  ```
- POST /api/chats/

  `Authorization header: on`

  `description:` create new chat OR modify chat

  `request:`
  ```json
  {
    "chat_id": opt[str],
    "users": [uuid, ...],
    "name": str,
  }
  ```
  `response:`
  ```json
  {"id": str}
  ```
- POST /api/chats/moderate/?id=str&action=Action

  `Authorization header: on`

  `Action = ban|del`
### Users
- GET /api/users/list/

  `Authorization header: on`

  `response:`
  ```json
  {"data": [UserShort]}
  ```
- GET /api/users/detail/?id=uuid

  `Authorization header: on`

  `response:`
  ```json
  {
    "uuid": uuid,
    "name": str,
    "description": str,
    "pic": str,
    "key": str,
    "key_code": str,
    "status": str,
  }
  ```
- POST /api/users/connect/

  `Authorization header: on`

  `request:`
  ```json
  {"contact": str}
  ```
- POST /api/users/moderate/?id=uuid&action=Action

  `Authorization header: on`

  `Action = ban|del|trust`
### Messages
- GET /api/msgs/last-unread/

  `Authorization header: on`

  `response:`
  ```json
  {
    "data": [
      {
        "id": int,
        "uuid": uuid,
        "user": uuid,
        "text": str,
        "chat": str,
      }
    ]
  }
  ```
- GET /api/chats/msgs/?chat=str&[last_msg=int]

  `Authorization header: on`

  `response:`
  ```json
  {
    "data": [
      {
        "id": int,
        "uuid": uuid,
        "user": opt[uuid],
        "text": str,
        "file": opt[DecFile],
        "read": bool,
        "sent": bool,
        "received": bool,
      }
    ]
  }
  ```
- POST /api/msgs/text/

  `Authorization header: on`

  `request:
  ```json
  {"chat": str, "text": str}
  ```
- POST /api/msg/file/

  `Authorization header: on`

  `request:`
  ```json
  {"chat": str, "path": option[str], "data": option[str], "name": str}
  ```
  path - local path to file(optional)
  data - base64 encoded bytes of file(optional)
  WARNING: path OR data must be set. If both parameters set together there will be error

- POST /api/msgs/mark-read/

  `Authorization header: on`

  `request:`
  ```json
  {"msgs": list[uuid]}
  ```
### Files
- POST /api/files/download/

  `Authorization header: on`

  `request:`
  ```json
  {"chat": str, "msg_id": uuid, "path": str}
  ```
  `response:`
  ```json
  {"task_id": int}
  ```
- GET /api/files/get-status/?task_id=int&user_id=uuid&file_id=uuid

  `Authorization header: on`

  `response:`
  ```json
  {"progress": float, "completed": bool, "failed":  bool}
  ```

### Tasks
- POST /api/tasks/trigger-periodic/?name=str

  `Authorization header: on`

# Models
- UserShort
  ```json
  {
    "uuid": str,
    "name": str,
    "pic": str,
    "key_code": str,
  }
  ```
- DecFile
  ```json
  {
    "name": str,
    "size": int,
    "user_id": opt[uuid],
    "file_id": uuid,
  }
  ```

