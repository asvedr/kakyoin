# What is user meta?
- name
- description
- picture

**IMPORTANT!** User info can be requested from server with ServerCall::ReqMeta by anyone.
User info **is not designed** for containing secure information.

# How does it saved on server?
On the server it stores in RAM. Code of containers is [here](../kakyoin-server/src/impls/local/user_table.rs). Structure of [User](../kakyoin-server/src/entities/state.rs).
It's formed on user side and server only stores what is received from clients.
Client periodic task [p_upd_meta](../kakyoin-client/src/impls/periodics/update_meta.rs) upload local meta to a server and fetch other meta from a server

# How does it saved on client?
### My meta:
Table `settings` column: `value`, keys: `"name", "descr", "photo"`
### Other users meta:
Table `users` columns: `"name", "description", "pic"`

# How is it formed?
User upload picture on local server, set name and description manually. Client code also calculate _hash_ for local meta.
Client download meta with _hash_ of other clients from server.

# User picture format
Client api `PATCH /api/settings/me` accept path to picture.
Client app load picture, resize it to 200x200 and convert to WebP format.
Picture is stored in local db as base64 string. Base64 is also used in client api responses with pictures.

# Update meta workflow
![alt text](./assets/Update_meta_wf.svg)
