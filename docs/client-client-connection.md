# What is client connect link:
`base64(uuid + public key)`

# User(friend) status in client base:
`New = 0` User's public key will not be used for msg validation on ID collision

`Trusted = 1` User's public key will be used for msg validation on ID collision

`Banned = 2` All messages from this user will be ignored

# How can client get connection to another client
1. If client generates connect link and share it anyhow to another client
  ![alt text](./assets/Connect_via_link.svg)
2. If client got message from another client(another client already got connect to us)
  ![alt text](./assets/Connect_via_msg.svg)
3. If client got invitation to chat with unknown client
  ![alt_text](./assets/Connect_via_invite.svg)
