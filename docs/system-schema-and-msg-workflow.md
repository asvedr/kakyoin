# Schema of all system components
![alt text](./assets/SystemSchema.svg)

# What do server and client do?
Server:
- The server keep clients meta(name, photo, description).
- The server work as ApiMsg queues between clients.

Every system message header include `sender uuid`, `receiver uuid` and `receiver key code`
[Check ApiMsgPacked](./../kakyoin-base/src/entities/api_msg.rs) for details.

Client:
- The client create and manage chats.
- The client encrypt and decrypt ApiMsg. 
- The client host files.
- The client store history. 
- The client detect if ApiMsg was delivered and sending not required any more.

# What is ApiMsg (api message)
- Text message `receiver - client`
- File declaration `receiver - client`
- Shared file part `receiver - client`
- Request for shared file part `receiver - client`
- Invite to group chat `receiver - client`
- Upload client meta `receiver - server`
- Download client meta `receiver - server`
- Download hash of client meta `receiver - server`

# Workflow of message moving (between client and server)
![alt text](./assets/client-send-msg-workflow.svg)
# Workflow of message moving (between client and client)
![alt text](./assets/send-msg-workflow.svg)
# How does file sharing work
![alt text](./assets/client-send-file-wf.svg)
