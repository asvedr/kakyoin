use std::io;

use hyper::Body;
use hyper::{Method, Request};
use kakyoin_base::entities::common::DynFutRes;
use mddd::macros::auto_impl;
use serde::{Deserialize, Serialize};

use crate::entities::errors::ApiError;

pub trait IAsyncScript<Req = (), Resp = (), Err = ()> {
    fn execute(&self, req: Req) -> DynFutRes<Resp, Err>;
}

pub trait IApiHandler: Send + Sync {
    type Response: Serialize + Send + Sync;
    type Body: for<'a> Deserialize<'a> + Send + Sync;
    type Query: for<'a> Deserialize<'a> + Send + Sync;
    fn override_query() -> Option<Self::Query> {
        None
    }
    fn query_fields_force_str() -> &'static [&'static str] {
        &[]
    }
    fn override_body() -> Option<Self::Body> {
        None
    }
    fn auth_token_required() -> bool {
        true
    }
    fn path(&self) -> &str;
    fn method(&self) -> Method;
    fn handle(&self, query: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError>;
}

#[auto_impl(link, dyn)]
pub trait IApiHandlerWrapped: Send + Sync {
    fn path(&self) -> &str;
    fn method(&self) -> Method;
    fn handle(&self, request: Request<Body>) -> DynFutRes<Vec<u8>, ApiError>;
}

pub trait IService: Send + Sync {
    fn run(&mut self) -> DynFutRes<(), io::Error>;
}
