use std::collections::HashMap;
use std::sync::Arc;

use kakyoin_base::entities::api_msg::{ApiMsg, ApiMsgBodySendFilePart, ApiMsgPacked, ApiMsgStatus};
use kakyoin_base::entities::common::{DynFut, DynFutRes};
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::entities::server_call::ApiUserMeta;
use mddd::macros::auto_impl;
use rsa::RsaPublicKey;
use uuid::Uuid;

use crate::entities::common::{Id, Me, ServerStatus, VecBox};
use crate::entities::errors::{
    FileDownloaderError, FileUploaderError, MsgDispatcherError, MsgFetcherError, MsgSenderError,
    ServerError,
};
use crate::entities::files::DownloadStatus;
use crate::entities::message::LocalMsg;

#[auto_impl(link, dyn)]
pub trait IServerClient: Send + Sync {
    fn addr(&self) -> &str;
    fn send_msgs(
        &self,
        msgs: Vec<ApiMsgPacked>,
    ) -> DynFutRes<HashMap<Uuid, ApiMsgStatus>, ServerError>;
    fn fetch_msgs(&self) -> DynFutRes<Vec<ApiMsgPacked>, ServerError>;
    fn get_statuses(
        &self,
        id_list: Vec<Uuid>,
    ) -> DynFutRes<HashMap<Uuid, ApiMsgStatus>, ServerError>;
    fn mark_received(&self, id_list: Vec<Uuid>) -> DynFutRes<(), ServerError>;
    fn request_meta(
        &self,
        id: Uuid,
        key: &RsaPublicKey,
    ) -> DynFutRes<Box<ApiUserMeta>, ServerError>;
    fn request_meta_hash(
        &self,
        id_list: &[(Uuid, &RsaPublicKey)],
    ) -> DynFutRes<HashMap<Uuid, String>, ServerError>;
    fn post_my_meta(&self, meta: Arc<ApiUserMeta>) -> DynFutRes<(), ServerError>;
    // use "get_token" api
    fn ping(&self) -> DynFutRes<(), ServerError>;
}

#[auto_impl(link, dyn)]
pub trait IServerClientFactory: Send + Sync {
    fn produce(
        &self,
        me: Arc<Me>,
        contact: &ServerContact,
    ) -> Result<Box<dyn IServerClient>, ServerError>;
}

#[auto_impl(link, dyn)]
pub trait IMsgSender: Send + Sync {
    fn send_msgs(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: VecBox<LocalMsg>,
    ) -> DynFutRes<(), MsgSenderError>;
}

#[auto_impl(link, dyn)]
pub trait IMsgFetcher: Send + Sync {
    fn fetch_msgs(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
    ) -> DynFutRes<Vec<ApiMsg>, MsgFetcherError>;
    fn report_received(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: Vec<Uuid>,
    ) -> DynFutRes<(), MsgFetcherError>;
}

#[auto_impl(link, dyn)]
pub trait IMsgDispatcher: Send + Sync {
    fn dispatch_msgs(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: Vec<ApiMsg>,
    ) -> DynFutRes<(), MsgDispatcherError>;
}

#[auto_impl(link, dyn)]
pub trait IFileDownloader: Send + Sync {
    fn start_loading(
        &self,
        user_id: Id,
        file_id: Id,
        max_parts: u64,
        path_to_save: String,
    ) -> DynFutRes<(), FileDownloaderError>;
    fn retry_block_request(&self, user_id: Id, file_id: Id) -> DynFutRes<(), FileDownloaderError>;
    fn get_loading_status(&self, user_id: Id, file_id: Id) -> DynFut<Option<DownloadStatus>>;
    fn add_block(
        &self,
        user_id: Id,
        data: Box<ApiMsgBodySendFilePart>,
    ) -> DynFutRes<(), FileDownloaderError>;
}

#[auto_impl(link, dyn)]
pub trait IFileUploader: Send + Sync {
    fn upload_file_part(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        user_id: Id,
        user_key: String,
        file_id: Id,
        part: u64,
    ) -> DynFutRes<(), FileUploaderError>;
}

#[auto_impl(link, dyn)]
pub trait IServerStatusStorage: Send + Sync {
    fn set_statuses(&self, updates: Vec<(String, ServerStatus)>) -> DynFut<()>;
    fn get_statuses(&self) -> DynFut<HashMap<String, ServerStatus>>;
}
