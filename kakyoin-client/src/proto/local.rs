use std::collections::{HashMap, HashSet};
use std::path::Path;
use std::sync::Arc;

use atex::{AtexError, NewTask, TaskId, TaskStatus};
use kakyoin_base::entities::common::{DynFut, DynFutRes};
use kakyoin_base::entities::contact::{ClientContact, ServerContact};
use kakyoin_base::entities::server_call::ApiUserMeta;
use mddd::macros::auto_impl;
use rsa::{RsaPrivateKey, RsaPublicKey};

use crate::entities::chat::{Chat, ChatId, ChatListItem, ChatShort};
use crate::entities::common::{Id, InitStatus, Me, VecBox};
use crate::entities::errors::{DbError, DbResult, InitializerError, PicLoaderError};
use crate::entities::files::LocalFileMeta;
use crate::entities::message::{LocalMsg, NewLocalMsg, NewMsgToSend};
use crate::entities::user::{User, UserStatus};

#[auto_impl(link, dyn)]
pub trait IInitializer: Send + Sync {
    fn check_init(&self) -> DynFutRes<(), InitializerError>;
    fn init(
        &self,
        id: Option<Id>,
        priv_key: Option<RsaPrivateKey>,
        name: String,
        description: String,
        password: Option<String>,
    ) -> DynFutRes<(), InitializerError>;
}

#[auto_impl(link, dyn)]
pub trait IStorage: Send + Sync {
    fn init(&self) -> DynFutRes<(), DbError> {
        unimplemented!()
    }
    fn lock(&self) -> DynFut<Box<dyn ISyncStorage>> {
        unimplemented!()
    }
}

#[auto_impl(link, dyn)]
pub trait ISyncStorage: Send + Sync {
    fn settings(&self) -> Box<dyn ISettingsRepo> {
        unimplemented!()
    }
    fn servers(&self) -> Box<dyn IServersRepo> {
        unimplemented!()
    }
    fn users(&self) -> Box<dyn IUsersRepo> {
        unimplemented!()
    }
    fn chats(&self) -> Box<dyn IChatRepo> {
        unimplemented!()
    }
    fn messages(&self) -> Box<dyn ILocalMsgRepo> {
        unimplemented!()
    }
    fn shared_files(&self) -> Box<dyn ISharedFilesRepo> {
        unimplemented!()
    }
    fn clear_cache(&self, _prefix: Option<&str>) {
        unimplemented!()
    }
}

pub trait ISettingsRepo {
    fn get_me(&self) -> DbResult<Arc<Me>>;
    fn set_me(&self, _me: &Me) -> DbResult<()>;
    fn set_name(&self, val: &str) -> DbResult<()>;
    fn set_description(&self, val: &str) -> DbResult<()>;
    fn set_photo(&self, val: &str) -> DbResult<()>;
    fn get_meta(&self) -> DbResult<Arc<ApiUserMeta>>;
    fn check_inited(&self) -> DbResult<InitStatus>;
    fn set_pwd_hash(&self, hash: Option<String>) -> DbResult<()>;
    fn get_pwd_hash(&self) -> DbResult<Option<String>>;
}

pub trait IServersRepo {
    fn add_servers(&self, contacts: Vec<ServerContact>) -> DbResult<()>;
    fn del_servers(&self, addrs: &[&str]) -> DbResult<()>;
    fn get_all(&self) -> DbResult<Arc<Vec<ServerContact>>>;
}

pub trait IUsersRepo {
    fn contact(&self, contact: &ClientContact, status: UserStatus) -> DbResult<()>;
    fn get_info(&self, uids: &[Id]) -> DbResult<HashMap<Id, Arc<ApiUserMeta>>>;
    fn set_info(&self, uid: &Id, info: &ApiUserMeta) -> DbResult<()>;
    fn get_status(&self, uids: &[Id]) -> DbResult<HashMap<Id, UserStatus>>;
    fn set_status(&self, uid: &Id, status: UserStatus) -> DbResult<()>;
    fn get_keys(&self, uids: &[Id]) -> DbResult<HashMap<Id, Arc<RsaPublicKey>>>;
    fn get_by_uid(&self, uid: &[Id]) -> DbResult<VecBox<User>>;
    fn get_by_name(&self, name: &str, limit: usize) -> DbResult<VecBox<User>>;
    fn delete_user(&self, _uid: &Id) -> DbResult<()>;
    fn get_hashes(&self) -> DbResult<Vec<(Id, Arc<RsaPublicKey>, String)>>;
    fn get_all_ids(&self) -> DbResult<Vec<Id>>;
}

pub trait IChatRepo {
    fn create_chat(&self, chat: Chat) -> DbResult<()>;
    fn get_chat(&self, id: &ChatId) -> DbResult<Chat>;
    fn set_name(&self, id: &ChatId, name: &str) -> DbResult<()>;
    fn add_participants(&self, chat_id: &ChatId, user_id_list: &[Id]) -> DbResult<()>;
    fn get_participants(&self, id: &ChatId) -> DbResult<Vec<Id>>;
    fn get_short_info_for_all(&self) -> DbResult<Vec<ChatShort>>;
    fn get_full_list(&self) -> DbResult<Vec<ChatListItem>>;
    fn ban_chat(&self, id: &ChatId) -> DbResult<()>;
    fn delete_chat(&self, id: &ChatId) -> DbResult<()>;
}

pub trait ILocalMsgRepo {
    fn create_messages(&self, msg: &[NewLocalMsg]) -> DbResult<()>;
    fn get_already_in_base(&self, id_list: &[Id]) -> DbResult<HashSet<Id>>;
    fn get_by_uid(&self, id: &Id) -> DbResult<Box<LocalMsg>>;
    fn get_chat_messages(
        &self,
        chat_id: &ChatId,
        last_id: Option<u64>,
        limit: usize,
    ) -> DbResult<VecBox<LocalMsg>>;
    fn get_last_unread_messages(&self, limit: usize) -> DbResult<VecBox<LocalMsg>>;
    fn get_messages_to_send(&self) -> DbResult<VecBox<LocalMsg>>;
    fn mark_as_read(&self, id_list: &[Id]) -> DbResult<()>;
    fn mark_as_sent(&self, id_list: &[Id]) -> DbResult<()>;
    fn mark_as_received(&self, id_list: &[Id]) -> DbResult<()>;
    fn delete_messages(&self, id_list: &[Id]) -> DbResult<()>;
    fn next_local_uniq_id(&self) -> DbResult<i64>;
    fn get_first_id_of_same_code(&self, id: u64) -> DbResult<u64>;
}

pub trait ISharedFilesRepo {
    fn register_file(&self, meta: &LocalFileMeta, users: &[Id]) -> DbResult<()>;
    fn get_file_for_user(&self, file_id: &Id, user_id: &Id) -> DbResult<LocalFileMeta>;
    fn delete_file_by_id(&self, id: &Id) -> DbResult<()>;
}

#[auto_impl(link, dyn)]
pub trait IUserMetaHashCalculator: Send + Sync {
    fn calculate(&self, meta: &ApiUserMeta) -> String;
}

#[auto_impl(link, dyn)]
pub trait ITaskScheduler: Send + Sync {
    fn schedule_task(&self, task: NewTask) -> DynFutRes<TaskId, AtexError>;
    fn schedule_periodic(&self, name: &str) -> DynFutRes<(), AtexError>;
    fn get_task_status(&self, task_id: TaskId) -> DynFutRes<TaskStatus, AtexError>;
    fn download_file(
        &self,
        user_id: Id,
        file_id: Id,
        max_parts: u64,
        path_to_save: &str,
    ) -> DynFutRes<TaskId, AtexError>;
}

#[auto_impl(link, dyn)]
pub trait IMsgMaker: Send + Sync {
    fn make(&self, msg: NewMsgToSend) -> DynFutRes<(), DbError>;
}

#[auto_impl(link, dyn)]
pub trait IPicLoader: Send + Sync {
    fn load_pic(&self, path: &Path) -> Result<String, PicLoaderError>;
    fn dump_to_file(&self, pic: &str, path: &Path) -> Result<(), PicLoaderError>;
}

#[auto_impl(link, dyn)]
pub trait IAuthTokenManager: Send + Sync {
    fn set_active(&self, value: bool) -> DynFut<()>;
    fn is_auth_turned_on(&self) -> DynFut<bool>;
    fn gen_token(&self) -> DynFut<String>;
    fn validate_token(&self, token: &str) -> DynFut<bool>;
}
