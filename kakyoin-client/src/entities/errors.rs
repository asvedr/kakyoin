use kakyoin_base::entities::errors::{CryptoErr, NetError, SerializerError};
use kakyoin_base::entities::net::HttpResp;
use mddd::macros::EnumAutoFrom;
use serde_derive::Deserialize;
use serde_json::from_str;

#[derive(Clone, Debug, PartialEq)]
pub enum DbError {
    NotFound,
    Internal(String),
    Constraint,
    Conflict,
}

pub type DbResult<T> = Result<T, DbError>;

impl From<rusqlite::Error> for DbError {
    fn from(value: rusqlite::Error) -> Self {
        match value {
            rusqlite::Error::SqliteFailure(err, _)
                if err.code == rusqlite::ErrorCode::ConstraintViolation =>
            {
                Self::Constraint
            }
            rusqlite::Error::QueryReturnedNoRows => Self::NotFound,
            other => Self::Internal(format!("{:?}", other)),
        }
    }
}

#[derive(Clone, Debug, EnumAutoFrom, PartialEq)]
pub enum ServerError {
    InvalidContact,
    ResponseNot200(u16),
    UnexpectedResponse,
    InvalidToken,
    #[accept(serdebin::SerializerError)]
    SerializerFailed,
    OtherCode(String),
    CryptoErr(CryptoErr),
    NetError(NetError),
}

#[derive(Deserialize)]
struct ServerErrData {
    code: String,
    #[allow(dead_code)]
    details: Option<String>,
}

#[derive(Debug, EnumAutoFrom)]
pub enum MsgSenderError {
    Server(ServerError),
    Db(DbError),
}

#[derive(Debug)]
pub enum MsgFetcherError {
    CanNotProduceClient(ServerError),
}

#[derive(Debug, EnumAutoFrom)]
pub enum MsgDispatcherError {
    Db(DbError),
    Ser(SerializerError),
    Download(FileDownloaderError),
    Upload(FileUploaderError),
}

#[derive(Debug, EnumAutoFrom)]
pub enum FileDownloaderError {
    UserNotFound,
    FileNotFound,
    CanNotWriteFile,
    Db(DbError),
    Srv(ServerError),
}

#[derive(Debug, EnumAutoFrom)]
pub enum PicLoaderError {
    InvalidFormat,
    CanNotRead,
    CanNotWrite,
    CanNotEncode,
    InvalidBase64,
}

impl PartialEq for FileDownloaderError {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", self).eq(&format!("{:?}", other))
    }
}

#[derive(Clone, Debug, EnumAutoFrom)]
pub enum FileUploaderError {
    FileForUserNotFound,
    InvalidKey,
    Db(DbError),
    Srv(ServerError),
    #[accept(std::io::Error)]
    #[method(|x|format!("{:?}", x))]
    CanNotReadFilePart(String),
}

impl PartialEq for FileUploaderError {
    fn eq(&self, other: &Self) -> bool {
        format!("{:?}", self).eq(&format!("{:?}", other))
    }
}

impl ServerError {
    pub fn parse_resp(resp: HttpResp) -> Self {
        let code = resp.code;
        let text = match String::from_utf8(resp.body) {
            Ok(val) => val,
            Err(_) => return Self::ResponseNot200(code),
        };
        let data: ServerErrData = match from_str(&text) {
            Ok(val) => val,
            Err(_) => return Self::ResponseNot200(code),
        };
        if data.code == "INVALID_TOKEN" {
            Self::InvalidToken
        } else {
            Self::OtherCode(data.code)
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct ApiError {
    pub code: &'static str,
    pub status: u16,
    pub details: Option<String>,
}

#[derive(Debug, EnumAutoFrom)]
pub enum InitializerError {
    #[accept(DbError)]
    DbIsInvalid,
    NotInited,
    #[accept(CryptoErr)]
    CanNotGenKey,
}

impl ApiError {
    pub fn internal<T: std::fmt::Debug>(details: T) -> Self {
        Self {
            code: "INTERNAL",
            status: 500,
            details: Some(format!("{:?}", details)),
        }
    }

    pub fn not_authorized() -> Self {
        Self {
            code: "NOT_AUTHORIZED",
            status: 403,
            details: None,
        }
    }

    pub fn invalid_creds() -> Self {
        Self {
            code: "INVALID_CREDS",
            status: 403,
            details: None,
        }
    }

    pub fn invalid_query() -> Self {
        Self {
            code: "INVALID_QUERY",
            status: 400,
            details: None,
        }
    }

    pub fn invalid_request_body() -> Self {
        Self {
            code: "INVALID_REQUEST_BODY",
            status: 400,
            details: None,
        }
    }

    pub fn invalid_file_path() -> Self {
        Self {
            code: "INVALID_FILE_PATH",
            status: 400,
            details: None,
        }
    }

    pub fn file_path_or_data_required() -> Self {
        Self {
            code: "PATH_OR_DATA_REQUIRED",
            status: 400,
            details: None,
        }
    }

    pub fn can_not_decode_base64() -> Self {
        Self {
            code: "CAN_NOT_DECODE_BASE64",
            status: 400,
            details: None,
        }
    }

    pub fn invalid_pic<T: std::fmt::Debug>(details: T) -> Self {
        Self {
            code: "INVALID_PIC",
            status: 400,
            details: Some(format!("{:?}", details)),
        }
    }

    pub fn not_found(msg: &str) -> Self {
        let details = if msg.is_empty() {
            None
        } else {
            Some(msg.to_string())
        };
        Self {
            code: "NOT_FOUND",
            status: 404,
            details,
        }
    }

    pub fn msg_has_no_file() -> Self {
        Self {
            code: "MSG_HAS_NO_FILE",
            status: 400,
            details: None,
        }
    }

    pub fn file_is_local() -> Self {
        Self {
            code: "FILE_IS_LOCAL",
            status: 400,
            details: None,
        }
    }
}

impl From<DbError> for ApiError {
    fn from(value: DbError) -> Self {
        match value {
            DbError::NotFound => Self::not_found(""),
            other => ApiError::internal(other),
        }
    }
}
