use crate::entities::common::Id;

#[derive(Debug)]
pub struct LocalFileMeta {
    pub id: Id,
    pub path: String,
    pub name: String,
    pub size: u64,
    pub parts: u64,
}

#[derive(Debug)]
pub struct DownloadStatus {
    pub last_update: u64,
    pub completed: bool,
    pub progress: f64,
}
