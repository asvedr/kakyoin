use std::fmt::Debug;
use std::str::FromStr;

use rsa::{RsaPrivateKey, RsaPublicKey};
use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ToSqlOutput, Value, ValueRef};
use rusqlite::ToSql;
use uuid::Uuid;

pub const HEADER_AUTH: &str = "Authorization";

pub type VecBox<T> = Vec<Box<T>>;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Id(pub Uuid);

pub struct Me {
    pub id: Id,
    pub pub_key: RsaPublicKey,
    pub priv_key: RsaPrivateKey,
}

#[derive(Debug, PartialEq)]
pub enum InitStatus {
    NoId,
    NoKey,
    Done,
}

#[derive(Debug, Copy, Clone)]
pub enum ServerStatus {
    Unknown,
    Valid,
    Invalid,
}

impl Default for Id {
    fn default() -> Self {
        Self::new()
    }
}

impl Id {
    #[inline(always)]
    pub fn new() -> Self {
        Self(Uuid::new_v4())
    }
}

impl FromSql for Id {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let bts = match value {
            ValueRef::Text(val) => val,
            _ => return Err(FromSqlError::InvalidType),
        };
        let txt = std::str::from_utf8(bts).map_err(|_| FromSqlError::InvalidType)?;
        Self::from_str(txt).map_err(|_| FromSqlError::InvalidType)
    }
}

impl ToSql for Id {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        let txt = self.0.to_string();
        Ok(ToSqlOutput::Owned(Value::Text(txt)))
    }
}

impl From<Uuid> for Id {
    fn from(value: Uuid) -> Self {
        Self(value)
    }
}

impl From<Id> for Uuid {
    fn from(value: Id) -> Self {
        value.0
    }
}

impl FromStr for Id {
    type Err = <Uuid as FromStr>::Err;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(Uuid::from_str(s)?))
    }
}

impl std::fmt::Display for Id {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}
