use kakyoin_base::entities::log::Level;
use mddd::macros::IConfig;

#[allow(clippy::manual_non_exhaustive)]
#[derive(IConfig)]
pub struct Config {
    #[default(false)]
    pub report_atex: bool,
    #[default(4)]
    pub threads: usize,
    #[default(5)]
    pub max_retries: usize,
    #[default(5)]
    pub http_timeout: u64,
    #[default(Level::Err)]
    pub log_level: Level,
    #[default("stderr".to_string())]
    pub log_file: String,
    #[default("".to_string())]
    pub log_prefix: String,
    #[default("storage.db".to_string())]
    pub db_file: String,
    #[default("uploads".to_string())]
    pub upload_dir: String,
    #[default(10)]
    pub task_pool_size: usize,
    #[default(3000)]
    pub api_server_port: u16,
    #[prefix("kakyoin_")]
    __meta__: (),
}
