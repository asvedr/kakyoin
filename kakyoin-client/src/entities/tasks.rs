pub struct TaskName;
pub struct PeriodicName;
pub struct PeriodicInterval;

impl TaskName {
    pub const SEND_MSG: &'static str = "t_send_msg";
    pub const DOWNLOAD_FILE: &'static str = "t_download_file";
}

impl PeriodicName {
    pub const SEND_MSGS: &'static str = "p_send_msgs";
    pub const FETCH_MSGS: &'static str = "p_fetch_msgs";
    pub const UPDATE_META: &'static str = "p_upd_meta";
    pub const UPDATE_SERVER_STATUS: &'static str = "p_upd_server";

    pub const ALL: &'static [&'static str] = &[
        Self::SEND_MSGS,
        Self::FETCH_MSGS,
        Self::UPDATE_META,
        Self::UPDATE_SERVER_STATUS,
    ];
}

impl PeriodicInterval {
    pub const SEND_MSGS: u64 = 1;
    pub const FETCH_MSGS: u64 = 1;
    pub const UPDATE_META: u64 = 10;
    pub const UPDATE_SERVER_STATUS: u64 = 10;
}
