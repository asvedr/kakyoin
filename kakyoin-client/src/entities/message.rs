use crate::entities::chat::ChatId;
use crate::entities::common::Id;
use std::str::FromStr;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct LocalMsg {
    pub db_id: u64,
    pub sender: Option<Id>,
    pub receiver: Option<Id>,
    pub sender_key_code: Option<String>,
    pub chat: ChatId,
    pub uuid: Id,
    pub local_uniq_id: Option<i64>,
    pub text: String,
    pub attached: LocalMsgAttached,
    pub sent: bool,     // to server
    pub received: bool, // by other user
    pub read: bool,     // by me
    pub created_at: u64,
    pub ts: u64,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct NewLocalMsg {
    pub sender: Option<Id>,
    pub receiver: Option<Id>,
    pub sender_key_code: Option<String>,
    pub chat: ChatId,
    pub uuid: Id,
    pub local_uniq_id: Option<i64>,
    pub text: String,
    pub attached: LocalMsgAttached,
    pub sent: bool,
    pub received: bool,
    pub read: bool,
    pub ts: u64,
}

impl From<LocalMsg> for NewLocalMsg {
    fn from(value: LocalMsg) -> Self {
        Self {
            sender: value.sender,
            receiver: value.receiver,
            sender_key_code: value.sender_key_code,
            chat: value.chat,
            uuid: value.uuid,
            local_uniq_id: value.local_uniq_id,
            text: value.text,
            attached: value.attached,
            sent: value.sent,
            received: value.received,
            read: value.read,
            ts: value.ts,
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub struct NewMsgToSend {
    pub chat: ChatId,
    pub text: String,
    pub attached: LocalMsgAttached,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum LocalMsgAttached {
    Nothing,
    FileId { id: Id, size: u64, parts: u64 },
    Invite { name: String, participants: Vec<Id> },
}

impl LocalMsgAttached {
    const KIND_NOTHING: u64 = 0;
    const KIND_FILE_ID: u64 = 1;
    const KIND_INVITE: u64 = 2;

    pub fn get_kind(&self) -> &'static u64 {
        match self {
            LocalMsgAttached::Nothing => &Self::KIND_NOTHING,
            LocalMsgAttached::FileId { .. } => &Self::KIND_FILE_ID,
            LocalMsgAttached::Invite { .. } => &Self::KIND_INVITE,
        }
    }

    pub fn dumps(&self) -> String {
        match self {
            LocalMsgAttached::Nothing => "".to_string(),
            LocalMsgAttached::FileId { id, size, parts } => format!("{} {} {}", id, size, parts),
            LocalMsgAttached::Invite { name, participants } => {
                let mut items = vec![participants.len().to_string()];
                for part in participants {
                    items.push(part.to_string());
                }
                items.push(name.clone());
                items.join(" ")
            }
        }
    }

    pub fn loads(kind: u64, text: &str) -> Option<Self> {
        match () {
            _ if kind == Self::KIND_NOTHING => Some(Self::Nothing),
            _ if kind == Self::KIND_FILE_ID => match text.split(' ').collect::<Vec<_>>()[..] {
                [s_id, s_size, s_parts] => Some(Self::FileId {
                    id: Id::from_str(s_id).ok()?,
                    size: u64::from_str(s_size).ok()?,
                    parts: u64::from_str(s_parts).ok()?,
                }),
                _ => None,
            },
            _ if kind == Self::KIND_INVITE => {
                let (len, mut rest) = text.split_once(' ')?;
                let mut participants = Vec::new();
                for _ in 0..usize::from_str(len).ok()? {
                    let (item, rest_) = rest.split_once(' ')?;
                    participants.push(Id::from_str(item).ok()?);
                    rest = rest_;
                }
                let name = rest.to_string();
                Some(Self::Invite { participants, name })
            }
            _ => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_dumps_loads_attached_ok() {
        let attached = [
            LocalMsgAttached::Nothing,
            LocalMsgAttached::Invite {
                name: "abc".to_string(),
                participants: vec![Id::new(), Id::new(), Id::new()],
            },
            LocalMsgAttached::FileId {
                id: Id::new(),
                size: 10,
                parts: 20,
            },
        ];
        for val in attached {
            let txt = val.dumps();
            let kind = *val.get_kind();
            let loads = LocalMsgAttached::loads(kind, &txt).unwrap();
            assert_eq!(loads, val);
        }
    }

    #[test]
    fn test_dumps_loads_attached_err() {
        let txt = LocalMsgAttached::Nothing.dumps();
        let loads = LocalMsgAttached::loads(LocalMsgAttached::KIND_INVITE, &txt);
        assert!(loads.is_none());
    }
}
