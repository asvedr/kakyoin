use std::str::FromStr;

use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ToSqlOutput, Value, ValueRef};
use rusqlite::ToSql;

use crate::entities::common::Id;

pub struct Chat {
    pub id: ChatId,
    pub name: String,
    pub users: Vec<Id>,
}

#[derive(Debug, PartialEq)]
pub struct ChatShort {
    pub id: ChatId,
    pub name: String,
    pub last_msg: String,
    pub read: bool,
}

#[derive(Debug, PartialEq)]
pub struct ChatListItem {
    pub id: ChatId,
    pub name: String,
    pub banned: bool,
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum ChatId {
    Chat(Id),
    User(Id),
}

impl FromSql for ChatId {
    fn column_result(value: ValueRef<'_>) -> FromSqlResult<Self> {
        let bts = match value {
            ValueRef::Text(val) if val.is_empty() => return Err(FromSqlError::InvalidType),
            ValueRef::Text(val) => val,
            _ => return Err(FromSqlError::InvalidType),
        };
        let txt = std::str::from_utf8(bts).map_err(|_| FromSqlError::InvalidType)?;
        if let Ok(val) = Self::from_str(txt) {
            Ok(val)
        } else {
            Err(FromSqlError::InvalidType)
        }
    }
}

impl ToSql for ChatId {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        let txt = self.to_string();
        Ok(ToSqlOutput::Owned(Value::Text(txt)))
    }
}

impl ToString for ChatId {
    fn to_string(&self) -> String {
        match self {
            ChatId::Chat(id) => format!("C{}", id),
            ChatId::User(id) => format!("U{}", id),
        }
    }
}

impl FromStr for ChatId {
    type Err = ();

    fn from_str(txt: &str) -> Result<Self, Self::Err> {
        let fun = match txt.chars().next() {
            Some('C') => Self::Chat,
            Some('U') => Self::User,
            _ => return Err(()),
        };
        let id = Id::from_str(&txt[1..]).map_err(|_| ())?;
        Ok(fun(id))
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_from_str_to_str() {
        let id = ChatId::Chat(Id::new());
        assert_eq!(id, ChatId::from_str(&id.to_string()).unwrap());
        let id = ChatId::User(Id::new());
        assert_eq!(id, ChatId::from_str(&id.to_string()).unwrap());
    }
}
