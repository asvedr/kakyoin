use std::sync::Arc;

use kakyoin_base::entities::server_call::ApiUserMeta;
use mddd::macros::IIntEnum;
use rsa::RsaPublicKey;

use crate::entities::common::Id;

#[derive(IIntEnum, Clone, Copy, Eq, PartialEq, Debug, Ord, PartialOrd)]
pub enum UserStatus {
    #[int_value(0)]
    New,
    #[int_value(1)]
    Trusted,
    #[int_value(2)]
    Banned,
}

#[derive(Debug, PartialEq)]
pub struct User {
    pub id: Id,
    pub pub_key: Arc<RsaPublicKey>,
    pub pub_key_code: String,
    pub info: Arc<ApiUserMeta>,
    pub status: UserStatus,
}
