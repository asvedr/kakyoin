use crate::entities::common::Id;
use crate::entities::errors::{DbError, MsgSenderError};
use crate::entities::tasks::TaskName;
use atex::{ITaskController, ITaskExecutor, Task, TaskStatus};
use kakyoin_base::entities::common::DynFut;
use mddd::macros::EnumAutoFrom;
use std::str::FromStr;
use std::sync::Arc;

use crate::proto::local::IStorage;
use crate::proto::net::IMsgSender;

pub struct Executor<S: IStorage, MS: IMsgSender> {
    ctx: Arc<Ctx<S, MS>>,
}

struct Ctx<S: IStorage, MS: IMsgSender> {
    storage: S,
    msg_sender: MS,
}

#[derive(Debug, EnumAutoFrom)]
pub enum Error {
    Payload(uuid::Error),
    Db(DbError),
    Msg(MsgSenderError),
}

impl<S: IStorage + 'static, MS: IMsgSender + 'static> Executor<S, MS> {
    pub fn new(storage: S, msg_sender: MS) -> Self {
        Self {
            ctx: Arc::new(Ctx {
                storage,
                msg_sender,
            }),
        }
    }

    async fn a_execute(self, payload: String) -> Result<TaskStatus, Error> {
        let msg_id = Id::from_str(&payload)?;
        let msg;
        let servers;
        let me;
        {
            let storage = self.ctx.storage.lock().await;
            me = storage.settings().get_me()?;
            msg = storage.messages().get_by_uid(&msg_id)?;
            servers = storage.servers().get_all()?;
        }
        self.ctx
            .msg_sender
            .send_msgs(servers, me, vec![msg])
            .await?;
        Ok(TaskStatus::Completed)
    }
}

impl<S: IStorage + 'static, MS: IMsgSender + 'static> ITaskExecutor for Executor<S, MS> {
    type Error = Error;

    fn name(&self) -> &'static str {
        TaskName::SEND_MSG
    }

    fn execute(
        &self,
        _: Box<dyn ITaskController>,
        task: Task,
    ) -> DynFut<Result<TaskStatus, Error>> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_execute(task.payload),
        )
    }
}
