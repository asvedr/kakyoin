use crate::entities::common::Id;
use crate::entities::errors::FileDownloaderError;
use crate::entities::tasks::TaskName;
use crate::proto::net::IFileDownloader;
use atex::{
    AtexError, ITaskController, ITaskExecutor, ITaskManager, NewTask, Task, TaskId, TaskStatus,
};
use kakyoin_base::entities::common::DynFutRes;
use mddd::macros::EnumAutoFrom;
use std::str::FromStr;
use std::time::Duration;
use tokio::time::sleep;

const SLEEP_TIME: Duration = Duration::from_secs(5);

pub struct Executor<D: IFileDownloader> {
    pub downloader: D,
}

#[derive(Debug, EnumAutoFrom)]
pub enum Error {
    #[accept(uuid::Error)]
    InvalidPayload,
    FileProgressReturnsToNone,
    Atex(AtexError),
    Dw(FileDownloaderError),
}

pub fn spawn_task(
    manager: &dyn ITaskManager,
    user_id: Id,
    file_id: Id,
    max_parts: u64,
    path_to_save: &str,
) -> DynFutRes<TaskId, AtexError> {
    let task = NewTask {
        executor: TaskName::DOWNLOAD_FILE,
        payload: format!("{},{},{},{}", user_id, file_id, max_parts, path_to_save,),
    };
    manager.create_task(task)
}

impl<D: IFileDownloader> Executor<D> {
    pub fn new(downloader: D) -> Self {
        Self { downloader }
    }

    async fn a_execute(
        self,
        ctrl: Box<dyn ITaskController>,
        task: Task,
    ) -> Result<TaskStatus, Error> {
        let task_id = task.id;
        let (user_id, file_id, parts, name) = Self::parse_payload(task)?;
        ctrl.set_progress(task_id, 1).await?;
        self.downloader
            .start_loading(user_id, file_id, parts, name)
            .await?;
        sleep(SLEEP_TIME).await;
        let mut last_update = None;
        loop {
            let opt_status = self.downloader.get_loading_status(user_id, file_id).await;
            let status = match opt_status {
                Some(val) => val,
                _ => return Err(Error::FileProgressReturnsToNone),
            };
            if status.completed {
                break;
            }
            if Some(status.last_update) == last_update {
                self.downloader
                    .retry_block_request(user_id, file_id)
                    .await?
            } else {
                last_update = Some(status.last_update)
            }
            sleep(SLEEP_TIME).await;
        }
        Ok(TaskStatus::Completed)
    }

    fn parse_payload(task: Task) -> Result<(Id, Id, u64, String), Error> {
        let (user_id_s, rest) = task.payload.split_once(',').ok_or(Error::InvalidPayload)?;
        let user_id = Id::from_str(user_id_s)?;
        let (file_id_s, rest) = rest.split_once(',').ok_or(Error::InvalidPayload)?;
        let file_id = Id::from_str(file_id_s)?;
        let (max_parts_s, name) = rest.split_once(',').ok_or(Error::InvalidPayload)?;
        let max_parts = u64::from_str(max_parts_s).map_err(|_| Error::InvalidPayload)?;
        Ok((user_id, file_id, max_parts, name.to_string()))
    }
}

impl<D: IFileDownloader + Clone + 'static> ITaskExecutor for Executor<D> {
    type Error = Error;

    fn name(&self) -> &'static str {
        TaskName::DOWNLOAD_FILE
    }

    fn execute(
        &self,
        ctrl: Box<dyn ITaskController>,
        task: Task,
    ) -> DynFutRes<TaskStatus, Self::Error> {
        let slf = Self {
            downloader: self.downloader.clone(),
        };
        Box::pin(slf.a_execute(ctrl, task))
    }
}
