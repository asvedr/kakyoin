use crate::entities::common::HEADER_AUTH;
use crate::entities::errors::ApiError;
use crate::proto::common::{IApiHandler, IApiHandlerWrapped};
use crate::proto::local::IAuthTokenManager;
use hyper::body::HttpBody;
use hyper::{Body, Method, Request};
use kakyoin_base::entities::common::DynFutRes;
use serde::Deserialize;
use serde_json::Value;
use serde_json::{from_str, from_value, to_string, Map};
use std::sync::Arc;

pub struct ApiWrapper<H: IApiHandler, TM: IAuthTokenManager> {
    ctx: Arc<Ctx<H, TM>>,
}

struct Ctx<H: IApiHandler, TM: IAuthTokenManager> {
    handler: H,
    token_manager: TM,
}

fn force_escape_str(src: String) -> String {
    if src.starts_with('"') && src.ends_with('"') {
        src
    } else {
        format!("\"{}\"", src)
    }
}

fn parse_query<T: for<'a> Deserialize<'a>>(
    src: Option<&str>,
    fields_force_str: &[&str],
) -> Result<T, ApiError> {
    let src = match src {
        Some(val) => val,
        _ => return Err(ApiError::invalid_query()),
    };
    let mut key_val_map = Map::new();
    for token in src.split('&') {
        let (key, value) = match token.split_once('=') {
            None => return Err(ApiError::invalid_query()),
            Some(val) => val,
        };
        let mut replaced = value.replace('\'', "\"");
        if fields_force_str.contains(&key) {
            replaced = force_escape_str(replaced)
        }
        let value = from_str(&replaced).map_err(|_| ApiError::invalid_query())?;
        key_val_map.insert(key.to_string(), value);
    }
    let result = from_value(Value::Object(key_val_map)).map_err(|_| ApiError::invalid_query())?;
    Ok(result)
}

async fn parse_body<T: for<'a> Deserialize<'a>>(mut src: Request<Body>) -> Result<T, ApiError> {
    let bts_res = match src.data().await {
        None => return serde_json::from_str("").map_err(|_| ApiError::invalid_request_body()),
        Some(val) => val,
    };
    let bts = bts_res.map_err(|_| ApiError::invalid_request_body())?;
    serde_json::from_slice(&bts).map_err(|_| ApiError::invalid_request_body())
}

impl<H: IApiHandler + 'static, TM: IAuthTokenManager + 'static> ApiWrapper<H, TM> {
    pub fn wrap(handler: H, token_manager: TM) -> Box<dyn IApiHandlerWrapped> {
        Box::new(Self {
            ctx: Arc::new(Ctx {
                handler,
                token_manager,
            }),
        })
    }

    async fn a_handle(self, request: Request<Body>) -> Result<Vec<u8>, ApiError> {
        if H::auth_token_required() && self.ctx.token_manager.is_auth_turned_on().await {
            self.check_auth(&request).await?;
        }
        let query = if let Some(val) = H::override_query() {
            val
        } else {
            parse_query(request.uri().query(), H::query_fields_force_str())?
        };
        let body = if let Some(val) = H::override_body() {
            val
        } else {
            parse_body(request).await?
        };
        let resp = self.ctx.handler.handle(query, body).await?;
        let resp = to_string(&resp).map_err(ApiError::internal)?;
        Ok(resp.into_bytes())
    }

    async fn check_auth(&self, request: &Request<Body>) -> Result<(), ApiError> {
        let token = match request.headers().get(HEADER_AUTH) {
            None => return Err(ApiError::not_authorized()),
            Some(val) => val.to_str().map_err(ApiError::internal)?,
        };
        let valid = self.ctx.token_manager.validate_token(token).await;
        if valid {
            Ok(())
        } else {
            Err(ApiError::not_authorized())
        }
    }
}

impl<H: IApiHandler + 'static, TM: IAuthTokenManager + 'static> IApiHandlerWrapped
    for ApiWrapper<H, TM>
{
    fn path(&self) -> &str {
        self.ctx.handler.path()
    }

    fn method(&self) -> Method {
        self.ctx.handler.method()
    }

    fn handle(&self, request: Request<Body>) -> DynFutRes<Vec<u8>, ApiError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_handle(request),
        )
    }
}
