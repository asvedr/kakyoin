use crate::entities::config::Config;
use crate::proto::common::IService;
use atex::ITaskManager;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;
use std::io::Error;
use std::sync::Arc;
use std::time::Duration;
use tokio::time::sleep;

#[derive(Clone)]
pub struct TaskReporter {
    pub manager: Arc<dyn ITaskManager>,
    pub logger: &'static dyn ILogger,
    pub config: &'static Config,
}

unsafe impl Send for TaskReporter {}
unsafe impl Sync for TaskReporter {}

impl TaskReporter {
    async fn a_run(self) -> Result<(), Error> {
        if !self.config.report_atex {
            return Ok(());
        }
        loop {
            let fut = self.manager.get_stats();
            let report = fut.await;
            let text = report
                .iter()
                .map(|opt_task| {
                    if let Some(task) = opt_task {
                        format!("[{}:{}]", task.executor, task.active_for)
                    } else {
                        "[_:_]".to_string()
                    }
                })
                .collect::<Vec<_>>()
                .join(",");
            self.logger.log(Level::Debug, &text);
            sleep(Duration::from_secs(5)).await
        }
    }
}

impl IService for TaskReporter {
    fn run(&mut self) -> DynFutRes<(), Error> {
        Box::pin(self.clone().a_run())
    }
}
