use crate::entities::common::Me;
use crate::entities::errors::{MsgFetcherError, ServerError};
use crate::proto::net::{IMsgFetcher, IServerClient, IServerClientFactory};
use futures_util::future::join_all;
use kakyoin_base::entities::api_msg::{ApiMsg, ApiMsgPacked};
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;
use kakyoin_base::proto::serializers::IApiMsgPacker;
use std::collections::HashSet;
use std::sync::Arc;
use uuid::Uuid;

pub struct MsgFetcher<L: ILogger, CF: IServerClientFactory, P: IApiMsgPacker> {
    ctx: Arc<Ctx<L, CF, P>>,
}

struct Ctx<L: ILogger, CF: IServerClientFactory, P: IApiMsgPacker> {
    logger: L,
    client_factory: CF,
    packer: P,
}

impl<L: ILogger, CF: IServerClientFactory, P: IApiMsgPacker> MsgFetcher<L, CF, P> {
    pub fn new(logger: L, client_factory: CF, packer: P) -> Self {
        let ctx = Ctx {
            logger,
            client_factory,
            packer,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_fetch_msgs(
        self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
    ) -> Result<Vec<ApiMsg>, MsgFetcherError> {
        let mut futures = Vec::new();
        for s_contact in servers.iter() {
            let addr = s_contact.addr.clone();
            let srv = self
                .ctx
                .client_factory
                .produce(me.clone(), s_contact)
                .map_err(MsgFetcherError::CanNotProduceClient)?;
            let fut = self.fetch_from_server(&me, srv);
            futures.push(async move { (addr, fut.await) });
        }
        let mut msgs = Vec::new();
        let mut id_set = HashSet::new();
        for (addr, result) in join_all(futures).await {
            let worker_msgs = match result {
                Err(err) => {
                    let txt = format!("Can not fetch msgs from({}): {:?}", addr, err,);
                    self.ctx.logger.log(Level::Warn, &txt);
                    continue;
                }
                Ok(val) => val,
            };
            for msg in worker_msgs {
                if id_set.contains(&msg.header.id) {
                    continue;
                }
                id_set.insert(msg.header.id);
                msgs.push(msg);
            }
        }
        Ok(msgs)
    }

    async fn a_report_received(
        self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: Vec<Uuid>,
    ) -> Result<(), MsgFetcherError> {
        let mut futs = Vec::new();
        for s_contact in servers.iter() {
            let srv = self
                .ctx
                .client_factory
                .produce(me.clone(), s_contact)
                .map_err(MsgFetcherError::CanNotProduceClient)?;
            futs.push(srv.mark_received(msgs.clone()));
        }
        join_all(futs).await;
        Ok(())
    }

    async fn fetch_from_server(
        &self,
        me: &Me,
        srv: Box<dyn IServerClient>,
    ) -> Result<Vec<ApiMsg>, ServerError> {
        let msgs = srv.fetch_msgs().await?;
        Ok(self.unpack_msgs(me, msgs))
    }

    fn unpack_msgs(&self, me: &Me, msgs: Vec<ApiMsgPacked>) -> Vec<ApiMsg> {
        let mut result = Vec::new();
        for msg in msgs {
            let res = self.ctx.packer.unpack(&msg, None, Some(&me.priv_key));
            match res {
                Ok(val) => result.push(val),
                Err(err) => {
                    let txt = format!("Msg({}) can not be unpacked: {:?}", msg.id, err);
                    self.ctx.logger.log(Level::Warn, &txt);
                }
            }
        }
        result
    }
}

impl<L: ILogger + 'static, CF: IServerClientFactory + 'static, P: IApiMsgPacker + 'static>
    IMsgFetcher for MsgFetcher<L, CF, P>
{
    fn fetch_msgs(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
    ) -> DynFutRes<Vec<ApiMsg>, MsgFetcherError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_fetch_msgs(servers, me),
        )
    }

    fn report_received(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: Vec<Uuid>,
    ) -> DynFutRes<(), MsgFetcherError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_report_received(servers, me, msgs),
        )
    }
}
