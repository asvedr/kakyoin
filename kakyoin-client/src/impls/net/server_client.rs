use crate::entities::common::Me;
use crate::entities::errors::ServerError;
use crate::proto::net::{IServerClient, IServerClientFactory};
use crate::utils::time::get_now;
use kakyoin_base::entities::api_msg::{ApiMsgPacked, ApiMsgStatus};
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::entities::server_call::{ApiUserMeta, ServerCall, ServerResponse};
use kakyoin_base::proto::common::ICryptoOps;
use kakyoin_base::proto::net::IHttpClient;
use rsa::RsaPublicKey;
use serdebin::{from_bytes, to_bytes};
use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::Mutex;
use uuid::Uuid;

pub struct ServerClientFactory<HC: IHttpClient, CO: ICryptoOps> {
    http: HC,
    crypto_ops: CO,
    max_retry: usize,
}

pub struct ServerClient<HC: IHttpClient, CO: ICryptoOps> {
    ctx: Arc<Ctx<HC, CO>>,
}

struct Ctx<HC: IHttpClient, CO: ICryptoOps> {
    http: HC,
    crypto_ops: CO,
    me: Arc<Me>,
    server_key: RsaPublicKey,
    url: String,
    token_mtx: Mutex<MtxData>,
    max_retry: usize,
}

struct MtxData {
    token: Vec<u8>,
    expired_ts: u64,
}

impl<HC: IHttpClient, CO: ICryptoOps> Clone for ServerClient<HC, CO> {
    fn clone(&self) -> Self {
        Self {
            ctx: self.ctx.clone(),
        }
    }
}

impl<HC: IHttpClient + 'static, CO: ICryptoOps + 'static> ServerClient<HC, CO> {
    pub fn new(
        http: HC,
        crypto_ops: CO,
        me: Arc<Me>,
        server_contact: &ServerContact,
        max_retry: usize,
    ) -> Result<Self, ServerError> {
        let ctx = Arc::new(Ctx {
            http,
            me,
            crypto_ops,
            server_key: server_contact
                .get_key()
                .map_err(|_| ServerError::InvalidContact)?,
            url: server_contact.addr.clone(),
            token_mtx: Mutex::new(MtxData {
                token: Vec::new(),
                expired_ts: 0,
            }),
            max_retry,
        });
        Ok(Self { ctx })
    }

    #[cfg(test)]
    pub async fn set_token(&self, token: Vec<u8>, expired: u64) {
        let mut mtx = self.ctx.token_mtx.lock().await;
        mtx.token = token;
        mtx.expired_ts = expired;
    }

    async fn request(&self, req: ServerCall) -> Result<ServerResponse, ServerError> {
        let req = self
            .ctx
            .crypto_ops
            .encrypt(&self.ctx.server_key, &to_bytes(&req)?)?;
        let resp = self.ctx.http.post(&self.ctx.url, "", req).await?;
        if resp.code != 200 {
            return Err(ServerError::parse_resp(resp));
        }
        let resp = self
            .ctx
            .crypto_ops
            .decrypt(&self.ctx.me.priv_key, &resp.body)?;
        let (resp, _): (ServerResponse, _) = from_bytes(&resp)?;
        Ok(resp)
    }

    async fn fetch_token(&self) -> Result<(Vec<u8>, u64), ServerError> {
        let req = ServerCall::GetToken {
            user_id: self.ctx.me.id.into(),
            pub_key: self.ctx.me.pub_key.clone(),
        };
        match self.request(req).await? {
            ServerResponse::Token { token, expired_at } => Ok((token, expired_at)),
            _ => Err(ServerError::UnexpectedResponse),
        }
    }

    async fn get_token(&self) -> Result<Vec<u8>, ServerError> {
        let mut mtx = self.ctx.token_mtx.lock().await;
        if mtx.token.is_empty() || mtx.expired_ts < get_now() {
            let (token, ts) = self.fetch_token().await?;
            mtx.token = token;
            mtx.expired_ts = ts;
        }
        Ok(mtx.token.clone())
    }

    async fn retry<T, Fun: Fn(Self) -> DynFutRes<T, ServerError>>(
        self,
        maker: Fun,
    ) -> Result<T, ServerError> {
        let mut error = None;
        for _ in 0..self.ctx.max_retry {
            let err = match maker(self.clone()).await {
                Ok(val) => return Ok(val),
                Err(val) => val,
            };
            error = Some(err.clone());
            if matches!(err, ServerError::InvalidToken) {
                let _ = self.get_token().await;
            }
        }
        Err(error.unwrap())
    }

    async fn a_send_msgs(
        self,
        msgs: Vec<ApiMsgPacked>,
    ) -> Result<HashMap<Uuid, ApiMsgStatus>, ServerError> {
        let token = self.get_token().await?;
        match self
            .request(ServerCall::SendMessages { token, msgs })
            .await?
        {
            ServerResponse::Statuses { map } => Ok(map),
            _ => Err(ServerError::UnexpectedResponse),
        }
    }

    async fn a_fetch_msgs(self) -> Result<Vec<ApiMsgPacked>, ServerError> {
        let token = self.get_token().await?;
        match self.request(ServerCall::FetchMessages { token }).await? {
            ServerResponse::Messages { msgs } => Ok(msgs),
            _ => Err(ServerError::UnexpectedResponse),
        }
    }

    async fn a_get_statuses(
        self,
        ids: Vec<Uuid>,
    ) -> Result<HashMap<Uuid, ApiMsgStatus>, ServerError> {
        let token = self.get_token().await?;
        match self.request(ServerCall::GetStatuses { token, ids }).await? {
            ServerResponse::Statuses { map } => Ok(map),
            _ => Err(ServerError::UnexpectedResponse),
        }
    }

    async fn a_mark_received(self, msgs: Vec<Uuid>) -> Result<(), ServerError> {
        let token = self.get_token().await?;
        match self
            .request(ServerCall::MarkReceived { token, msgs })
            .await?
        {
            ServerResponse::Nothing => Ok(()),
            _ => Err(ServerError::UnexpectedResponse),
        }
    }

    async fn a_request_meta(
        self,
        user: Uuid,
        key_code: String,
    ) -> Result<Box<ApiUserMeta>, ServerError> {
        let token = self.get_token().await?;
        let request = ServerCall::ReqMeta {
            token,
            user,
            key_code,
        };
        match self.request(request).await? {
            ServerResponse::UserMeta { meta } => Ok(meta),
            _ => Err(ServerError::UnexpectedResponse),
        }
    }

    async fn a_request_meta_hash(
        self,
        users: Vec<(Uuid, String)>,
    ) -> Result<HashMap<Uuid, String>, ServerError> {
        let token = self.get_token().await?;
        let request = ServerCall::ReqMetaHash { token, users };
        let users = match self.request(request).await? {
            ServerResponse::MetaHashes { users } => users,
            _ => return Err(ServerError::UnexpectedResponse),
        };
        let resp = users
            .into_iter()
            .map(|((id, _), hash)| (id, hash))
            .collect::<HashMap<_, _>>();
        Ok(resp)
    }

    async fn a_post_my_meta(self, meta: Arc<ApiUserMeta>) -> Result<(), ServerError> {
        let token = self.get_token().await?;
        let request = ServerCall::PostMyMeta {
            token,
            meta: Box::new((*meta).clone()),
        };
        match self.request(request).await? {
            ServerResponse::Nothing => Ok(()),
            _ => Err(ServerError::UnexpectedResponse),
        }
    }

    async fn a_ping(self) -> Result<(), ServerError> {
        self.fetch_token().await?;
        Ok(())
    }
}

impl<HC: IHttpClient + 'static, CO: ICryptoOps + 'static> IServerClient for ServerClient<HC, CO> {
    fn addr(&self) -> &str {
        &self.ctx.url
    }

    fn send_msgs(
        &self,
        msgs: Vec<ApiMsgPacked>,
    ) -> DynFutRes<HashMap<Uuid, ApiMsgStatus>, ServerError> {
        Box::pin(
            self.clone()
                .retry(move |slf| Box::pin(slf.a_send_msgs(msgs.clone()))),
        )
    }

    fn fetch_msgs(&self) -> DynFutRes<Vec<ApiMsgPacked>, ServerError> {
        Box::pin(self.clone().retry(|slf| Box::pin(slf.a_fetch_msgs())))
    }

    fn get_statuses(
        &self,
        id_list: Vec<Uuid>,
    ) -> DynFutRes<HashMap<Uuid, ApiMsgStatus>, ServerError> {
        Box::pin(
            self.clone()
                .retry(move |slf| Box::pin(slf.a_get_statuses(id_list.clone()))),
        )
    }

    fn mark_received(&self, id_list: Vec<Uuid>) -> DynFutRes<(), ServerError> {
        Box::pin(
            self.clone()
                .retry(move |slf| Box::pin(slf.a_mark_received(id_list.clone()))),
        )
    }

    fn request_meta(
        &self,
        id: Uuid,
        key: &RsaPublicKey,
    ) -> DynFutRes<Box<ApiUserMeta>, ServerError> {
        let code = self.ctx.crypto_ops.get_key_code(key);
        Box::pin(
            self.clone()
                .retry(move |slf| Box::pin(slf.a_request_meta(id, code.clone()))),
        )
    }

    fn request_meta_hash(
        &self,
        id_list: &[(Uuid, &RsaPublicKey)],
    ) -> DynFutRes<HashMap<Uuid, String>, ServerError> {
        let req = id_list
            .iter()
            .map(|(id, key)| (*id, self.ctx.crypto_ops.get_key_code(key)))
            .collect::<Vec<_>>();
        Box::pin(
            self.clone()
                .retry(move |slf| Box::pin(slf.a_request_meta_hash(req.clone()))),
        )
    }

    fn post_my_meta(&self, meta: Arc<ApiUserMeta>) -> DynFutRes<(), ServerError> {
        Box::pin(
            self.clone()
                .retry(move |slf| Box::pin(slf.a_post_my_meta(meta.clone()))),
        )
    }

    fn ping(&self) -> DynFutRes<(), ServerError> {
        Box::pin(self.clone().retry(|slf| Box::pin(slf.a_ping())))
    }
}

impl<HC: IHttpClient + Clone + 'static, CO: ICryptoOps + Clone + 'static>
    ServerClientFactory<HC, CO>
{
    pub fn new(http: HC, crypto_ops: CO, max_retry: usize) -> Self {
        Self {
            http,
            crypto_ops,
            max_retry,
        }
    }
}

impl<HC: IHttpClient + Clone + 'static, CO: ICryptoOps + Clone + 'static> IServerClientFactory
    for ServerClientFactory<HC, CO>
{
    fn produce(
        &self,
        me: Arc<Me>,
        contact: &ServerContact,
    ) -> Result<Box<dyn IServerClient>, ServerError> {
        let client = ServerClient::new(
            self.http.clone(),
            self.crypto_ops.clone(),
            me,
            contact,
            self.max_retry,
        )?;
        Ok(Box::new(client))
    }
}
