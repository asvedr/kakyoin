use crate::entities::common::{Id, Me};
use crate::entities::errors::{DbError, FileUploaderError};
use crate::proto::local::IStorage;
use crate::proto::net::{IFileUploader, IServerClientFactory};
use crate::utils::time::get_now;
use futures_util::future::join_all;
use kakyoin_base::entities::api_msg::{ApiMsg, ApiMsgBody, ApiMsgBodySendFilePart, ApiMsgHeader};
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::proto::common::{ICryptoOps, IFS};
use kakyoin_base::proto::serializers::{IApiMsgPacker, IStrSerializer};
use rsa::RsaPublicKey;
use std::mem;
use std::path::Path;
use std::sync::Arc;
use uuid::Uuid;

pub struct FileUploader<
    S: IStorage,
    SCF: IServerClientFactory,
    FS: IFS,
    P: IApiMsgPacker,
    Cr: ICryptoOps,
    KeySer: IStrSerializer<RsaPublicKey>,
> {
    ctx: Arc<Ctx<S, SCF, FS, P, Cr, KeySer>>,
}

struct Ctx<
    S: IStorage,
    SCF: IServerClientFactory,
    FS: IFS,
    P: IApiMsgPacker,
    Cr: ICryptoOps,
    KeySer: IStrSerializer<RsaPublicKey>,
> {
    storage: S,
    factory: SCF,
    fs: FS,
    packer: P,
    crypto: Cr,
    key_ser: KeySer,
}

impl<
        S: IStorage,
        SCF: IServerClientFactory,
        FS: IFS,
        P: IApiMsgPacker,
        Cr: ICryptoOps,
        KeySer: IStrSerializer<RsaPublicKey>,
    > FileUploader<S, SCF, FS, P, Cr, KeySer>
{
    pub fn new(storage: S, factory: SCF, fs: FS, packer: P, crypto: Cr, key_ser: KeySer) -> Self {
        let ctx = Ctx {
            storage,
            factory,
            fs,
            packer,
            crypto,
            key_ser,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_upload_file_part(
        self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        user_id: Id,
        user_key: String,
        file_id: Id,
        part: u64,
    ) -> Result<(), FileUploaderError> {
        let data = self.get_file_data(&user_id, &file_id, part).await?;
        let body = Box::new(ApiMsgBodySendFilePart {
            file_id: file_id.into(),
            part,
            data,
        });
        let pub_key = self
            .ctx
            .key_ser
            .deserialize(&user_key)
            .map_err(|_| FileUploaderError::InvalidKey)?;
        let receiver_key_code = self.ctx.crypto.get_key_code(&pub_key).into_bytes();
        let msg = ApiMsg {
            header: ApiMsgHeader {
                id: Uuid::new_v4(),
                sender: me.id.into(),
                sender_pub_key: self.ctx.key_ser.serialize(&me.pub_key),
                receiver: user_id.into(),
                receiver_key_code,
                ts: get_now(),
            },
            body: ApiMsgBody::SendFilePart(body),
        };
        let packed = self.ctx.packer.pack(msg, Some(&pub_key), &me.priv_key);
        let mut futs = Vec::new();
        for s_cnn in servers.iter() {
            let srv = self.ctx.factory.produce(me.clone(), s_cnn)?;
            futs.push(srv.send_msgs(vec![packed.clone()]));
        }
        join_all(futs).await;
        Ok(())
    }

    async fn get_file_data(
        &self,
        user_id: &Id,
        file_id: &Id,
        part: u64,
    ) -> Result<Vec<u8>, FileUploaderError> {
        let storage = self.ctx.storage.lock().await;
        let meta = match storage.shared_files().get_file_for_user(file_id, user_id) {
            Ok(val) => val,
            Err(DbError::NotFound) => return Err(FileUploaderError::FileForUserNotFound),
            Err(err) => return Err(err.into()),
        };
        mem::drop(storage);
        Ok(self.ctx.fs.get_block(Path::new(&meta.path), part)?)
    }
}

impl<
        S: IStorage + 'static,
        SCF: IServerClientFactory + 'static,
        FS: IFS + 'static,
        P: IApiMsgPacker + 'static,
        Cr: ICryptoOps + 'static,
        KeySer: IStrSerializer<RsaPublicKey> + 'static,
    > IFileUploader for FileUploader<S, SCF, FS, P, Cr, KeySer>
{
    fn upload_file_part(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        user_id: Id,
        user_key: String,
        file_id: Id,
        part: u64,
    ) -> DynFutRes<(), FileUploaderError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_upload_file_part(servers, me, user_id, user_key, file_id, part),
        )
    }
}
