use crate::entities::chat::ChatId;
use crate::entities::common::{Id, Me, VecBox};
use futures_util::future::join_all;
use kakyoin_base::entities::api_msg::{
    ApiMsg, ApiMsgBody, ApiMsgBodyAddToChat, ApiMsgBodyDeclareFile, ApiMsgBodyText, ApiMsgHeader,
    ApiMsgPacked, ApiMsgStatus,
};
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::{ClientContact, ServerContact};
use kakyoin_base::proto::common::ICryptoOps;
use kakyoin_base::proto::serializers::{IApiMsgPacker, IStrSerializer};
use rsa::RsaPublicKey;
use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use uuid::Uuid;

use crate::entities::errors::MsgSenderError;
use crate::entities::message::{LocalMsg, LocalMsgAttached};
use crate::proto::local::IStorage;
use crate::proto::net::{IMsgSender, IServerClient, IServerClientFactory};

pub struct MsgSender<
    F: IServerClientFactory,
    S: IStorage,
    Ser: IStrSerializer<RsaPublicKey>,
    P: IApiMsgPacker,
    CO: ICryptoOps,
> {
    ctx: Arc<Ctx<F, S, Ser, P, CO>>,
}

struct Ctx<
    F: IServerClientFactory,
    S: IStorage,
    Ser: IStrSerializer<RsaPublicKey>,
    P: IApiMsgPacker,
    CO: ICryptoOps,
> {
    server_factory: F,
    storage: S,
    ser: Ser,
    packer: P,
    crypto: CO,
}

#[derive(Default)]
struct MsgStatuses {
    sent: Vec<Id>,
    received: Vec<Id>,
}

impl<
        F: IServerClientFactory,
        S: IStorage,
        Ser: IStrSerializer<RsaPublicKey>,
        P: IApiMsgPacker,
        CO: ICryptoOps,
    > Clone for MsgSender<F, S, Ser, P, CO>
{
    fn clone(&self) -> Self {
        Self {
            ctx: self.ctx.clone(),
        }
    }
}

impl<
        F: IServerClientFactory + 'static,
        S: IStorage + 'static,
        Ser: IStrSerializer<RsaPublicKey> + 'static,
        P: IApiMsgPacker + 'static,
        CO: ICryptoOps + 'static,
    > MsgSender<F, S, Ser, P, CO>
{
    pub fn new(server_factory: F, storage: S, ser: Ser, packer: P, crypto: CO) -> Self {
        let ctx = Ctx {
            server_factory,
            storage,
            ser,
            packer,
            crypto,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_send_msgs(
        self,
        all_servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: VecBox<LocalMsg>,
    ) -> Result<(), MsgSenderError> {
        // let me = self.ctx.storage.lock().await.settings().get_me()?;
        let user_ids = msgs
            .iter()
            .map(|msg| *msg.receiver.as_ref().unwrap())
            .collect::<Vec<_>>();
        let users = self.get_user_map(user_ids).await?;
        let mut futures = Vec::new();
        for srv_cnt in all_servers.iter() {
            let srv = self.ctx.server_factory.produce(me.clone(), srv_cnt)?;
            let addr = srv.addr().to_string();
            let fut = self.send_to_srv(&me, &users, srv, &msgs);
            futures.push(async move { (addr, fut.await) });
        }
        let results = join_all(futures).await;
        let collected = Self::collect_results(results, msgs);
        if !collected.sent.is_empty() || !collected.received.is_empty() {
            let storage = self.ctx.storage.lock().await;
            let repo = storage.messages();
            repo.mark_as_sent(&collected.sent)?;
            repo.mark_as_received(&collected.received)?;
        }
        Ok(())
    }

    async fn send_to_srv(
        &self,
        me: &Me,
        users: &HashMap<Id, Arc<RsaPublicKey>>,
        srv: Box<dyn IServerClient>,
        msgs: &[Box<LocalMsg>],
    ) -> Result<Vec<Id>, MsgSenderError> {
        let packed = self.pack_msgs(me, users, msgs)?;
        let statuses = srv.send_msgs(packed).await?;
        let res = statuses
            .into_iter()
            .filter_map(|(id, status)| {
                if matches!(status, ApiMsgStatus::Received) {
                    Some(id.into())
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        Ok(res)
    }

    async fn get_user_map(
        &self,
        user_ids: Vec<Id>,
    ) -> Result<HashMap<Id, Arc<RsaPublicKey>>, MsgSenderError> {
        let storage = self.ctx.storage.lock().await;
        Ok(storage.users().get_keys(&user_ids)?)
    }

    fn pack_msgs(
        &self,
        me: &Me,
        users: &HashMap<Id, Arc<RsaPublicKey>>,
        msgs: &[Box<LocalMsg>],
    ) -> Result<Vec<ApiMsgPacked>, MsgSenderError> {
        let mut packed = Vec::new();
        let sender: Uuid = me.id.into();
        let sender_pub_key = self.ctx.ser.serialize(&me.pub_key);
        for msg in msgs {
            let receiver_id = *msg.receiver.as_ref().unwrap();
            let receiver_key = users.get(&receiver_id).unwrap();
            let receiver_key_code = self.ctx.crypto.get_key_code(receiver_key).into_bytes();
            let chat = match msg.chat {
                ChatId::Chat(id) => Some(id.into()),
                ChatId::User(_) => None,
            };
            let body = match &msg.attached {
                LocalMsgAttached::Nothing => ApiMsgBody::Text(Box::new(ApiMsgBodyText {
                    chat,
                    text: msg.text.clone(),
                })),
                LocalMsgAttached::FileId { id, size, parts } => {
                    ApiMsgBody::DeclareFile(Box::new(ApiMsgBodyDeclareFile {
                        chat,
                        file_id: (*id).into(),
                        name: msg.text.clone(),
                        size: *size,
                        parts: *parts,
                    }))
                }
                LocalMsgAttached::Invite { name, participants } => {
                    ApiMsgBody::AddToChat(Box::new(ApiMsgBodyAddToChat {
                        chat_id: chat.unwrap(),
                        chat_name: name.clone(),
                        other_participants: Self::make_contacts(users, participants),
                    }))
                }
            };
            let header = ApiMsgHeader {
                id: msg.uuid.into(),
                sender,
                sender_pub_key: sender_pub_key.clone(),
                receiver: receiver_id.into(),
                receiver_key_code,
                ts: msg.ts,
            };
            let msg =
                self.ctx
                    .packer
                    .pack(ApiMsg { header, body }, Some(&**receiver_key), &me.priv_key);
            packed.push(msg);
        }
        Ok(packed)
    }

    fn make_contacts(
        users: &HashMap<Id, Arc<RsaPublicKey>>,
        participants: &[Id],
    ) -> Vec<ClientContact> {
        let mut result = Vec::new();
        for id in participants {
            let key = users.get(id).unwrap();
            result.push(ClientContact::new((*id).into(), key))
        }
        result
    }

    fn collect_results(
        results: Vec<(String, Result<Vec<Id>, MsgSenderError>)>,
        msgs: VecBox<LocalMsg>,
    ) -> MsgStatuses {
        let mut received_total = HashSet::new();
        let mut at_least_one_got = false;
        for (_addr, result) in results {
            let received = match result {
                Err(_err) => continue,
                Ok(val) => val,
            };
            received_total.extend(received.into_iter());
            at_least_one_got = true;
        }
        let mut statuses = MsgStatuses::default();
        for msg in msgs {
            if received_total.contains(&msg.uuid) {
                statuses.received.push(msg.uuid);
            } else if at_least_one_got {
                statuses.sent.push(msg.uuid);
            }
        }
        statuses
    }
}

impl<
        F: IServerClientFactory + 'static,
        S: IStorage + 'static,
        Ser: IStrSerializer<RsaPublicKey> + 'static,
        P: IApiMsgPacker + 'static,
        CO: ICryptoOps + 'static,
    > IMsgSender for MsgSender<F, S, Ser, P, CO>
{
    fn send_msgs(
        &self,
        all_servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: VecBox<LocalMsg>,
    ) -> DynFutRes<(), MsgSenderError> {
        Box::pin(self.clone().a_send_msgs(all_servers, me, msgs))
    }
}
