use crate::entities::chat::{Chat, ChatId};
use crate::entities::common::{Id, Me, VecBox};
use crate::entities::errors::{
    DbError, DbResult, FileDownloaderError, FileUploaderError, MsgDispatcherError,
};
use crate::entities::message::{LocalMsgAttached, NewLocalMsg};
use crate::entities::user::UserStatus;
use crate::proto::local::{IStorage, IUsersRepo};
use crate::proto::net::{IFileDownloader, IFileUploader, IMsgDispatcher};
use kakyoin_base::entities::api_msg::{
    ApiMsg, ApiMsgBody, ApiMsgBodyAddToChat, ApiMsgBodyReqFilePart, ApiMsgBodySendFilePart,
};
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::{ClientContact, ServerContact};
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::{ICryptoOps, ILogger};
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::RsaPublicKey;
use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use uuid::Uuid;

pub struct MsgDispatcher<
    L: ILogger,
    S: IStorage,
    KeySer: IStrSerializer<RsaPublicKey>,
    Dw: IFileDownloader,
    Up: IFileUploader,
    Cr: ICryptoOps,
> {
    ctx: Arc<Ctx<L, S, KeySer, Dw, Up, Cr>>,
}

struct Ctx<
    L: ILogger,
    S: IStorage,
    KeySer: IStrSerializer<RsaPublicKey>,
    Dw: IFileDownloader,
    Up: IFileUploader,
    Cr: ICryptoOps,
> {
    logger: L,
    storage: S,
    key_ser: KeySer,
    downloader: Dw,
    uploader: Up,
    crypto: Cr,
}

impl<
        L: ILogger,
        S: IStorage,
        KeySer: IStrSerializer<RsaPublicKey>,
        Dw: IFileDownloader,
        Up: IFileUploader,
        Cr: ICryptoOps,
    > MsgDispatcher<L, S, KeySer, Dw, Up, Cr>
{
    pub fn new(
        logger: L,
        storage: S,
        key_ser: KeySer,
        downloader: Dw,
        uploader: Up,
        crypto: Cr,
    ) -> Self {
        let ctx = Ctx {
            logger,
            storage,
            key_ser,
            downloader,
            uploader,
            crypto,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_dispatch_msgs(
        self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: Vec<ApiMsg>,
    ) -> Result<(), MsgDispatcherError> {
        let users = self.get_senders_of_msgs(&msgs).await?;
        let mut to_save = Vec::new();
        let mut to_invite = Vec::new();
        let mut to_download = Vec::new();
        let mut to_upload = Vec::new();
        let mut users_to_contact = Vec::new();
        for msg in msgs {
            if !self.filter_msg_by_user(&users, &msg) {
                continue;
            }
            if !users.contains_key(&msg.header.sender.into()) {
                users_to_contact.push((msg.header.sender, msg.header.sender_pub_key.clone()));
            }
            match msg.body {
                ApiMsgBody::ReqFilePart(body) => {
                    let key = msg.header.sender_pub_key;
                    to_upload.push((msg.header.sender, key, body))
                }
                ApiMsgBody::SendFilePart(body) => to_download.push((msg.header.sender, body)),
                ApiMsgBody::AddToChat(mut body) => {
                    let key = self.ctx.key_ser.deserialize(&msg.header.sender_pub_key)?;
                    let contact = ClientContact::new(msg.header.sender, &key);
                    body.other_participants.push(contact);
                    to_invite.push(body);
                }
                _ => to_save.push(msg),
            }
        }
        self.contact_users(users_to_contact).await?;
        self.save_msgs_to_base(to_save).await?;
        self.update_chats(&me, to_invite).await?;
        self.download_blocks(to_download).await?;
        self.upload_blocks(&servers, &me, to_upload).await?;
        Ok(())
    }

    async fn upload_blocks(
        &self,
        servers: &Arc<Vec<ServerContact>>,
        me: &Arc<Me>,
        requests: Vec<(Uuid, String, Box<ApiMsgBodyReqFilePart>)>,
    ) -> Result<(), MsgDispatcherError> {
        for (user_id, key, request) in requests {
            let res = self
                .ctx
                .uploader
                .upload_file_part(
                    servers.clone(),
                    me.clone(),
                    user_id.into(),
                    key,
                    request.file_id.into(),
                    request.part,
                )
                .await;
            let err = match res {
                Ok(_) => continue,
                Err(err) => err,
            };
            let (level, txt) = match err {
                FileUploaderError::FileForUserNotFound => (
                    Level::Debug,
                    format!("File({}) for user({}) not found", request.file_id, user_id),
                ),
                FileUploaderError::CanNotReadFilePart(details) => (
                    Level::Err,
                    format!(
                        "Can not read file({}) for upload: {}",
                        request.file_id, details
                    ),
                ),
                other => return Err(other.into()),
            };
            self.ctx.logger.log(level, &txt);
        }
        Ok(())
    }

    async fn get_senders_of_msgs(
        &self,
        msgs: &[ApiMsg],
    ) -> DbResult<HashMap<Id, (Arc<RsaPublicKey>, UserStatus)>> {
        let id_list = msgs
            .iter()
            .map(|m| m.header.sender.into())
            .collect::<Vec<_>>();
        let result = self
            .ctx
            .storage
            .lock()
            .await
            .users()
            .get_by_uid(&id_list)?
            .into_iter()
            .map(|usr| (usr.id, (usr.pub_key, usr.status)))
            .collect::<HashMap<_, _>>();
        Ok(result)
    }

    async fn save_msgs_to_base(&self, msgs: Vec<ApiMsg>) -> Result<(), MsgDispatcherError> {
        if msgs.is_empty() {
            return Ok(());
        }
        let id_list = msgs.iter().map(|m| m.header.id.into()).collect::<Vec<Id>>();
        let storage = self.ctx.storage.lock().await;
        let msg_repo = storage.messages();
        let already_in_base = msg_repo.get_already_in_base(&id_list)?;
        let mut filtered = Vec::new();
        for msg in msgs {
            if already_in_base.contains(&msg.header.id.into()) {
                continue;
            }
            let msg_id = msg.header.id;
            if let Ok(val) = self.convert_to_local(msg) {
                filtered.push(val)
            } else {
                let txt = format!("Invalid message found: {:?}", msg_id);
                self.ctx.logger.log(Level::Err, &txt);
            }
        }
        msg_repo.create_messages(&filtered)?;
        Ok(())
    }

    async fn update_chats(
        &self,
        me: &Me,
        updates: VecBox<ApiMsgBodyAddToChat>,
    ) -> Result<(), MsgDispatcherError> {
        if updates.is_empty() {
            return Ok(());
        }
        let storage = self.ctx.storage.lock().await;
        let chat_repo = storage.chats();
        let user_repo = storage.users();
        for update in updates {
            let users = Self::get_or_create_users(&*user_repo, me, &update.other_participants)?;
            let chat_id = ChatId::Chat(update.chat_id.into());
            match chat_repo.get_chat(&chat_id) {
                Ok(_) => chat_repo.add_participants(&chat_id, &users)?,
                Err(DbError::NotFound) => chat_repo.create_chat(Chat {
                    id: chat_id,
                    name: update.chat_name,
                    users,
                })?,
                Err(err) => return Err(err.into()),
            }
        }
        Ok(())
    }

    async fn contact_users(&self, users: Vec<(Uuid, String)>) -> Result<(), MsgDispatcherError> {
        if users.is_empty() {
            return Ok(());
        }
        let storage = self.ctx.storage.lock().await;
        let repo = storage.users();
        for (id, key_src) in users {
            let key = self.ctx.key_ser.deserialize(&key_src)?;
            repo.contact(&ClientContact::new(id, &key), UserStatus::New)?;
        }
        Ok(())
    }

    async fn download_blocks(
        &self,
        blocks: Vec<(Uuid, Box<ApiMsgBodySendFilePart>)>,
    ) -> Result<(), MsgDispatcherError> {
        for (user_id, block) in blocks {
            let res = self.ctx.downloader.add_block(user_id.into(), block).await;
            if let Err(FileDownloaderError::CanNotWriteFile) = res {
                self.ctx
                    .logger
                    .log(Level::Err, "Can not download file: Can not save")
            }
        }
        Ok(())
    }

    fn filter_msg_by_user(
        &self,
        users: &HashMap<Id, (Arc<RsaPublicKey>, UserStatus)>,
        msg: &ApiMsg,
    ) -> bool {
        let id = msg.header.sender.into();
        let key = match users.get(&id) {
            None => return true,
            Some((_, UserStatus::Banned)) => return false,
            Some((_, UserStatus::New)) => return true,
            Some((key, UserStatus::Trusted)) => key,
        };
        self.ctx.key_ser.serialize(key) == msg.header.sender_pub_key
    }

    fn convert_to_local(&self, msg: ApiMsg) -> Result<NewLocalMsg, MsgDispatcherError> {
        let mut chat = ChatId::User(msg.header.sender.into());
        let mut text = "".to_string();
        let mut attached = LocalMsgAttached::Nothing;
        match msg.body {
            ApiMsgBody::Nothing => (),
            ApiMsgBody::Text(body) => {
                text = body.text;
                if let Some(id) = body.chat {
                    chat = ChatId::Chat(id.into())
                }
            }
            ApiMsgBody::DeclareFile(body) => {
                text = body.name;
                attached = LocalMsgAttached::FileId {
                    id: body.file_id.into(),
                    size: body.size,
                    parts: body.parts,
                }
            }
            ApiMsgBody::AddToChat(_) | ApiMsgBody::ReqFilePart(_) | ApiMsgBody::SendFilePart(_) => {
                unreachable!()
            }
        };
        let key = self.ctx.key_ser.deserialize(&msg.header.sender_pub_key)?;
        let sender_key_code = Some(self.ctx.crypto.get_key_code(&key));
        Ok(NewLocalMsg {
            sender: Some(msg.header.sender.into()),
            receiver: None,
            chat,
            uuid: msg.header.id.into(),
            sender_key_code,
            text,
            attached,
            sent: true,
            received: true,
            read: false,
            ts: msg.header.ts,
            local_uniq_id: None,
        })
    }

    fn get_or_create_users(
        repo: &dyn IUsersRepo,
        me: &Me,
        participants: &[ClientContact],
    ) -> Result<Vec<Id>, MsgDispatcherError> {
        let id_list = participants
            .iter()
            .map(|p| p.id.into())
            .collect::<Vec<Id>>();
        let known = repo
            .get_by_uid(&id_list)?
            .into_iter()
            .map(|user| user.id)
            .collect::<HashSet<_>>();
        let mut result = Vec::new();
        for user in participants {
            let id: Id = user.id.into();
            if id == me.id {
                continue;
            }
            result.push(id);
            if known.contains(&id) {
                continue;
            } else {
                repo.contact(user, UserStatus::New)?
            }
        }
        Ok(result)
    }
}

impl<
        L: ILogger + 'static,
        S: IStorage + 'static,
        KeySer: IStrSerializer<RsaPublicKey> + 'static,
        Dw: IFileDownloader + 'static,
        Up: IFileUploader + 'static,
        Cr: ICryptoOps + 'static,
    > IMsgDispatcher for MsgDispatcher<L, S, KeySer, Dw, Up, Cr>
{
    fn dispatch_msgs(
        &self,
        servers: Arc<Vec<ServerContact>>,
        me: Arc<Me>,
        msgs: Vec<ApiMsg>,
    ) -> DynFutRes<(), MsgDispatcherError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_dispatch_msgs(servers, me, msgs),
        )
    }
}
