use crate::entities::common::Id;
use crate::entities::errors::FileDownloaderError;
use crate::entities::files::DownloadStatus;
use crate::proto::local::IStorage;
use crate::proto::net::{IFileDownloader, IServerClientFactory};
use crate::utils::time::get_now;
use futures_util::future::join_all;
use kakyoin_base::entities::api_msg::{
    ApiMsg, ApiMsgBody, ApiMsgBodyReqFilePart, ApiMsgBodySendFilePart, ApiMsgHeader,
};
use kakyoin_base::entities::common::{DynFut, DynFutRes};
use kakyoin_base::proto::common::IFS;
use kakyoin_base::proto::serializers::{IApiMsgPacker, IStrSerializer};
use rsa::RsaPublicKey;
use std::collections::HashMap;
use std::path::Path;
use std::sync::Arc;
use tokio::sync::Mutex;
use uuid::Uuid;

#[allow(clippy::type_complexity)]
pub struct FileDownloader<
    S: IStorage,
    FS: IFS,
    SCF: IServerClientFactory,
    Ser: IStrSerializer<RsaPublicKey>,
    P: IApiMsgPacker,
> {
    mtx: Arc<Mutex<Ctx<S, FS, SCF, Ser, P>>>,
}

struct Ctx<
    S: IStorage,
    FS: IFS,
    SCF: IServerClientFactory,
    Ser: IStrSerializer<RsaPublicKey>,
    P: IApiMsgPacker,
> {
    storage: S,
    fs: FS,
    factory: SCF,
    key_ser: Ser,
    packer: P,
    file_map: HashMap<(Id, Id), DwFile>,
}

struct DwFile {
    path_to_save: String,
    last_written_part: Option<u64>,
    max_parts: u64,
    parts_queue: HashMap<u64, Vec<u8>>,
    last_update: u64,
}

impl<
        S: IStorage,
        FS: IFS,
        SCF: IServerClientFactory,
        Ser: IStrSerializer<RsaPublicKey>,
        P: IApiMsgPacker,
    > FileDownloader<S, FS, SCF, Ser, P>
{
    pub fn new(storage: S, fs: FS, factory: SCF, key_ser: Ser, packer: P) -> Self {
        let ctx = Ctx {
            storage,
            fs,
            factory,
            key_ser,
            packer,
            file_map: Default::default(),
        };
        Self {
            mtx: Arc::new(Mutex::new(ctx)),
        }
    }

    async fn a_start_loading(
        self,
        user_id: Id,
        file_id: Id,
        max_parts: u64,
        path_to_save: String,
    ) -> Result<(), FileDownloaderError> {
        let mut ctx = self.mtx.lock().await;
        if ctx.file_map.get(&(user_id, file_id)).is_some() {
            return Ok(());
        }
        ctx.send_req_msg(user_id, file_id, 0).await?;
        let meta = DwFile {
            path_to_save,
            last_written_part: None,
            max_parts,
            parts_queue: Default::default(),
            last_update: get_now(),
        };
        ctx.file_map.insert((user_id, file_id), meta);
        Ok(())
    }

    async fn a_retry_block_request(
        self,
        user_id: Id,
        file_id: Id,
    ) -> Result<(), FileDownloaderError> {
        let ctx = self.mtx.lock().await;
        let dw_file = match ctx.file_map.get(&(user_id, file_id)) {
            Some(val) => val,
            _ => return Err(FileDownloaderError::FileNotFound),
        };
        let next_part = match dw_file.last_written_part {
            None => 0,
            Some(ref val) => val + 1,
        };
        if next_part >= dw_file.max_parts {
            return Ok(());
        }
        ctx.send_req_msg(user_id, file_id, next_part).await
    }

    async fn a_get_loading_status(self, user_id: Id, file_id: Id) -> Option<DownloadStatus> {
        let ctx = self.mtx.lock().await;
        let dw_file = ctx.file_map.get(&(user_id, file_id))?;
        let saved = match dw_file.last_written_part {
            None => 0,
            Some(val) => val + 1,
        };
        Some(DownloadStatus {
            last_update: dw_file.last_update,
            completed: saved == dw_file.max_parts,
            progress: (saved as f64) / (dw_file.max_parts as f64),
        })
    }

    async fn a_add_block(
        self,
        user_id: Id,
        data: Box<ApiMsgBodySendFilePart>,
    ) -> Result<(), FileDownloaderError> {
        let mut ctx = self.mtx.lock().await;
        ctx.add_block(user_id, data).await
    }
}

impl<
        S: IStorage,
        FS: IFS,
        SCF: IServerClientFactory,
        Ser: IStrSerializer<RsaPublicKey>,
        P: IApiMsgPacker,
    > Ctx<S, FS, SCF, Ser, P>
{
    async fn add_block(
        &mut self,
        user_id: Id,
        data: Box<ApiMsgBodySendFilePart>,
    ) -> Result<(), FileDownloaderError> {
        let dw_file = match self.file_map.get_mut(&(user_id, data.file_id.into())) {
            None => return Ok(()),
            Some(val) => val,
        };
        let expected_part = match dw_file.last_written_part {
            None => 0,
            Some(ref val) => *val + 1,
        };
        if data.part < expected_part {
            return Ok(());
        }
        if data.part > expected_part {
            dw_file.parts_queue.insert(data.part, data.data);
            return Ok(());
        }
        dw_file
            .write_part(&self.fs, &data.data)
            .map_err(|_| FileDownloaderError::CanNotWriteFile)?;
        dw_file.last_written_part = Some(data.part);
        dw_file
            .write_rest(&self.fs)
            .map_err(|_| FileDownloaderError::CanNotWriteFile)?;
        let last_written = match dw_file.last_written_part {
            None => unreachable!(),
            Some(ref val) => *val,
        };
        if last_written + 1 < dw_file.max_parts {
            self.send_req_msg(user_id, data.file_id.into(), last_written + 1)
                .await?
        }
        Ok(())
    }

    async fn send_req_msg(
        &self,
        user_id: Id,
        file_id: Id,
        part: u64,
    ) -> Result<(), FileDownloaderError> {
        let s_cnn_list;
        let me;
        let user;
        {
            let storage = self.storage.lock().await;
            s_cnn_list = storage.servers().get_all()?;
            me = storage.settings().get_me()?;
            let mut opt_user = storage.users().get_by_uid(&[user_id])?;
            if opt_user.is_empty() {
                return Err(FileDownloaderError::UserNotFound);
            }
            user = opt_user.remove(0);
        }
        let mut futs = Vec::new();
        let sender_pub_key = self.key_ser.serialize(&me.pub_key);
        let receiver_key_code = user.pub_key_code.as_bytes().to_vec();
        let body = Box::new(ApiMsgBodyReqFilePart {
            file_id: file_id.into(),
            part,
        });
        let msg = ApiMsg {
            header: ApiMsgHeader {
                id: Uuid::new_v4(),
                sender: me.id.into(),
                sender_pub_key,
                receiver: user_id.into(),
                receiver_key_code,
                ts: get_now(),
            },
            body: ApiMsgBody::ReqFilePart(body),
        };
        let packed = self.packer.pack(msg, Some(&user.pub_key), &me.priv_key);
        for s_cnn in s_cnn_list.iter() {
            let srv = self.factory.produce(me.clone(), s_cnn)?;
            futs.push(srv.send_msgs(vec![packed.clone()]));
        }
        join_all(futs).await;
        Ok(())
    }
}

impl DwFile {
    fn write_part<F: IFS>(&mut self, fs: &F, data: &[u8]) -> std::io::Result<()> {
        fs.append_file(Path::new(&self.path_to_save), data)?;
        self.last_update = get_now();
        Ok(())
    }

    fn write_rest<F: IFS>(&mut self, fs: &F) -> std::io::Result<()> {
        loop {
            let part = self.last_written_part.unwrap();
            let data = match self.parts_queue.remove(&(part + 1)) {
                None => break,
                Some(val) => val,
            };
            self.write_part(fs, &data)?;
        }
        Ok(())
    }
}

impl<
        S: IStorage + 'static,
        FS: IFS + 'static,
        SCF: IServerClientFactory + 'static,
        Ser: IStrSerializer<RsaPublicKey> + 'static,
        P: IApiMsgPacker + 'static,
    > IFileDownloader for FileDownloader<S, FS, SCF, Ser, P>
{
    fn start_loading(
        &self,
        user_id: Id,
        file_id: Id,
        max_parts: u64,
        path_to_save: String,
    ) -> DynFutRes<(), FileDownloaderError> {
        Box::pin(
            Self {
                mtx: self.mtx.clone(),
            }
            .a_start_loading(user_id, file_id, max_parts, path_to_save),
        )
    }

    fn retry_block_request(&self, user_id: Id, file_id: Id) -> DynFutRes<(), FileDownloaderError> {
        Box::pin(
            Self {
                mtx: self.mtx.clone(),
            }
            .a_retry_block_request(user_id, file_id),
        )
    }

    fn get_loading_status(&self, user_id: Id, file_id: Id) -> DynFut<Option<DownloadStatus>> {
        Box::pin(
            Self {
                mtx: self.mtx.clone(),
            }
            .a_get_loading_status(user_id, file_id),
        )
    }

    fn add_block(
        &self,
        user_id: Id,
        data: Box<ApiMsgBodySendFilePart>,
    ) -> DynFutRes<(), FileDownloaderError> {
        Box::pin(
            Self {
                mtx: self.mtx.clone(),
            }
            .a_add_block(user_id, data),
        )
    }
}
