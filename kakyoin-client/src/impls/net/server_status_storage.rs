use crate::entities::common::ServerStatus;
use crate::proto::net::IServerStatusStorage;
use kakyoin_base::entities::common::DynFut;
use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::Mutex;

#[derive(Default)]
pub struct ServerStatusStorage {
    mtx: Arc<Mutex<HashMap<String, ServerStatus>>>,
}

impl IServerStatusStorage for ServerStatusStorage {
    fn set_statuses(&self, updates: Vec<(String, ServerStatus)>) -> DynFut<()> {
        let mtx = self.mtx.clone();
        Box::pin(async move {
            let mut data = mtx.lock().await;
            for (key, val) in updates {
                data.insert(key, val);
            }
        })
    }

    fn get_statuses(&self) -> DynFut<HashMap<String, ServerStatus>> {
        let mtx = self.mtx.clone();
        Box::pin(async move {
            let res: HashMap<String, ServerStatus> = mtx.lock().await.clone();
            res
        })
    }
}
