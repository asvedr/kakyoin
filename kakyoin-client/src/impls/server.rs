use std::convert::Infallible;
use std::io;
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::Arc;

use hyper::header::HeaderValue;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server};
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;

use crate::entities::errors::ApiError;
use crate::proto::common::{IApiHandlerWrapped, IService};

pub struct ApiServer<L: ILogger, H: IApiHandlerWrapped + 'static> {
    ctx: Arc<Ctx<L, H>>,
}

struct Ctx<L: ILogger, H: IApiHandlerWrapped + 'static> {
    addr: SocketAddr,
    logger: L,
    handlers: &'static [H],
}

unsafe impl<L: ILogger, H: IApiHandlerWrapped + 'static> Send for Ctx<L, H> {}
unsafe impl<L: ILogger, H: IApiHandlerWrapped + 'static> Sync for Ctx<L, H> {}

impl<L: ILogger, H: IApiHandlerWrapped + 'static> Clone for ApiServer<L, H> {
    fn clone(&self) -> Self {
        Self {
            ctx: self.ctx.clone(),
        }
    }
}

impl<L: ILogger + 'static, H: IApiHandlerWrapped + 'static> ApiServer<L, H> {
    pub fn new(logger: L, handlers: &'static [H], addr: &str) -> Self {
        Self {
            ctx: Arc::new(Ctx {
                addr: SocketAddr::from_str(addr).unwrap(),
                logger,
                handlers,
            }),
        }
    }
}

fn make_response(status: u16, body: Body) -> Response<Body> {
    match Response::builder().status(status).body(body) {
        Ok(val) => val,
        Err(err) => panic!("Can not build response: {:?}", err),
    }
}

impl<L: ILogger + 'static, H: IApiHandlerWrapped + 'static> ApiServer<L, H> {
    async fn a_run(self) -> Result<(), io::Error> {
        let addr = self.ctx.addr;
        let maker = make_service_fn(|_cnn| {
            let slf = self.clone();
            async move {
                let f = service_fn(move |req| {
                    let slf = slf.clone();
                    async move { slf.handle(req).await }
                });
                Ok::<_, Infallible>(f)
            }
        });
        let msg = format!("run api server on: {:?}", addr);
        self.ctx.logger.log(Level::Debug, &msg);
        let server = Server::bind(&addr).serve(maker);
        if let Err(err) = server.await {
            let msg = format!("api server failed: {:?}", err);
            self.ctx.logger.log(Level::Err, &msg);
            return Err(io::Error::new(io::ErrorKind::Other, err));
        }
        Ok(())
    }

    async fn handle(self, req: Request<Body>) -> Result<Response<Body>, Infallible> {
        if req.method() == Method::OPTIONS {
            return Ok(self.options_response());
        }
        let path = req.uri().path();
        let mut norm_path = path.to_string();
        if !path.ends_with('/') {
            norm_path.push('/');
        }
        let method = req.method();
        let log_text = format!("api server: {:?} {}", method, path);
        let opt_handler = self
            .ctx
            .handlers
            .iter()
            .find(|h| h.method() == method && h.path() == norm_path);
        if let Some(handler) = opt_handler {
            let res = handler.handle(req).await;
            let res = self.proc_result(log_text, res)?;
            return Ok(self.add_cors(res));
        }
        self.ctx
            .logger
            .log(Level::Debug, &format!("{} => 404", log_text));
        Ok(make_response(404, Body::from("Path not found")))
    }

    fn options_response(&self) -> Response<Body> {
        self.add_cors(make_response(204, Body::empty()))
    }

    fn add_cors(&self, mut response: Response<Body>) -> Response<Body> {
        let headers = response.headers_mut();
        headers.insert("Access-Control-Allow-Origin", HeaderValue::from_static("*"));
        headers.insert(
            "Access-Control-Allow-Methods",
            HeaderValue::from_static("GET, POST, PATCH, DELETE"),
        );
        headers.insert(
            "Access-Control-Allow-Headers",
            HeaderValue::from_static("*"),
        );
        headers.insert("Access-Control-Max-Age", HeaderValue::from_static("1000"));
        response
    }

    fn proc_result(
        &self,
        log_text: String,
        res: Result<Vec<u8>, ApiError>,
    ) -> Result<Response<Body>, Infallible> {
        let err = match res {
            Ok(val) => {
                self.ctx
                    .logger
                    .log(Level::Debug, &format!("{} => OK", log_text));
                return Ok(Response::new(Body::from(val)));
            }
            Err(val) => val,
        };
        let js = format!(
            r#"{op}"code": {code:?}, "details": {det:?}{cl}"#,
            op = '{',
            cl = '}',
            code = err.code,
            det = err.details.as_ref().map(|s| -> &str { s }).unwrap_or(""),
        );
        self.ctx
            .logger
            .log(Level::Debug, &format!("{} => {}", log_text, js));
        Ok(make_response(err.status, Body::from(js)))
    }
}

impl<L: ILogger + 'static, H: IApiHandlerWrapped + 'static> IService for ApiServer<L, H> {
    fn run(&mut self) -> DynFutRes<(), io::Error> {
        Box::pin(self.clone().a_run())
    }
}
