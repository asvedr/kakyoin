use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::common::ICryptoOps;
use rsa::RsaPrivateKey;
use std::sync::Arc;

use crate::entities::common::{Id, InitStatus, Me};
use crate::entities::errors::InitializerError;
use crate::proto::local::{IAuthTokenManager, IInitializer, IStorage};
use crate::utils::password::hash_pwd;

pub struct Initializer<S: IStorage, CO: ICryptoOps, TM: IAuthTokenManager> {
    ctx: Arc<Ctx<S, CO, TM>>,
}

struct Ctx<S: IStorage, CO: ICryptoOps, TM: IAuthTokenManager> {
    storage: S,
    crypto: CO,
    token_manager: TM,
}

impl<S: IStorage, CO: ICryptoOps, TM: IAuthTokenManager> Initializer<S, CO, TM> {
    pub fn new(storage: S, crypto: CO, token_manager: TM) -> Self {
        Self {
            ctx: Arc::new(Ctx {
                storage,
                crypto,
                token_manager,
            }),
        }
    }

    async fn a_check_init(self) -> Result<(), InitializerError> {
        let storage = self.ctx.storage.lock().await;
        let status = storage.settings().check_inited()?;
        if matches!(status, InitStatus::Done) {
            Ok(())
        } else {
            Err(InitializerError::NotInited)
        }
    }

    async fn a_init(
        self,
        id: Option<Id>,
        priv_key: Option<RsaPrivateKey>,
        name: String,
        description: String,
        password: Option<String>,
    ) -> Result<(), InitializerError> {
        let id = id.unwrap_or_else(Id::new);
        let priv_key = match priv_key {
            None => self.ctx.crypto.gen_priv_key()?,
            Some(val) => val,
        };
        let me = Me {
            id,
            pub_key: priv_key.to_public_key(),
            priv_key,
        };
        self.ctx.token_manager.set_active(password.is_some()).await;
        let storage = self.ctx.storage.lock().await;
        let settings = storage.settings();
        settings.set_me(&me)?;
        settings.set_name(&name)?;
        settings.set_description(&description)?;
        settings.set_pwd_hash(password.map(|p| hash_pwd(&p)))?;
        Ok(())
    }
}

impl<S: IStorage + 'static, CO: ICryptoOps + 'static, TM: IAuthTokenManager + 'static> IInitializer
    for Initializer<S, CO, TM>
{
    fn check_init(&self) -> DynFutRes<(), InitializerError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_check_init(),
        )
    }

    fn init(
        &self,
        id: Option<Id>,
        priv_key: Option<RsaPrivateKey>,
        name: String,
        description: String,
        password: Option<String>,
    ) -> DynFutRes<(), InitializerError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_init(id, priv_key, name, description, password),
        )
    }
}
