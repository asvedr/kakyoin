use crate::entities::common::Id;
use crate::impls::tasks::download_file;
use crate::proto::local::ITaskScheduler;
use atex::{AtexError, ITaskManager, NewTask, TaskId, TaskStatus};
use kakyoin_base::entities::common::DynFutRes;
use std::sync::Arc;

#[derive(Clone)]
pub struct TaskScheduler {
    pub manager: Arc<dyn ITaskManager>,
}

unsafe impl Send for TaskScheduler {}
unsafe impl Sync for TaskScheduler {}

impl ITaskScheduler for TaskScheduler {
    fn schedule_task(&self, task: NewTask) -> DynFutRes<TaskId, AtexError> {
        self.manager.create_task(task)
    }

    fn schedule_periodic(&self, name: &str) -> DynFutRes<(), AtexError> {
        self.manager.schedule_periodic_now(name)
    }

    fn get_task_status(&self, task_id: TaskId) -> DynFutRes<TaskStatus, AtexError> {
        let fut = self.manager.get(task_id);
        Box::pin(async move {
            match fut.await {
                Ok(Some(task)) => Ok(task.status),
                Ok(None) => Err(AtexError::InvalidId),
                Err(err) => Err(err),
            }
        })
    }

    fn download_file(
        &self,
        user_id: Id,
        file_id: Id,
        max_parts: u64,
        path_to_save: &str,
    ) -> DynFutRes<TaskId, AtexError> {
        download_file::spawn_task(&*self.manager, user_id, file_id, max_parts, path_to_save)
    }
}
