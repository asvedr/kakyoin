use rusqlite::ToSql;

use crate::entities::chat::{Chat, ChatId, ChatListItem, ChatShort};
use crate::entities::common::Id;
use crate::entities::errors::{DbError, DbResult};
use crate::impls::local::db::proto::ILock;
use crate::proto::local::IChatRepo;

pub struct ChatRepo<L: ILock> {
    pub lock: L,
}

impl<L: ILock> IChatRepo for ChatRepo<L> {
    fn create_chat(&self, chat: Chat) -> DbResult<()> {
        self.lock.cnn().execute(
            "INSERT OR IGNORE INTO chats (id, name, banned) VALUES (?, ?, FALSE)",
            (&chat.id, &chat.name),
        )?;
        if matches!(chat.id, ChatId::Chat(_)) {
            self.add_participants(&chat.id, &chat.users)?;
        }
        Ok(())
    }

    fn get_chat(&self, id: &ChatId) -> DbResult<Chat> {
        let query = "SELECT name FROM chats WHERE id = ? LIMIT 1";
        let mut stm = self.lock.cnn().prepare(query)?;
        let name = stm.query_row([id], |r| r.get(0))?;
        let users = self.get_participants(id)?;
        Ok(Chat {
            id: id.clone(),
            name,
            users,
        })
    }

    fn set_name(&self, id: &ChatId, name: &str) -> DbResult<()> {
        let query = "UPDATE chats SET name = ? WHERE id = ?";
        self.lock.cnn().execute(query, (name, id))?;
        Ok(())
    }

    fn add_participants(&self, chat_id: &ChatId, user_id_list: &[Id]) -> DbResult<()> {
        if user_id_list.is_empty() {
            return Ok(());
        }
        if matches!(chat_id, ChatId::User(_)) {
            return Err(DbError::Conflict);
        }
        let query = format!(
            "INSERT OR IGNORE INTO user_chat (user_id, chat_id) VALUES {}",
            vec!["(?,?)"; user_id_list.len()].join(",")
        );
        let mut params: Vec<&dyn ToSql> = Vec::with_capacity(user_id_list.len() * 2);
        for uid in user_id_list {
            params.push(uid);
            params.push(&chat_id);
        }
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        Ok(())
    }

    fn get_participants(&self, id: &ChatId) -> DbResult<Vec<Id>> {
        if let ChatId::User(id) = id {
            return Ok(vec![*id]);
        }
        let query = "SELECT user_id FROM user_chat WHERE chat_id = ? ORDER BY user_id";
        let mut stm = self.lock.cnn().prepare(query)?;
        let rows = stm.query_map([id], |r| r.get(0))?;
        let mut result = Vec::new();
        for row in rows {
            result.push(row?);
        }
        Ok(result)
    }

    fn get_short_info_for_all(&self) -> DbResult<Vec<ChatShort>> {
        let query = r#"
            WITH results AS (
                SELECT
                    c.id,
                    c.name,
                    COALESCE(m.text, '') AS text,
                    COALESCE(m.ts, 0) AS ts,
                    COALESCE(m.read, TRUE) AS read
                FROM chats AS c LEFT OUTER JOIN msg AS m ON c.id = m.chat
                WHERE c.banned = FALSE
                    AND (m.last_in_chat = TRUE OR m.last_in_chat IS NULL)
            ) SELECT id, name, text, read FROM results ORDER BY read ASC, ts DESC, id
        "#;
        let mut stm = self.lock.cnn().prepare(query)?;
        let chats = stm
            .query_map((), |r| {
                Ok(ChatShort {
                    id: r.get::<_, ChatId>("id")?,
                    name: r.get("name")?,
                    last_msg: r.get("text")?,
                    read: r.get("read")?,
                })
            })?
            .collect::<Result<Vec<ChatShort>, _>>()?;
        Ok(chats)
    }

    fn get_full_list(&self) -> DbResult<Vec<ChatListItem>> {
        let query = "SELECT id, name, banned FROM chats ORDER BY id";
        let mut stm = self.lock.cnn().prepare(query)?;
        let chats = stm
            .query_map((), |r| {
                Ok(ChatListItem {
                    id: r.get::<_, ChatId>(0)?,
                    name: r.get(1)?,
                    banned: r.get(2)?,
                })
            })?
            .collect::<Result<Vec<ChatListItem>, _>>()?;
        Ok(chats)
    }

    fn ban_chat(&self, id: &ChatId) -> DbResult<()> {
        self.lock
            .cnn()
            .execute("UPDATE chats SET banned = TRUE WHERE id = ?", [id])?;
        self.lock
            .cnn()
            .execute("DELETE FROM msg WHERE chat = ?", [id])?;
        Ok(())
    }

    fn delete_chat(&self, id: &ChatId) -> DbResult<()> {
        self.lock
            .cnn()
            .execute("DELETE FROM chats WHERE id = ?", [id])?;
        Ok(())
    }
}
