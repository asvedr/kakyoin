use crate::entities::common::{Id, InitStatus, Me};
use crate::entities::errors::{DbError, DbResult};
use crate::impls::local::db::proto::{ICache, ILock};
use crate::proto::local::{ISettingsRepo, IUserMetaHashCalculator};
use kakyoin_base::entities::server_call::ApiUserMeta;
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::RsaPrivateKey;
use std::collections::{HashMap, HashSet};
use std::str::FromStr;
use std::sync::Arc;

pub struct SettingsRepo<L: ILock, Ser: IStrSerializer<RsaPrivateKey>, HC: IUserMetaHashCalculator> {
    pub lock: L,
    pub key_ser: Ser,
    pub hasher: HC,
}

const KEY_NAME: &str = "name";
const KEY_DESCRIPTION: &str = "descr";
const KEY_PHOTO: &str = "photo";
const KEY_ID: &str = "id";
const KEY_PRIV_KEY: &str = "priv_key";
const KEY_PWD_HASH: &str = "pwd_hash";
const CACHE_KEY_ME: &str = "settings:me";
const CACHE_KEY_META: &str = "settings:meta";
const CACHE_KEY_PWD_HASH: &str = "settings:pwdhash";

impl<L: ILock, Ser: IStrSerializer<RsaPrivateKey>, HC: IUserMetaHashCalculator> ISettingsRepo
    for SettingsRepo<L, Ser, HC>
{
    fn get_me(&self) -> DbResult<Arc<Me>> {
        if let Some(me) = self.lock.cache().get(CACHE_KEY_ME) {
            return Ok(me);
        }
        let cnn = self.lock.cnn();
        let mut stm = cnn.prepare("SELECT key, val FROM settings WHERE key IN (?, ?)")?;
        let rows = stm.query_map([&KEY_ID, &KEY_PRIV_KEY], |row| {
            Ok((row.get(0)?, row.get(1)?))
        })?;
        let pairs = rows.collect::<Result<HashMap<String, String>, _>>()?;
        let id = Id::from_str(pairs.get(KEY_ID).ok_or(DbError::NotFound)?)
            .map_err(|_| DbError::Internal("invalid uuid".to_string()))?;
        let priv_key = pairs.get(KEY_PRIV_KEY).ok_or(DbError::NotFound)?;
        let priv_key = self
            .key_ser
            .deserialize(priv_key)
            .map_err(|_| DbError::Internal("Invalid priv key".to_string()))?;
        let pub_key = priv_key.to_public_key();
        let me = Arc::new(Me {
            id,
            pub_key,
            priv_key,
        });
        let copy: Arc<Me> = me.clone();
        self.lock.cache().put(CACHE_KEY_ME, copy);
        Ok(me)
    }

    fn set_me(&self, me: &Me) -> DbResult<()> {
        self.lock.cache().del(CACHE_KEY_ME);
        let id = me.id.to_string();
        let key = self.key_ser.serialize(&me.priv_key);
        self.lock.cnn().execute(
            "INSERT OR REPLACE INTO settings (key, val) VALUES (?, ?), (?, ?)",
            (&KEY_ID, &id, &KEY_PRIV_KEY, &key),
        )?;
        Ok(())
    }

    fn set_name(&self, val: &str) -> DbResult<()> {
        self.lock.cache().del(CACHE_KEY_META);
        self.lock.cnn().execute(
            "INSERT OR REPLACE INTO settings (key, val) VALUES (?, ?)",
            (&KEY_NAME, &val),
        )?;
        Ok(())
    }

    fn set_description(&self, val: &str) -> DbResult<()> {
        self.lock.cache().del(CACHE_KEY_META);
        self.lock.cnn().execute(
            "INSERT OR REPLACE INTO settings (key, val) VALUES (?, ?)",
            (&KEY_DESCRIPTION, &val),
        )?;
        Ok(())
    }

    fn set_photo(&self, txt: &str) -> DbResult<()> {
        self.lock.cache().del(CACHE_KEY_META);
        self.lock.cnn().execute(
            "INSERT OR REPLACE INTO settings (key, val) VALUES (?, ?)",
            (&KEY_PHOTO, &txt),
        )?;
        Ok(())
    }

    fn get_meta(&self) -> DbResult<Arc<ApiUserMeta>> {
        if let Some(me) = self.lock.cache().get(CACHE_KEY_META) {
            return Ok(me);
        }
        let query = "SELECT key, val FROM settings WHERE key in (?, ?, ?)";
        let mut stm = self.lock.cnn().prepare(query)?;
        let mut rows = stm
            .query_map([KEY_NAME, KEY_DESCRIPTION, KEY_PHOTO], |row| {
                Ok((row.get(0)?, row.get(1)?))
            })?
            .collect::<Result<HashMap<String, String>, _>>()?;
        let name = rows.remove(KEY_NAME).unwrap_or_default();
        let description = rows.remove(KEY_DESCRIPTION).unwrap_or_default();
        let photo = rows.remove(KEY_PHOTO).unwrap_or_default();
        let mut meta = ApiUserMeta {
            name,
            description,
            photo,
            hash: "".to_string(),
        };
        let hash = self.hasher.calculate(&meta);
        meta.hash = hash;
        let arc_meta = Arc::new(meta);
        let copy: Arc<ApiUserMeta> = arc_meta.clone();
        self.lock.cache().put(CACHE_KEY_META, copy);
        Ok(arc_meta)
    }

    fn check_inited(&self) -> DbResult<InitStatus> {
        let cnn = self.lock.cnn();
        let mut stm = cnn.prepare("SELECT key FROM settings WHERE key IN (?, ?)")?;
        let rows = stm.query_map([&KEY_ID, &KEY_PRIV_KEY], |row| row.get(0))?;
        let pairs = rows.collect::<Result<HashSet<String>, _>>()?;
        if !pairs.contains(KEY_ID) {
            return Ok(InitStatus::NoId);
        }
        if !pairs.contains(KEY_PRIV_KEY) {
            return Ok(InitStatus::NoKey);
        }
        Ok(InitStatus::Done)
    }

    fn set_pwd_hash(&self, opt_hash: Option<String>) -> DbResult<()> {
        let cnn = self.lock.cnn();
        if let Some(hash) = opt_hash {
            cnn.execute(
                "INSERT OR REPLACE INTO settings (key, val) VALUES (?, ?)",
                (KEY_PWD_HASH, hash),
            )
        } else {
            cnn.execute("DELETE FROM settings WHERE key = ?", &[KEY_PWD_HASH])
        }?;
        self.lock.cache().del(CACHE_KEY_PWD_HASH);
        Ok(())
    }

    fn get_pwd_hash(&self) -> DbResult<Option<String>> {
        if let Some(val) = self.lock.cache().get(CACHE_KEY_PWD_HASH) {
            return Ok(val);
        }
        let cnn = self.lock.cnn();
        let mut stm = cnn.prepare("SELECT val FROM settings WHERE key = ?")?;
        let mut rows = stm.query_map(&[KEY_PWD_HASH], |row| row.get(0))?;
        let hash = match rows.next() {
            None => None,
            Some(res_hash) => Some(res_hash?),
        };
        self.lock.cache().put(CACHE_KEY_PWD_HASH, hash.clone());
        Ok(hash)
    }
}
