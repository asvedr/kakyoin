use crate::entities::errors::DbResult;
use crate::impls::local::db::proto::{IMigration, IMigrationManager};
use rusqlite::Connection;
use std::collections::HashSet;

pub struct MigrationManager;

fn get_applied(cnn: &mut Connection) -> DbResult<HashSet<String>> {
    let mut stm = cnn.prepare("SELECT name FROM migrations")?;
    let qmap = stm.query_map((), |row| row.get(0))?;
    Ok(qmap.collect::<Result<HashSet<String>, _>>()?)
}

fn set_applied(cnn: &mut Connection, name: String) -> DbResult<()> {
    cnn.execute("INSERT INTO migrations (name) VALUES (?)", [name])?;
    Ok(())
}

impl IMigrationManager for MigrationManager {
    fn migrate(&self, cnn: &mut Connection, migrations: &[Box<dyn IMigration>]) -> DbResult<()> {
        cnn.execute(
            "CREATE TABLE IF NOT EXISTS migrations (name TEXT PRIMARY KEY)",
            (),
        )?;
        let applied = get_applied(cnn)?;
        for migration in migrations {
            let name = migration.name().to_string();
            if applied.contains(&name) {
                continue;
            }
            migration.migrate(cnn)?;
            set_applied(cnn, name)?;
        }
        Ok(())
    }
}
