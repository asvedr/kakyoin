use crate::entities::errors::{DbError, DbResult};
use crate::impls::local::db::cache::Cache;
use crate::impls::local::db::chats_repo::ChatRepo;
use crate::impls::local::db::migration_manager::MigrationManager;
use crate::impls::local::db::migrations;
use crate::impls::local::db::msg_repo::MsgRepo;
use crate::impls::local::db::proto::{ICache, ILock, IMigrationManager};
use crate::impls::local::db::servers_repo::ServersRepo;
use crate::impls::local::db::settings_repo::SettingsRepo;
use crate::impls::local::db::shared_files_repo::SharedFilesRepo;
use crate::impls::local::db::users_repo::UsersRepo;
use crate::proto::local::{
    IChatRepo, ILocalMsgRepo, IServersRepo, ISettingsRepo, ISharedFilesRepo, IStorage,
    ISyncStorage, IUserMetaHashCalculator, IUsersRepo,
};
use kakyoin_base::entities::common::{DynFut, DynFutRes};
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::proto::common::ICryptoOps;
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::{RsaPrivateKey, RsaPublicKey};
use rusqlite::Connection;
use std::mem;
use std::sync::Arc;
use tokio::sync::{Mutex, MutexGuard};

#[derive(Clone)]
pub struct Storage<
    KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + 'static,
    SCntSer: IStrSerializer<ServerContact> + 'static,
    HC: IUserMetaHashCalculator + Clone + 'static,
    Cr: ICryptoOps + Clone + 'static,
> {
    mtx: Arc<Mutex<State<KeySer, SCntSer, HC, Cr>>>,
}

unsafe impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + 'static,
        SCntSer: IStrSerializer<ServerContact> + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > Send for Storage<KeySer, SCntSer, HC, Cr>
{
}
unsafe impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + 'static,
        SCntSer: IStrSerializer<ServerContact> + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > Sync for Storage<KeySer, SCntSer, HC, Cr>
{
}

#[derive(Clone)]
pub struct Lock<
    KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + 'static,
    SCntSer: IStrSerializer<ServerContact> + 'static,
    HC: IUserMetaHashCalculator + Clone + 'static,
    Cr: ICryptoOps + Clone + 'static,
> {
    #[allow(dead_code)]
    mtx: Arc<Mutex<State<KeySer, SCntSer, HC, Cr>>>,
    #[allow(clippy::type_complexity)]
    guard: Option<Arc<MutexGuard<'static, State<KeySer, SCntSer, HC, Cr>>>>,
}

unsafe impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + 'static,
        SCntSer: IStrSerializer<ServerContact> + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > Send for Lock<KeySer, SCntSer, HC, Cr>
{
}
unsafe impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + 'static,
        SCntSer: IStrSerializer<ServerContact> + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > Sync for Lock<KeySer, SCntSer, HC, Cr>
{
}

struct State<
    KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + 'static,
    SCntSer: IStrSerializer<ServerContact> + 'static,
    HC: IUserMetaHashCalculator + Clone + 'static,
    Cr: ICryptoOps + Clone + 'static,
> {
    cnn: Connection,
    cache: Cache,
    key_ser: KeySer,
    s_cnt_ser: SCntSer,
    hasher: HC,
    crypto: Cr,
}

impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + 'static,
        SCntSer: IStrSerializer<ServerContact> + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > Drop for Lock<KeySer, SCntSer, HC, Cr>
{
    fn drop(&mut self) {
        let _ = mem::replace(&mut self.guard, None);
    }
}

impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + Clone + 'static,
        SCntSer: IStrSerializer<ServerContact> + Clone + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > Storage<KeySer, SCntSer, HC, Cr>
{
    pub fn new(
        path: &str,
        key_ser: KeySer,
        s_cnt_ser: SCntSer,
        hasher: HC,
        crypto: Cr,
    ) -> DbResult<Self> {
        let cnn = Connection::open(path)?;
        let cache = Cache::default();
        let state = State {
            cnn,
            cache,
            key_ser,
            s_cnt_ser,
            hasher,
            crypto,
        };
        Ok(Self {
            mtx: Arc::new(Mutex::new(state)),
        })
    }

    async fn a_init(self) -> DbResult<()> {
        let mut state = self.mtx.lock().await;
        let migrations = migrations::all();
        MigrationManager.migrate(&mut state.cnn, &migrations)
    }

    async fn a_lock(self) -> Box<dyn ISyncStorage> {
        let guard = self.mtx.lock().await;
        let guard: MutexGuard<'static, State<KeySer, SCntSer, HC, Cr>> =
            unsafe { mem::transmute(guard) };
        Box::new(Lock {
            mtx: self.mtx.clone(),
            guard: Some(Arc::new(guard)),
        })
    }
}

impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + Clone + 'static,
        SCntSer: IStrSerializer<ServerContact> + Clone + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > IStorage for Storage<KeySer, SCntSer, HC, Cr>
{
    fn init(&self) -> DynFutRes<(), DbError> {
        Box::pin(self.clone().a_init())
    }

    fn lock(&self) -> DynFut<Box<dyn ISyncStorage>> {
        Box::pin(self.clone().a_lock())
    }
}

impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + Clone + 'static,
        SCntSer: IStrSerializer<ServerContact> + Clone + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > ILock for Lock<KeySer, SCntSer, HC, Cr>
{
    type Cache = Cache;

    fn cnn(&self) -> &Connection {
        match self.guard {
            None => unreachable!(),
            Some(ref state) => &state.cnn,
        }
    }

    fn cache(&self) -> &Self::Cache {
        match self.guard {
            None => unreachable!(),
            Some(ref state) => &state.cache,
        }
    }
}

impl<
        KeySer: IStrSerializer<RsaPrivateKey> + IStrSerializer<RsaPublicKey> + Clone + 'static,
        SCntSer: IStrSerializer<ServerContact> + Clone + 'static,
        HC: IUserMetaHashCalculator + Clone + 'static,
        Cr: ICryptoOps + Clone + 'static,
    > ISyncStorage for Lock<KeySer, SCntSer, HC, Cr>
{
    fn settings(&self) -> Box<dyn ISettingsRepo> {
        let key_ser;
        let hasher;
        match self.guard {
            Some(ref state) => {
                key_ser = state.key_ser.clone();
                hasher = state.hasher.clone();
            }
            _ => unreachable!(),
        };
        Box::new(SettingsRepo {
            lock: self.clone(),
            key_ser,
            hasher,
        })
    }

    fn servers(&self) -> Box<dyn IServersRepo> {
        let cnt_ser = match self.guard {
            Some(ref state) => state.s_cnt_ser.clone(),
            _ => unreachable!(),
        };
        Box::new(ServersRepo {
            lock: self.clone(),
            cnt_ser,
        })
    }

    fn users(&self) -> Box<dyn IUsersRepo> {
        let (key_ser, crypto) = match self.guard {
            Some(ref state) => (state.key_ser.clone(), state.crypto.clone()),
            _ => unreachable!(),
        };
        Box::new(UsersRepo {
            lock: self.clone(),
            key_ser,
            crypto,
        })
    }

    fn chats(&self) -> Box<dyn IChatRepo> {
        Box::new(ChatRepo { lock: self.clone() })
    }

    fn messages(&self) -> Box<dyn ILocalMsgRepo> {
        Box::new(MsgRepo { lock: self.clone() })
    }

    fn shared_files(&self) -> Box<dyn ISharedFilesRepo> {
        Box::new(SharedFilesRepo { lock: self.clone() })
    }

    fn clear_cache(&self, prefix: Option<&str>) {
        if let Some(p) = prefix {
            self.cache().del_all_with_prefix(p)
        } else {
            self.cache().del_all()
        }
    }
}
