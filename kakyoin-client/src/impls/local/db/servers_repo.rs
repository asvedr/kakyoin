use crate::entities::errors::{DbError, DbResult};
use crate::impls::local::db::proto::{ICache, ILock};
use crate::proto::local::IServersRepo;
use crate::utils::time::get_now;
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::proto::serializers::IStrSerializer;
use rusqlite::ToSql;
use std::mem;
use std::sync::Arc;

pub struct ServersRepo<L: ILock, CntSer: IStrSerializer<ServerContact>> {
    pub lock: L,
    pub cnt_ser: CntSer,
}

const CACHE_KEY: &str = "servers:all";

impl<L: ILock, CntSer: IStrSerializer<ServerContact>> IServersRepo for ServersRepo<L, CntSer> {
    fn add_servers(&self, contacts: Vec<ServerContact>) -> DbResult<()> {
        if contacts.is_empty() {
            return Ok(());
        }
        self.lock.cache().del(CACHE_KEY);
        let query = format!(
            "INSERT OR REPLACE INTO servers (addr, val, created_at) VALUES {}",
            vec!["(?,?,?)"; contacts.len()].join(",")
        );
        let mut data = Vec::new();
        let mut values: Vec<&dyn ToSql> = Vec::new();
        let now = get_now();
        for contact in contacts.iter() {
            data.push(self.cnt_ser.serialize(contact));
            values.push(&contact.addr);
            let ptr: &'static String = unsafe { mem::transmute(data.last().unwrap()) };
            values.push(ptr);
            values.push(&now);
        }
        self.lock.cnn().execute(&query, &values as &[&dyn ToSql])?;
        Ok(())
    }

    fn del_servers(&self, addrs: &[&str]) -> DbResult<()> {
        if addrs.is_empty() {
            return Ok(());
        }
        self.lock.cache().del(CACHE_KEY);
        let query = format!(
            "DELETE FROM servers WHERE addr IN ({})",
            vec!["?"; addrs.len()].join(","),
        );
        let params: Vec<&dyn ToSql> = addrs.iter().map(|s| -> &dyn ToSql { s }).collect();
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        Ok(())
    }

    fn get_all(&self) -> DbResult<Arc<Vec<ServerContact>>> {
        if let Some(res) = self.lock.cache().get(CACHE_KEY) {
            return Ok(res);
        }
        let mut stm = self
            .lock
            .cnn()
            .prepare("SELECT val FROM servers ORDER BY created_at, addr")?;
        let mut contacts = Vec::new();
        for res_val in stm.query_map((), |row| row.get(0))? {
            let val: String = res_val?;
            let server = self
                .cnt_ser
                .deserialize(&val)
                .map_err(|_| DbError::Internal("Invalid server value".to_string()))?;
            contacts.push(server);
        }
        let contacts = Arc::new(contacts);
        let contacts_copy: Arc<Vec<ServerContact>> = contacts.clone();
        self.lock.cache().put(CACHE_KEY, contacts_copy);
        Ok(contacts)
    }
}
