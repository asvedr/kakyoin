use crate::entities::errors::DbResult;
use rusqlite::Connection;
use std::any::Any;

pub trait IMigration {
    fn name(&self) -> &str;
    fn description(&self) -> &str;
    fn migrate(&self, cnn: &mut Connection) -> DbResult<()>;
}

pub trait IMigrationManager {
    fn migrate(&self, cnn: &mut Connection, migrations: &[Box<dyn IMigration>]) -> DbResult<()>;
}

pub trait ILock {
    type Cache: ICache;

    fn cnn(&self) -> &Connection;
    fn cache(&self) -> &Self::Cache;
}

pub trait ICache {
    fn put<T: Any + Clone>(&self, key: &str, value: T);
    fn get<T: Any + Clone>(&self, key: &str) -> Option<T>;
    fn del(&self, key: &str);
    fn del_all_with_prefix(&self, prefix: &str);
    fn del_all(&self);
}
