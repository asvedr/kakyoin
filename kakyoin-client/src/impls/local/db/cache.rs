use std::any::Any;
use std::cell::RefCell;
use std::collections::HashMap;

use crate::impls::local::db::proto::ICache;

#[derive(Default)]
pub struct Cache {
    data: RefCell<HashMap<String, Box<dyn Any>>>,
}

unsafe impl Send for Cache {}
unsafe impl Sync for Cache {}

impl ICache for Cache {
    fn put<T: Any + Clone>(&self, key: &str, value: T) {
        self.data
            .borrow_mut()
            .insert(key.to_string(), Box::new(value));
    }

    fn get<T: Any + Clone>(&self, key: &str) -> Option<T> {
        let data = self.data.borrow();
        let boxed = data.get(&key.to_string())?;
        let val: &T = boxed.downcast_ref()?;
        Some(val.clone())
    }

    fn del(&self, key: &str) {
        self.data.borrow_mut().remove(&key.to_string());
    }

    fn del_all_with_prefix(&self, prefix: &str) {
        self.data.borrow_mut().retain(|k, _| !k.starts_with(prefix));
    }

    fn del_all(&self) {
        self.data.borrow_mut().clear();
    }
}
