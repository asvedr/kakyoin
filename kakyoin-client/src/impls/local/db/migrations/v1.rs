use crate::entities::errors::DbResult;
use crate::impls::local::db::proto::IMigration;
use rusqlite::Connection;

pub struct Migration;

impl IMigration for Migration {
    fn name(&self) -> &str {
        "v1_init"
    }

    fn description(&self) -> &str {
        "initial"
    }

    fn migrate(&self, cnn: &mut Connection) -> DbResult<()> {
        cnn.execute(
            "CREATE TABLE settings (key TEXT PRIMARY KEY, val TEXT NOT NULL)",
            (),
        )?;
        cnn.execute(
            "CREATE TABLE servers (addr TEXT PRIMARY KEY, val TEXT NOT NULL, created_at INTEGER NOT NULL)",
            (),
        )?;
        cnn.execute(
            r#"CREATE TABLE users (
                id TEXT PRIMARY KEY,
                pub_key TEXT NOT NULL,
                pub_key_code TEXT NOT NULL,
                status INTEGER NOT NULL,
                name TEXT,
                name_for_search TEXT,
                description TEXT,
                hash TEXT NOT NULL,
                pic TEXT
            )"#,
            (),
        )?;
        cnn.execute(
            "CREATE INDEX ix_user_name_for_seach ON users (name_for_search)",
            (),
        )?;
        cnn.execute(
            r#"CREATE TABLE chats (
                id TEXT PRIMARY KEY,
                name TEXT NOT NULL,
                banned BOOLEAN
            )"#,
            (),
        )?;
        cnn.execute(
            r#"CREATE TABLE user_chat (
                id INTEGER PRIMARY KEY,
                user_id TEXT NOT NULL,
                chat_id TEXT NOT NULL,
                FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
                FOREIGN KEY(chat_id) REFERENCES chats(id) ON DELETE CASCADE
            )"#,
            (),
        )?;
        cnn.execute("CREATE INDEX ix_user_chat_user ON user_chat (user_id)", ())?;
        cnn.execute("CREATE INDEX ix_user_chat_chat ON user_chat (chat_id)", ())?;
        cnn.execute(
            "CREATE UNIQUE INDEX ix_user_chat_uniq ON user_chat (user_id, chat_id)",
            (),
        )?;
        cnn.execute(
            r#"CREATE TABLE msg (
                id INTEGER PRIMARY KEY,
                sender TEXT,
                receiver TEXT,
                sender_key_code TEXT,
                chat TEXT NOT NULL,
                uuid TEXT NOT NULL,
                local_uniq_id INTEGER,
                text TEXT,
                sent BOOLEAN NOT NULL,
                received BOOLEAN NOT NULL,
                read BOOLEAN NOT NULL,
                created_at INTEGER NOT NULL,
                ts INTEGER NOT NULL,
                last_in_chat BOOL,
                FOREIGN KEY(sender) REFERENCES users(id) ON DELETE CASCADE,
                FOREIGN KEY(chat) REFERENCES chats(id) ON DELETE CASCADE
            )"#,
            (),
        )?;
        cnn.execute(
            "CREATE TABLE msg_local_uniq_seq (id INTEGER PRIMARY KEY, plug INTEGER);",
            (),
        )?;
        cnn.execute("CREATE UNIQUE INDEX ix_msg_uuid ON msg (uuid)", ())?;
        cnn.execute("CREATE INDEX ix_msg_sent ON msg (sent)", ())?;
        cnn.execute("CREATE INDEX ix_msg_read ON msg (read)", ())?;
        cnn.execute("CREATE INDEX ix_msg_ts ON msg (ts)", ())?;
        cnn.execute("CREATE INDEX ix_msg_chat ON msg (chat)", ())?;
        cnn.execute("CREATE INDEX ix_msg_last_in_chat ON msg(last_in_chat)", ())?;
        cnn.execute(
            r#"CREATE TABLE msg_attach (
                id INTEGER PRIMARY KEY,
                msg_id INTEGER NOT NULL,
                kind INTEGER,
                payload STRING,
                FOREIGN KEY(msg_id) REFERENCES msg(id) ON DELETE CASCADE
            )"#,
            (),
        )?;
        cnn.execute(
            r#"CREATE TABLE shared_file (
                id TEXT PRIMARY KEY,
                path TEXT NOT NULL,
                name TEXT NOT NULL,
                size INTEGER,
                parts INTEGER
            )"#,
            (),
        )?;
        cnn.execute(
            r#"CREATE TABLE user_shared_file (
                id INTEGER PRIMARY KEY,
                user_id TEXT NOT NULL,
                file_id TEXT NOT NULL,
                FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
                FOREIGN KEY(file_id) REFERENCES shared_file(id) ON DELETE CASCADE
            )"#,
            (),
        )?;
        cnn.execute(
            r#"CREATE INDEX ix_usf_user_id ON user_shared_file (user_id)"#,
            (),
        )?;
        cnn.execute(
            r#"CREATE UNIQUE INDEX ix_usf_uniq ON user_shared_file (user_id, file_id)"#,
            (),
        )?;
        Ok(())
    }
}
