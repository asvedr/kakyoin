use crate::impls::local::db::proto::IMigration;

mod v1;

pub fn all() -> Vec<Box<dyn IMigration>> {
    vec![Box::new(v1::Migration)]
}
