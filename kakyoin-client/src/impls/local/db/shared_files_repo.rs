use crate::entities::common::Id;
use crate::entities::errors::DbResult;
use crate::entities::files::LocalFileMeta;
use crate::impls::local::db::proto::ILock;
use crate::proto::local::ISharedFilesRepo;
use rusqlite::ToSql;

pub struct SharedFilesRepo<L: ILock> {
    pub lock: L,
}

impl<L: ILock> ISharedFilesRepo for SharedFilesRepo<L> {
    fn register_file(&self, meta: &LocalFileMeta, users: &[Id]) -> DbResult<()> {
        let query = r#"
            INSERT INTO shared_file
            (id, path, name, size, parts)
            VALUES (?, ?, ?, ?, ?)
        "#;
        self.lock.cnn().execute(
            query,
            (&meta.id, &meta.path, &meta.name, meta.size, meta.parts),
        )?;
        let query = format!(
            "INSERT INTO user_shared_file (user_id, file_id) VALUES {}",
            vec!["(?, ?)"; users.len()].join(",")
        );
        let mut params: Vec<&dyn ToSql> = Default::default();
        for id in users {
            params.push(id);
            params.push(&meta.id);
        }
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        Ok(())
    }

    fn get_file_for_user(&self, file_id: &Id, user_id: &Id) -> DbResult<LocalFileMeta> {
        let query = r#"
        SELECT f.path, f.name, f.size, f.parts
        FROM shared_file as f JOIN user_shared_file as uf ON f.id = uf.file_id
        WHERE f.id = ? AND uf.user_id = ?
        "#;
        let mut stm = self.lock.cnn().prepare(query)?;
        let res = stm.query_row([file_id, user_id], |row| {
            Ok(LocalFileMeta {
                id: *file_id,
                path: row.get(0)?,
                name: row.get(1)?,
                size: row.get(2)?,
                parts: row.get(3)?,
            })
        })?;
        Ok(res)
    }

    fn delete_file_by_id(&self, id: &Id) -> DbResult<()> {
        self.lock
            .cnn()
            .execute("DELETE FROM shared_file WHERE id = ?", [id])?;
        Ok(())
    }
}
