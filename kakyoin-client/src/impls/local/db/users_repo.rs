use kakyoin_base::entities::contact::ClientContact;
use kakyoin_base::entities::server_call::ApiUserMeta;
use kakyoin_base::proto::serializers::IStrSerializer;
use mddd::traits::IIntEnum;
use rsa::RsaPublicKey;
use rusqlite::ToSql;
use std::collections::HashMap;

use kakyoin_base::proto::common::ICryptoOps;
use std::sync::Arc;

use crate::entities::common::{Id, VecBox};
use crate::entities::errors::{DbError, DbResult};
use crate::entities::user::{User, UserStatus};
use crate::impls::local::db::proto::{ICache, ILock};
use crate::proto::local::IUsersRepo;

pub struct UsersRepo<L: ILock, KeySer: IStrSerializer<RsaPublicKey>, C: ICryptoOps> {
    pub lock: L,
    pub key_ser: KeySer,
    pub crypto: C,
}

const CACHE_KEY_INFO: &str = "user_repo:info:";
const CACHE_KEY_PUB_KEY: &str = "user_repo:key:";

#[inline]
fn normalize_name(name: &str) -> String {
    name.to_lowercase()
}

#[inline]
fn make_search_patt(name: &str) -> String {
    format!("%{}%", name.to_lowercase())
}

impl<L: ILock, KeySer: IStrSerializer<RsaPublicKey>, C: ICryptoOps> UsersRepo<L, KeySer, C> {
    fn create_user(&self, id: Id, key: RsaPublicKey, status: UserStatus) -> DbResult<()> {
        self.lock.cnn().execute(
            "INSERT INTO users (id, pub_key, pub_key_code, status, hash) VALUES (?, ?, ?, ?, '')",
            (
                id.to_string(),
                self.key_ser.serialize(&key),
                self.crypto.get_key_code(&key),
                status.val_to_int(),
            ),
        )?;
        let cache_key = format!("{}{}", CACHE_KEY_PUB_KEY, id);
        self.lock.cache().put(&cache_key, Box::new(Arc::new(key)));
        Ok(())
    }

    fn get_statuses_and_code(&self, ids: &[Id]) -> DbResult<HashMap<Id, (UserStatus, String)>> {
        let query = format!(
            "SELECT id, status, pub_key_code FROM users WHERE id IN ({})",
            vec!["?"; ids.len()].join(",")
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let params = ids
            .iter()
            .map(|id| -> &dyn ToSql { id })
            .collect::<Vec<_>>();
        let mut result = HashMap::new();
        let rows = stm.query_map(&params as &[&dyn ToSql], |row| {
            Ok((row.get(0)?, row.get(1)?, row.get(2)?))
        })?;
        for row in rows {
            let (id, status, code): (Id, isize, String) = row?;
            let status = UserStatus::int_to_val(status)
                .map_err(|_| DbError::Internal("Invalid user status in base".to_string()))?;
            result.insert(id, (status, code));
        }
        Ok(result)
    }
}

impl<L: ILock, KeySer: IStrSerializer<RsaPublicKey>, C: ICryptoOps> IUsersRepo
    for UsersRepo<L, KeySer, C>
{
    fn contact(&self, contact: &ClientContact, status: UserStatus) -> DbResult<()> {
        let mut got = self.get_by_uid(&[Id(contact.id)])?;
        let key = contact
            .get_key()
            .map_err(|_| DbError::Internal("Invalid key in contact".to_string()))?;
        if got.is_empty() {
            return self.create_user(Id(contact.id), key, status);
        }
        let user = got.remove(0);
        if *user.pub_key == key {
            if status > user.status {
                self.set_status(&user.id, status)?;
            }
            Ok(())
        } else {
            Err(DbError::Conflict)
        }
    }

    fn get_info(&self, uids: &[Id]) -> DbResult<HashMap<Id, Arc<ApiUserMeta>>> {
        let mut not_in_cache: Vec<&dyn ToSql> = Vec::new();
        let mut result = HashMap::new();
        for uid in uids {
            let cache_key = format!("{}{}", CACHE_KEY_INFO, uid);
            if let Some(val) = self.lock.cache().get(&cache_key) {
                result.insert(*uid, val);
            } else {
                not_in_cache.push(uid);
            }
        }
        if not_in_cache.is_empty() {
            return Ok(result);
        }
        let query = format!(
            "SELECT id, name, description, pic, hash FROM users WHERE id IN ({})",
            vec!["?"; not_in_cache.len()].join(",")
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let rows = stm.query_map(&not_in_cache as &[&dyn ToSql], |row| {
            let id = row.get("id")?;
            let info = ApiUserMeta {
                name: row.get::<_, Option<String>>("name")?.unwrap_or_default(),
                description: row
                    .get::<_, Option<String>>("description")?
                    .unwrap_or_default(),
                photo: row.get::<_, Option<String>>("pic")?.unwrap_or_default(),
                hash: row.get::<_, Option<String>>("hash")?.unwrap_or_default(),
            };
            Ok((id, info))
        })?;
        for row_res in rows {
            let (id, info): (Id, ApiUserMeta) = row_res?;
            let info = Arc::new(info);
            result.insert(id, info.clone());
            let cache_key = format!("{}{}", CACHE_KEY_INFO, id);
            self.lock.cache().put(&cache_key, info);
        }
        Ok(result)
    }

    fn set_info(&self, uid: &Id, info: &ApiUserMeta) -> DbResult<()> {
        self.lock.cache().del(&format!("{}{}", CACHE_KEY_INFO, uid));
        let query = r#"
            UPDATE users
            SET
                name = ?,
                name_for_search = ?,
                description = ?,
                pic = ?,
                hash = ?
            WHERE id = ?
        "#;
        self.lock.cnn().execute(
            query,
            (
                &info.name,
                normalize_name(&info.name),
                &info.description,
                &info.photo,
                &info.hash,
                uid,
            ),
        )?;
        Ok(())
    }

    fn get_status(&self, uids: &[Id]) -> DbResult<HashMap<Id, UserStatus>> {
        let query = format!(
            "SELECT id, status FROM users WHERE id IN ({})",
            vec!["?"; uids.len()].join(",")
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let params = uids.iter().map(|s| -> &dyn ToSql { s }).collect::<Vec<_>>();
        let rows = stm.query_map(&params as &[&dyn ToSql], |r| Ok((r.get(0)?, r.get(1)?)))?;
        let mut result = HashMap::new();
        for row in rows {
            let (id, status): (Id, u64) = row?;
            let status = UserStatus::int_to_val(status as isize)
                .map_err(|_| DbError::Internal("Invalid user status in base".to_string()))?;
            result.insert(id, status);
        }
        Ok(result)
    }

    fn set_status(&self, uid: &Id, status: UserStatus) -> DbResult<()> {
        self.lock.cnn().execute(
            "UPDATE users SET status = ? WHERE id = ?",
            (status.val_to_int(), uid),
        )?;
        Ok(())
    }

    fn get_keys(&self, uids: &[Id]) -> DbResult<HashMap<Id, Arc<RsaPublicKey>>> {
        let mut not_in_cache: Vec<&dyn ToSql> = Vec::new();
        let mut result = HashMap::new();
        for id in uids {
            let cache_key = format!("{}{}", CACHE_KEY_PUB_KEY, id);
            if let Some(val) = self.lock.cache().get(&cache_key) {
                result.insert(*id, val);
            } else {
                not_in_cache.push(id);
            }
        }
        if not_in_cache.is_empty() {
            return Ok(result);
        }
        let query = format!(
            "SELECT id, pub_key FROM users WHERE id IN ({})",
            vec!["?"; not_in_cache.len()].join(","),
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let map = stm.query_map(&not_in_cache as &[&dyn ToSql], |r| {
            Ok((r.get(0)?, r.get(1)?))
        })?;
        for res_pair in map {
            let (id, key): (Id, String) = res_pair?;
            let pub_key = self
                .key_ser
                .deserialize(&key)
                .map_err(|_| DbError::Internal("invalid pub_key in base".to_string()))?;
            let pub_key = Arc::new(pub_key);
            result.insert(id, pub_key.clone());
            let cache_key = format!("{}{}", CACHE_KEY_PUB_KEY, id);
            self.lock.cache().put(&cache_key, pub_key);
        }
        Ok(result)
    }

    fn get_by_uid(&self, uids: &[Id]) -> DbResult<VecBox<User>> {
        if uids.is_empty() {
            return Ok(Vec::new());
        }
        let mut keys = self.get_keys(uids)?;
        let mut infos = self.get_info(uids)?;
        let mut stats_and_code = self.get_statuses_and_code(uids)?;
        let mut result = Vec::new();
        for uid in uids {
            let (pub_key, info, status, pub_key_code) = match (
                keys.remove(uid),
                infos.remove(uid),
                stats_and_code.remove(uid),
            ) {
                (Some(k), Some(m), Some((s, c))) => (k, m, s, c),
                _ => continue,
            };
            let user = User {
                id: *uid,
                pub_key,
                info,
                status,
                pub_key_code,
            };
            result.push(Box::new(user));
        }
        Ok(result)
    }

    fn get_by_name(&self, name: &str, limit: usize) -> DbResult<VecBox<User>> {
        let query = format!(
            "SELECT id FROM users WHERE name_for_search like ? LIMIT {}",
            limit,
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let rows = stm.query_map([make_search_patt(name)], |r| r.get(0))?;
        let mut id_list = Vec::new();
        for row in rows {
            id_list.push(row?);
        }
        self.get_by_uid(&id_list)
    }

    fn delete_user(&self, uid: &Id) -> DbResult<()> {
        for prefix in [CACHE_KEY_INFO, CACHE_KEY_PUB_KEY] {
            let key = format!("{}{}", prefix, uid);
            self.lock.cache().del(&key);
        }
        let query = "DELETE FROM users WHERE id = ?";
        self.lock.cnn().execute(query, [uid])?;
        Ok(())
    }

    fn get_hashes(&self) -> DbResult<Vec<(Id, Arc<RsaPublicKey>, String)>> {
        let query = "SELECT id, hash FROM users";
        let mut stm = self.lock.cnn().prepare(query)?;
        let mut id_list = Vec::new();
        let mut hash_map = Vec::new();
        let rows = stm.query_map((), |row| Ok((row.get(0)?, row.get(1)?)))?;
        for row in rows {
            let (id, hash): (Id, String) = row?;
            id_list.push(id);
            hash_map.push((id, hash));
        }
        let mut id_key_map = self.get_keys(&id_list)?;
        let mut result = Vec::new();
        for (id, hash) in hash_map {
            let key = id_key_map.remove(&id).unwrap();
            result.push((id, key, hash));
        }
        Ok(result)
    }

    fn get_all_ids(&self) -> DbResult<Vec<Id>> {
        let mut stm = self
            .lock
            .cnn()
            .prepare("SELECT id FROM users ORDER BY id")?;
        let res = stm
            .query_map((), |r| r.get(0))?
            .collect::<Result<Vec<Id>, _>>()?;
        Ok(res)
    }
}
