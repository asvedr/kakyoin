use std::collections::{HashMap, HashSet};

use rusqlite::ToSql;

use crate::entities::chat::ChatId;
use crate::entities::common::{Id, VecBox};
use crate::entities::errors::{DbError, DbResult};
use crate::entities::message::{LocalMsg, LocalMsgAttached, NewLocalMsg};
use crate::impls::local::db::proto::ILock;
use crate::proto::local::ILocalMsgRepo;
use crate::utils::time::get_now;

pub struct MsgRepo<L: ILock> {
    pub lock: L,
}

fn deserialize(r: &rusqlite::Row) -> rusqlite::Result<Box<LocalMsg>> {
    let opt_att_kind: Option<u64> = r.get("kind")?;
    let attached = if let Some(kind) = opt_att_kind {
        let payload: String = r.get("payload")?;
        LocalMsgAttached::loads(kind, &payload).ok_or(rusqlite::Error::InvalidQuery)?
    } else {
        LocalMsgAttached::Nothing
    };
    let msg = LocalMsg {
        db_id: r.get("id")?,
        sender: r.get("sender")?,
        receiver: r.get("receiver")?,
        sender_key_code: r.get("sender_key_code")?,
        chat: r.get("chat")?,
        uuid: r.get("uuid")?,
        local_uniq_id: r.get("local_uniq_id")?,
        text: r.get("text")?,
        attached,
        sent: r.get("sent")?,
        received: r.get("received")?,
        read: r.get("read")?,
        created_at: r.get("created_at")?,
        ts: r.get("ts")?,
    };
    Ok(Box::new(msg))
}

impl<L: ILock> MsgRepo<L> {
    fn update_last_in_chat(&self, chats: HashSet<ChatId>) -> DbResult<()> {
        if chats.is_empty() {
            return Ok(());
        }
        let tmpl = vec!["?"; chats.len()].join(",");
        let query = format!(
            "UPDATE msg SET last_in_chat = FALSE WHERE chat IN ({}) AND last_in_chat = TRUE",
            tmpl,
        );
        self.lock.cnn().execute(
            &query,
            &chats
                .iter()
                .map(|id| -> &dyn ToSql { id })
                .collect::<Vec<_>>() as &[&dyn ToSql],
        )?;
        let mut ids_to_mark = Vec::new();
        for chat_id in chats {
            if let Some(val) = self.get_last_id_in_chat(&chat_id)? {
                ids_to_mark.push(val);
            }
        }
        if ids_to_mark.is_empty() {
            return Ok(());
        }
        let query = format!(
            "UPDATE msg SET last_in_chat = TRUE WHERE id IN ({})",
            ids_to_mark.join(",")
        );
        self.lock.cnn().execute(&query, ())?;
        Ok(())
    }

    fn get_last_id_in_chat(&self, chat_id: &ChatId) -> DbResult<Option<String>> {
        let query = "SELECT id FROM msg WHERE chat = ? ORDER BY ts DESC, id DESC LIMIT 1";
        let mut stm = self.lock.cnn().prepare(query)?;
        match stm.query_row::<isize, _, _>(&[chat_id], |r| r.get(0)) {
            Ok(val) => Ok(Some(val.to_string())),
            Err(rusqlite::Error::QueryReturnedNoRows) => Ok(None),
            Err(err) => Err(err.into()),
        }
    }

    fn get_id_map(&self, ids: &[Id]) -> DbResult<HashMap<Id, u64>> {
        if ids.is_empty() {
            return Ok(Default::default());
        }
        let query = format!(
            "SELECT uuid, id FROM msg WHERE uuid IN ({})",
            vec!["?"; ids.len()].join(",")
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let params = ids
            .iter()
            .map(|id| -> &dyn ToSql { id })
            .collect::<Vec<_>>();
        let p_link: &[&dyn ToSql] = &params;
        let map = stm
            .query_map(p_link, |r| Ok((r.get(0)?, r.get(1)?)))?
            .collect::<Result<HashMap<Id, u64>, _>>()?;
        Ok(map)
    }

    fn insert_msgs(&self, msgs: &[NewLocalMsg]) -> DbResult<()> {
        let query = format!(
            r#"
            INSERT INTO msg
            (local_uniq_id, sender, receiver, sender_key_code, chat, uuid, text, sent, received, read, created_at, ts, last_in_chat)
            VALUES {}
            "#,
            vec!["(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, false)"; msgs.len()].join(",")
        );
        let mut chats = HashSet::new();
        let mut params: Vec<&dyn ToSql> = Vec::with_capacity(msgs.len() * 8);
        let now = get_now();
        for msg in msgs {
            chats.insert(msg.chat.clone());
            params.push(&msg.local_uniq_id);
            params.push(&msg.sender);
            params.push(&msg.receiver);
            params.push(&msg.sender_key_code);
            params.push(&msg.chat);
            params.push(&msg.uuid);
            params.push(&msg.text);
            params.push(&msg.sent);
            params.push(&msg.received);
            params.push(&msg.read);
            params.push(&now);
            params.push(&msg.ts);
        }
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        self.update_last_in_chat(chats)?;
        Ok(())
    }

    fn insert_attachments(
        &self,
        id_map: HashMap<Id, u64>,
        msgs: Vec<&NewLocalMsg>,
    ) -> DbResult<()> {
        let query = format!(
            "INSERT INTO msg_attach (msg_id, kind, payload) VALUES {}",
            vec!["(?,?,?)"; msgs.len()].join(",")
        );
        let payloads = msgs.iter().map(|m| m.attached.dumps()).collect::<Vec<_>>();
        let mut params: Vec<&dyn ToSql> = Vec::new();
        for i in 0..msgs.len() {
            let msg = msgs[i];
            params.push(id_map.get(&msg.uuid).unwrap());
            params.push(msg.attached.get_kind());
            params.push(&payloads[i]);
        }
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        Ok(())
    }

    fn get_messages(
        &self,
        condition: &str,
        params: &[&dyn ToSql],
        limit: Option<usize>,
        order_by: Option<&str>,
    ) -> DbResult<VecBox<LocalMsg>> {
        let limit = if let Some(val) = limit {
            format!("LIMIT {}", val)
        } else {
            "".to_string()
        };
        let query = format!(
            r#"
            SELECT
                m.id, m.sender, m.chat, m.uuid, m.text, m.sent, m.received, m.read, m.created_at,
                m.ts, m.receiver, m.sender_key_code,
                ma.kind, ma.payload, m.local_uniq_id
            FROM msg as m LEFT JOIN msg_attach as ma ON m.id = ma.msg_id
            WHERE {condition}
            ORDER BY {order_by}
            {limit}
            "#,
            limit = limit,
            condition = condition,
            order_by = order_by.unwrap_or("m.ts DESC, m.id DESC"),
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let mut res = stm
            .query_map(params, deserialize)?
            .collect::<Result<VecBox<LocalMsg>, _>>()?;
        res.reverse();
        Ok(res)
    }
}

impl<L: ILock> ILocalMsgRepo for MsgRepo<L> {
    fn create_messages(&self, msgs: &[NewLocalMsg]) -> DbResult<()> {
        if msgs.is_empty() {
            return Ok(());
        }
        self.insert_msgs(msgs)?;
        let mut with_attachment = Vec::new();
        let mut id_to_req = Vec::new();
        for msg in msgs {
            if !matches!(msg.attached, LocalMsgAttached::Nothing) {
                id_to_req.push(msg.uuid);
                with_attachment.push(msg);
            }
        }
        if id_to_req.is_empty() {
            return Ok(());
        }
        let id_map = self.get_id_map(&id_to_req)?;
        self.insert_attachments(id_map, with_attachment)
    }

    fn get_already_in_base(&self, id_list: &[Id]) -> DbResult<HashSet<Id>> {
        if id_list.is_empty() {
            return Ok(Default::default());
        }
        let query = format!(
            "SELECT uuid FROM msg WHERE uuid IN ({})",
            vec!["?"; id_list.len()].join(",")
        );
        let params = id_list
            .iter()
            .map(|id| -> &dyn ToSql { id })
            .collect::<Vec<_>>();
        let mut stm = self.lock.cnn().prepare(&query)?;
        let res = stm
            .query_map(&params as &[&dyn ToSql], |r| r.get(0))?
            .collect::<Result<HashSet<_>, _>>()?;
        Ok(res)
    }

    fn get_by_uid(&self, id: &Id) -> DbResult<Box<LocalMsg>> {
        let mut msgs = self.get_messages("m.uuid = ?", &[id], Some(1), None)?;
        if msgs.is_empty() {
            return Err(DbError::NotFound);
        }
        Ok(msgs.remove(0))
    }

    fn get_chat_messages(
        &self,
        chat_id: &ChatId,
        last_id: Option<u64>,
        limit: usize,
    ) -> DbResult<VecBox<LocalMsg>> {
        let condition;
        let params: Vec<&dyn ToSql>;
        if let Some(ref id) = last_id {
            condition = "m.chat = ? AND m.id < ?";
            params = vec![chat_id, id];
        } else {
            condition = "m.chat = ?";
            params = vec![chat_id];
        }
        self.get_messages(condition, &params, Some(limit), None)
    }

    fn get_last_unread_messages(&self, limit: usize) -> DbResult<VecBox<LocalMsg>> {
        let query = format!(
            r#"
            SELECT
                m.id, m.sender, m.chat, m.uuid, m.text, m.sent, m.received, m.read, m.created_at,
                m.ts, m.receiver, m.sender_key_code,
                ma.kind, ma.payload, m.local_uniq_id
            FROM msg as m LEFT JOIN msg_attach as ma ON m.id = ma.msg_id
            WHERE m.sender IS NOT NULL
            ORDER BY m.id DESC
            LIMIT {limit}
            "#,
            limit = limit,
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let res = stm
            .query_map((), deserialize)?
            .collect::<Result<VecBox<LocalMsg>, _>>()?;
        Ok(res)
    }

    fn get_messages_to_send(&self) -> DbResult<VecBox<LocalMsg>> {
        self.get_messages("m.received = FALSE", &[], None, Some("m.created_at"))
    }

    fn mark_as_read(&self, id_list: &[Id]) -> DbResult<()> {
        if id_list.is_empty() {
            return Ok(());
        }
        let params = id_list
            .iter()
            .map(|id| -> &dyn ToSql { id })
            .collect::<Vec<_>>();
        let query = format!(
            "UPDATE msg SET read = TRUE WHERE uuid IN ({})",
            vec!["?"; id_list.len()].join(",")
        );
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        Ok(())
    }

    fn mark_as_sent(&self, id_list: &[Id]) -> DbResult<()> {
        if id_list.is_empty() {
            return Ok(());
        }
        let tmpl = vec!["?"; id_list.len()].join(",");
        let params = id_list
            .iter()
            .map(|id| -> &dyn ToSql { id })
            .collect::<Vec<_>>();
        let query = format!("UPDATE msg SET sent = TRUE WHERE uuid IN ({})", tmpl);
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        Ok(())
    }

    fn mark_as_received(&self, id_list: &[Id]) -> DbResult<()> {
        if id_list.is_empty() {
            return Ok(());
        }
        let tmpl = vec!["?"; id_list.len()].join(",");
        let params = id_list
            .iter()
            .map(|id| -> &dyn ToSql { id })
            .collect::<Vec<_>>();
        let query = format!(
            "UPDATE msg SET sent = TRUE, received = TRUE WHERE uuid IN ({})",
            tmpl
        );
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        Ok(())
    }

    fn delete_messages(&self, id_list: &[Id]) -> DbResult<()> {
        if id_list.is_empty() {
            return Ok(());
        }
        let tmpl = vec!["?"; id_list.len()].join(",");
        let params = id_list
            .iter()
            .map(|id| -> &dyn ToSql { id })
            .collect::<Vec<_>>();
        let query = format!(
            "SELECT DISTINCT chat FROM msg WHERE uuid IN ({}) ORDER BY chat",
            tmpl
        );
        let mut stm = self.lock.cnn().prepare(&query)?;
        let chats = stm
            .query_map(&params as &[&dyn ToSql], |r| r.get(0))?
            .collect::<Result<HashSet<ChatId>, _>>()?;
        let query = format!("DELETE FROM msg WHERE uuid IN ({})", tmpl);
        self.lock.cnn().execute(&query, &params as &[&dyn ToSql])?;
        self.update_last_in_chat(chats)
    }

    fn next_local_uniq_id(&self) -> DbResult<i64> {
        let query = "INSERT INTO msg_local_uniq_seq (plug) VALUES (1)";
        self.lock.cnn().execute(query, ())?;
        Ok(self.lock.cnn().last_insert_rowid())
    }

    fn get_first_id_of_same_code(&self, id: u64) -> DbResult<u64> {
        let query = r#"
            WITH code AS (
                SELECT local_uniq_id FROM msg WHERE id = ?
            ) SELECT m.id, m.local_uniq_id
            FROM msg AS m JOIN code AS c
                ON m.local_uniq_id = c.local_uniq_id
            ORDER BY m.ts ASC, m.id ASC
            LIMIT 1
        "#;
        let mut stm = self.lock.cnn().prepare(query)?;
        let (new_id, code): (u64, Option<i64>) =
            stm.query_row([id], |r| Ok((r.get(0)?, r.get(1)?)))?;
        if code.is_none() {
            Ok(id)
        } else {
            Ok(new_id)
        }
    }
}
