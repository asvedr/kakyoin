use crate::entities::errors::PicLoaderError;
use crate::proto::local::IPicLoader;
use base64::{engine::general_purpose, Engine as _};
use image::imageops;
use image::imageops::FilterType;
use image::io::Reader as ImageReader;
use image::{DynamicImage, ImageFormat, RgbImage};
use kakyoin_base::proto::common::IFS;
use std::fs::File;
use std::io::Cursor;
use std::path::Path;

const IMAGE_SIZE: u32 = 200;

pub struct PicLoader<FS: IFS> {
    pub fs: FS,
}

impl<FS: IFS> PicLoader<FS> {
    fn load_original(&self, path: &Path) -> Result<DynamicImage, PicLoaderError> {
        let data = self
            .fs
            .read_file(path)
            .map_err(|_| PicLoaderError::CanNotRead)?;
        Self::pic_from_bytes(data)
    }

    fn pic_from_bytes(buffer: Vec<u8>) -> Result<DynamicImage, PicLoaderError> {
        let reader = match ImageReader::new(Cursor::new(buffer)).with_guessed_format() {
            Ok(val) => val,
            Err(err) => panic!("Can not read image from memory: {:?}", err),
        };
        reader.decode().map_err(|_| PicLoaderError::InvalidFormat)
    }

    #[inline]
    fn convert_to_correct_format(pic: DynamicImage) -> RgbImage {
        imageops::resize(
            &pic.into_rgb8(),
            IMAGE_SIZE,
            IMAGE_SIZE,
            FilterType::Triangle,
        )
    }

    #[inline]
    fn dump_to_bytes(pic: RgbImage) -> Result<Vec<u8>, PicLoaderError> {
        let mut buffer = Vec::new();
        pic.write_to(&mut Cursor::new(&mut buffer), ImageFormat::WebP)
            .map_err(|_| PicLoaderError::CanNotEncode)?;
        Ok(buffer)
    }
}

impl<FS: IFS> IPicLoader for PicLoader<FS> {
    fn load_pic(&self, path: &Path) -> Result<String, PicLoaderError> {
        let pic = self.load_original(path)?;
        let pic = Self::convert_to_correct_format(pic);
        let bytes = Self::dump_to_bytes(pic)?;
        Ok(general_purpose::STANDARD.encode(bytes))
    }

    fn dump_to_file(&self, pic: &str, path: &Path) -> Result<(), PicLoaderError> {
        let bytes = general_purpose::STANDARD
            .decode(pic)
            .map_err(|_| PicLoaderError::InvalidBase64)?;
        let pic = Self::pic_from_bytes(bytes)?;
        let mut file = File::create(path).map_err(|_| PicLoaderError::CanNotWrite)?;
        pic.write_to(&mut file, ImageFormat::WebP)
            .map_err(|_| PicLoaderError::CanNotWrite)
    }
}
