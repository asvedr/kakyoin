use crate::entities::common::Id;
use crate::entities::errors::DbError;
use crate::entities::message::{NewLocalMsg, NewMsgToSend};
use crate::entities::tasks::PeriodicName;
use crate::proto::local::{IMsgMaker, IStorage, ITaskScheduler};
use crate::utils::time::get_now;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;
use std::sync::Arc;

pub struct MsgMaker<L: ILogger, S: IStorage, TS: ITaskScheduler> {
    ctx: Arc<Ctx<L, S, TS>>,
}

struct Ctx<L, S, TS> {
    logger: L,
    storage: S,
    task_sheduler: TS,
}

impl<L: ILogger + 'static, S: IStorage + 'static, TS: ITaskScheduler + 'static> MsgMaker<L, S, TS> {
    pub fn new(logger: L, storage: S, task_sheduler: TS) -> Self {
        Self {
            ctx: Arc::new(Ctx {
                logger,
                storage,
                task_sheduler,
            }),
        }
    }

    async fn a_make(self, src: NewMsgToSend) -> Result<(), DbError> {
        self.create(src).await?;
        self.schedule().await;
        Ok(())
    }

    async fn create(&self, src: NewMsgToSend) -> Result<(), DbError> {
        let storage = self.ctx.storage.lock().await;
        let users = storage.chats().get_participants(&src.chat)?;
        let mut msgs = Vec::with_capacity(users.len());
        let msg_repo = storage.messages();
        let uniq_id = msg_repo.next_local_uniq_id()?;
        for user_id in users {
            let msg = NewLocalMsg {
                sender: None,
                receiver: Some(user_id),
                sender_key_code: None,
                chat: src.chat.clone(),
                uuid: Id::new(),
                local_uniq_id: Some(uniq_id),
                text: src.text.clone(),
                attached: src.attached.clone(),
                sent: false,
                received: false,
                read: true,
                ts: get_now(),
            };
            msgs.push(msg)
        }
        msg_repo.create_messages(&msgs)
    }

    async fn schedule(&self) {
        let res = self
            .ctx
            .task_sheduler
            .schedule_periodic(PeriodicName::SEND_MSGS)
            .await;
        if let Err(err) = res {
            self.ctx.logger.log(
                Level::Err,
                &format!("Can not spawn task 'send msgs': {:?}", err),
            )
        }
    }
}

impl<L: ILogger + 'static, S: IStorage + 'static, TS: ITaskScheduler + 'static> IMsgMaker
    for MsgMaker<L, S, TS>
{
    fn make(&self, msg: NewMsgToSend) -> DynFutRes<(), DbError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_make(msg),
        )
    }
}
