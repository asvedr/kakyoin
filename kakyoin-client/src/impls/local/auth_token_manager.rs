use crate::proto::local::IAuthTokenManager;
use kakyoin_base::entities::common::DynFut;
use std::sync::Arc;
use tokio::sync::Mutex;
use uuid::Uuid;

pub struct AuthTokenManager {
    mtx: Arc<Mutex<Ctx>>,
}

struct Ctx {
    is_on: bool,
    token: String,
}

impl AuthTokenManager {
    pub fn new() -> Self {
        let ctx = Ctx {
            is_on: false,
            token: "".to_string(),
        };
        Self {
            mtx: Arc::new(Mutex::new(ctx)),
        }
    }
}

impl IAuthTokenManager for AuthTokenManager {
    fn set_active(&self, is_on: bool) -> DynFut<()> {
        let mtx = self.mtx.clone();
        Box::pin(async move {
            let mut guard = mtx.lock().await;
            guard.is_on = is_on;
        })
    }

    fn is_auth_turned_on(&self) -> DynFut<bool> {
        let mtx = self.mtx.clone();
        Box::pin(async move { mtx.lock().await.is_on })
    }

    fn gen_token(&self) -> DynFut<String> {
        let token = Uuid::new_v4().to_string().replace('-', "");
        let mtx = self.mtx.clone();
        Box::pin(async move {
            let mut guard = mtx.lock().await;
            guard.token = token.clone();
            token
        })
    }

    fn validate_token(&self, token: &str) -> DynFut<bool> {
        let mtx = self.mtx.clone();
        let token = token.to_string();
        Box::pin(async move {
            let guard = mtx.lock().await;
            !guard.is_on || guard.token == token
        })
    }
}
