use kakyoin_base::entities::server_call::ApiUserMeta;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

use crate::proto::local::IUserMetaHashCalculator;

#[derive(Copy, Clone)]
pub struct UserMetaHashCalculator;

impl IUserMetaHashCalculator for UserMetaHashCalculator {
    fn calculate(&self, meta: &ApiUserMeta) -> String {
        if meta.name.len() + meta.description.len() + meta.photo.len() == 0 {
            return "".to_string();
        }
        let mut hasher = DefaultHasher::new();
        meta.name.hash(&mut hasher);
        meta.description.hash(&mut hasher);
        meta.photo.hash(&mut hasher);
        let code = hasher.finish();
        format!(
            "{}{}{}{}",
            code,
            meta.name.len(),
            meta.description.len(),
            meta.photo.len(),
        )
    }
}
