use crate::entities::common::HEADER_AUTH;
use crate::entities::errors::ApiError;
use crate::impls::api_wrapper::ApiWrapper;
use crate::impls::local::auth_token_manager::AuthTokenManager;
use crate::proto::common::IApiHandler;
use crate::proto::local::IAuthTokenManager;
use hyper::{Body, Method, Request};
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

struct HandlerWithOverride;
struct HandlerNoOverride;

#[derive(Serialize)]
struct Response {
    status: u16,
}

impl IApiHandler for HandlerWithOverride {
    type Response = Response;
    type Body = usize;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn override_body() -> Option<Self::Body> {
        Some(10)
    }

    fn path(&self) -> &str {
        "/api/v1/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, query: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        assert_eq!(query, ());
        assert_eq!(body, 10);
        Box::pin(async move { Ok(Response { status: 2 }) })
    }
}

#[derive(Deserialize)]
struct HBody {
    data: String,
}

#[derive(Deserialize)]
struct Query {
    a: String,
    b: isize,
}

impl IApiHandler for HandlerNoOverride {
    type Response = ();
    type Body = HBody;
    type Query = Query;

    fn path(&self) -> &str {
        "/api/v2/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, query: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        assert_eq!(query.a, "hello");
        assert_eq!(query.b, -2);
        assert_eq!(body.data, "abc");
        Box::pin(async move { Ok(()) })
    }
}

#[tokio::test]
async fn test_wrap_with_override() {
    let handler = ApiWrapper::wrap(HandlerWithOverride, AuthTokenManager::new());
    assert_eq!(handler.path(), "/api/v1/");
    assert_eq!(handler.method(), Method::GET);
    let request = Request::builder()
        .uri("/api/v1/")
        .body(Body::from(vec![]))
        .unwrap();
    let resp = handler.handle(request).await.unwrap();
    assert_eq!(resp, b"{\"status\":2}");
}

#[tokio::test]
async fn test_wrap_no_override() {
    let handler = ApiWrapper::wrap(HandlerNoOverride, AuthTokenManager::new());
    assert_eq!(handler.path(), "/api/v2/");
    assert_eq!(handler.method(), Method::POST);
    let request = Request::builder()
        .uri("/api/v2/?a='hello'&b=-2")
        .body(Body::from(b"{\"data\": \"abc\"}".to_vec()))
        .unwrap();
    let resp = handler.handle(request).await.unwrap();
    assert_eq!(resp, b"null");
}

#[tokio::test]
async fn test_handle_with_valid_token() {
    let token_man = AuthTokenManager::new();
    token_man.set_active(true).await;
    let token = token_man.gen_token().await;
    let handler = ApiWrapper::wrap(HandlerWithOverride, token_man);
    assert_eq!(handler.path(), "/api/v1/");
    assert_eq!(handler.method(), Method::GET);
    let request = Request::builder()
        .uri("/api/v1/")
        .header(HEADER_AUTH, token)
        .body(Body::from(vec![]))
        .unwrap();
    handler.handle(request).await.unwrap();
}

#[tokio::test]
async fn test_handle_with_invalid_token() {
    let token_man = AuthTokenManager::new();
    token_man.set_active(true).await;
    let handler = ApiWrapper::wrap(HandlerWithOverride, token_man);
    assert_eq!(handler.path(), "/api/v1/");
    assert_eq!(handler.method(), Method::GET);
    let token = Uuid::new_v4().to_string().replace('-', "");
    let request = Request::builder()
        .uri("/api/v1/")
        .header(HEADER_AUTH, token)
        .body(Body::from(vec![]))
        .unwrap();
    let err = handler.handle(request).await.unwrap_err();
    assert_eq!(err, ApiError::not_authorized())
}

#[tokio::test]
async fn test_handle_no_token_when_requied() {
    let token_man = AuthTokenManager::new();
    token_man.set_active(true).await;
    let handler = ApiWrapper::wrap(HandlerWithOverride, token_man);
    assert_eq!(handler.path(), "/api/v1/");
    assert_eq!(handler.method(), Method::GET);
    let request = Request::builder()
        .uri("/api/v1/")
        .body(Body::from(vec![]))
        .unwrap();
    let err = handler.handle(request).await.unwrap_err();
    assert_eq!(err, ApiError::not_authorized())
}
