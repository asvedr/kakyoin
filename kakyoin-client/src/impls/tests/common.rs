#![allow(dead_code)]
use crate::entities::common::{Id, Me};
use crate::entities::errors::{DbError, ServerError};
use crate::impls::local::db::storage::Storage;
use crate::impls::local::user_meta_hash_calculator::UserMetaHashCalculator;
use crate::proto::local::{IStorage, ISyncStorage, ITaskScheduler};
use crate::proto::net::{IServerClient, IServerClientFactory};
use atex::{AtexError, NewTask, TaskId, TaskStatus};
use kakyoin_base::entities::api_msg::{ApiMsg, ApiMsgPacked, ApiMsgStatus};
use kakyoin_base::entities::common::{DynFut, DynFutRes};
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::entities::errors::PackerError;
use kakyoin_base::entities::fs::FileMeta;
use kakyoin_base::entities::server_call::ApiUserMeta;
use kakyoin_base::impls::crypto_ops::CryptoOps;
use kakyoin_base::impls::logger::Logger;
use kakyoin_base::impls::serializers::{rsa_key, server_contact};
use kakyoin_base::proto::common::IFS;
use kakyoin_base::proto::serializers::{IApiMsgPacker, IStrSerializer};
use mddd::macros::singleton;
use rsa::{RsaPrivateKey, RsaPublicKey};
use std::cell::RefCell;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::rc::Rc;
use std::sync::Arc;
use tokio::sync::Mutex;
use uuid::Uuid;

const PRIV_KEY_1: &str = "MIIJKAIBAAKCAgEAqJuvpdGTcFkvcPdk7wyf3U+JfhCrwBaAbgO2dSqFZWZIfdoeSq7B4hzWFd5lk2ydVhFUUwLfkBW9TxAopxfeF7148DK5CawIwnsBhYifhS/DMAtw9TtuaFql1AVXTgbIGAwRO0sf5Lsak+ZKhrYo6WtmU70UmWwbe+XOrK5EM76gHL1P/v44rwedBUUpGUI0LNyg71+uLF4CyJjVdFYg7X+YidC2sePSipKIBjiYMWRm64DRwb6s6iVhVdba3EeLTg2m0igVg7qrKrI+rPMjn1pZVzo/bQ/dsbLtCBz7RM4hACQ45tyjCpxztZYW58u1BnO7l0kZEYuuGpxuHTRyQMX9A0MSnQGlI5tg7AtWkxBg3HbP2tYMFsQ0IE9BMSA2+E8hcb/uKXEYORzNmrlchPDhcPlW82xarS1ogvItUTFC6kJ5bxNim/i3qPtEqF9mX9TSsuh+QfJnZ8nEMbBsWjtWTUDuLBlGmCL+ccc2Qo+6HezYfbRhYWqEbYjBI2KNtYDR1x9sOJ43GYEIj+ZWpB3nW4CmM1sH9dZByTIzv0SJRZxi7lqntfOPyV2rtVQ137C8afF/9j35qsDBfhk19NTY8NmGhLzXZc+fMdgayNTdA2G/yxonBtqH1C/bYBuLdgq5RPnG20UgL01UKeXpM7mrpeLEU+cYK7OZvgKDqdMCAwEAAQKCAgAkxZeALX3fUcQT+hCMRg+Tf6Y31UDv6rhlxe+EDBr9q1oCAJD8rrJgMvPJ3y/+AAEsZSkj1YuWa/dWh+qlzoHEYbE/iYBriJZ8e8EwM0S/ElSaZNRk12pT3fkujV+mB7qZ6//SE6dIBjpcqSv1x0KS7bZqauFUOIyh79OsO3Fqa5eVzP/mV+Fa4uku3g+Bj7u7UP7UVeSxjmtC/noHHkWeJmbJH7jIgSxIxn5eBPfEReZgVD7q5z/k7PQWeX5P7laGVlncmZtfmFV+QVOD+p75QQYdDPoKOhyahbBGRbPTJLVzx3KVfuccCFT/9FTYhsfbOWBfNTT/a5wz94vYWLjV5veMTBzRQgFM+NVkTSJ39TkbN7U8o2I3Zos6VQUUbNmLea7hOHADkRQ1HmLgBWlgbVBkpZ0h/tL8ndBEAdIvweuK2jxkRzU1pSmqQdc8Ld7hoE8Zi/hcWFlnyoH9fIIYtYrOkTSmGvs8H3YypDyELyjLYroERO+eQ0ahP1cDug94EvxLJ+gDiLavxDXHAP0KS+HyTzAgMqQVoql+5iwAJceEjlSL9TqOfkssmsL00XCt+KETdhixNAgsk1A8RnrNrKR+fSmCugfCqtUo9Re6XIFcTO4CEWfGLuq0N+1I6InR4iTyZD/dFe33ToVp/pXRgjuQzk36tmHKZ9pc/m85IQKCAQEA0hnMN3JPR/7z8psoZMkYK9tGB9W0bsJuZCtXwGkcorKDC0UmHAqRyM9mZf6gbAJHxLjRkETTqDFHb+g70kbpDdokvwEw3+YV2Jqvh84NZHX70ADYe9yNFHlp97rCzLt6qkyqpvG5g3Qbr/t9OIlObltsNWhRddWMH6v+Hyu/ZBbclt3wz7j9Q6888y64HDEOWFvyaebUTsj+5anmG2n0mE7K4A4EZBt+VzA2T3ZXUOuSI6rgcX1F0QKkQlgGPC62Qkx6LYzJPeAyL33GdyhCoIT3zzHjdmXqZse8UKq4ecgifJWQ3uhw0kY6v/jQm0lLcgcgZHRKqEKHFBVDQxPHUQKCAQEAzXFZ1Zret4H9HMZRLD5UjQ/aA7f/vZ7mPlnCu4Y4K4WiZXkNl115zFxp/x1iW/FWgQYJmpSTb3Ynztvb+vJKC9C2XWwymh/2lMLW5FQplat/IelxxRhXqfmGCL2UrCONfM6ZV2PAfHjk5Ji0yXA12eNVEn1kIIeVsVnr4CYXXra3DeAWc8BsnQehCcQ4fp2sG56t3TpbuHjGo4U/7U74njpm23oS5LeaMeYxvzYlGlm8F2Stf29CbAtzZqpicnUNnTPfws/XRBoPxnZ/9CmS+II6kL1i92s3zr1VIjQYvf7RmDnwcXIDZKpBIVv7/mrRXvYWO8J8ejxiMraPnhnd4wKCAQEAlwbgW8MZ5oEIzw5lnP1JGM2Iq70c9LljLTJS5AcrBHEZ7hII8QNQBG/CBfNhUgLZoDnSusge9bpmxbTEu8UpZkG3RG4QugHlFk0rfI3L5rlBCCZzd6X07BDS8bcYrJ7f8GcoDNWaKzpdZMV9IBTjgkTJpiKU3AVjsqiqBbIm5EXiz7g+kLNDq1HemFivE+TXkQCXYzUAzJmDUYgt1ur1Xj7VavXohiWVTgtkHv7MiWitJmKA8FPCyhoCBYbNBJn3ffUHppKbXpU4vhbFgYY5TrYGhLQx9DwSoUtWvQ3bif8pezDwcKvZ3e2J8dFj8xxe1Vr2F//YYtdfzjnyndcr4QKCAQBBq4sQyamJtUaIbShDYUU7tiZ1a9qW7XUh1yV1Ngk8eEIZ96ENkna9iYjkYKsxQqrpIVnujLG4PmepKwRdahebQUMaB90BAVib4tA4OmL0kdZYq7uPSiGzlqxunNMqV08B5zSDB8LexvI71Eht362HEbD+8RARieWixGCuzN5Ji1RmXgwvQgI2C9tBOPTqHFKInadsARySkiDE4nI11DmKspZwb8wcSkhCzRd3zMH9c1O5kpgXFSSBTwO7fJlthtcsOgm7xx5DiaUjRnvOO3/cahx4aeF5v08eiczt2wNRsGOlKlgem1hUVEvvcuzRPanhlf4zag7KMAUfutWbrx8LAoIBABcuU2wDs3IO1iLQ3gSNI5QmyjhdY2qherahc2IbocHvwwkJVjtDs4yo8tLA4aSQPeaqcOjNt3mG6PEreUZXbs7JEJBOuhrcOHKvUcwPtw70ce29XjTyEOMoyopgf7fN3I/p5YkHGSYioQ5FkkmtwwS61M4/PJGynzk/RoyIIZctd+C4JuimZAYq8wpCr6BdIxH+3bUM6f2t+6zcIwZ4/qe97bMSqiSatXKubao3o11FzFc/5ssm9Wkgmgz42s5MZlmz6XxI+/oknkDJ9jrLvT+JgsUhel5jkbde+4yUa8ligleqtuifS3hcdA9hhotIU48N9jZP0zxPdduQz/+4iFM=";
const PRIV_KEY_2: &str = "MIIJKQIBAAKCAgEA+NeyTBp25gFxlx741WxX00kAedhPniY+bAZ9qQv2oYsPjsm9wgZQh/SHg3YNHPITUky5wfUAVc2zDEhfWZ9ILRbJ5U+5mUA6vwAMUEeSTvL7Ma5fHhZa62cCkG8lVynULyhtjcv4IYtsZvGlGbdKc8cVuu4SL3TWhzEFIdZvs012NHb2eyFw17OqwigimucnGp88cTZtCUH34cspy3eU1MIAyodKfpI66wF+kIRV8zT2YbUHzH6GrPD6N6vaulLQ1rBVb54xNxWa11uAnxEK8vvN8yW90+G+VqETcgsT3MTnRciLn35/aoMKa7hwaRwOCKsKskM0IZSmf3Y3lwvlMBAHWwkNYWuvycynzXV6b0ecj4/mUgtgO3KQr7oEHzAr/vK3rn72+wD9hLozfmkl9Cvpth8TkRiNPZUdO93MRCrzT4AUfZ9UHIyN4BmUq9NHIk/qg990n+Hl2ivqHgOn5qvDMKZmOjz3FDevVZv/cTyB8Vg/N+BowPISO7E9yXMGA4USHpB/OFQo5q2qz1CjUbhIVoFfxRSdZm6p80WfkKgPGfqZB8LnPN4MWFSkVRqy4zCKyMiiDVsxbRVQ4QbY4Wb2RWfJWGvdgg5f/AMMTHJRrnhNL65+hhHlAbjd6nMCXf+h2PkwKw1EjV+BlzUL9RKLWJn++BRL5l5h6ornmRMCAwEAAQKCAgAHP5k0+ExgVMhvQ3qyFM7WG7lE01U2YRzOXr4IT43mlLtmSGAbp71uyGOuI6EsJ60CZ7O9f5p75VsCz5TfcnFSxugootLgMOvFJoY1THBpllBU2SRyJuqNEtoTRPmMeW5x1Taju75Fk98bfIx3nEwx5Smv4UFsWnXpSDdDDhEA4w+/ZXu2iHnV/qjJsQuZAYS1FNBY5+HzNBW5YQado6UVMXV+cMRtvWqz7741DXXHeNJ4dCJ/BWx82yQevQXJCaKFrms+rhf2580z2rl7aK6t90G/hGjsJBY8RcLgPoxckOu/r6DJi5DAlIKYMq4rkaKjBABywtxg4gibi+EAqFsLpadn0wI8LkeknyeGowP4QmiiORa8Zhrx0Vd5vvt+wIXEMQ/McVbEZu52ufI5eFDzx2AAILYT7FXbTV/N7E079GzE7k4wVB0fa1HnMPkEIyKkWKP7YayB5WzSCayl7Zukv54EwbSxcKlt0dEZQluiK0lUgiM//swovv2R+NTY6xr4HIVa9C16F4iVB5cb1u4tVcExguPJh7Z5g8m52IA4WdEBoJrolnXIOUBOLiAc6op80EqyoDFEBHQ0jVqtH9qQ8cZ+9d8aAcBGfic6JZVbJAzn2ONJiKh39U81XJj0Rxg9jzPRqOvF/Z6XG33axaegsJC4rNOf5xeY32qG7BGG4QKCAQEA+aWmZRhTTj54UOWtkdVuM7Nc1V8RJ5OwwqXP62wl5RcN95Nlr9zFmoXePhT3x/J+O539Humiwei+Gmr7kUUw366w95XVkt6Izl3wZgFMyCGqtTQDdgFbF5HubqGj7dYvWekaJEAk6PK4lxGlVfxg5phGmADwpRcHDTwt5ElY+6PcJOp1bhX9h+ieuas+riCtQQhqCRKEhhxkHgG+yJtMEvmu1mt8WfZoVEYx9aTTFStv0VyVUe7LqztqPizZmkwBYUNCTnMUtklFdzbb9Y+03uXeDB/mNalC6RuAZf467doTQY7ZlE2a2nnuzelMlWR2/dmjtk7xaqz6WLP581uHsQKCAQEA/yzOMsR8JLUv2eC6TTw75ctXxyOvRtqh+4LvCH5DC/wJaRs7YWEMiigKM5OsxrFej6NbkgnuA8wbRj+N7fcEDVmgIeQWGwQOQJ/6C0AUVCCTMP8uhaBib5Q6Qa773HssOKwKeeiC13REqb7vghqYU0xCW25nJDupHHInOd6dQzujOEOfz1gpoaaGa93ZC2AkisH3dIHDgcN+MJMQ68aZ3bgGNwOt0xmBZAVA/+F2q1vR8WqxIbHA2nD7QXPjrpHdKQsEqK7sBs6Jxok8X5+hLTq/JrG5DUiB0Iah77r1YWq3+dUx/DpHD3HheXYxvx1SVnuho65RqdJr9A776SeiAwKCAQBYPNB/UwW+1h24mjWrIgD9jRv23s81aeJEKk8/Ks6KFof9Rb/fVO0THNXco5Sgu3kaoVDiKDo6x4TJ2Z5pTkdkCF7MY81FSBqatmFN7I+y/RNVULkYcIo93nqj58YFPnsmEFoJDhL9syzDWJ1Tswyc9J2fBFYKRAVeDDM8SSCGcPhSo/w1mNgvvWy8u2ZALZSPM+YmpsDolghdAsjoJtvQqSe+6ebd9VYS02y9IjDpfpzbJxowB3M9Zpa6uyd+ffbO5fL9v3dGpsdCx8UfGlgXoK/HIU9BED5yHiJX6m/xue6scU0+Od/N8OMTLK7A7u2P8+kFewmhZtpll9d0yJlBAoIBAQDM6Jrwc43JIGI7kNWp/euPfTmQpVw0Bo863Aq9BD8s2YhpCcAmU3txWfPPNQ/yQi5QI/r9i9HA5N8mCxUa+VgcrFMFxBXoPdqAFLcC38MzRzFLkC84iPFi7mHVqwF/LL33CFglSwv+ndQhhzznehnGEh/bMg3LL8l+GUDcnKImgjZwkQU8yGwt7f5VkkGfe+X8MwRWX3mys1ya/w4Ad2vJeuQqu16TfRDFU969W70in8o4FdgF2aqm/0EUb05vOZyiP8HO/yBz2tkSP9PmadZQ7rLfDKowO4BbBjH1w2kWgE8aZRQNmnLDuUw5cCtEb/RVlFPZj7ruJxYfVwT9VqEnAoIBAQCKIdJztqmE2hRJu9n15V3cGXqYDgd2PGZSwBoCa4GUe6lxx5WSLE2LjuMoGAnM/ubXsU6EPlvbSQ/inHwXodYXjZpmhac4fyYdsgO2eF+T7X/zNOLHsIpvoYXCa9mRiBrSQJKYisnVmfv1Fxp7hqTgyp1QCmeTlsMBHx4tvAGcuTYXjgWY4gcg4b+rfe8VgulqA137Jh3w1Jgd7PZh6FkWh2kXf+VRNhdcUWW2CF1lnNj/Pm7bjmY1hqZnGlzAa7dgdL9ojzRoN7MFtAC03CShEd+MgGX4pjjT88OlVa5XK5hCbQBCWdja85gnFvBRjIV6C3wxetumXVrJ2yv/SWqW";

pub struct WrappedStorage {
    storage: Arc<dyn IStorage>,
}

unsafe impl Send for WrappedStorage {}
unsafe impl Sync for WrappedStorage {}

#[derive(Clone)]
pub struct MockTaskScheduler {
    mtx: Arc<Mutex<MockTaskSchedulerCtx>>,
}

#[derive(Default)]
struct MockTaskSchedulerCtx {
    schedule_task: Vec<NewTask>,
    schedule_periodic: Vec<String>,
    get_task_status_res: Vec<Result<TaskStatus, AtexError>>,
    download_file: Vec<(Id, Id, u64, String)>,
}

#[derive(Clone)]
pub struct MockServerFactory {
    me: Arc<Me>,
    contact: ServerContact,

    send_msgs: Result<HashMap<Uuid, ApiMsgStatus>, ServerError>,
    fetch_msgs: Result<Vec<ApiMsgPacked>, ServerError>,
    get_statuses: Result<HashMap<Uuid, ApiMsgStatus>, ServerError>,
    mark_received: Result<(), ServerError>,
    request_meta: Result<Box<ApiUserMeta>, ServerError>,
    request_meta_hash: Result<HashMap<Uuid, String>, ServerError>,
    post_my_meta: Result<(), ServerError>,
    ping: Result<(), ServerError>,
}

impl MockServerFactory {
    pub fn new() -> MockServerFactory {
        MockServerFactory {
            me: Arc::new(Me {
                id: Id::new(),
                pub_key: (**pub_key_1()).clone(),
                priv_key: (**priv_key_1()).clone(),
            }),
            contact: ServerContact::new("127.0.0.1".to_string(), &pub_key_1()),

            send_msgs: Err(ServerError::OtherCode("not set".to_string())),
            fetch_msgs: Err(ServerError::OtherCode("not set".to_string())),
            get_statuses: Err(ServerError::OtherCode("not set".to_string())),
            mark_received: Err(ServerError::OtherCode("not set".to_string())),
            request_meta: Err(ServerError::OtherCode("not set".to_string())),
            request_meta_hash: Err(ServerError::OtherCode("not set".to_string())),
            post_my_meta: Err(ServerError::OtherCode("not set".to_string())),
            ping: Err(ServerError::OtherCode("not set".to_string())),
        }
    }

    pub fn with_send_msgs(mut self, res: Result<HashMap<Uuid, ApiMsgStatus>, ServerError>) -> Self {
        self.send_msgs = res;
        self
    }

    pub fn with_fetch_msgs(mut self, res: Result<Vec<ApiMsgPacked>, ServerError>) -> Self {
        self.fetch_msgs = res;
        self
    }

    pub fn with_get_statuses(
        mut self,
        res: Result<HashMap<Uuid, ApiMsgStatus>, ServerError>,
    ) -> Self {
        self.get_statuses = res;
        self
    }

    pub fn mark_received(mut self, res: Result<(), ServerError>) -> Self {
        self.mark_received = res;
        self
    }

    pub fn request_meta(mut self, res: Result<Box<ApiUserMeta>, ServerError>) -> Self {
        self.request_meta = res;
        self
    }

    pub fn request_meta_hash(mut self, res: Result<HashMap<Uuid, String>, ServerError>) -> Self {
        self.request_meta_hash = res;
        self
    }

    pub fn post_my_meta(mut self, res: Result<(), ServerError>) -> Self {
        self.post_my_meta = res;
        self
    }

    pub fn ping(mut self, res: Result<(), ServerError>) -> Self {
        self.ping = res;
        self
    }
}

#[derive(Default, Clone)]
pub struct MockMsgPacker {
    pub packed: Rc<RefCell<Vec<ApiMsg>>>,
    pub to_unpack: HashMap<Uuid, Result<ApiMsg, PackerError>>,
}

unsafe impl Send for MockMsgPacker {}
unsafe impl Sync for MockMsgPacker {}

#[derive(Clone)]
pub struct MockFS {
    req: Rc<RefCell<MockFSCtxReq>>,
    resp: Rc<RefCell<MockFSCtxResp>>,
}

unsafe impl Send for MockFS {}
unsafe impl Sync for MockFS {}

#[derive(Default, Clone)]
pub struct MockFSCtxReq {
    pub create_dir: Vec<String>,
    pub read_file: Vec<String>,
    pub write_file: Vec<(String, Vec<u8>)>,
    pub append_file: Vec<(String, Vec<u8>)>,
    pub get_block: Vec<(String, u64)>,
    pub get_meta: Vec<String>,
}

#[derive(Default)]
pub struct MockFSCtxResp {
    pub read_file: Vec<std::io::Result<Vec<u8>>>,
    pub write_file: Vec<std::io::Result<()>>,
    pub append_file: Vec<std::io::Result<()>>,
    pub get_block: Vec<std::io::Result<Vec<u8>>>,
    pub get_meta: Vec<std::io::Result<FileMeta>>,
}

impl MockFS {
    pub fn new(req: MockFSCtxReq, resp: MockFSCtxResp) -> Self {
        Self {
            req: Rc::new(RefCell::new(req)),
            resp: Rc::new(RefCell::new(resp)),
        }
    }

    pub fn get_req(&self) -> MockFSCtxReq {
        self.req.borrow().clone()
    }
}

impl Clone for WrappedStorage {
    fn clone(&self) -> Self {
        Self {
            storage: self.storage.clone(),
        }
    }
}

impl IStorage for WrappedStorage {
    fn init(&self) -> DynFutRes<(), DbError> {
        self.storage.init()
    }
    fn lock(&self) -> DynFut<Box<dyn ISyncStorage>> {
        self.storage.lock()
    }
}

impl MockTaskScheduler {
    pub fn new() -> Self {
        Self {
            mtx: Arc::new(Mutex::new(Default::default())),
        }
    }
    pub async fn get_schedule_task(&self) -> Vec<NewTask> {
        self.mtx.lock().await.schedule_task.clone()
    }
    pub async fn get_schedule_periodic(&self) -> Vec<String> {
        self.mtx.lock().await.schedule_periodic.clone()
    }
    pub async fn download_file(&self) -> Vec<(Id, Id, u64, String)> {
        self.mtx.lock().await.download_file.clone()
    }
}

impl ITaskScheduler for MockTaskScheduler {
    fn schedule_task(&self, task: NewTask) -> DynFutRes<TaskId, AtexError> {
        let slf = self.clone();
        Box::pin(async move {
            let mut ctx = slf.mtx.lock().await;
            ctx.schedule_task.push(task);
            Ok(1)
        })
    }

    fn schedule_periodic(&self, name: &str) -> DynFutRes<(), AtexError> {
        let slf = self.clone();
        let name = name.to_string();
        Box::pin(async move {
            let mut ctx = slf.mtx.lock().await;
            ctx.schedule_periodic.push(name);
            Ok(())
        })
    }

    fn get_task_status(&self, _: TaskId) -> DynFutRes<TaskStatus, AtexError> {
        let slf = self.clone();
        Box::pin(async move {
            let mut ctx = slf.mtx.lock().await;
            ctx.get_task_status_res.remove(0)
        })
    }

    fn download_file(
        &self,
        user_id: Id,
        file_id: Id,
        max_parts: u64,
        path_to_save: &str,
    ) -> DynFutRes<TaskId, AtexError> {
        let slf = self.clone();
        let path_to_save = path_to_save.to_string();
        Box::pin(async move {
            let mut ctx = slf.mtx.lock().await;
            ctx.download_file
                .push((user_id, file_id, max_parts, path_to_save));
            Ok(1)
        })
    }
}

impl IFS for MockFS {
    fn exec_dir(&self) -> std::io::Result<PathBuf> {
        Ok(PathBuf::from("/exec/dir/"))
    }

    fn create_dir(&self, path: &Path) -> std::io::Result<()> {
        let req = self.req.borrow_mut().create_dir.pop().unwrap();
        assert_eq!(req, path.to_str().unwrap().to_string());
        Ok(())
    }

    fn read_file(&self, path: &Path) -> std::io::Result<Vec<u8>> {
        let req = self.req.borrow_mut().read_file.pop().unwrap();
        assert_eq!(req, path.to_str().unwrap().to_string());
        let mut resp = self.resp.borrow_mut();
        resp.read_file.pop().unwrap()
    }

    fn write_file(&self, path: &Path, data: &[u8]) -> std::io::Result<()> {
        let req = self.req.borrow_mut().write_file.pop().unwrap();
        assert_eq!(req, (path.to_str().unwrap().to_string(), data.to_vec()),);
        self.resp.borrow_mut().write_file.pop().unwrap()
    }

    fn append_file(&self, path: &Path, data: &[u8]) -> std::io::Result<()> {
        let req = self.req.borrow_mut().append_file.pop().unwrap();
        assert_eq!(req, (path.to_str().unwrap().to_string(), data.to_vec()),);
        self.resp.borrow_mut().append_file.pop().unwrap()
    }

    fn get_block(&self, path: &Path, block_id: u64) -> std::io::Result<Vec<u8>> {
        let req = self.req.borrow_mut().get_block.pop().unwrap();
        assert_eq!(req, (path.to_str().unwrap().to_string(), block_id),);
        self.resp.borrow_mut().get_block.pop().unwrap()
    }

    fn get_meta(&self, path: &Path) -> std::io::Result<FileMeta> {
        let req = self.req.borrow_mut().get_meta.pop().unwrap();
        assert_eq!(req, path.to_str().unwrap().to_string());
        self.resp.borrow_mut().get_meta.pop().unwrap()
    }
}

impl IServerClientFactory for MockServerFactory {
    fn produce(
        &self,
        me: Arc<Me>,
        contact: &ServerContact,
    ) -> Result<Box<dyn IServerClient>, ServerError> {
        let mut slf = self.clone();
        slf.me = me;
        slf.contact = contact.clone();
        Ok(Box::new(slf))
    }
}

impl IServerClient for MockServerFactory {
    fn addr(&self) -> &str {
        &self.contact.addr
    }

    fn send_msgs(
        &self,
        _: Vec<ApiMsgPacked>,
    ) -> DynFutRes<HashMap<Uuid, ApiMsgStatus>, ServerError> {
        let res = self.send_msgs.clone();
        Box::pin(async move { res })
    }

    fn fetch_msgs(&self) -> DynFutRes<Vec<ApiMsgPacked>, ServerError> {
        let res = self.fetch_msgs.clone();
        Box::pin(async move { res })
    }

    fn get_statuses(&self, _: Vec<Uuid>) -> DynFutRes<HashMap<Uuid, ApiMsgStatus>, ServerError> {
        let res = self.get_statuses.clone();
        Box::pin(async move { res })
    }

    fn mark_received(&self, _: Vec<Uuid>) -> DynFutRes<(), ServerError> {
        let res = self.mark_received.clone();
        Box::pin(async move { res })
    }

    fn request_meta(&self, _: Uuid, _: &RsaPublicKey) -> DynFutRes<Box<ApiUserMeta>, ServerError> {
        let res = self.request_meta.clone();
        Box::pin(async move { res })
    }

    fn request_meta_hash(
        &self,
        _: &[(Uuid, &RsaPublicKey)],
    ) -> DynFutRes<HashMap<Uuid, String>, ServerError> {
        let res = self.request_meta_hash.clone();
        Box::pin(async move { res })
    }

    fn post_my_meta(&self, _: Arc<ApiUserMeta>) -> DynFutRes<(), ServerError> {
        let res = self.post_my_meta.clone();
        Box::pin(async move { res })
    }

    fn ping(&self) -> DynFutRes<(), ServerError> {
        let res = self.ping.clone();
        Box::pin(async move { res })
    }
}

impl IApiMsgPacker for MockMsgPacker {
    fn pack(&self, msg: ApiMsg, _: Option<&RsaPublicKey>, _: &RsaPrivateKey) -> ApiMsgPacked {
        let res = ApiMsgPacked {
            receiver: msg.header.receiver,
            id: msg.header.id,
            key_code: "code".to_string(),
            data: vec![1, 2, 3],
            signature: vec![1, 2, 3],
        };
        self.packed.borrow_mut().push(msg);
        res
    }

    fn unpack(
        &self,
        msg: &ApiMsgPacked,
        _: Option<&RsaPublicKey>,
        _: Option<&RsaPrivateKey>,
    ) -> Result<ApiMsg, PackerError> {
        (*self.to_unpack.get(&msg.id).unwrap()).clone()
    }
}

#[singleton]
pub fn logger() -> Logger {
    Logger::mock()
}

#[singleton]
pub fn crypto() -> CryptoOps<&'static Logger> {
    CryptoOps::new(logger())
}

pub async fn storage_not_inited() -> WrappedStorage {
    let result = Storage::new(
        ":memory:",
        rsa_key::Serializer,
        server_contact::Serializer,
        UserMetaHashCalculator,
        crypto(),
    )
    .unwrap();
    result.init().await.unwrap();
    WrappedStorage {
        storage: Arc::new(result),
    }
}

pub async fn storage_inited() -> WrappedStorage {
    let storage = storage_not_inited().await;
    {
        let settings = storage.lock().await.settings();
        settings
            .set_me(&Me {
                id: Uuid::NAMESPACE_OID.into(),
                pub_key: (**pub_key_1()).clone(),
                priv_key: (**priv_key_1()).clone(),
            })
            .unwrap();
    }
    WrappedStorage {
        storage: Arc::new(storage),
    }
}

#[singleton]
pub fn priv_key_1() -> Arc<RsaPrivateKey> {
    Arc::new(rsa_key::Serializer.deserialize(PRIV_KEY_1).unwrap())
}

#[singleton]
pub fn pub_key_1() -> Arc<RsaPublicKey> {
    Arc::new(priv_key_1().to_public_key())
}

#[singleton]
pub fn priv_key_2() -> Arc<RsaPrivateKey> {
    Arc::new(rsa_key::Serializer.deserialize(PRIV_KEY_2).unwrap())
}

#[singleton]
pub fn pub_key_2() -> Arc<RsaPublicKey> {
    Arc::new(priv_key_2().to_public_key())
}
