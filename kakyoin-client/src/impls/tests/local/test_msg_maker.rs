use crate::entities::chat::{Chat, ChatId};
use crate::entities::common::Id;
use crate::entities::message::{LocalMsgAttached, NewMsgToSend};
use crate::entities::tasks::PeriodicName;
use crate::entities::user::UserStatus;
use crate::impls::local::msg_maker::MsgMaker;
use crate::impls::tests::common::{pub_key_2, storage_inited, MockTaskScheduler, WrappedStorage};
use crate::proto::local::{IMsgMaker, IStorage};
use kakyoin_base::entities::contact::ClientContact;
use kakyoin_base::impls::logger::Logger;

struct Setup {
    maker: Box<dyn IMsgMaker>,
    storage: WrappedStorage,
    task_scheduler: MockTaskScheduler,
    users: Vec<Id>,
    chat: ChatId,
}

impl Setup {
    async fn new() -> Self {
        let storage = storage_inited().await;
        let lock = storage.lock().await;
        let id1 = Id::new();
        let id2 = Id::new();
        lock.users()
            .contact(
                &ClientContact::new(id1.into(), &pub_key_2()),
                UserStatus::Trusted,
            )
            .unwrap();
        lock.users()
            .contact(
                &ClientContact::new(id2.into(), &pub_key_2()),
                UserStatus::Trusted,
            )
            .unwrap();
        let chat_id = ChatId::Chat(Id::new());
        lock.chats()
            .create_chat(Chat {
                id: chat_id.clone(),
                name: "aaa".to_string(),
                users: vec![id1, id2],
            })
            .unwrap();
        let scheduler = MockTaskScheduler::new();
        let maker = MsgMaker::new(Logger::mock(), storage.clone(), scheduler.clone());
        let mut users = vec![id1, id2];
        users.sort();
        lock.chats()
            .create_chat(Chat {
                id: ChatId::User(users[0]),
                name: "---".to_string(),
                users: vec![users[0]],
            })
            .unwrap();
        Self {
            maker: Box::new(maker),
            storage,
            task_scheduler: scheduler,
            users,
            chat: chat_id,
        }
    }
}

#[tokio::test]
async fn test_make_msg_for_chat() {
    let setup = Setup::new().await;
    setup
        .maker
        .make(NewMsgToSend {
            chat: setup.chat.clone(),
            text: "asd".to_string(),
            attached: LocalMsgAttached::Nothing,
        })
        .await
        .unwrap();
    assert_eq!(
        setup.task_scheduler.get_schedule_periodic().await,
        vec![PeriodicName::SEND_MSGS.to_string()],
    );
    let mut msgs = setup
        .storage
        .lock()
        .await
        .messages()
        .get_messages_to_send()
        .unwrap();
    msgs.sort_by_key(|m| m.receiver.clone());
    assert_eq!(msgs.len(), 2);
    assert_eq!(msgs[0].receiver, Some(setup.users[0]));
    assert_eq!(msgs[1].receiver, Some(setup.users[1]));
    assert_eq!(msgs[0].chat, setup.chat);
    assert_eq!(msgs[1].chat, setup.chat);
    assert!(!msgs[0].sent);
    assert!(!msgs[0].received);
    assert!(msgs[0].read);
    assert_eq!(msgs[0].local_uniq_id, msgs[1].local_uniq_id);
    assert!(msgs[0].local_uniq_id.is_some());
    assert!(msgs[0].sender.is_none());
    assert_eq!(msgs[0].text, "asd");
    assert_eq!(msgs[1].text, "asd");
}

#[tokio::test]
async fn test_make_msg_for_user() {
    let setup = Setup::new().await;
    setup
        .maker
        .make(NewMsgToSend {
            chat: ChatId::User(setup.users[0]),
            text: "asd".to_string(),
            attached: LocalMsgAttached::Nothing,
        })
        .await
        .unwrap();
    assert_eq!(
        setup.task_scheduler.get_schedule_periodic().await,
        vec![PeriodicName::SEND_MSGS.to_string()],
    );
    let msgs = setup
        .storage
        .lock()
        .await
        .messages()
        .get_messages_to_send()
        .unwrap();
    assert_eq!(msgs.len(), 1);
    assert_eq!(msgs[0].receiver, Some(setup.users[0]));
    assert_eq!(msgs[0].chat, ChatId::User(setup.users[0]));
    assert!(!msgs[0].sent);
    assert!(!msgs[0].received);
    assert!(msgs[0].read);
    assert!(msgs[0].local_uniq_id.is_some());
    assert!(msgs[0].sender.is_none());
    assert_eq!(msgs[0].text, "asd");
}
