use crate::entities::chat::{Chat, ChatId};
use crate::entities::common::Id;
use crate::entities::errors::DbError;
use crate::entities::message::{LocalMsgAttached, NewLocalMsg};
use crate::entities::user::UserStatus;
use crate::impls::tests::common::{pub_key_2, storage_inited};
use crate::proto::local::ILocalMsgRepo;
use crate::proto::local::IStorage;
use kakyoin_base::entities::contact::ClientContact;

async fn repo_with_user_and_chat() -> (Box<dyn ILocalMsgRepo>, Id, ChatId) {
    let storage = storage_inited().await.lock().await;
    let user_id = Id::new();
    let chat_id = ChatId::Chat(Id::new());
    storage
        .users()
        .contact(
            &ClientContact::new(user_id.into(), &pub_key_2()),
            UserStatus::Trusted,
        )
        .unwrap();
    storage
        .chats()
        .create_chat(Chat {
            id: chat_id.clone(),
            name: "abc".to_string(),
            users: vec![user_id],
        })
        .unwrap();
    storage
        .chats()
        .create_chat(Chat {
            id: ChatId::User(user_id),
            name: "cde".to_string(),
            users: vec![user_id],
        })
        .unwrap();
    (storage.messages(), user_id, chat_id)
}

async fn repo_with_msgs() -> (Box<dyn ILocalMsgRepo>, Id) {
    let (repo, id, _) = repo_with_user_and_chat().await;
    repo.create_messages(&[
        NewLocalMsg {
            sender: Some(id),
            receiver: None,
            sender_key_code: Some("xyz".to_string()),
            chat: ChatId::User(id),
            uuid: Id::new(),
            local_uniq_id: None,
            text: "msg1".to_string(),
            attached: LocalMsgAttached::Nothing,
            sent: false,
            received: false,
            read: false,
            ts: 0,
        },
        NewLocalMsg {
            sender: Some(id),
            receiver: None,
            sender_key_code: Some("xyz".to_string()),
            chat: ChatId::User(id),
            uuid: Id::new(),
            local_uniq_id: Some(1),
            text: "msg2".to_string(),
            attached: LocalMsgAttached::Nothing,
            sent: false,
            received: false,
            read: false,
            ts: 1,
        },
    ])
    .unwrap();
    (repo, id)
}

#[tokio::test]
async fn test_filter_by_chat() {
    let (repo, user, chat) = repo_with_user_and_chat().await;
    let mut chat_msg = NewLocalMsg {
        sender: None,
        receiver: Some(user),
        sender_key_code: None,
        chat: chat.clone(),
        uuid: Id::new(),
        local_uniq_id: None,
        text: "Hello Alice".to_string(),
        attached: LocalMsgAttached::Nothing,
        sent: false,
        received: false,
        read: true,
        ts: 0,
    };
    let user_msg = NewLocalMsg {
        sender: Some(user),
        receiver: None,
        sender_key_code: Some("xyz".to_string()),
        chat: ChatId::User(user),
        uuid: Id::new(),
        local_uniq_id: Some(1),
        text: "Hello Bob".to_string(),
        attached: LocalMsgAttached::Nothing,
        sent: true,
        received: true,
        read: false,
        ts: 0,
    };
    repo.create_messages(&[chat_msg.clone(), user_msg.clone()])
        .unwrap();
    let mut got = repo.get_chat_messages(&chat, None, 10).unwrap();
    assert_eq!(got.len(), 1);
    let got: NewLocalMsg = (*got.remove(0)).into();
    assert_eq!(got, chat_msg);
    let mut got = repo
        .get_chat_messages(&ChatId::User(user), None, 10)
        .unwrap();
    assert_eq!(got.len(), 1);
    let got: NewLocalMsg = (*got.remove(0)).into();
    assert_eq!(got, user_msg);
    assert!(repo
        .get_chat_messages(&ChatId::Chat(Id::new()), None, 10)
        .unwrap()
        .is_empty());
    chat_msg.uuid = Id::new();
    chat_msg.chat = ChatId::Chat(Id::new());
    repo.create_messages(&[chat_msg]).unwrap_err();
}

#[tokio::test]
async fn test_order_in_one_chat() {
    let (repo, user_id) = repo_with_msgs().await;
    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 10)
        .unwrap();
    let txt = msgs.iter().map(|m| -> &str { &m.text }).collect::<Vec<_>>();
    assert_eq!(txt, &["msg1", "msg2"]);

    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), Some(msgs[1].db_id), 10)
        .unwrap();
    let txt = msgs.iter().map(|m| -> &str { &m.text }).collect::<Vec<_>>();
    assert_eq!(txt, &["msg1"]);

    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 1)
        .unwrap();
    let txt = msgs.iter().map(|m| -> &str { &m.text }).collect::<Vec<_>>();
    assert_eq!(txt, &["msg2"]);
}

#[tokio::test]
async fn test_already_in_base() {
    let (repo, user_id) = repo_with_msgs().await;
    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 10)
        .unwrap()
        .into_iter()
        .map(|m| m.uuid)
        .collect::<Vec<_>>();
    let got = repo
        .get_already_in_base(&[Id::new(), msgs[0], Id::new()])
        .unwrap()
        .into_iter()
        .collect::<Vec<_>>();
    assert_eq!(got, &[msgs[0]]);
}

#[tokio::test]
async fn test_create_twice() {
    let (repo, user_id) = repo_with_msgs().await;
    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 10)
        .unwrap()
        .into_iter()
        .collect::<Vec<_>>();
    let new_msgs = msgs
        .iter()
        .map(|msg| NewLocalMsg::from((**msg).clone()))
        .collect::<Vec<_>>();
    let err = repo.create_messages(&new_msgs).unwrap_err();
    assert_eq!(err, DbError::Constraint);
}

#[tokio::test]
async fn test_attachment() {
    let (repo, user_id) = repo_with_msgs().await;
    let new_msg = NewLocalMsg {
        sender: None,
        receiver: Some(user_id),
        sender_key_code: None,
        chat: ChatId::User(user_id),
        uuid: Id::new(),
        local_uniq_id: None,
        text: "hohoho".to_string(),
        attached: LocalMsgAttached::FileId {
            id: Id::new(),
            size: 20,
            parts: 10,
        },
        sent: false,
        received: false,
        read: false,
        ts: 100,
    };
    repo.create_messages(&[new_msg.clone()]).unwrap();
    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 1)
        .unwrap();
    assert_eq!(NewLocalMsg::from((*msgs[0]).clone()), new_msg);
    let got_by_id = repo.get_by_uid(&new_msg.uuid).unwrap();
    assert_eq!(got_by_id, msgs[0]);
}

#[tokio::test]
async fn test_get_by_uid() {
    let (repo, user_id) = repo_with_msgs().await;
    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 1)
        .unwrap();
    let got_by_id = repo.get_by_uid(&msgs[0].uuid).unwrap();
    assert_eq!(msgs[0], got_by_id);
}

#[tokio::test]
async fn test_mark_received() {
    let (repo, user_id) = repo_with_msgs().await;

    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 1)
        .unwrap();
    assert!(!msgs[0].sent);
    assert!(!msgs[0].read);
    assert!(!msgs[0].received);

    repo.mark_as_received(&[msgs[0].uuid]).unwrap();
    let msg = repo.get_by_uid(&msgs[0].uuid).unwrap();
    assert!(msg.sent);
    assert!(!msg.read);
    assert!(msg.received);
}

#[tokio::test]
async fn test_mark_sent() {
    let (repo, user_id) = repo_with_msgs().await;

    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 1)
        .unwrap();
    assert!(!msgs[0].sent);
    assert!(!msgs[0].read);
    assert!(!msgs[0].received);

    repo.mark_as_sent(&[msgs[0].uuid]).unwrap();
    let msg = repo.get_by_uid(&msgs[0].uuid).unwrap();
    assert!(msg.sent);
    assert!(!msg.read);
    assert!(!msg.received);
}

#[tokio::test]
async fn test_mark_read() {
    let (repo, user_id) = repo_with_msgs().await;

    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 1)
        .unwrap();
    assert!(!msgs[0].sent);
    assert!(!msgs[0].read);
    assert!(!msgs[0].received);

    repo.mark_as_read(&[msgs[0].uuid]).unwrap();
    let msg = repo.get_by_uid(&msgs[0].uuid).unwrap();
    assert!(!msg.sent);
    assert!(msg.read);
    assert!(!msg.received);
}

#[tokio::test]
async fn test_delete() {
    let (repo, user_id) = repo_with_msgs().await;

    let msgs = repo
        .get_chat_messages(&ChatId::User(user_id), None, 1)
        .unwrap();
    let msg_id = msgs[0].uuid;
    assert!(repo.get_by_uid(&msg_id).is_ok());
    repo.delete_messages(&[msg_id]).unwrap();
    assert_eq!(repo.get_by_uid(&msg_id).unwrap_err(), DbError::NotFound,)
}

#[tokio::test]
async fn test_get_messages_to_send() {
    let (repo, _) = repo_with_msgs().await;
    let msgs = repo.get_messages_to_send().unwrap();
    assert_eq!(msgs.len(), 2);
    repo.mark_as_received(&[msgs[0].uuid]).unwrap();
    let msgs = repo.get_messages_to_send().unwrap();
    assert_eq!(msgs.len(), 1);
}

#[tokio::test]
async fn test_next_uniq_id() {
    let (repo, _) = repo_with_msgs().await;
    let a = repo.next_local_uniq_id().unwrap();
    let b = repo.next_local_uniq_id().unwrap();
    let c = repo.next_local_uniq_id().unwrap();
    assert!(c > b && b > a);
}

#[tokio::test]
async fn get_first_id_of_same_code_some() {
    let (repo, id, _) = repo_with_user_and_chat().await;
    repo.create_messages(&[
        NewLocalMsg {
            sender: Some(id),
            receiver: None,
            sender_key_code: Some("xxx".to_string()),
            chat: ChatId::User(id),
            uuid: Id::new(),
            local_uniq_id: Some(3),
            text: "abc".to_string(),
            attached: LocalMsgAttached::Nothing,
            sent: false,
            received: false,
            read: false,
            ts: 0,
        },
        NewLocalMsg {
            sender: Some(id),
            receiver: None,
            sender_key_code: Some("xxx".to_string()),
            chat: ChatId::User(id),
            uuid: Id::new(),
            local_uniq_id: Some(3),
            text: "def".to_string(),
            attached: LocalMsgAttached::Nothing,
            sent: false,
            received: false,
            read: false,
            ts: 1,
        },
    ])
    .unwrap();
    let msgs = repo.get_chat_messages(&ChatId::User(id), None, 5).unwrap();
    assert_eq!(msgs.len(), 2);
    assert_eq!(msgs[0].text, "abc");
    assert_eq!(msgs[1].text, "def");
    let first_id = repo.get_first_id_of_same_code(msgs[0].db_id).unwrap();
    assert_eq!(first_id, msgs[0].db_id);
    let first_id = repo.get_first_id_of_same_code(msgs[1].db_id).unwrap();
    assert_eq!(first_id, msgs[0].db_id);
}

#[tokio::test]
async fn get_first_id_of_same_code_none() {
    let (repo, id) = repo_with_msgs().await;
    let msgs = repo.get_chat_messages(&ChatId::User(id), None, 5).unwrap();
    assert_eq!(msgs[1].local_uniq_id, Some(1));
    let new_id = repo.get_first_id_of_same_code(msgs[1].db_id).unwrap();
    assert_eq!(new_id, msgs[1].db_id);
}

#[tokio::test]
async fn test_get_last_unread_messages_none() {
    let (repo, _, _) = repo_with_user_and_chat().await;
    let msgs = repo.get_last_unread_messages(6).unwrap();
    assert!(msgs.is_empty())
}

#[tokio::test]
async fn test_get_last_unread_messages_some() {
    let (repo, _) = repo_with_msgs().await;
    let msgs = repo.get_last_unread_messages(6).unwrap();
    assert_eq!(msgs.len(), 2)
}
