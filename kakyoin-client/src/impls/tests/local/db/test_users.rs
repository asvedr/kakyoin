use crate::entities::common::Id;
use crate::entities::user::{User, UserStatus};
use crate::impls::tests::common::{crypto, pub_key_1, pub_key_2, storage_inited};
use crate::proto::local::IStorage;
use crate::proto::local::IUsersRepo;
use kakyoin_base::entities::contact::ClientContact;
use kakyoin_base::entities::server_call::ApiUserMeta;
use kakyoin_base::proto::common::ICryptoOps;
use std::collections::HashMap;
use std::sync::Arc;

async fn repo_with_contact() -> (Box<dyn IUsersRepo>, Id, Id) {
    let repo = storage_inited().await.lock().await.users();
    let id1 = Id::new();
    let id2 = Id::new();
    repo.contact(
        &ClientContact::new(id1.into(), &pub_key_1()),
        UserStatus::New,
    )
    .unwrap();
    repo.contact(
        &ClientContact::new(id2.into(), &pub_key_2()),
        UserStatus::Trusted,
    )
    .unwrap();
    (repo, id1, id2)
}

#[tokio::test]
async fn test_contact() {
    let repo = storage_inited().await.lock().await.users();
    assert!(repo.get_all_ids().unwrap().is_empty());
    let id1 = Id::new();
    let id2 = Id::new();
    repo.contact(
        &ClientContact::new(id1.into(), &pub_key_1()),
        UserStatus::New,
    )
    .unwrap();
    repo.contact(
        &ClientContact::new(id2.into(), &pub_key_1()),
        UserStatus::Trusted,
    )
    .unwrap();
    let ids = repo.get_all_ids().unwrap();
    assert!(ids == &[id1, id2] || ids == &[id2, id1]);
}

#[tokio::test]
async fn test_set_get_info() {
    let (repo, id1, _) = repo_with_contact().await;
    assert!(repo.get_info(&[Id::new()]).unwrap().is_empty());
    let meta = repo.get_info(&[id1]).unwrap().remove(&id1).unwrap();
    assert_eq!(
        *meta,
        ApiUserMeta {
            name: "".to_string(),
            description: "".to_string(),
            photo: "".to_string(),
            hash: "".to_string(),
        }
    );
    let meta = ApiUserMeta {
        name: "Alex".to_string(),
        description: "Vence".to_string(),
        photo: "".to_string(),
        hash: "aabbff".to_string(),
    };
    repo.set_info(&id1, &meta).unwrap();
    let got = repo.get_info(&[id1]).unwrap().remove(&id1).unwrap();
    assert_eq!(*got, meta);
}

#[tokio::test]
async fn test_set_get_status() {
    let (repo, id1, id2) = repo_with_contact().await;
    let map = repo.get_status(&[id1, id2]).unwrap();
    assert_eq!(
        map,
        [(id1, UserStatus::New), (id2, UserStatus::Trusted)]
            .into_iter()
            .collect::<HashMap<_, _>>()
    );
    repo.set_status(&id1, UserStatus::Trusted).unwrap();
    repo.set_status(&id2, UserStatus::Banned).unwrap();
    let map = repo.get_status(&[id1, id2]).unwrap();
    assert_eq!(
        map,
        [(id1, UserStatus::Trusted), (id2, UserStatus::Banned)]
            .into_iter()
            .collect::<HashMap<_, _>>()
    );
}

#[tokio::test]
async fn test_get_keys() {
    let (repo, id1, id2) = repo_with_contact().await;
    let map = repo.get_keys(&[id1, id2]).unwrap();
    assert_eq!(
        map,
        [(id1, pub_key_1().clone()), (id2, pub_key_2().clone())]
            .into_iter()
            .collect::<HashMap<_, _>>()
    )
}

#[tokio::test]
async fn test_get_by_uids() {
    let (repo, id1, _) = repo_with_contact().await;
    let meta = ApiUserMeta {
        name: "Alex".to_string(),
        description: "Vence".to_string(),
        photo: "base64data".to_string(),
        hash: "aabbff".to_string(),
    };
    repo.set_info(&id1, &meta).unwrap();
    let user = *repo.get_by_uid(&[id1]).unwrap().remove(0);
    let expected = User {
        id: id1,
        pub_key: pub_key_1().clone(),
        pub_key_code: crypto().get_key_code(&pub_key_1()),
        info: Arc::new(meta),
        status: UserStatus::New,
    };
    assert_eq!(user, expected)
}

#[tokio::test]
async fn test_get_by_name() {
    let (repo, id1, _) = repo_with_contact().await;
    let meta = ApiUserMeta {
        name: "Alex".to_string(),
        description: "Vence".to_string(),
        photo: "base64data".to_string(),
        hash: "aabbff".to_string(),
    };
    repo.set_info(&id1, &meta).unwrap();
    let user = repo.get_by_name("Ale", 3).unwrap().remove(0);
    assert_eq!(user.id, id1);
}

#[tokio::test]
async fn test_del() {
    let (repo, id1, id2) = repo_with_contact().await;
    assert_eq!(repo.get_all_ids().unwrap().len(), 2);
    repo.delete_user(&id1).unwrap();
    assert_eq!(repo.get_all_ids().unwrap(), [id2]);
}

#[tokio::test]
async fn test_get_hashes() {
    let (repo, id1, id2) = repo_with_contact().await;
    assert_eq!(
        repo.get_hashes().unwrap(),
        &[
            (id1, pub_key_1().clone(), "".to_string()),
            (id2, pub_key_2().clone(), "".to_string()),
        ]
    );
}
