use crate::entities::common::Id;
use crate::entities::errors::DbError;
use crate::entities::files::LocalFileMeta;
use crate::entities::user::UserStatus;
use crate::impls::tests::common::{pub_key_1, storage_inited};
use crate::proto::local::ISharedFilesRepo;
use crate::proto::local::IStorage;
use kakyoin_base::entities::contact::ClientContact;

async fn repo_with_file() -> (Box<dyn ISharedFilesRepo>, Id, Id) {
    let storage = storage_inited().await.lock().await;
    let user_id = Id::new();
    let file_id = Id::new();
    storage
        .users()
        .contact(
            &ClientContact::new(user_id.into(), &**pub_key_1()),
            UserStatus::Trusted,
        )
        .unwrap();
    let meta = LocalFileMeta {
        id: file_id,
        path: "/home/file.txt".to_string(),
        name: "file.txt".to_string(),
        size: 20,
        parts: 10,
    };
    storage
        .shared_files()
        .register_file(&meta, &[user_id])
        .unwrap();
    (storage.shared_files(), user_id, file_id)
}

#[tokio::test]
async fn test_get_for_user() {
    let (repo, user_id, file_id) = repo_with_file().await;
    let err = repo.get_file_for_user(&file_id, &Id::new()).unwrap_err();
    assert_eq!(err, DbError::NotFound);
    let meta = repo.get_file_for_user(&file_id, &user_id).unwrap();
    assert_eq!(meta.path, "/home/file.txt");
}

#[tokio::test]
async fn test_del() {
    let (repo, user_id, file_id) = repo_with_file().await;
    let meta = repo.get_file_for_user(&file_id, &user_id).unwrap();
    assert_eq!(meta.path, "/home/file.txt");
    repo.delete_file_by_id(&file_id).unwrap();
    let err = repo.get_file_for_user(&file_id, &user_id).unwrap_err();
    assert_eq!(err, DbError::NotFound);
}
