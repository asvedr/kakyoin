use crate::entities::chat::{Chat, ChatId, ChatShort};
use crate::entities::common::Id;
use crate::entities::message::{LocalMsgAttached, NewLocalMsg};
use crate::entities::user::UserStatus;
use crate::impls::tests::common::{pub_key_1, pub_key_2, storage_inited};
use crate::proto::local::IStorage;
use crate::proto::local::{IChatRepo, ILocalMsgRepo};
use kakyoin_base::entities::contact::ClientContact;

async fn repo_with_users() -> (Box<dyn IChatRepo>, Box<dyn ILocalMsgRepo>, Id, Id) {
    let storage = storage_inited().await.lock().await;
    let id1 = Id::new();
    let id2 = Id::new();
    storage
        .users()
        .contact(
            &ClientContact::new(id1.into(), &pub_key_1()),
            UserStatus::New,
        )
        .unwrap();
    storage
        .users()
        .contact(
            &ClientContact::new(id2.into(), &pub_key_2()),
            UserStatus::Trusted,
        )
        .unwrap();
    (storage.chats(), storage.messages(), id1, id2)
}

#[tokio::test]
async fn test_create_chat_user() {
    let (repo, _, user1, _) = repo_with_users().await;
    let id = ChatId::User(user1);
    repo.create_chat(Chat {
        id: id.clone(),
        name: "abc".to_string(),
        users: vec![user1],
    })
    .unwrap();
    repo.create_chat(Chat {
        id: id.clone(),
        name: "cde".to_string(),
        users: vec![user1],
    })
    .unwrap();
    let chat = repo.get_chat(&id).unwrap();
    assert_eq!(chat.id, id);
    assert_eq!(chat.name, "abc");
    assert_eq!(chat.users, &[user1]);
    repo.set_name(&id, "zxc").unwrap();
    let chat = repo.get_chat(&id).unwrap();
    assert_eq!(chat.name, "zxc");
}

#[tokio::test]
async fn test_create_chat_chat() {
    let (repo, _, user1, user2) = repo_with_users().await;
    let id = ChatId::Chat(Id::new());
    repo.create_chat(Chat {
        id: id.clone(),
        name: "abc".to_string(),
        users: vec![user1],
    })
    .unwrap();
    repo.create_chat(Chat {
        id: id.clone(),
        name: "cde".to_string(),
        users: vec![user1, user2],
    })
    .unwrap();
    let mut chat = repo.get_chat(&id).unwrap();
    chat.users.sort();
    let mut users = vec![user1, user2];
    users.sort();
    assert_eq!(chat.id, id);
    assert_eq!(chat.name, "abc");
    assert_eq!(chat.users, users);
    repo.set_name(&id, "zxc").unwrap();
    let chat = repo.get_chat(&id).unwrap();
    assert_eq!(chat.name, "zxc");
}

#[tokio::test]
async fn test_add_participants_ok() {
    let (repo, _, user1, user2) = repo_with_users().await;
    let id = ChatId::Chat(Id::new());
    repo.create_chat(Chat {
        id: id.clone(),
        name: "abc".to_string(),
        users: vec![user1],
    })
    .unwrap();
    assert_eq!(repo.get_participants(&id).unwrap(), &[user1],);
    repo.add_participants(&id, &[user2]).unwrap();
    let mut got = repo.get_participants(&id).unwrap();
    got.sort();
    let mut expected = vec![user1, user2];
    expected.sort();
    assert_eq!(got, expected);
}

#[tokio::test]
async fn test_add_participants_err() {
    let (repo, _, user1, user2) = repo_with_users().await;
    let id = ChatId::User(user1);
    repo.create_chat(Chat {
        id: id.clone(),
        name: "abc".to_string(),
        users: vec![user1],
    })
    .unwrap();
    assert_eq!(repo.get_participants(&id).unwrap(), &[user1],);
    repo.add_participants(&id, &[user2]).unwrap_err();
    assert_eq!(repo.get_participants(&id).unwrap(), &[user1],)
}

#[tokio::test]
async fn test_get_short_info_for_all() {
    let (repo, msg_repo, user1, user2) = repo_with_users().await;
    let id = ChatId::Chat(Id::new());
    repo.create_chat(Chat {
        id: id.clone(),
        name: "abc".to_string(),
        users: vec![user1, user2],
    })
    .unwrap();
    repo.create_chat(Chat {
        id: ChatId::User(user1),
        name: "cde".to_string(),
        users: vec![user1],
    })
    .unwrap();
    repo.create_chat(Chat {
        id: ChatId::User(user2),
        name: "efg".to_string(),
        users: vec![user2],
    })
    .unwrap();
    msg_repo
        .create_messages(&[
            NewLocalMsg {
                sender: None,
                receiver: None,
                sender_key_code: None,
                chat: id.clone(),
                uuid: Id::new(),
                local_uniq_id: None,
                text: "hi!".to_string(),
                attached: LocalMsgAttached::Nothing,
                sent: false,
                received: false,
                read: false,
                ts: 1,
            },
            NewLocalMsg {
                sender: None,
                receiver: None,
                sender_key_code: None,
                chat: id.clone(),
                uuid: Id::new(),
                local_uniq_id: Some(1),
                text: "!ih".to_string(),
                attached: LocalMsgAttached::Nothing,
                sent: false,
                received: false,
                read: false,
                ts: 0,
            },
            NewLocalMsg {
                sender: None,
                receiver: None,
                sender_key_code: None,
                chat: ChatId::User(user1),
                uuid: Id::new(),
                local_uniq_id: Some(2),
                text: "op op".to_string(),
                attached: LocalMsgAttached::Nothing,
                sent: false,
                received: false,
                read: false,
                ts: 2,
            },
        ])
        .unwrap();
    let info = repo.get_short_info_for_all().unwrap();
    let expected = &[
        ChatShort {
            id: ChatId::User(user1),
            name: "cde".to_string(),
            last_msg: "op op".to_string(),
            read: false,
        },
        ChatShort {
            id,
            name: "abc".to_string(),
            last_msg: "hi!".to_string(),
            read: false,
        },
        ChatShort {
            id: ChatId::User(user2),
            name: "efg".to_string(),
            last_msg: "".to_string(),
            read: true,
        },
    ];
    assert_eq!(info, expected)
}

#[tokio::test]
async fn test_ban_chat() {
    let (repo, _, user1, user2) = repo_with_users().await;
    repo.create_chat(Chat {
        id: ChatId::User(user1),
        name: "abc".to_string(),
        users: vec![user1],
    })
    .unwrap();
    repo.create_chat(Chat {
        id: ChatId::User(user2),
        name: "cde".to_string(),
        users: vec![user2],
    })
    .unwrap();
    assert_eq!(repo.get_short_info_for_all().unwrap().len(), 2);
    assert_eq!(repo.get_full_list().unwrap().len(), 2);
    repo.ban_chat(&ChatId::User(user1)).unwrap();
    assert_eq!(repo.get_short_info_for_all().unwrap().len(), 1);
    assert_eq!(repo.get_full_list().unwrap().len(), 2);
    repo.ban_chat(&ChatId::User(user2)).unwrap();
    assert!(repo.get_short_info_for_all().unwrap().is_empty());
    assert_eq!(repo.get_full_list().unwrap().len(), 2);
}

#[tokio::test]
async fn test_delete_chat() {
    let (repo, _, user1, user2) = repo_with_users().await;
    repo.create_chat(Chat {
        id: ChatId::User(user1),
        name: "abc".to_string(),
        users: vec![user1],
    })
    .unwrap();
    repo.create_chat(Chat {
        id: ChatId::User(user2),
        name: "cde".to_string(),
        users: vec![user2],
    })
    .unwrap();
    assert_eq!(repo.get_short_info_for_all().unwrap().len(), 2);
    assert_eq!(repo.get_full_list().unwrap().len(), 2);
    repo.delete_chat(&ChatId::User(user1)).unwrap();
    assert_eq!(repo.get_short_info_for_all().unwrap().len(), 1);
    assert_eq!(repo.get_full_list().unwrap().len(), 1);
    repo.delete_chat(&ChatId::User(user2)).unwrap();
    assert!(repo.get_short_info_for_all().unwrap().is_empty());
    assert!(repo.get_full_list().unwrap().is_empty());
}
