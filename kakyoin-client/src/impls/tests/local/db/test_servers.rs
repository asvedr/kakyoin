use crate::impls::tests::common::{pub_key_1, storage_inited};
use crate::proto::local::IStorage;
use kakyoin_base::entities::contact::ServerContact;

#[tokio::test]
async fn test_empty() {
    let repo = storage_inited().await.lock().await.servers();
    assert!(repo.get_all().unwrap().is_empty())
}

#[tokio::test]
async fn test_add_del_server() {
    let repo = storage_inited().await.lock().await.servers();
    repo.get_all().unwrap();
    let servers = vec![
        ServerContact::new("addr1".to_string(), &pub_key_1()),
        ServerContact::new("addr2".to_string(), &pub_key_1()),
    ];
    repo.add_servers(servers.clone()).unwrap();
    let got = repo.get_all().unwrap();
    assert_eq!(servers, *got);
    repo.del_servers(&["addr2"]).unwrap();
    let got = repo.get_all().unwrap();
    assert_eq!(vec![servers[0].clone()], *got);
}
