use crate::entities::common::{Id, InitStatus, Me};
use crate::impls::tests::common::{priv_key_1, pub_key_1, storage_inited, storage_not_inited};
use crate::proto::local::IStorage;
use kakyoin_base::entities::server_call::ApiUserMeta;
use uuid::Uuid;

#[tokio::test]
async fn test_check_init() {
    let repo = storage_not_inited().await.lock().await.settings();
    assert_eq!(repo.check_inited().unwrap(), InitStatus::NoId);
    repo.set_me(&Me {
        id: Id::new(),
        pub_key: (**pub_key_1()).clone(),
        priv_key: (**priv_key_1()).clone(),
    })
    .unwrap();
    assert_eq!(repo.check_inited().unwrap(), InitStatus::Done);
}

#[tokio::test]
async fn test_get_me() {
    let repo = storage_inited().await.lock().await.settings();
    let me = repo.get_me().unwrap();
    assert_eq!(me.id, Uuid::NAMESPACE_OID.into());
    assert_eq!(me.pub_key, **pub_key_1());
    assert_eq!(me.priv_key, **priv_key_1());
}

#[tokio::test]
async fn test_meta() {
    let repo = storage_inited().await.lock().await.settings();
    let meta = repo.get_meta().unwrap();
    assert_eq!(
        *meta,
        ApiUserMeta {
            name: "".to_string(),
            description: "".to_string(),
            photo: "".to_string(),
            hash: "".to_string()
        }
    );
    repo.set_name("Alice").unwrap();
    let meta = repo.get_meta().unwrap();
    assert_eq!(
        *meta,
        ApiUserMeta {
            name: "Alice".to_string(),
            description: "".to_string(),
            photo: "".to_string(),
            hash: "7238095430289858522500".to_string()
        }
    );
    repo.set_description("PHd").unwrap();
    let meta = repo.get_meta().unwrap();
    assert_eq!(
        *meta,
        ApiUserMeta {
            name: "Alice".to_string(),
            description: "PHd".to_string(),
            photo: "".to_string(),
            hash: "17485374184006363148530".to_string()
        }
    );
    repo.set_photo("base64data").unwrap();
    let meta = repo.get_meta().unwrap();
    assert_eq!(
        *meta,
        ApiUserMeta {
            name: "Alice".to_string(),
            description: "PHd".to_string(),
            photo: "base64data".to_string(),
            hash: "69361637755917585475310".to_string()
        }
    );
}

#[tokio::test]
async fn test_get_set_pwd_hash() {
    let repo = storage_inited().await.lock().await.settings();
    assert!(repo.get_pwd_hash().unwrap().is_none());

    repo.set_pwd_hash(Some("abc".to_string())).unwrap();

    assert_eq!(repo.get_pwd_hash().unwrap(), Some("abc".to_string()));

    repo.set_pwd_hash(None).unwrap();

    assert!(repo.get_pwd_hash().unwrap().is_none());
}
