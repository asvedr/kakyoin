use crate::impls::local::auth_token_manager::AuthTokenManager;
use crate::proto::local::IAuthTokenManager;
use uuid::Uuid;

fn manager() -> Box<dyn IAuthTokenManager> {
    Box::new(AuthTokenManager::new())
}

#[tokio::test]
async fn test_on_off() {
    let man = manager();

    assert!(!man.is_auth_turned_on().await);

    man.set_active(true).await;

    assert!(man.is_auth_turned_on().await);

    man.set_active(false).await;

    assert!(!man.is_auth_turned_on().await);
}

#[tokio::test]
async fn test_gen_validate_mode_on() {
    let man = manager();
    man.set_active(true).await;

    let random_token = Uuid::new_v4().to_string().replace('-', "");
    assert!(!man.validate_token(&random_token).await);

    let correct_token = man.gen_token().await;

    assert!(!man.validate_token(&random_token).await);
    assert!(man.validate_token(&correct_token).await);
}

#[tokio::test]
async fn test_gen_validate_mode_off() {
    let man = manager();
    man.set_active(false).await;

    let random_token = Uuid::new_v4().to_string().replace('-', "");
    assert!(man.validate_token(&random_token).await);

    let correct_token = man.gen_token().await;

    assert!(man.validate_token(&random_token).await);
    assert!(man.validate_token(&correct_token).await);
}
