use crate::entities::common::Id;
use crate::impls::local::initializer::Initializer;
use crate::impls::tests::common::{
    priv_key_1, priv_key_2, pub_key_1, pub_key_2, storage_inited, storage_not_inited,
    WrappedStorage,
};
use crate::proto::local::IAuthTokenManager;
use crate::proto::local::{IInitializer, IStorage};
use kakyoin_base::entities::common::DynFut;
use kakyoin_base::entities::errors::CryptoErr;
use kakyoin_base::proto::common::ICryptoOps;
use rsa::{RsaPrivateKey, RsaPublicKey};
use std::sync::Arc;
use tokio::sync::Mutex;

struct MockCrypto;
#[derive(Clone)]
struct MockAuthTokenManager {
    active: Arc<Mutex<Option<bool>>>,
}
struct Setup {
    initer: Box<dyn IInitializer>,
    token_manager: MockAuthTokenManager,
}

impl MockAuthTokenManager {
    fn new() -> Self {
        Self {
            active: Arc::new(Mutex::new(None)),
        }
    }
}

impl IAuthTokenManager for MockAuthTokenManager {
    fn set_active(&self, val: bool) -> DynFut<()> {
        let mtx = self.active.clone();
        Box::pin(async move { *(mtx.lock().await) = Some(val) })
    }

    fn is_auth_turned_on(&self) -> DynFut<bool> {
        unimplemented!()
    }

    fn gen_token(&self) -> DynFut<String> {
        unimplemented!()
    }

    fn validate_token(&self, _: &str) -> DynFut<bool> {
        unimplemented!()
    }
}

impl ICryptoOps for MockCrypto {
    fn gen_priv_key(&self) -> Result<RsaPrivateKey, CryptoErr> {
        Ok((**priv_key_2()).clone())
    }

    fn encrypt(&self, _: &RsaPublicKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }

    fn decrypt(&self, _: &RsaPrivateKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }

    fn sign_len(&self) -> usize {
        unimplemented!()
    }

    fn sign(&self, _: &RsaPrivateKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }

    fn check_signature(&self, _: &RsaPublicKey, _: &[u8], _: &[u8]) -> Result<bool, CryptoErr> {
        unimplemented!()
    }

    fn get_key_code(&self, _: &RsaPublicKey) -> String {
        unimplemented!()
    }
}

impl Setup {
    fn new(storage: WrappedStorage) -> Self {
        let token_manager = MockAuthTokenManager::new();
        let initer = Box::new(Initializer::new(storage, MockCrypto, token_manager.clone()));
        Self {
            initer,
            token_manager,
        }
    }
}

#[tokio::test]
async fn test_check_init() {
    let storage = storage_not_inited().await;
    let initer = Setup::new(storage.clone()).initer;
    assert!(initer.check_init().await.is_err());
    let storage = storage_inited().await;
    let initer = Setup::new(storage.clone()).initer;
    assert!(initer.check_init().await.is_ok());
}

#[tokio::test]
async fn test_init_dflt() {
    let storage = storage_not_inited().await;
    let setup = Setup::new(storage.clone());
    setup
        .initer
        .init(None, None, "Name".to_string(), "Descr".to_string(), None)
        .await
        .unwrap();
    let repo = storage.lock().await.settings();
    let me = repo.get_me().unwrap();
    assert_eq!(me.priv_key, **priv_key_2());
    assert_eq!(me.pub_key, **pub_key_2());
    let meta = repo.get_meta().unwrap();
    assert_eq!(meta.name, "Name");
    assert_eq!(meta.description, "Descr");
    assert!(repo.get_pwd_hash().unwrap().is_none());
    let value = setup.token_manager.active.lock().await.clone();
    assert_eq!(value, Some(false))
}

#[tokio::test]
async fn test_init_with_params() {
    let storage = storage_not_inited().await;
    let setup = Setup::new(storage.clone());
    let id = Id::new();
    setup
        .initer
        .init(
            Some(id),
            Some((**priv_key_1()).clone()),
            "Name".to_string(),
            "Descr".to_string(),
            Some("123".to_string()),
        )
        .await
        .unwrap();
    let repo = storage.lock().await.settings();
    let me = repo.get_me().unwrap();
    assert_eq!(me.id, id);
    assert_eq!(me.priv_key, **priv_key_1());
    assert_eq!(me.pub_key, **pub_key_1());
    let meta = repo.get_meta().unwrap();
    assert_eq!(meta.name, "Name");
    assert_eq!(meta.description, "Descr");
    assert_eq!(
        repo.get_pwd_hash().unwrap(),
        Some("A665A45920422F9D417E4867EFDC4FB8A04A1F3FFF1FA07E998E86F7F7A27AE3".to_string())
    );
    let value = setup.token_manager.active.lock().await.clone();
    assert_eq!(value, Some(true))
}
