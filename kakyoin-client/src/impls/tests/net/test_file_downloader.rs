use crate::entities::chat::{Chat, ChatId};
use crate::entities::common::Id;
use crate::entities::errors::FileDownloaderError;

use crate::entities::user::UserStatus;
use crate::impls::net::file_downloader::FileDownloader;
use crate::impls::tests::common::{
    pub_key_2, storage_inited, MockFS, MockFSCtxReq, MockFSCtxResp, MockMsgPacker,
    MockServerFactory,
};
use crate::proto::local::IStorage;
use crate::proto::net::IFileDownloader;
use kakyoin_base::entities::api_msg::{ApiMsgBody, ApiMsgBodyReqFilePart, ApiMsgBodySendFilePart};
use kakyoin_base::entities::contact::ClientContact;
use kakyoin_base::impls::serializers::rsa_key;
use uuid::Uuid;

struct Setup {
    user: Id,
    loader: Box<dyn IFileDownloader>,
    packer: MockMsgPacker,
    fs: MockFS,
}

impl Setup {
    async fn new(append_file: &[(&str, &[u8])]) -> Self {
        let storage = storage_inited().await;
        let fs = MockFS::new(
            MockFSCtxReq {
                append_file: append_file
                    .iter()
                    .map(|(n, v)| (n.to_string(), v.to_vec()))
                    .collect(),
                ..Default::default()
            },
            MockFSCtxResp {
                append_file: append_file.iter().map(|_| Ok(())).collect(),
                ..Default::default()
            },
        );
        let user_id = Id::new();
        {
            let sync = storage.lock().await;
            sync.users()
                .contact(
                    &ClientContact::new(user_id.into(), pub_key_2()),
                    UserStatus::Trusted,
                )
                .unwrap();
            sync.chats()
                .create_chat(Chat {
                    id: ChatId::User(user_id),
                    name: "".to_string(),
                    users: vec![user_id],
                })
                .unwrap();
        }
        let factory = MockServerFactory::new();
        let packer = MockMsgPacker::default();
        let loader = FileDownloader::new(
            storage,
            fs.clone(),
            factory,
            rsa_key::Serializer,
            packer.clone(),
        );
        Self {
            user: user_id,
            loader: Box::new(loader),
            packer,
            fs,
        }
    }
}

#[tokio::test]
async fn test_start_loading_ok() {
    let setup = Setup::new(&[]).await;
    let file_id = Id::new();
    setup
        .loader
        .start_loading(setup.user, file_id, 5, "/home/file.bin".to_string())
        .await
        .unwrap();
    let msg = setup.packer.packed.borrow_mut().remove(0);
    assert_eq!(msg.header.receiver, setup.user.into());
    assert_eq!(msg.header.sender, Uuid::NAMESPACE_OID.into());
    assert_eq!(
        msg.body,
        ApiMsgBody::ReqFilePart(Box::new(ApiMsgBodyReqFilePart {
            file_id: file_id.into(),
            part: 0
        }))
    );
}

#[tokio::test]
async fn test_retry_ok() {
    let setup = Setup::new(&[]).await;
    let file_id = Id::new();
    setup
        .loader
        .start_loading(setup.user, file_id, 5, "/home/file.bin".to_string())
        .await
        .unwrap();
    setup
        .loader
        .retry_block_request(setup.user, file_id)
        .await
        .unwrap();
}

#[tokio::test]
async fn test_retry_err() {
    let setup = Setup::new(&[]).await;
    let file_id = Id::new();
    let err = setup
        .loader
        .retry_block_request(setup.user, file_id)
        .await
        .unwrap_err();
    assert_eq!(err, FileDownloaderError::FileNotFound)
}

#[tokio::test]
async fn test_get_status_ok() {
    let setup = Setup::new(&[]).await;
    let file_id = Id::new();
    setup
        .loader
        .start_loading(setup.user, file_id, 5, "/home/file.bin".to_string())
        .await
        .unwrap();
    let status = setup
        .loader
        .get_loading_status(setup.user, file_id)
        .await
        .unwrap();
    assert!(!status.completed);
    assert!(status.progress < 0.01);
}

#[tokio::test]
async fn test_get_status_none() {
    let setup = Setup::new(&[]).await;
    let file_id = Id::new();
    assert!(setup
        .loader
        .get_loading_status(setup.user, file_id)
        .await
        .is_none());
}

#[tokio::test]
async fn test_add_block_err() {
    let setup = Setup::new(&[]).await;
    // no error spawned
    let file_id = Id::new();
    setup
        .loader
        .add_block(
            setup.user,
            Box::new(ApiMsgBodySendFilePart {
                file_id: file_id.into(),
                part: 0,
                data: vec![1, 2, 3],
            }),
        )
        .await
        .unwrap();
    // but no action did
    assert!(setup
        .loader
        .get_loading_status(setup.user, file_id)
        .await
        .is_none());
    assert!(setup.fs.get_req().append_file.is_empty());
}

#[tokio::test]
async fn test_add_block_not_last() {
    let setup = Setup::new(&[("/home/file.bin", &[1, 1, 1])]).await;
    let file_id = Id::new();
    setup
        .loader
        .start_loading(setup.user, file_id, 2, "/home/file.bin".to_string())
        .await
        .unwrap();
    setup
        .loader
        .add_block(
            setup.user,
            Box::new(ApiMsgBodySendFilePart {
                file_id: file_id.into(),
                part: 0,
                data: vec![1, 1, 1],
            }),
        )
        .await
        .unwrap();

    let status = setup
        .loader
        .get_loading_status(setup.user, file_id)
        .await
        .unwrap();
    assert!(!status.completed);
    assert!(status.progress > 0.49 && status.progress < 0.51);

    assert!(setup.fs.get_req().append_file.is_empty());
    let packed = setup.packer.packed.borrow();
    assert_eq!(packed.len(), 2); // 2 = start + req next
    assert_eq!(
        packed[1].body,
        ApiMsgBody::ReqFilePart(Box::new(ApiMsgBodyReqFilePart {
            file_id: file_id.into(),
            part: 1
        }))
    )
}

#[tokio::test]
async fn test_add_block_last() {
    let setup = Setup::new(&[("/home/file.bin", &[1, 1, 1])]).await;
    let file_id = Id::new();
    setup
        .loader
        .start_loading(setup.user, file_id, 1, "/home/file.bin".to_string())
        .await
        .unwrap();
    setup
        .loader
        .add_block(
            setup.user,
            Box::new(ApiMsgBodySendFilePart {
                file_id: file_id.into(),
                part: 0,
                data: vec![1, 1, 1],
            }),
        )
        .await
        .unwrap();

    let status = setup
        .loader
        .get_loading_status(setup.user, file_id)
        .await
        .unwrap();
    assert!(status.completed);
    assert!(status.progress > 0.99);

    assert!(setup.fs.get_req().append_file.is_empty());
    let packed = setup.packer.packed.borrow();
    assert_eq!(packed.len(), 1); // 1 = start
}

#[tokio::test]
async fn test_retry_after_add_block() {
    let setup = Setup::new(&[("/home/file.bin", &[1, 1, 1])]).await;
    let file_id = Id::new();
    setup
        .loader
        .start_loading(setup.user, file_id, 2, "/home/file.bin".to_string())
        .await
        .unwrap();
    setup
        .loader
        .add_block(
            setup.user,
            Box::new(ApiMsgBodySendFilePart {
                file_id: file_id.into(),
                part: 0,
                data: vec![1, 1, 1],
            }),
        )
        .await
        .unwrap();
    setup
        .loader
        .retry_block_request(setup.user, file_id)
        .await
        .unwrap();
    let packed = setup.packer.packed.borrow();
    assert_eq!(packed.len(), 3); // 3 = start + req next + retry
    assert_eq!(
        packed[2].body,
        ApiMsgBody::ReqFilePart(Box::new(ApiMsgBodyReqFilePart {
            file_id: file_id.into(),
            part: 1
        }))
    )
}

#[tokio::test]
async fn test_retry_after_last_block() {
    let setup = Setup::new(&[("/home/file.bin", &[1, 1, 1])]).await;
    let file_id = Id::new();
    setup
        .loader
        .start_loading(setup.user, file_id, 1, "/home/file.bin".to_string())
        .await
        .unwrap();
    setup
        .loader
        .add_block(
            setup.user,
            Box::new(ApiMsgBodySendFilePart {
                file_id: file_id.into(),
                part: 0,
                data: vec![1, 1, 1],
            }),
        )
        .await
        .unwrap();
    setup
        .loader
        .retry_block_request(setup.user, file_id)
        .await
        .unwrap();
    let packed = setup.packer.packed.borrow();
    assert_eq!(packed.len(), 1); // 1 = start
}
