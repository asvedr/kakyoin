use crate::entities::chat::{Chat, ChatId};
use crate::entities::common::{Id, Me};
use crate::entities::errors::{FileDownloaderError, FileUploaderError, MsgDispatcherError};
use crate::entities::files::DownloadStatus;
use crate::entities::message::LocalMsgAttached;
use crate::entities::user::UserStatus;
use crate::impls::net::msg_dispatcher::MsgDispatcher;
use crate::impls::tests::common::{
    priv_key_1, pub_key_1, pub_key_2, storage_inited, WrappedStorage,
};
use crate::proto::local::IStorage;
use crate::proto::net::{IFileDownloader, IFileUploader, IMsgDispatcher};
use kakyoin_base::entities::api_msg::{
    ApiMsg, ApiMsgBody, ApiMsgBodyAddToChat, ApiMsgBodyReqFilePart, ApiMsgBodySendFilePart,
    ApiMsgBodyText, ApiMsgHeader,
};
use kakyoin_base::entities::common::{DynFut, DynFutRes};
use kakyoin_base::entities::contact::{ClientContact, ServerContact};
use kakyoin_base::entities::errors::CryptoErr;
use kakyoin_base::impls::logger::Logger;
use kakyoin_base::impls::serializers::rsa_key;
use kakyoin_base::proto::common::ICryptoOps;
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::{RsaPrivateKey, RsaPublicKey};

use std::sync::Arc;
use tokio::sync::Mutex;
use uuid::Uuid;

pub struct Setup {
    storage: WrappedStorage,
    downloader: MockDownloader,
    uploader: MockUploader,
    user_id: Id,
    dispatcher: Box<dyn IMsgDispatcher>,
}

struct MockCrypto;

#[derive(Clone)]
struct MockDownloader {
    calls: Arc<Mutex<Vec<(Id, Id, u64)>>>,
}

#[derive(Clone)]
struct MockUploader {
    resp: Arc<Mutex<Result<(), FileUploaderError>>>,
    calls: Arc<Mutex<Vec<(Id, Id, u64)>>>,
}

impl ICryptoOps for MockCrypto {
    fn gen_priv_key(&self) -> Result<RsaPrivateKey, CryptoErr> {
        unimplemented!()
    }
    fn encrypt(&self, _: &RsaPublicKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }
    fn decrypt(&self, _: &RsaPrivateKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }
    fn sign_len(&self) -> usize {
        unimplemented!()
    }
    fn sign(&self, _: &RsaPrivateKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }
    fn check_signature(&self, _: &RsaPublicKey, _: &[u8], _: &[u8]) -> Result<bool, CryptoErr> {
        unimplemented!()
    }
    fn get_key_code(&self, _: &RsaPublicKey) -> String {
        "xxx".to_string()
    }
}

impl IFileUploader for MockUploader {
    fn upload_file_part(
        &self,
        _: Arc<Vec<ServerContact>>,
        _: Arc<Me>,
        user_id: Id,
        _: String,
        file_id: Id,
        part: u64,
    ) -> DynFutRes<(), FileUploaderError> {
        let mtx_req = self.calls.clone();
        let mtx_resp = self.resp.clone();
        Box::pin(async move {
            mtx_req.lock().await.push((user_id, file_id, part));
            (*mtx_resp.lock().await).clone()
        })
    }
}

impl IFileDownloader for MockDownloader {
    fn start_loading(&self, _: Id, _: Id, _: u64, _: String) -> DynFutRes<(), FileDownloaderError> {
        unimplemented!()
    }
    fn retry_block_request(&self, _: Id, _: Id) -> DynFutRes<(), FileDownloaderError> {
        unimplemented!()
    }
    fn get_loading_status(&self, _: Id, _: Id) -> DynFut<Option<DownloadStatus>> {
        unimplemented!()
    }
    fn add_block(
        &self,
        user_id: Id,
        data: Box<ApiMsgBodySendFilePart>,
    ) -> DynFutRes<(), FileDownloaderError> {
        let mtx = self.calls.clone();
        Box::pin(async move {
            mtx.lock()
                .await
                .push((user_id, data.file_id.into(), data.part));
            Ok(())
        })
    }
}

impl Setup {
    async fn new() -> Self {
        let storage = storage_inited().await;
        let user_id = Id::new();
        {
            let lock = storage.lock().await;
            lock.users()
                .contact(
                    &ClientContact::new(user_id.into(), &pub_key_2()),
                    UserStatus::Trusted,
                )
                .unwrap();
            lock.chats()
                .create_chat(Chat {
                    id: ChatId::User(user_id),
                    name: "".to_string(),
                    users: vec![user_id],
                })
                .unwrap();
        }
        let downloader = MockDownloader {
            calls: Arc::new(Mutex::default()),
        };
        let uploader = MockUploader {
            resp: Arc::new(Mutex::new(Ok(()))),
            calls: Arc::new(Mutex::default()),
        };
        let dispatcher = MsgDispatcher::new(
            Logger::mock(),
            storage.clone(),
            rsa_key::Serializer,
            downloader.clone(),
            uploader.clone(),
            MockCrypto,
        );
        Self {
            storage,
            downloader,
            uploader,
            user_id,
            dispatcher: Box::new(dispatcher),
        }
    }

    async fn dispatch(&self, body: ApiMsgBody) -> Result<(), MsgDispatcherError> {
        let sender_pub_key = rsa_key::Serializer.serialize(&**pub_key_2());
        let msg = ApiMsg {
            header: ApiMsgHeader {
                id: Id::new().into(),
                sender: self.user_id.into(),
                sender_pub_key,
                receiver: Uuid::NAMESPACE_OID.into(),
                receiver_key_code: vec![1, 2, 3],
                ts: 0,
            },
            body,
        };
        self.dispatcher
            .dispatch_msgs(
                Arc::new(vec![ServerContact::new(
                    "127.0.0.1".to_string(),
                    pub_key_1(),
                )]),
                Arc::new(Me {
                    id: Id::new(),
                    pub_key: (**pub_key_1()).clone(),
                    priv_key: (**priv_key_1()).clone(),
                }),
                vec![msg],
            )
            .await
    }
}

#[tokio::test]
async fn test_dispatch_text() {
    let setup = Setup::new().await;
    let body = ApiMsgBody::Text(Box::new(ApiMsgBodyText {
        chat: None,
        text: "Hello".to_string(),
    }));
    setup.dispatch(body).await.unwrap();
    assert!(setup.uploader.calls.lock().await.is_empty());
    assert!(setup.downloader.calls.lock().await.is_empty());
    let lock = setup.storage.lock().await;
    let msg = lock
        .messages()
        .get_chat_messages(&ChatId::User(setup.user_id), None, 1)
        .unwrap()
        .remove(0);

    assert_eq!(msg.sender, Some(setup.user_id));
    assert_eq!(msg.receiver, None);
    assert_eq!(msg.sender_key_code, Some("xxx".to_string()));
    assert_eq!(msg.chat, ChatId::User(setup.user_id));
    assert_eq!(msg.text, "Hello".to_string());
    assert_eq!(msg.attached, LocalMsgAttached::Nothing);
    assert!(msg.sent);
    assert!(msg.received);
    assert!(!msg.read);
    assert_eq!(msg.ts, 0);
}

#[tokio::test]
async fn test_dispatch_invite() {
    let setup = Setup::new().await;
    let chat_id = Id::new();
    let new_user = Id::new();
    let body = ApiMsgBody::AddToChat(Box::new(ApiMsgBodyAddToChat {
        chat_id: chat_id.into(),
        chat_name: "New Chat".to_string(),
        other_participants: vec![ClientContact::new(new_user.into(), &pub_key_2())],
    }));
    setup.dispatch(body).await.unwrap();
    assert!(setup.uploader.calls.lock().await.is_empty());
    assert!(setup.downloader.calls.lock().await.is_empty());
    let lock = setup.storage.lock().await;
    let msgs = lock
        .messages()
        .get_chat_messages(&ChatId::User(setup.user_id), None, 1)
        .unwrap();
    assert!(msgs.is_empty());
    let users = lock.users().get_by_uid(&[new_user]).unwrap();
    assert_eq!(users.len(), 1);
    assert_eq!(users[0].pub_key, *pub_key_2());
    let mut chat = lock.chats().get_chat(&ChatId::Chat(chat_id)).unwrap();
    assert_eq!(chat.name, "New Chat");
    chat.users.sort();
    let mut expected = vec![setup.user_id, new_user];
    expected.sort();
    assert_eq!(chat.users, expected);
}

#[tokio::test]
async fn test_dispatch_req_file_ok() {
    let setup = Setup::new().await;
    let file_id = Id::new();
    let body = ApiMsgBody::ReqFilePart(Box::new(ApiMsgBodyReqFilePart {
        file_id: file_id.into(),
        part: 0,
    }));
    setup.dispatch(body).await.unwrap();
    let msgs = setup
        .storage
        .lock()
        .await
        .messages()
        .get_chat_messages(&ChatId::User(setup.user_id), None, 1)
        .unwrap();
    assert!(msgs.is_empty());
}

#[tokio::test]
async fn test_dispatch_req_file_err() {
    let setup = Setup::new().await;
    {
        let mut guard = setup.uploader.resp.lock().await;
        *guard = Err(FileUploaderError::FileForUserNotFound)
    }
    let file_id = Id::new();
    let body = ApiMsgBody::ReqFilePart(Box::new(ApiMsgBodyReqFilePart {
        file_id: file_id.into(),
        part: 0,
    }));
    setup.dispatch(body).await.unwrap();
    let msgs = setup
        .storage
        .lock()
        .await
        .messages()
        .get_chat_messages(&ChatId::User(setup.user_id), None, 1)
        .unwrap();
    assert!(msgs.is_empty());
}

#[tokio::test]
async fn test_dispatch_send_file() {
    let setup = Setup::new().await;
    let file_id = Id::new();
    let body = ApiMsgBody::SendFilePart(Box::new(ApiMsgBodySendFilePart {
        file_id: file_id.into(),
        part: 0,
        data: vec![1, 2, 3],
    }));
    setup.dispatch(body).await.unwrap();
    let msgs = setup
        .storage
        .lock()
        .await
        .messages()
        .get_chat_messages(&ChatId::User(setup.user_id), None, 1)
        .unwrap();
    assert!(msgs.is_empty());
    let calls = setup.downloader.calls.lock().await;
    assert_eq!(calls.len(), 1);
    assert_eq!(calls[0], (setup.user_id, file_id, 0));
}
