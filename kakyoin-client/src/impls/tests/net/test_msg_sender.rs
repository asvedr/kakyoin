use crate::entities::chat::{Chat, ChatId};
use crate::entities::common::{Id, Me};
use crate::entities::errors::ServerError;
use crate::entities::message::{LocalMsgAttached, NewLocalMsg};
use crate::entities::user::UserStatus;
use crate::impls::net::msg_sender::MsgSender;
use crate::impls::tests::common::{
    crypto, priv_key_1, pub_key_1, pub_key_2, storage_inited, MockMsgPacker, MockServerFactory,
    WrappedStorage,
};
use crate::proto::local::IStorage;
use crate::proto::net::IMsgSender;
use kakyoin_base::entities::api_msg::ApiMsgStatus;
use kakyoin_base::entities::contact::{ClientContact, ServerContact};
use kakyoin_base::impls::serializers::rsa_key;
use mddd::macros::singleton;
use std::collections::HashMap;
use std::sync::Arc;
use uuid::Uuid;

struct Setup {
    storage: WrappedStorage,
    sender: Box<dyn IMsgSender>,
    servers: Arc<Vec<ServerContact>>,
    me: Arc<Me>,
}

#[singleton]
fn msg_id() -> Id {
    Id::new()
}

impl Setup {
    async fn new(resp: Result<HashMap<Uuid, ApiMsgStatus>, ServerError>) -> Self {
        let user = Id::new();
        let msg_id = *msg_id();
        let storage = storage_inited().await;
        {
            let lock = storage.lock().await;
            lock.users()
                .contact(
                    &ClientContact::new(user.into(), &pub_key_2()),
                    UserStatus::Trusted,
                )
                .unwrap();
            lock.chats()
                .create_chat(Chat {
                    id: ChatId::User(user),
                    name: "".to_string(),
                    users: vec![user],
                })
                .unwrap();
            lock.messages()
                .create_messages(&[NewLocalMsg {
                    sender: None,
                    receiver: Some(user.into()),
                    sender_key_code: None,
                    chat: ChatId::User(user),
                    uuid: msg_id,
                    local_uniq_id: None,
                    text: "Hello".to_string(),
                    attached: LocalMsgAttached::Nothing,
                    sent: false,
                    received: false,
                    read: true,
                    ts: 0,
                }])
                .unwrap();
        }
        let factory = MockServerFactory::new().with_send_msgs(resp);
        let packer = MockMsgPacker::default();
        let sender = MsgSender::new(
            factory,
            storage.clone(),
            rsa_key::Serializer,
            packer,
            crypto(),
        );
        let servers = Arc::new(vec![ServerContact::new(
            "127.0.0.1".to_string(),
            &pub_key_2(),
        )]);
        let me = Arc::new(Me {
            id: Id::new(),
            pub_key: (**pub_key_1()).clone(),
            priv_key: (**priv_key_1()).clone(),
        });
        Self {
            storage,
            sender: Box::new(sender),
            servers,
            me,
        }
    }
}

#[tokio::test]
async fn test_send_response_new() {
    let setup = Setup::new(Ok([((*msg_id()).into(), ApiMsgStatus::NotReceived)]
        .into_iter()
        .collect()))
    .await;
    let msg = setup
        .storage
        .lock()
        .await
        .messages()
        .get_by_uid(msg_id())
        .unwrap();
    assert!(!msg.sent);
    assert!(!msg.received);
    setup
        .sender
        .send_msgs(setup.servers.clone(), setup.me.clone(), vec![msg])
        .await
        .unwrap();
    let msg = setup
        .storage
        .lock()
        .await
        .messages()
        .get_by_uid(msg_id())
        .unwrap();
    assert!(msg.sent);
    assert!(!msg.received);
}

#[tokio::test]
async fn test_send_response_received() {
    let setup = Setup::new(Ok([((*msg_id()).into(), ApiMsgStatus::Received)]
        .into_iter()
        .collect()))
    .await;
    let msg = setup
        .storage
        .lock()
        .await
        .messages()
        .get_by_uid(msg_id())
        .unwrap();
    assert!(!msg.sent);
    assert!(!msg.received);
    setup
        .sender
        .send_msgs(setup.servers.clone(), setup.me.clone(), vec![msg])
        .await
        .unwrap();
    let msg = setup
        .storage
        .lock()
        .await
        .messages()
        .get_by_uid(msg_id())
        .unwrap();
    assert!(msg.sent);
    assert!(msg.received);
}

#[tokio::test]
async fn test_send_err() {
    let setup = Setup::new(Err(ServerError::ResponseNot200(500))).await;
    let msg = setup
        .storage
        .lock()
        .await
        .messages()
        .get_by_uid(msg_id())
        .unwrap();
    assert!(!msg.sent);
    assert!(!msg.received);
    setup
        .sender
        .send_msgs(setup.servers.clone(), setup.me.clone(), vec![msg])
        .await
        .unwrap();
    let msg = setup
        .storage
        .lock()
        .await
        .messages()
        .get_by_uid(msg_id())
        .unwrap();
    assert!(!msg.sent);
    assert!(!msg.received);
}
