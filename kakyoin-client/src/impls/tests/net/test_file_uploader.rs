use crate::entities::chat::{Chat, ChatId};
use crate::entities::common::{Id, Me};
use crate::entities::errors::FileUploaderError;
use crate::entities::files::LocalFileMeta;
use crate::entities::user::UserStatus;
use crate::impls::net::file_uploader::FileUploader;
use crate::impls::tests::common::{
    priv_key_1, pub_key_1, pub_key_2, storage_inited, MockFS, MockFSCtxReq, MockFSCtxResp,
    MockMsgPacker, MockServerFactory, WrappedStorage,
};
use crate::proto::local::IStorage;
use crate::proto::net::IFileUploader;
use kakyoin_base::entities::contact::{ClientContact, ServerContact};
use kakyoin_base::entities::errors::CryptoErr;
use kakyoin_base::impls::serializers::rsa_key;
use kakyoin_base::proto::common::ICryptoOps;
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::{RsaPrivateKey, RsaPublicKey};
use std::sync::Arc;

struct MockCrypto;

struct Setup {
    storage: WrappedStorage,
    uploader: Box<dyn IFileUploader>,
}

impl ICryptoOps for MockCrypto {
    fn gen_priv_key(&self) -> Result<RsaPrivateKey, CryptoErr> {
        unimplemented!()
    }
    fn encrypt(&self, _: &RsaPublicKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }
    fn decrypt(&self, _: &RsaPrivateKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }
    fn sign_len(&self) -> usize {
        unimplemented!()
    }
    fn sign(&self, _: &RsaPrivateKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }
    fn check_signature(&self, _: &RsaPublicKey, _: &[u8], _: &[u8]) -> Result<bool, CryptoErr> {
        unimplemented!()
    }
    fn get_key_code(&self, _: &RsaPublicKey) -> String {
        "xxx".to_string()
    }
}

impl Setup {
    async fn new(get_block: &[(&str, u64, &[u8])]) -> Self {
        let storage = storage_inited().await;
        let fs = MockFS::new(
            MockFSCtxReq {
                get_block: get_block
                    .iter()
                    .map(|(a, b, _)| (a.to_string(), *b))
                    .collect(),
                ..Default::default()
            },
            MockFSCtxResp {
                get_block: get_block
                    .iter()
                    .map(|(_, _, bts)| Ok(bts.to_vec()))
                    .collect(),
                ..Default::default()
            },
        );
        let factory = MockServerFactory::new();
        let packer = MockMsgPacker::default();
        let uploader = FileUploader::new(
            storage.clone(),
            factory,
            fs,
            packer,
            MockCrypto,
            rsa_key::Serializer,
        );
        Self {
            storage,
            uploader: Box::new(uploader),
        }
    }
}

#[tokio::test]
async fn test_upload_file_part_no_file() {
    let setup = Setup::new(&[]).await;
    let me = Arc::new(Me {
        id: Id::new(),
        pub_key: (**pub_key_1()).clone(),
        priv_key: (**priv_key_1()).clone(),
    });
    let servers = Arc::new(vec![ServerContact::new(
        "127.0.0.1".to_string(),
        pub_key_2(),
    )]);
    let user_id = Id::new();
    let file_id = Id::new();
    let err = setup
        .uploader
        .upload_file_part(servers, me, user_id, "key".to_string(), file_id, 1)
        .await
        .unwrap_err();
    assert_eq!(err, FileUploaderError::FileForUserNotFound)
}

#[tokio::test]
async fn test_upload_file_part_ok() {
    let setup = Setup::new(&[("/path/to/file", 1, &[1, 1, 1])]).await;
    let me = Arc::new(Me {
        id: Id::new(),
        pub_key: (**pub_key_1()).clone(),
        priv_key: (**priv_key_1()).clone(),
    });
    let servers = Arc::new(vec![ServerContact::new(
        "127.0.0.1".to_string(),
        pub_key_2(),
    )]);
    let user_id = Id::new();
    let file_id = Id::new();
    {
        let storage = setup.storage.lock().await;
        storage
            .users()
            .contact(
                &ClientContact::new(user_id.into(), pub_key_2()),
                UserStatus::Trusted,
            )
            .unwrap();
        storage
            .chats()
            .create_chat(Chat {
                id: ChatId::User(user_id),
                name: "".to_string(),
                users: vec![user_id],
            })
            .unwrap();
        storage
            .shared_files()
            .register_file(
                &LocalFileMeta {
                    id: file_id,
                    path: "/path/to/file".to_string(),
                    name: "name".to_string(),
                    size: 10,
                    parts: 5,
                },
                &[user_id],
            )
            .unwrap();
    }
    setup
        .uploader
        .upload_file_part(
            servers,
            me,
            user_id,
            rsa_key::Serializer.serialize(&**pub_key_2()),
            file_id,
            1,
        )
        .await
        .unwrap();
}
