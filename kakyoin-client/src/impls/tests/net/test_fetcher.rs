use crate::entities::common::{Id, Me};
use crate::entities::errors::ServerError;
use crate::impls::net::msg_fetcher::MsgFetcher;
use crate::impls::tests::common::{
    priv_key_1, pub_key_1, pub_key_2, MockMsgPacker, MockServerFactory,
};
use crate::proto::net::IMsgFetcher;
use kakyoin_base::entities::api_msg::{
    ApiMsg, ApiMsgBody, ApiMsgBodyText, ApiMsgHeader, ApiMsgPacked,
};
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::entities::errors::PackerError;
use kakyoin_base::impls::logger::Logger;

use std::sync::Arc;
use uuid::Uuid;

struct Setup {
    fetcher: Box<dyn IMsgFetcher>,
}

impl Setup {
    async fn new(
        fetched: Result<Vec<ApiMsgPacked>, ServerError>,
        to_unpack: &[(Uuid, Result<ApiMsg, PackerError>)],
    ) -> Self {
        let mut packer = MockMsgPacker::default();
        let server_factory = MockServerFactory::new().with_fetch_msgs(fetched);
        packer.to_unpack = to_unpack.iter().cloned().collect();
        let fetcher = MsgFetcher::new(Logger::mock(), server_factory, packer);
        Self {
            fetcher: Box::new(fetcher),
        }
    }
}

#[tokio::test]
async fn test_fetch_msgs_ok() {
    let msg_id = Id::new().into();
    let me = Arc::new(Me {
        id: Id::new(),
        pub_key: (**pub_key_1()).clone(),
        priv_key: (**priv_key_1()).clone(),
    });
    let sender = Id::new().into();
    let packed = ApiMsgPacked {
        receiver: me.id.into(),
        id: msg_id,
        key_code: "xxx".to_string(),
        data: vec![1, 2, 3],
        signature: vec![3, 2, 1],
    };
    let msg = ApiMsg {
        header: ApiMsgHeader {
            id: msg_id,
            sender,
            sender_pub_key: "xxx".to_string(),
            receiver: me.id.into(),
            receiver_key_code: vec![1, 1, 1],
            ts: 0,
        },
        body: ApiMsgBody::Text(Box::new(ApiMsgBodyText {
            chat: None,
            text: "Hello".to_string(),
        })),
    };
    let setup = Setup::new(Ok(vec![packed]), &[(msg_id, Ok(msg.clone()))]).await;
    let got = setup
        .fetcher
        .fetch_msgs(
            Arc::new(vec![
                ServerContact::new("127.0.0.1".to_string(), &pub_key_2()),
                ServerContact::new("127.0.0.2".to_string(), &pub_key_2()),
            ]),
            me,
        )
        .await
        .unwrap();
    assert_eq!(got, &[msg]);
}

#[tokio::test]
async fn test_fetch_msgs_server_err() {
    let me = Arc::new(Me {
        id: Id::new(),
        pub_key: (**pub_key_1()).clone(),
        priv_key: (**priv_key_1()).clone(),
    });
    let setup = Setup::new(Err(ServerError::ResponseNot200(500)), &[]).await;
    let got = setup
        .fetcher
        .fetch_msgs(
            Arc::new(vec![
                ServerContact::new("127.0.0.1".to_string(), &pub_key_2()),
                ServerContact::new("127.0.0.2".to_string(), &pub_key_2()),
            ]),
            me,
        )
        .await
        .unwrap();
    assert!(got.is_empty());
}

#[tokio::test]
async fn test_fetch_msgs_invalid_msgs() {
    let msg_id = Id::new().into();
    let me = Arc::new(Me {
        id: Id::new(),
        pub_key: (**pub_key_1()).clone(),
        priv_key: (**priv_key_1()).clone(),
    });
    let packed = ApiMsgPacked {
        receiver: me.id.into(),
        id: msg_id,
        key_code: "xxx".to_string(),
        data: vec![1, 2, 3],
        signature: vec![3, 2, 1],
    };
    let setup = Setup::new(
        Ok(vec![packed]),
        &[(msg_id, Err(PackerError::CanNotDecrypt))],
    )
    .await;
    let got = setup
        .fetcher
        .fetch_msgs(
            Arc::new(vec![
                ServerContact::new("127.0.0.1".to_string(), &pub_key_2()),
                ServerContact::new("127.0.0.2".to_string(), &pub_key_2()),
            ]),
            me,
        )
        .await
        .unwrap();
    assert!(got.is_empty());
}

#[tokio::test]
async fn test_report_received() {
    let setup = Setup::new(Ok(vec![]), &[]).await;
    let me = Arc::new(Me {
        id: Id::new(),
        pub_key: (**pub_key_1()).clone(),
        priv_key: (**priv_key_1()).clone(),
    });
    let msg_id = Id::new().into();
    setup
        .fetcher
        .report_received(
            Arc::new(vec![ServerContact::new(
                "127.0.0.1".to_string(),
                &pub_key_2(),
            )]),
            me,
            vec![msg_id],
        )
        .await
        .unwrap();
}
