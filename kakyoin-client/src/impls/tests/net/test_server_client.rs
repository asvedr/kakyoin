use crate::entities::common::{Id, Me};
use crate::entities::errors::ServerError;
use crate::impls::net::server_client::ServerClient;
use crate::impls::tests::common::{priv_key_1, pub_key_1, pub_key_2};
use crate::proto::net::IServerClient;
use crate::utils::time::get_now;
use kakyoin_base::entities::api_msg::{ApiMsgPacked, ApiMsgStatus};
use kakyoin_base::entities::common::DynFut;
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::entities::errors::{CryptoErr, NetError};
use kakyoin_base::entities::net::HttpResp;
use kakyoin_base::entities::server_call::{ApiUserMeta, ServerCall, ServerResponse};
use kakyoin_base::proto::common::ICryptoOps;
use kakyoin_base::proto::net::IHttpClient;
use mddd::macros::singleton;
use rsa::{RsaPrivateKey, RsaPublicKey};
use serdebin::{from_bytes, to_bytes};
use std::cell::RefCell;
use std::sync::Arc;

use uuid::Uuid;

struct MockHttpClient {
    req: ServerCall,
    resp: RefCell<Box<dyn FnMut() -> Result<HttpResp, NetError>>>,
}

struct NoCrypto;

unsafe impl Send for MockHttpClient {}
unsafe impl Sync for MockHttpClient {}

impl IHttpClient for MockHttpClient {
    fn get(&self, _: &str, _: &str) -> DynFut<Result<HttpResp, NetError>> {
        unimplemented!()
    }

    fn post(
        &self,
        base_url: &str,
        path: &str,
        data: Vec<u8>,
    ) -> DynFut<Result<HttpResp, NetError>> {
        assert_eq!(base_url, "http://1.2.3.4:1234/");
        assert_eq!(path, "");
        let call: ServerCall = from_bytes(&data).unwrap().0;
        assert_eq!(call, self.req);
        let mut fun = self.resp.borrow_mut();
        let resp = (*fun)();
        Box::pin(async move { resp })
    }
}

impl MockHttpClient {
    fn new(req: ServerCall, resp: Box<dyn FnMut() -> Result<HttpResp, NetError>>) -> Self {
        Self {
            req,
            resp: RefCell::new(resp),
        }
    }
}

impl ICryptoOps for NoCrypto {
    fn gen_priv_key(&self) -> Result<RsaPrivateKey, CryptoErr> {
        unimplemented!()
    }

    fn encrypt(&self, _: &RsaPublicKey, data: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        Ok(data.to_vec())
    }

    fn decrypt(&self, _: &RsaPrivateKey, data: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        Ok(data.to_vec())
    }

    fn sign_len(&self) -> usize {
        unimplemented!()
    }

    fn sign(&self, _: &RsaPrivateKey, _: &[u8]) -> Result<Vec<u8>, CryptoErr> {
        unimplemented!()
    }

    fn check_signature(&self, _: &RsaPublicKey, _: &[u8], _: &[u8]) -> Result<bool, CryptoErr> {
        unimplemented!()
    }

    fn get_key_code(&self, _: &RsaPublicKey) -> String {
        "abc".to_string()
    }
}

#[singleton]
fn me() -> Arc<Me> {
    Arc::new(Me {
        id: Id::new(),
        pub_key: (**pub_key_1()).clone(),
        priv_key: (**priv_key_1()).clone(),
    })
}

#[singleton]
fn server_contact() -> ServerContact {
    ServerContact::new("http://1.2.3.4:1234/".to_string(), pub_key_2())
}

#[tokio::test]
async fn test_retry_ok() {
    let mut calls = vec![
        Err(NetError::Timeout),
        Ok(HttpResp {
            code: 200,
            body: to_bytes(ServerResponse::Token {
                token: vec![1, 2, 3],
                expired_at: 1,
            })
            .unwrap(),
        }),
    ];
    let http = MockHttpClient::new(
        ServerCall::GetToken {
            user_id: me().id.into(),
            pub_key: me().pub_key.clone(),
        },
        Box::new(move || calls.pop().unwrap()),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    assert!(client.ping().await.is_ok());
}

#[tokio::test]
async fn test_retry_err() {
    let http = MockHttpClient::new(
        ServerCall::GetToken {
            user_id: me().id.into(),
            pub_key: me().pub_key.clone(),
        },
        Box::new(|| Err(NetError::Timeout)),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    assert_eq!(
        client.ping().await.unwrap_err(),
        ServerError::NetError(NetError::Timeout)
    )
}

#[test]
fn test_addr() {
    let http = MockHttpClient::new(
        ServerCall::FetchMessages { token: vec![] },
        Box::new(|| Err(NetError::Timeout)),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    assert_eq!(client.addr(), "http://1.2.3.4:1234/")
}

#[tokio::test]
async fn test_send_msgs() {
    let id1 = Id::new();
    let id1_: Uuid = id1.into();
    let id2 = Id::new();
    let id2_: Uuid = id2.into();
    let msgs = vec![
        ApiMsgPacked {
            receiver: Id::new().into(),
            id: id1.into(),
            key_code: "abc".to_string(),
            data: vec![2, 3, 4],
            signature: vec![3, 4, 5],
        },
        ApiMsgPacked {
            receiver: Id::new().into(),
            id: id2.into(),
            key_code: "cba".to_string(),
            data: vec![4, 3, 2],
            signature: vec![5, 4, 3],
        },
    ];
    let http = MockHttpClient::new(
        ServerCall::SendMessages {
            token: vec![1, 2, 3],
            msgs: msgs.clone(),
        },
        Box::new(move || {
            Ok(HttpResp {
                code: 200,
                body: to_bytes(ServerResponse::Statuses {
                    map: [
                        (id1_, ApiMsgStatus::Received),
                        (id2_, ApiMsgStatus::NotReceived),
                    ]
                    .into_iter()
                    .collect(),
                })
                .unwrap(),
            })
        }),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    client.set_token(vec![1, 2, 3], get_now() + 200).await;
    let resp = client.send_msgs(msgs).await.unwrap();
    assert_eq!(resp.get(&id1.into()), Some(&ApiMsgStatus::Received));
    assert_eq!(resp.get(&id2.into()), Some(&ApiMsgStatus::NotReceived));
}

#[tokio::test]
async fn test_fetch_msgs() {
    let msgs = vec![
        ApiMsgPacked {
            receiver: Id::new().into(),
            id: Id::new().into(),
            key_code: "abc".to_string(),
            data: vec![2, 3, 4],
            signature: vec![3, 4, 5],
        },
        ApiMsgPacked {
            receiver: Id::new().into(),
            id: Id::new().into(),
            key_code: "cba".to_string(),
            data: vec![4, 3, 2],
            signature: vec![5, 4, 3],
        },
    ];
    let msgs_ = msgs.clone();
    let http = MockHttpClient::new(
        ServerCall::FetchMessages {
            token: vec![1, 2, 3],
        },
        Box::new(move || {
            Ok(HttpResp {
                code: 200,
                body: to_bytes(ServerResponse::Messages {
                    msgs: msgs_.clone(),
                })
                .unwrap(),
            })
        }),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    client.set_token(vec![1, 2, 3], get_now() + 200).await;
    let resp = client.fetch_msgs().await.unwrap();
    assert_eq!(resp, msgs)
}

#[tokio::test]
async fn test_get_statuses() {
    let id: Uuid = Id::new().into();
    let id_ = id;
    let http = MockHttpClient::new(
        ServerCall::GetStatuses {
            token: vec![1, 2, 3],
            ids: vec![id],
        },
        Box::new(move || {
            Ok(HttpResp {
                code: 200,
                body: to_bytes(ServerResponse::Statuses {
                    map: [(id_, ApiMsgStatus::Received)].into_iter().collect(),
                })
                .unwrap(),
            })
        }),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    client.set_token(vec![1, 2, 3], get_now() + 200).await;
    let resp = client.get_statuses(vec![id]).await.unwrap();
    assert_eq!(resp.get(&id), Some(&ApiMsgStatus::Received));
}

#[tokio::test]
async fn test_mark_received() {
    let id: Uuid = Id::new().into();
    let http = MockHttpClient::new(
        ServerCall::MarkReceived {
            token: vec![1, 2, 3],
            msgs: vec![id],
        },
        Box::new(move || {
            Ok(HttpResp {
                code: 200,
                body: to_bytes(ServerResponse::Nothing).unwrap(),
            })
        }),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    client.set_token(vec![1, 2, 3], get_now() + 200).await;
    client.mark_received(vec![id]).await.unwrap();
}

#[tokio::test]
async fn test_request_meta() {
    let id: Uuid = Id::new().into();
    let http = MockHttpClient::new(
        ServerCall::ReqMeta {
            token: vec![1, 2, 3],
            user: id,
            key_code: "abc".to_string(),
        },
        Box::new(move || {
            Ok(HttpResp {
                code: 200,
                body: to_bytes(ServerResponse::UserMeta {
                    meta: Box::new(ApiUserMeta {
                        name: "Alice".to_string(),
                        description: "Bob".to_string(),
                        photo: "base64".to_string(),
                        hash: "xxx".to_string(),
                    }),
                })
                .unwrap(),
            })
        }),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    client.set_token(vec![1, 2, 3], get_now() + 200).await;
    let resp = client.request_meta(id, &**pub_key_2()).await.unwrap();
    assert_eq!(
        *resp,
        ApiUserMeta {
            name: "Alice".to_string(),
            description: "Bob".to_string(),
            photo: "base64".to_string(),
            hash: "xxx".to_string()
        }
    );
}

#[tokio::test]
async fn test_request_meta_hash() {
    let id: Uuid = Id::new().into();
    let id_ = id;
    let http = MockHttpClient::new(
        ServerCall::ReqMetaHash {
            token: vec![1, 2, 3],
            users: vec![(id, "abc".to_string())],
        },
        Box::new(move || {
            Ok(HttpResp {
                code: 200,
                body: to_bytes(ServerResponse::MetaHashes {
                    users: vec![((id_, "abc".to_string()), "xxx".to_string())],
                })
                .unwrap(),
            })
        }),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    client.set_token(vec![1, 2, 3], get_now() + 200).await;
    let resp = client
        .request_meta_hash(&[(id, &**pub_key_2())])
        .await
        .unwrap();
    assert_eq!(resp, [(id, "xxx".to_string())].into_iter().collect());
}

#[tokio::test]
async fn test_post_my_meta() {
    let http = MockHttpClient::new(
        ServerCall::PostMyMeta {
            token: vec![1, 2, 3],
            meta: Box::new(ApiUserMeta {
                name: "a".to_string(),
                description: "b".to_string(),
                photo: "base64".to_string(),
                hash: "c".to_string(),
            }),
        },
        Box::new(move || {
            Ok(HttpResp {
                code: 200,
                body: to_bytes(ServerResponse::Nothing).unwrap(),
            })
        }),
    );
    let client = ServerClient::new(http, NoCrypto, me().clone(), server_contact(), 1).unwrap();
    client.set_token(vec![1, 2, 3], get_now() + 200).await;
    client
        .post_my_meta(Arc::new(ApiUserMeta {
            name: "a".to_string(),
            description: "b".to_string(),
            photo: "base64".to_string(),
            hash: "c".to_string(),
        }))
        .await
        .unwrap();
}
