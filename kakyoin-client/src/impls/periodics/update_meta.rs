use crate::entities::chat::ChatId;
use atex::{ITaskController, ITaskExecutor, Task, TaskStatus};
use futures_util::future::join_all;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::ServerContact;
use kakyoin_base::entities::log::Level;
use kakyoin_base::entities::server_call::ApiUserMeta;
use kakyoin_base::proto::common::ILogger;
use mddd::macros::EnumAutoFrom;
use rsa::RsaPublicKey;
use std::collections::{HashMap, HashSet};
use std::sync::Arc;
use uuid::Uuid;

use crate::entities::common::{Id, Me};
use crate::entities::errors::{DbError, ServerError};
use crate::entities::tasks::{PeriodicInterval, PeriodicName};
use crate::proto::local::IStorage;
use crate::proto::net::IServerClientFactory;

pub struct Executor<L: ILogger, S: IStorage, SF: IServerClientFactory> {
    ctx: Arc<Ctx<L, S, SF>>,
}

struct Ctx<L: ILogger, S: IStorage, SF: IServerClientFactory> {
    logger: L,
    storage: S,
    server_factory: SF,
}

#[derive(Debug, EnumAutoFrom)]
pub enum Error {
    NoRequestWereMade,
    NotInited,
    Db(DbError),
    Ser(ServerError),
}

impl<L: ILogger, S: IStorage + 'static, SF: IServerClientFactory + 'static> Executor<L, S, SF> {
    pub fn new(logger: L, storage: S, server_factory: SF) -> Self {
        let ctx = Ctx {
            logger,
            storage,
            server_factory,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_execute(self) -> Result<TaskStatus, Error> {
        let (servers, me, my_hash, user_hashes) = match self.get_servers_and_hashes().await {
            Ok(val) => val,
            Err(Error::NotInited) => return Ok(TaskStatus::Completed),
            Err(err) => return Err(err),
        };
        if servers.is_empty() {
            return Err(Error::NoRequestWereMade);
        }
        let received = self
            .request_hashes(
                &servers,
                me.clone(),
                user_hashes
                    .iter()
                    .map(|(id, key, _)| ((*id).into(), &**key))
                    .collect::<Vec<_>>(),
            )
            .await?;
        let users_to_pull = Self::find_diff(&me.id, &user_hashes, &received);
        self.update_my_meta(&servers, &received, &me, my_hash)
            .await?;
        self.pull_users(&servers, &me, users_to_pull).await?;
        Ok(TaskStatus::Completed)
    }

    async fn pull_users(
        &self,
        servers: &[ServerContact],
        me: &Arc<Me>,
        users_to_pull: Vec<(Uuid, Arc<RsaPublicKey>)>,
    ) -> Result<(), Error> {
        let mut futs = Vec::new();
        for (id, key) in users_to_pull {
            let fut = async move { (id, self.pull_user(servers, me, id, key).await) };
            futs.push(fut);
        }
        let mut metas = Vec::new();
        for (id, res) in join_all(futs).await {
            match res {
                Ok(val) => {
                    metas.push((id, val));
                }
                Err(err) => {
                    let msg = format!("Can not fetch meta for user({}): {:?}", id, err,);
                    self.ctx.logger.log(Level::Err, &msg);
                }
            }
        }
        let storage = self.ctx.storage.lock().await;
        let user_repo = storage.users();
        let chat_repo = storage.chats();
        for (id, meta) in metas {
            chat_repo.set_name(&ChatId::User(id.into()), &meta.name)?;
            user_repo.set_info(&id.into(), &meta)?;
        }
        Ok(())
    }

    async fn pull_user(
        &self,
        servers: &[ServerContact],
        me: &Arc<Me>,
        id: Uuid,
        key: Arc<RsaPublicKey>,
    ) -> Result<Box<ApiUserMeta>, Error> {
        let mut err = None;
        for s_cnn in servers {
            let srv = self.ctx.server_factory.produce(me.clone(), s_cnn)?;
            match srv.request_meta(id, &key).await {
                Ok(val) => return Ok(val),
                Err(e) => {
                    err = Some(e);
                    continue;
                }
            }
        }
        Err(err.unwrap().into())
    }

    async fn update_my_meta(
        &self,
        servers: &[ServerContact],
        diff: &HashMap<Id, HashSet<String>>,
        me: &Arc<Me>,
        my_hash: String,
    ) -> Result<(), Error> {
        if Self::is_my_hash_same_to_servers(diff, me, my_hash) {
            return Ok(());
        }
        let my_meta = self.ctx.storage.lock().await.settings().get_meta()?;
        let mut futs = Vec::new();
        for s_cnn in servers {
            let srv = self.ctx.server_factory.produce(me.clone(), s_cnn)?;
            futs.push(srv.post_my_meta(my_meta.clone()));
        }
        join_all(futs).await;
        Ok(())
    }

    async fn request_hashes(
        &self,
        servers: &[ServerContact],
        me: Arc<Me>,
        ids: Vec<(Uuid, &RsaPublicKey)>,
    ) -> Result<HashMap<Id, HashSet<String>>, Error> {
        let mut futs = Vec::new();
        for s_cnn in servers {
            let srv = self.ctx.server_factory.produce(me.clone(), s_cnn)?;
            futs.push(srv.request_meta_hash(&ids));
        }
        let mut err: Option<ServerError> = None;
        let mut has_ok = false;
        let mut result = ids
            .iter()
            .map(|(id, _)| ((*id).into(), HashSet::new()))
            .collect::<HashMap<_, _>>();
        for res in join_all(futs).await {
            let map = match res {
                Ok(val) => {
                    has_ok = true;
                    val
                }
                Err(e) => {
                    err = Some(e);
                    continue;
                }
            };
            for (id, hash) in map {
                result.get_mut(&id.into()).unwrap().insert(hash);
            }
        }
        if has_ok {
            Ok(result)
        } else {
            Err(err.unwrap().into())
        }
    }

    async fn get_servers_and_hashes(
        &self,
    ) -> Result<
        (
            Arc<Vec<ServerContact>>,
            Arc<Me>,
            String,
            Vec<(Id, Arc<RsaPublicKey>, String)>,
        ),
        Error,
    > {
        let storage = self.ctx.storage.lock().await;
        let servers = storage.servers().get_all()?;
        let settings = storage.settings();
        let me = match settings.get_me() {
            Ok(val) => val,
            Err(DbError::NotFound) => return Err(Error::NotInited),
            Err(err) => return Err(err.into()),
        };
        let my_hash = settings.get_meta()?.hash.clone();
        let user_hashes = storage.users().get_hashes()?;
        Ok((servers, me, my_hash, user_hashes))
    }

    fn find_diff(
        me: &Id,
        user_hashes: &[(Id, Arc<RsaPublicKey>, String)],
        received: &HashMap<Id, HashSet<String>>,
    ) -> Vec<(Uuid, Arc<RsaPublicKey>)> {
        let mut result = Vec::new();
        for (id, key, hash) in user_hashes {
            if id == me {
                continue;
            }
            let hashes = received.get(id).unwrap();
            if hashes.contains(hash) {
                continue;
            }
            result.push(((*id).into(), key.clone()))
        }
        result
    }

    fn is_my_hash_same_to_servers(
        diff: &HashMap<Id, HashSet<String>>,
        me: &Arc<Me>,
        my_hash: String,
    ) -> bool {
        let hashes = match diff.get(&me.id) {
            Some(val) => val,
            _ => return false,
        };
        hashes.len() == 1 && hashes.contains(&my_hash)
    }
}

impl<S: IStorage + 'static, SF: IServerClientFactory + 'static, L: ILogger + 'static> ITaskExecutor
    for Executor<L, S, SF>
{
    type Error = Error;

    fn name(&self) -> &'static str {
        PeriodicName::UPDATE_META
    }

    fn periodic_interval(&self) -> Option<u64> {
        Some(PeriodicInterval::UPDATE_META)
    }

    fn execute(&self, _: Box<dyn ITaskController>, _: Task) -> DynFutRes<TaskStatus, Self::Error> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_execute(),
        )
    }
}
