use crate::entities::common::{Me, ServerStatus};
use crate::entities::errors::DbError;
use crate::entities::tasks::{PeriodicInterval, PeriodicName};
use crate::proto::local::IStorage;
use crate::proto::net::{IServerClientFactory, IServerStatusStorage};
use atex::{ITaskController, ITaskExecutor, Task, TaskStatus};
use futures_util::future::join_all;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::ServerContact;
use std::sync::Arc;

pub struct Executor<S: IStorage, SS: IServerStatusStorage, SF: IServerClientFactory> {
    ctx: Arc<Ctx<S, SS, SF>>,
}

struct Ctx<S, SS, SF> {
    storage: S,
    server_status: SS,
    server_factory: SF,
}

impl<S: IStorage, SS: IServerStatusStorage, SF: IServerClientFactory> Executor<S, SS, SF> {
    pub fn new(storage: S, server_status: SS, server_factory: SF) -> Self {
        let ctx = Ctx {
            storage,
            server_factory,
            server_status,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_execute(self) -> Result<TaskStatus, DbError> {
        let me;
        let servers;
        {
            let storage = self.ctx.storage.lock().await;
            servers = storage.servers().get_all()?;
            me = match storage.settings().get_me() {
                Ok(val) => val,
                Err(DbError::NotFound) =>
                // System not init
                {
                    return Ok(TaskStatus::InProgress(0))
                }
                Err(err) => return Err(err),
            };
        }
        let mut futs = Vec::new();
        for s_cnn in servers.iter() {
            let addr = s_cnn.addr.clone();
            let fut = self.get_status(me.clone(), s_cnn);
            futs.push(async move { (addr, fut.await) });
        }
        let update = join_all(futs).await;
        self.ctx.server_status.set_statuses(update).await;
        Ok(TaskStatus::Completed)
    }

    async fn get_status(&self, me: Arc<Me>, s_cnn: &ServerContact) -> ServerStatus {
        let srv = match self.ctx.server_factory.produce(me, s_cnn) {
            Ok(val) => val,
            Err(_) => return ServerStatus::Invalid,
        };
        if srv.ping().await.is_ok() {
            ServerStatus::Valid
        } else {
            ServerStatus::Invalid
        }
    }
}

impl<
        S: IStorage + 'static,
        SS: IServerStatusStorage + 'static,
        SF: IServerClientFactory + 'static,
    > ITaskExecutor for Executor<S, SS, SF>
{
    type Error = DbError;

    fn name(&self) -> &'static str {
        PeriodicName::UPDATE_SERVER_STATUS
    }

    fn periodic_interval(&self) -> Option<u64> {
        Some(PeriodicInterval::UPDATE_SERVER_STATUS)
    }

    fn execute(&self, _: Box<dyn ITaskController>, _: Task) -> DynFutRes<TaskStatus, Self::Error> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_execute(),
        )
    }
}
