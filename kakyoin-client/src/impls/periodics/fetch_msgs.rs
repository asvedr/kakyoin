use crate::entities::errors::{DbError, MsgDispatcherError, MsgFetcherError};
use crate::entities::tasks::{PeriodicInterval, PeriodicName};
use crate::proto::local::IStorage;
use crate::proto::net::{IMsgDispatcher, IMsgFetcher};
use atex::{ITaskController, ITaskExecutor, Task, TaskStatus};
use kakyoin_base::entities::common::DynFut;
use mddd::macros::EnumAutoFrom;
use std::sync::Arc;

pub struct Executor<Str: IStorage, F: IMsgFetcher, D: IMsgDispatcher> {
    ctx: Arc<Ctx<Str, F, D>>,
}

struct Ctx<Str: IStorage, F: IMsgFetcher, D: IMsgDispatcher> {
    storage: Str,
    fetcher: F,
    dispatcher: D,
}

#[derive(Debug, EnumAutoFrom)]
pub enum Error {
    Db(DbError),
    Fetcher(MsgFetcherError),
    Dispatcher(MsgDispatcherError),
}

impl<Str: IStorage, F: IMsgFetcher, D: IMsgDispatcher> Executor<Str, F, D> {
    pub fn new(storage: Str, fetcher: F, dispatcher: D) -> Self {
        let ctx = Ctx {
            storage,
            fetcher,
            dispatcher,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_execute(self) -> Result<TaskStatus, Error> {
        let servers;
        let me;
        {
            let storage = self.ctx.storage.lock().await;
            servers = storage.servers().get_all()?;
            me = match storage.settings().get_me() {
                Ok(val) => val,
                Err(DbError::NotFound) =>
                // System not inited
                {
                    return Ok(TaskStatus::Completed)
                }
                Err(err) => return Err(err.into()),
            };
        }
        let msgs = self
            .ctx
            .fetcher
            .fetch_msgs(servers.clone(), me.clone())
            .await?;
        let id_list = msgs.iter().map(|m| m.header.id).collect::<Vec<_>>();
        self.ctx
            .dispatcher
            .dispatch_msgs(servers.clone(), me.clone(), msgs)
            .await?;
        self.ctx
            .fetcher
            .report_received(servers, me, id_list)
            .await?;
        Ok(TaskStatus::Completed)
    }
}

impl<Str: IStorage + 'static, F: IMsgFetcher + 'static, D: IMsgDispatcher + 'static> ITaskExecutor
    for Executor<Str, F, D>
{
    type Error = Error;

    fn name(&self) -> &'static str {
        PeriodicName::FETCH_MSGS
    }

    fn periodic_interval(&self) -> Option<u64> {
        Some(PeriodicInterval::FETCH_MSGS)
    }

    fn execute(
        &self,
        _ctrl: Box<dyn ITaskController>,
        _task: Task,
    ) -> DynFut<Result<TaskStatus, Self::Error>> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_execute(),
        )
    }
}
