use atex::{ITaskController, ITaskExecutor, Task, TaskStatus};
use kakyoin_base::entities::common::DynFut;
use mddd::macros::EnumAutoFrom;

use std::sync::Arc;

use crate::entities::common::VecBox;
use crate::entities::errors::{DbError, MsgSenderError};
use crate::entities::message::{LocalMsg, LocalMsgAttached};
use crate::entities::tasks::{PeriodicInterval, PeriodicName};
use crate::proto::local::IStorage;
use crate::proto::net::IMsgSender;

const MAX_WEIGHT_OF_CHUNK: usize = 10;
const DEFAULT_WEIGHT: usize = 1;
const DEC_FILE_WEIGHT: usize = 1;
// const FILE_PART_WEIGHT: usize = 7;

pub struct Executor<S: IStorage, MS: IMsgSender> {
    ctx: Arc<Ctx<S, MS>>,
}

struct Ctx<S: IStorage, MS: IMsgSender> {
    storage: S,
    msg_sender: MS,
}

#[derive(Debug, EnumAutoFrom)]
pub enum Error {
    Payload(uuid::Error),
    Db(DbError),
    Msg(MsgSenderError),
}

fn chunkify(msgs: VecBox<LocalMsg>) -> Vec<VecBox<LocalMsg>> {
    let mut result = Vec::new();
    let mut chunk = Vec::new();
    let mut chunk_weight = 0;
    for msg in msgs {
        let weight = match &msg.attached {
            LocalMsgAttached::Nothing => DEFAULT_WEIGHT,
            LocalMsgAttached::FileId { .. } => DEC_FILE_WEIGHT,
            LocalMsgAttached::Invite { .. } => DEFAULT_WEIGHT,
        };
        if chunk_weight + weight > MAX_WEIGHT_OF_CHUNK {
            result.push(chunk);
            chunk = Vec::new();
            chunk_weight = 0;
        }
        chunk.push(msg);
        chunk_weight += weight;
    }
    if !chunk.is_empty() {
        result.push(chunk);
    }
    result
}

impl<S: IStorage + 'static, MS: IMsgSender + 'static> Executor<S, MS> {
    pub fn new(storage: S, msg_sender: MS) -> Self {
        let ctx = Ctx {
            storage,
            msg_sender,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_execute(self) -> Result<TaskStatus, Error> {
        let msgs;
        let servers;
        let me;
        {
            let storage = self.ctx.storage.lock().await;
            msgs = storage.messages().get_messages_to_send()?;
            servers = storage.servers().get_all()?;
            me = match storage.settings().get_me() {
                Ok(val) => val,
                Err(DbError::NotFound) =>
                // System not inited
                {
                    return Ok(TaskStatus::Completed)
                }
                Err(err) => return Err(err.into()),
            };
        }
        for chunk in chunkify(msgs) {
            self.ctx
                .msg_sender
                .send_msgs(servers.clone(), me.clone(), chunk)
                .await?;
        }
        Ok(TaskStatus::Completed)
    }
}

impl<S: IStorage + 'static, MS: IMsgSender + 'static> ITaskExecutor for Executor<S, MS> {
    type Error = Error;

    fn name(&self) -> &'static str {
        PeriodicName::SEND_MSGS
    }

    fn periodic_interval(&self) -> Option<u64> {
        Some(PeriodicInterval::SEND_MSGS)
    }

    fn execute(&self, _: Box<dyn ITaskController>, _: Task) -> DynFut<Result<TaskStatus, Error>> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_execute(),
        )
    }
}
