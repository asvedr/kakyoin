use crate::entities::errors::ApiError;
use atex::TaskStatus;

use crate::proto::common::IApiHandler;
use crate::proto::local::ITaskScheduler;
use crate::proto::net::IFileDownloader;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone)]
pub struct GetFileStatusHandler<FD: IFileDownloader, TS: ITaskScheduler> {
    pub downloader: FD,
    pub scheduler: TS,
}

#[derive(Serialize)]
pub struct Response {
    progress: f64,
    completed: bool,
    failed: bool,
}

#[derive(Deserialize)]
pub struct Query {
    task_id: u64,
    user_id: Uuid,
    file_id: Uuid,
}

impl<FD: IFileDownloader, TS: ITaskScheduler> GetFileStatusHandler<FD, TS> {
    async fn a_handle(self, query: Query) -> Result<Response, ApiError> {
        let task_status = self.scheduler.get_task_status(query.task_id).await;
        let dw_status = self
            .downloader
            .get_loading_status(query.user_id.into(), query.file_id.into())
            .await;
        let (progress, completed, failed) = match (task_status, dw_status) {
            (Ok(TaskStatus::Error(_)), _) => (0.0, false, true),
            (_, Some(status)) => (status.progress, status.completed, false),
            (Ok(TaskStatus::InProgress(_)), _) => (0.0, false, false),
            (Ok(TaskStatus::Completed), _) => (1.0, true, false),
            (Err(err), _) => return Err(ApiError::internal(format!("{:?}", err))),
        };
        Ok(Response {
            progress,
            completed,
            failed,
        })
    }
}

impl<FD: IFileDownloader + Clone + 'static, TS: ITaskScheduler + Clone + 'static> IApiHandler
    for GetFileStatusHandler<FD, TS>
{
    type Response = Response;
    type Body = ();
    type Query = Query;

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn query_fields_force_str() -> &'static [&'static str] {
        &["user_id", "file_id"]
    }

    fn path(&self) -> &str {
        "/api/files/get-status/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(query))
    }
}
