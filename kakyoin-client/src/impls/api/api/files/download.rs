use crate::entities::errors::ApiError;
use crate::entities::message::LocalMsgAttached;
use crate::proto::common::IApiHandler;
use crate::proto::local::{IStorage, ITaskScheduler};

use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone)]
pub struct DownloadFileHandler<S: IStorage, TS: ITaskScheduler> {
    pub storage: S,
    pub task_scheduler: TS,
}

#[derive(Deserialize)]
pub struct Body {
    msg_id: Uuid,
    path: String,
}

#[derive(Serialize)]
pub struct Response {
    task_id: u64,
}

impl<S: IStorage, TS: ITaskScheduler> DownloadFileHandler<S, TS> {
    async fn a_handle(self, body: Body) -> Result<Response, ApiError> {
        let msg = self
            .storage
            .lock()
            .await
            .messages()
            .get_by_uid(&body.msg_id.into())?;
        let (file_id, max_parts) = match msg.attached {
            LocalMsgAttached::FileId { id, parts, .. } => (id, parts),
            _ => return Err(ApiError::msg_has_no_file()),
        };
        let user_id = match msg.sender {
            Some(val) => val,
            None => return Err(ApiError::file_is_local()),
        };
        let task_id = self
            .task_scheduler
            .download_file(user_id, file_id, max_parts, &body.path)
            .await
            .map_err(ApiError::internal)?;
        Ok(Response { task_id })
    }
}

impl<S: IStorage + Clone + 'static, TS: ITaskScheduler + Clone + 'static> IApiHandler
    for DownloadFileHandler<S, TS>
{
    type Response = Response;
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/files/download/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
