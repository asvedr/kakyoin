use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::ClientContact;
use kakyoin_base::proto::serializers::IStrSerializer;
use serde_derive::Serialize;

#[derive(Clone)]
pub struct GetMyContactHandler<S: IStorage, Ser: IStrSerializer<ClientContact>> {
    pub storage: S,
    pub ser: Ser,
}

#[derive(Serialize)]
pub struct Response {
    data: String,
}

impl<S: IStorage + Clone + 'static, Ser: IStrSerializer<ClientContact> + Clone + 'static>
    GetMyContactHandler<S, Ser>
{
    async fn a_handle(self) -> Result<Response, ApiError> {
        let me = self.storage.lock().await.settings().get_me()?;
        let contact = ClientContact::new(me.id.into(), &me.pub_key);
        Ok(Response {
            data: self.ser.serialize(&contact),
        })
    }
}

impl<S: IStorage + Clone + 'static, Ser: IStrSerializer<ClientContact> + Clone + 'static>
    IApiHandler for GetMyContactHandler<S, Ser>
{
    type Response = Response;
    type Body = ();
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/settings/contact/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, _: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle())
    }
}
