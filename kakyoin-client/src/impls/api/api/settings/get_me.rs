use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::common::ICryptoOps;
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::{RsaPrivateKey, RsaPublicKey};
use serde_derive::Serialize;
use std::sync::Arc;
use uuid::Uuid;

pub struct GetMeHandler<
    S: IStorage,
    PrivKeySer: IStrSerializer<RsaPrivateKey>,
    PubKeySer: IStrSerializer<RsaPublicKey>,
    C: ICryptoOps,
> {
    ctx: Arc<Ctx<S, PrivKeySer, PubKeySer, C>>,
}

struct Ctx<S, PrivKeySer, PubKeySer, C> {
    storage: S,
    priv_key_ser: PrivKeySer,
    pub_key_ser: PubKeySer,
    crypto: C,
}

#[derive(Serialize)]
pub struct Response {
    uuid: Uuid,
    priv_key: String,
    pub_key: String,
    key_code: String,
    name: String,
    description: String,
    pic: String,
}

impl<
        S: IStorage + 'static,
        PrivKeySer: IStrSerializer<RsaPrivateKey> + 'static,
        PubKeySer: IStrSerializer<RsaPublicKey> + 'static,
        C: ICryptoOps + 'static,
    > GetMeHandler<S, PrivKeySer, PubKeySer, C>
{
    pub fn new(storage: S, priv_key_ser: PrivKeySer, pub_key_ser: PubKeySer, crypto: C) -> Self {
        let ctx = Ctx {
            storage,
            priv_key_ser,
            pub_key_ser,
            crypto,
        };
        Self { ctx: Arc::new(ctx) }
    }

    async fn a_handle(self) -> Result<Response, ApiError> {
        let me;
        let meta;
        {
            let repo = self.ctx.storage.lock().await.settings();
            me = repo.get_me()?;
            meta = repo.get_meta()?;
        }
        Ok(Response {
            uuid: me.id.into(),
            priv_key: self.ctx.priv_key_ser.serialize(&me.priv_key),
            pub_key: self.ctx.pub_key_ser.serialize(&me.pub_key),
            key_code: self.ctx.crypto.get_key_code(&me.pub_key),
            name: meta.name.clone(),
            description: meta.description.clone(),
            pic: meta.photo.clone(),
        })
    }
}

impl<
        S: IStorage + 'static,
        PrivKeySer: IStrSerializer<RsaPrivateKey> + 'static,
        PubKeySer: IStrSerializer<RsaPublicKey> + 'static,
        C: ICryptoOps + 'static,
    > IApiHandler for GetMeHandler<S, PrivKeySer, PubKeySer, C>
{
    type Response = Response;
    type Body = ();
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/settings/me/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, _: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(
            Self {
                ctx: self.ctx.clone(),
            }
            .a_handle(),
        )
    }
}
