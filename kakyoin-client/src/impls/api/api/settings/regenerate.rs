use crate::entities::common::{Id, Me};
use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::common::ICryptoOps;
use serde_derive::Deserialize;

#[derive(Clone)]
pub struct RegenerateHandler<S: IStorage, CO: ICryptoOps> {
    pub storage: S,
    pub crypto: CO,
}

#[derive(Deserialize)]
pub struct Query {
    uuid: Option<bool>,
    priv_key: Option<bool>,
}

impl<S: IStorage, CO: ICryptoOps> RegenerateHandler<S, CO> {
    async fn a_handle(self, query: Query) -> Result<(), ApiError> {
        let opt_id = match query.uuid {
            Some(true) => Some(Id::new()),
            _ => None,
        };
        let opt_key = match query.priv_key {
            Some(true) => {
                let key = self
                    .crypto
                    .gen_priv_key()
                    .map_err(|e| ApiError::internal(format!("Can not gen key: {:?}", e)))?;
                Some(key)
            }
            _ => None,
        };
        if opt_id.is_none() && opt_key.is_none() {
            return Ok(());
        }
        let storage = self.storage.lock().await;
        let repo = storage.settings();
        let me = repo.get_me()?;
        let id = opt_id.unwrap_or(me.id);
        let pub_key;
        let priv_key;
        if let Some(key) = opt_key {
            pub_key = key.to_public_key();
            priv_key = key;
        } else {
            pub_key = me.pub_key.clone();
            priv_key = me.priv_key.clone();
        }
        repo.set_me(&Me {
            id,
            pub_key,
            priv_key,
        })?;
        Ok(())
    }
}

impl<S: IStorage + Clone + 'static, CO: ICryptoOps + Clone + 'static> IApiHandler
    for RegenerateHandler<S, CO>
{
    type Response = ();
    type Body = ();
    type Query = Query;

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/settings/regenerate/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(query))
    }
}
