use crate::entities::common::{Id, Me};
use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::{IPicLoader, IStorage};
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::RsaPrivateKey;
use serde_derive::Deserialize;
use std::path::Path;
use uuid::Uuid;

#[derive(Clone)]
pub struct PatchMeHandler<S: IStorage, KeySer: IStrSerializer<RsaPrivateKey>, PL: IPicLoader> {
    pub storage: S,
    pub key_ser: KeySer,
    pub pic_loader: PL,
}

#[derive(Deserialize)]
pub struct Body {
    uuid: Option<Uuid>,
    priv_key: Option<String>,
    name: Option<String>,
    description: Option<String>,
    pic_path: Option<String>,
}

impl<S: IStorage, KeySer: IStrSerializer<RsaPrivateKey>, PL: IPicLoader>
    PatchMeHandler<S, KeySer, PL>
{
    async fn a_handle(self, body: Body) -> Result<(), ApiError> {
        let repo = self.storage.lock().await.settings();
        if let Some(path) = body.pic_path {
            let txt = self
                .pic_loader
                .load_pic(Path::new(&path))
                .map_err(ApiError::invalid_pic)?;
            repo.set_photo(&txt)?;
        }
        if let Some(name) = body.name {
            repo.set_name(&name)?;
        }
        if let Some(desc) = body.description {
            repo.set_description(&desc)?;
        }
        let me = repo.get_me()?;
        let id: Id = body.uuid.map(|id| id.into()).unwrap_or(me.id);
        let mut priv_key = me.priv_key.clone();
        let mut pub_key = me.pub_key.clone();
        if let Some(txt) = body.priv_key {
            priv_key = self
                .key_ser
                .deserialize(&txt)
                .map_err(|_| ApiError::invalid_request_body())?;
            pub_key = priv_key.to_public_key();
        }
        repo.set_me(&Me {
            id,
            pub_key,
            priv_key,
        })?;
        Ok(())
    }
}

impl<
        S: IStorage + Clone + 'static,
        KeySer: IStrSerializer<RsaPrivateKey> + Clone + 'static,
        PL: IPicLoader + Clone + 'static,
    > IApiHandler for PatchMeHandler<S, KeySer, PL>
{
    type Response = ();
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/settings/me/"
    }

    fn method(&self) -> Method {
        Method::PATCH
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
