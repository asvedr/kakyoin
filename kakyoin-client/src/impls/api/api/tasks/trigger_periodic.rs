use crate::entities::errors::ApiError;
use crate::entities::tasks::PeriodicName;
use crate::proto::common::IApiHandler;
use crate::proto::local::ITaskScheduler;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Deserialize;

pub struct TriggerPeriodicHandler<TS: ITaskScheduler> {
    pub task_scheduler: TS,
}

#[derive(Deserialize)]
pub struct Query {
    name: String,
}

impl<TS: ITaskScheduler> IApiHandler for TriggerPeriodicHandler<TS> {
    type Response = ();
    type Body = ();
    type Query = Query;

    fn query_fields_force_str() -> &'static [&'static str] {
        &["name"]
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/tasks/trigger-periodic/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        let name: &str = &query.name;
        if !PeriodicName::ALL.contains(&name) {
            return Box::pin(async move { Err(ApiError::not_found("Task not found")) });
        }
        let fut = self.task_scheduler.schedule_periodic(name);
        Box::pin(async move { fut.await.map_err(ApiError::internal) })
    }
}
