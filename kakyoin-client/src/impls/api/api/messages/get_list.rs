use crate::entities::chat::ChatId;
use std::collections::HashSet;

use crate::entities::common::VecBox;
use crate::entities::errors::{ApiError, DbResult};
use crate::entities::message::{LocalMsg, LocalMsgAttached};
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::{Deserialize, Serialize};
use std::str::FromStr;
use uuid::Uuid;

const LIMIT: usize = 20;

#[derive(Clone)]
pub struct GetMessagesHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Deserialize)]
pub struct Query {
    chat: String,
    last_msg: Option<u64>,
}

#[derive(Serialize)]
struct DecFile {
    name: String,
    size: u64,
    user_id: Option<Uuid>,
    file_id: Uuid,
}

#[derive(Serialize)]
struct ApiMsg {
    id: u64,
    uuid: Uuid,
    user: Option<Uuid>,
    text: String,
    file: Option<DecFile>,
    sent: bool,
    read: bool,
    received: bool,
}

#[derive(Serialize)]
pub struct Response {
    data: Vec<ApiMsg>,
}

impl<S: IStorage> GetMessagesHandler<S> {
    async fn a_handle(self, query: Query) -> Result<Response, ApiError> {
        let chat_id = ChatId::from_str(&query.chat).map_err(|_| ApiError::invalid_query())?;
        let msgs = if let Some(msg_id) = query.last_msg {
            self.fetch_msgs_with_last(&chat_id, msg_id).await?
        } else {
            self.fetch_msgs_no_last(&chat_id).await?
        };
        let mut data = Vec::new();
        let mut used_codes = HashSet::new();
        for msg in msgs {
            if let Some(id) = msg.local_uniq_id {
                if used_codes.contains(&id) {
                    continue;
                }
                used_codes.insert(id);
            }
            let user = msg.sender.map(|id| id.into());
            let file = Self::make_file_attach(&user, &msg.text, msg.attached);
            data.push(ApiMsg {
                id: msg.db_id,
                uuid: msg.uuid.into(),
                user,
                text: msg.text.clone(),
                file,
                sent: msg.sent,
                read: msg.read,
                received: msg.received,
            })
        }
        Ok(Response { data })
    }

    async fn fetch_msgs_no_last(&self, id: &ChatId) -> DbResult<VecBox<LocalMsg>> {
        self.storage
            .lock()
            .await
            .messages()
            .get_chat_messages(id, None, LIMIT)
    }

    async fn fetch_msgs_with_last(
        &self,
        chat_id: &ChatId,
        msg_id: u64,
    ) -> DbResult<VecBox<LocalMsg>> {
        let repo = self.storage.lock().await.messages();
        let msg_id = repo.get_first_id_of_same_code(msg_id)?;
        repo.get_chat_messages(chat_id, Some(msg_id), LIMIT)
    }

    fn make_file_attach(
        user_id: &Option<Uuid>,
        text: &str,
        attach: LocalMsgAttached,
    ) -> Option<DecFile> {
        let (id, size) = match attach {
            LocalMsgAttached::FileId { id, size, .. } => (id, size),
            _ => return None,
        };
        Some(DecFile {
            name: text.to_string(),
            size,
            user_id: *user_id,
            file_id: id.into(),
        })
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for GetMessagesHandler<S> {
    type Response = Response;
    type Body = ();
    type Query = Query;

    fn query_fields_force_str() -> &'static [&'static str] {
        &["chat"]
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/chats/msgs/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(query))
    }
}
