use std::mem;
use crate::entities::chat::ChatId;
use crate::entities::common::Id;
use crate::entities::errors::ApiError;
use crate::entities::files::LocalFileMeta;
use crate::entities::message::{LocalMsgAttached, NewMsgToSend};
use crate::proto::common::IApiHandler;
use crate::proto::local::{IMsgMaker, IStorage};
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::common::IFS;
use serde_derive::Deserialize;
use std::path::Path;
use std::str::FromStr;
use base64::{engine::general_purpose, Engine as _};

#[derive(Clone)]
pub struct SendFileMsgHandler<S: IStorage, M: IMsgMaker, FS: IFS> {
    pub storage: S,
    pub msg_maker: M,
    pub fs: FS,
    pub upload_dir: String,
}

#[derive(Deserialize)]
pub struct Body {
    chat: String,
    path: Option<String>,
    data: Option<String>,
    name: String,
}

impl<S: IStorage, M: IMsgMaker, FS: IFS> SendFileMsgHandler<S, M, FS> {
    async fn a_handle(self, body: Body) -> Result<(), ApiError> {
        let chat = ChatId::from_str(&body.chat).map_err(|_| ApiError::invalid_request_body())?;
        let text = body.name.clone();
        let attached = match (body.path, body.data) {
            (Some(path), _) => self.register_file_via_path(path, body.name, &chat).await?,
            (_, Some(data)) => self.register_file_via_data(data, body.name, &chat).await?,
            (None, None) =>
                return Err(ApiError::file_path_or_data_required()),
        };
        let msg = NewMsgToSend {
            chat,
            text,
            attached,
        };
        self.msg_maker.make(msg).await?;
        Ok(())
    }

    async fn register_file_via_data(
        &self,
        data: String,
        name: String,
        chat: &ChatId,
    ) -> Result<LocalMsgAttached, ApiError> {
        let normalized = data.replace('\n', "");
        mem::drop(data);
        let bts = general_purpose::STANDARD.decode(&normalized)
            .map_err(|err| {
                println!(">>\n{}\n<<ERR: {:?}", &normalized, err);
                ApiError::can_not_decode_base64()
            })?;
        let upload_dir = Path::new(&self.upload_dir);
        self.fs.create_dir(upload_dir)
            .map_err(ApiError::internal)?;
        let filename = Id::new().to_string();
        let full_path = upload_dir.join(filename);
        self.fs.write_file(&full_path, &bts)
            .map_err(ApiError::internal)?;
        mem::drop(bts);
        self.register_file_via_path(
            full_path,
            name,
            chat,
        ).await
    }

    async fn register_file_via_path<P: AsRef<Path>>(
        &self,
        path: P,
        name: String,
        chat: &ChatId,
    ) -> Result<LocalMsgAttached, ApiError> {
        let path: &Path = path.as_ref();
        let fs_meta = self
            .fs
            .get_meta(path)
            .map_err(|_| ApiError::invalid_file_path())?;
        let str_path = match path.to_str() {
            None => return Err(
                ApiError::internal("Can not convert path to str")
            ),
            Some(val) => val.to_string(),
        };
        let meta = LocalFileMeta {
            id: Id::new(),
            path: str_path,
            name,
            size: fs_meta.size,
            parts: fs_meta.blocks,
        };
        let result = LocalMsgAttached::FileId {
            id: meta.id,
            size: fs_meta.size,
            parts: fs_meta.blocks,
        };
        let storage = self.storage.lock().await;
        let users = storage.chats().get_participants(chat)?;
        storage.shared_files().register_file(&meta, &users)?;
        Ok(result)
    }
}

impl<S: IStorage + Clone + 'static, M: IMsgMaker + Clone + 'static, FS: IFS + Clone + 'static>
    IApiHandler for SendFileMsgHandler<S, M, FS>
{
    type Response = ();
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/msg/file/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
