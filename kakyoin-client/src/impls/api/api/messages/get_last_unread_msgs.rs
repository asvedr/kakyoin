use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Serialize;
use uuid::Uuid;

const LIMIT: usize = 5;

#[derive(Clone)]
pub struct GetLastUnreadMsgsHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Serialize)]
struct ApiUnreadMsg {
    id: u64,
    uuid: Uuid,
    user: Uuid,
    text: String,
    chat: String,
}

#[derive(Serialize)]
pub struct Response {
    data: Vec<ApiUnreadMsg>,
}

impl<S: IStorage + Clone + 'static> GetLastUnreadMsgsHandler<S> {
    async fn a_handle(self) -> Result<Response, ApiError> {
        let storage = self.storage.lock().await;
        let data = storage
            .messages()
            .get_last_unread_messages(LIMIT)
            .map_err(ApiError::internal)?
            .into_iter()
            .map(|msg| ApiUnreadMsg {
                id: msg.db_id,
                uuid: msg.uuid.into(),
                user: msg.sender.unwrap().into(),
                text: msg.text,
                chat: msg.chat.to_string(),
            })
            .collect::<Vec<_>>();
        Ok(Response { data })
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for GetLastUnreadMsgsHandler<S> {
    type Response = Response;
    type Body = ();
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/msgs/last-unread/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, _: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle())
    }
}
