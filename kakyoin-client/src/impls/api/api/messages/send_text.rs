use crate::entities::chat::ChatId;
use crate::entities::errors::ApiError;
use crate::entities::message::{LocalMsgAttached, NewMsgToSend};
use crate::proto::common::IApiHandler;
use crate::proto::local::IMsgMaker;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Deserialize;
use std::str::FromStr;

#[derive(Clone)]
pub struct SendTextMsgHandler<M: IMsgMaker> {
    pub msg_maker: M,
}

#[derive(Deserialize)]
pub struct Body {
    chat: String,
    text: String,
}

impl<M: IMsgMaker + Clone + 'static> SendTextMsgHandler<M> {
    async fn a_handle(self, body: Body) -> Result<(), ApiError> {
        let chat = ChatId::from_str(&body.chat).map_err(|_| ApiError::invalid_request_body())?;
        let msg = NewMsgToSend {
            chat,
            text: body.text,
            attached: LocalMsgAttached::Nothing,
        };
        self.msg_maker.make(msg).await?;
        Ok(())
    }
}

impl<M: IMsgMaker + Clone + 'static> IApiHandler for SendTextMsgHandler<M> {
    type Response = ();
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/msgs/text/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
