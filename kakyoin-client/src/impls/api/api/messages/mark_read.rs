use crate::entities::common::Id;
use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Deserialize;
use uuid::Uuid;

#[derive(Clone)]
pub struct MarkReadMsgHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Deserialize)]
pub struct Body {
    msgs: Vec<Uuid>,
}

impl<S: IStorage + Clone + 'static> MarkReadMsgHandler<S> {
    async fn a_handle(self, body: Body) -> Result<(), ApiError> {
        let id_list = body.msgs.into_iter().map(Id::from).collect::<Vec<_>>();
        self.storage
            .lock()
            .await
            .messages()
            .mark_as_read(&id_list)
            .map_err(ApiError::internal)?;
        Ok(())
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for MarkReadMsgHandler<S> {
    type Response = ();
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/msgs/mark-read/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
