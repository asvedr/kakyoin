use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;

use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;

use serde_derive::Deserialize;

pub struct DelServerHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Deserialize)]
pub struct Query {
    addr: String,
}

impl<S: IStorage> DelServerHandler<S> {
    async fn a_handle(self, query: Query) -> Result<(), ApiError> {
        let repo = self.storage.lock().await.servers();
        repo.del_servers(&[&query.addr])?;
        Ok(())
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for DelServerHandler<S> {
    type Response = ();
    type Body = ();
    type Query = Query;

    fn query_fields_force_str() -> &'static [&'static str] {
        &["addr"]
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/servers/"
    }

    fn method(&self) -> Method {
        Method::DELETE
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(
            Self {
                storage: self.storage.clone(),
            }
            .a_handle(query),
        )
    }
}
