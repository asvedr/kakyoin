use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::{IStorage, ITaskScheduler};

use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::ServerContact;

use crate::entities::tasks::PeriodicName;
use kakyoin_base::proto::serializers::IStrSerializer;
use serde_derive::Deserialize;

#[derive(Clone)]
pub struct CreateServerHandler<S: IStorage, Ser: IStrSerializer<ServerContact>, TS: ITaskScheduler>
{
    pub storage: S,
    pub ser: Ser,
    pub scheduler: TS,
}

#[derive(Deserialize)]
pub struct Body {
    data: String,
}

impl<S: IStorage, Ser: IStrSerializer<ServerContact>, TS: ITaskScheduler>
    CreateServerHandler<S, Ser, TS>
{
    async fn a_handle(self, body: Body) -> Result<(), ApiError> {
        let srv = self
            .ser
            .deserialize(&body.data)
            .map_err(|_| ApiError::invalid_request_body())?;
        self.storage.lock().await.servers().add_servers(vec![srv])?;
        self.scheduler
            .schedule_periodic(PeriodicName::UPDATE_SERVER_STATUS)
            .await
            .map_err(|err| ApiError::internal(format!("Can not schedule periodic: {:?}", err)))?;
        Ok(())
    }
}

impl<
        S: IStorage + Clone + 'static,
        Ser: IStrSerializer<ServerContact> + Clone + 'static,
        TS: ITaskScheduler + Clone + 'static,
    > IApiHandler for CreateServerHandler<S, Ser, TS>
{
    type Response = ();
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/servers/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
