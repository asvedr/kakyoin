use crate::entities::common::ServerStatus;
use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use crate::proto::net::IServerStatusStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::common::ICryptoOps;
use serde_derive::Serialize;

#[derive(Clone)]
pub struct GetServersHandler<S: IStorage, SS: IServerStatusStorage, C: ICryptoOps> {
    pub storage: S,
    pub server_status: SS,
    pub crypto: C,
}

#[derive(Serialize)]
struct ApiServer {
    addr: String,
    key_code: String,
    status: String,
}

#[derive(Serialize)]
pub struct Response {
    data: Vec<ApiServer>,
}

impl<S: IStorage, SS: IServerStatusStorage, C: ICryptoOps> GetServersHandler<S, SS, C> {
    async fn a_handle(self) -> Result<Response, ApiError> {
        let servers = self.storage.lock().await.servers().get_all()?;
        let statuses = self.server_status.get_statuses().await;
        let mut data = Vec::new();
        for srv in servers.iter() {
            let addr = srv.addr.clone();
            let key = srv.get_key().map_err(ApiError::internal)?;
            let key_code = self.crypto.get_key_code(&key);
            let status = statuses.get(&addr).unwrap_or(&ServerStatus::Unknown);
            let status = format!("{:?}", status);
            data.push(ApiServer {
                addr,
                key_code,
                status,
            });
        }
        Ok(Response { data })
    }
}

impl<
        S: IStorage + Clone + 'static,
        SS: IServerStatusStorage + Clone + 'static,
        C: ICryptoOps + Clone + 'static,
    > IApiHandler for GetServersHandler<S, SS, C>
{
    type Response = Response;
    type Body = ();
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/servers/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, _: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle())
    }
}
