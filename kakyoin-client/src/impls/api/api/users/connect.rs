use crate::entities::chat::{Chat, ChatId};
use crate::entities::errors::ApiError;
use crate::entities::user::UserStatus;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::entities::contact::ClientContact;
use kakyoin_base::proto::serializers::IStrSerializer;
use serde_derive::Deserialize;

#[derive(Clone)]
pub struct ConnectUserHandler<S: IStorage, Ser: IStrSerializer<ClientContact>> {
    pub storage: S,
    pub con_ser: Ser,
}

#[derive(Deserialize)]
pub struct Body {
    contact: String,
}

impl<S: IStorage, Ser: IStrSerializer<ClientContact>> ConnectUserHandler<S, Ser> {
    async fn a_handle(self, body: Body) -> Result<(), ApiError> {
        let contact = self
            .con_ser
            .deserialize(&body.contact)
            .map_err(|_| ApiError::invalid_request_body())?;
        let storage = self.storage.lock().await;
        storage.users().contact(&contact, UserStatus::Trusted)?;
        storage.chats().create_chat(Chat {
            id: ChatId::User(contact.id.into()),
            name: contact.id.to_string(),
            users: vec![contact.id.into()],
        })?;
        Ok(())
    }
}

impl<S: IStorage + Clone + 'static, Ser: IStrSerializer<ClientContact> + Clone + 'static>
    IApiHandler for ConnectUserHandler<S, Ser>
{
    type Response = ();
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/users/connect/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
