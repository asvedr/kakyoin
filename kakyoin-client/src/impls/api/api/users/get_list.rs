use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Serialize;

#[derive(Clone)]
pub struct GetUserListHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Serialize)]
struct UserShort {
    uuid: String,
    name: String,
    pic: String,
    key_code: String,
}

#[derive(Serialize)]
pub struct Response {
    data: Vec<UserShort>,
}

impl<S: IStorage> GetUserListHandler<S> {
    async fn a_handle(self) -> Result<Response, ApiError> {
        let repo = self.storage.lock().await.users();
        let id_list = repo.get_all_ids()?;
        let mut data = Vec::new();
        for user in repo.get_by_uid(&id_list)? {
            data.push(UserShort {
                uuid: user.id.to_string(),
                name: user.info.name.clone(),
                pic: user.info.photo.clone(),
                key_code: user.pub_key_code,
            })
        }
        Ok(Response { data })
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for GetUserListHandler<S> {
    type Response = Response;
    type Body = ();
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/users/list/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, _: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle())
    }
}
