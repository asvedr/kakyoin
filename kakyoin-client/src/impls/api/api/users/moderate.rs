use crate::entities::common::Id;
use crate::entities::errors::ApiError;
use crate::entities::user::UserStatus;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Deserialize;
use uuid::Uuid;

#[derive(Clone)]
pub struct ModerateUserHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Deserialize)]
pub struct Query {
    id: Uuid,
    action: String,
}

impl<S: IStorage> ModerateUserHandler<S> {
    async fn a_handle(self, query: Query) -> Result<(), ApiError> {
        let id: Id = query.id.into();
        let repo = self.storage.lock().await.users();
        match &query.action as &str {
            "ban" => repo.set_status(&id, UserStatus::Banned)?,
            "del" => repo.delete_user(&id)?,
            "trust" => repo.set_status(&id, UserStatus::Trusted)?,
            _ => return Err(ApiError::invalid_query()),
        }
        Ok(())
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for ModerateUserHandler<S> {
    type Response = ();
    type Body = ();
    type Query = Query;

    fn query_fields_force_str() -> &'static [&'static str] {
        &["id", "action"]
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/users/moderate/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(query))
    }
}
