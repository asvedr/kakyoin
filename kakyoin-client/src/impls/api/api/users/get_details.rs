use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::RsaPublicKey;
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone)]
pub struct GetUserDetailsHandler<S: IStorage, KeySer: IStrSerializer<RsaPublicKey>> {
    pub storage: S,
    pub key_ser: KeySer,
}

#[derive(Deserialize)]
pub struct Query {
    id: Uuid,
}

#[derive(Serialize)]
pub struct Response {
    uuid: Uuid,
    name: String,
    description: String,
    pic: String,
    key: String,
    key_code: String,
    status: String,
}

impl<S: IStorage, KeySer: IStrSerializer<RsaPublicKey>> GetUserDetailsHandler<S, KeySer> {
    async fn a_handler(self, query: Query) -> Result<Response, ApiError> {
        let repo = self.storage.lock().await.users();
        let mut users = repo.get_by_uid(&[query.id.into()])?;
        if users.is_empty() {
            return Err(ApiError::not_found("User not found"));
        }
        let user = users.remove(0);
        let key = self.key_ser.serialize(&user.pub_key);
        Ok(Response {
            uuid: user.id.into(),
            name: user.info.name.clone(),
            description: user.info.description.clone(),
            pic: user.info.photo.clone(),
            key,
            key_code: user.pub_key_code,
            status: format!("{:?}", user.status),
        })
    }
}

impl<S: IStorage + Clone + 'static, KeySer: IStrSerializer<RsaPublicKey> + Clone + 'static>
    IApiHandler for GetUserDetailsHandler<S, KeySer>
{
    type Response = Response;
    type Body = ();
    type Query = Query;

    fn query_fields_force_str() -> &'static [&'static str] {
        &["id"]
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/users/detail/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handler(query))
    }
}
