use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::{IAuthTokenManager, IStorage};
use crate::utils::password::hash_pwd;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::{Deserialize, Serialize};

#[derive(Clone)]
pub struct LoginHandler<S: IStorage, TM: IAuthTokenManager> {
    pub storage: S,
    pub token_manager: TM,
}

#[derive(Deserialize)]
pub struct Body {
    password: String,
}

#[derive(Serialize)]
pub struct Response {
    token: String,
}

impl<S: IStorage + 'static, TM: IAuthTokenManager + 'static> LoginHandler<S, TM> {
    async fn a_handle(self, body: Body) -> Result<Response, ApiError> {
        let hashed = hash_pwd(&body.password);
        let expected = self.storage.lock().await.settings().get_pwd_hash()?;
        if expected != Some(hashed) {
            return Err(ApiError::invalid_creds());
        }
        let token = self.token_manager.gen_token().await;
        Ok(Response { token })
    }
}

impl<S: IStorage + Clone + 'static, TM: IAuthTokenManager + Clone + 'static> IApiHandler
    for LoginHandler<S, TM>
{
    type Response = Response;
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn auth_token_required() -> bool {
        false
    }

    fn path(&self) -> &str {
        "/api/login/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
