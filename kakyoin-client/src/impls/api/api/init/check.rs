use crate::entities::errors::{ApiError, InitializerError};
use crate::proto::common::IApiHandler;
use crate::proto::local::IInitializer;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Serialize;

pub struct CheckInitHandler<I: IInitializer> {
    pub initializer: I,
}

const INITED: &str = "INITED";
const NOT_INITED: &str = "NOT_INITED";

#[derive(Serialize)]
pub struct Response {
    status: &'static str,
}

impl<I: IInitializer + 'static> IApiHandler for CheckInitHandler<I> {
    type Response = Response;
    type Body = ();
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn auth_token_required() -> bool {
        false
    }

    fn path(&self) -> &str {
        "/api/check-init/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, _: (), _: ()) -> DynFutRes<Response, ApiError> {
        let fut = self.initializer.check_init();
        Box::pin(async move {
            let status = match fut.await {
                Ok(_) => INITED,
                Err(InitializerError::NotInited) => NOT_INITED,
                Err(err) => return Err(ApiError::internal(err)),
            };
            Ok(Response { status })
        })
    }
}
