use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IInitializer;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::serializers::IStrSerializer;
use rsa::RsaPrivateKey;
use serde_derive::Deserialize;
use uuid::Uuid;

#[derive(Clone)]
pub struct InitHandler<I: IInitializer, KeySer: IStrSerializer<RsaPrivateKey>> {
    pub initializer: I,
    pub key_ser: KeySer,
}

#[derive(Deserialize)]
pub struct Body {
    uuid: Option<Uuid>,
    priv_key: Option<String>,
    name: String,
    description: String,
    password: Option<String>,
}

impl<
        I: IInitializer + Clone + 'static,
        KeySer: IStrSerializer<RsaPrivateKey> + Clone + 'static,
    > InitHandler<I, KeySer>
{
    async fn a_handle(self, body: Body) -> Result<(), ApiError> {
        let priv_key = match body.priv_key {
            Some(val) => Some(self.deser_key(val)?),
            None => None,
        };
        self.initializer
            .init(
                body.uuid.map(|id| id.into()),
                priv_key,
                body.name,
                body.description,
                body.password,
            )
            .await
            .map_err(ApiError::internal)
    }

    fn deser_key(&self, src: String) -> Result<RsaPrivateKey, ApiError> {
        self.key_ser
            .deserialize(&src)
            .map_err(|_| ApiError::invalid_request_body())
    }
}

impl<
        I: IInitializer + Clone + 'static,
        KeySer: IStrSerializer<RsaPrivateKey> + Clone + 'static,
    > IApiHandler for InitHandler<I, KeySer>
{
    type Response = ();
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/init/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: (), body: Body) -> DynFutRes<(), ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
