use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Serialize;

pub struct GetChatListHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Serialize)]
struct ApiChat {
    id: String,
    name: String,
    last_msg: String,
    new: bool,
}

#[derive(Serialize)]
pub struct Response {
    data: Vec<ApiChat>,
}

impl<S: IStorage> GetChatListHandler<S> {
    async fn a_handle(self) -> Result<Response, ApiError> {
        let data = self
            .storage
            .lock()
            .await
            .chats()
            .get_short_info_for_all()?
            .into_iter()
            .map(|chat| ApiChat {
                id: chat.id.to_string(),
                name: chat.name,
                last_msg: chat.last_msg,
                new: !chat.read,
            })
            .collect::<Vec<_>>();
        Ok(Response { data })
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for GetChatListHandler<S> {
    type Response = Response;
    type Body = ();
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/chats/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, _: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(
            Self {
                storage: self.storage.clone(),
            }
            .a_handle(),
        )
    }
}
