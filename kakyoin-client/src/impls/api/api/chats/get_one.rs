use crate::entities::chat::ChatId;
use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use base64::{engine::general_purpose, Engine as _};
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::{Deserialize, Serialize};
use std::str::FromStr;

#[derive(Clone)]
pub struct GetChatDetailsHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Deserialize)]
pub struct Query {
    id: String,
}

#[derive(Serialize)]
struct ApiUser {
    uuid: String,
    name: String,
    pic: String,
    key_code: String,
}

#[derive(Serialize)]
pub struct Response {
    id: String,
    name: String,
    participants: Vec<ApiUser>,
}

impl<S: IStorage> GetChatDetailsHandler<S> {
    async fn a_handle(self, query: Query) -> Result<Response, ApiError> {
        let id = ChatId::from_str(&query.id).map_err(|_| ApiError::invalid_query())?;
        let storage = self.storage.lock().await;
        let chat = storage.chats().get_chat(&id)?;
        let user_id_list = storage.chats().get_participants(&id)?;
        let mut participants = Vec::new();
        for usr in storage.users().get_by_uid(&user_id_list)? {
            let pic = general_purpose::STANDARD.encode(&usr.info.photo);
            let api_usr = ApiUser {
                uuid: usr.id.to_string(),
                name: usr.info.name.to_string(),
                pic,
                key_code: usr.pub_key_code,
            };
            participants.push(api_usr)
        }
        Ok(Response {
            id: chat.id.to_string(),
            name: chat.name,
            participants,
        })
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for GetChatDetailsHandler<S> {
    type Response = Response;
    type Body = ();
    type Query = Query;

    fn query_fields_force_str() -> &'static [&'static str] {
        &["id"]
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/chats/info/"
    }

    fn method(&self) -> Method {
        Method::GET
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(query))
    }
}
