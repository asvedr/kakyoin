use crate::entities::chat::{Chat, ChatId};
use crate::entities::common::Id;
use crate::entities::errors::ApiError;
use crate::entities::message::{LocalMsgAttached, NewMsgToSend};
use crate::proto::common::IApiHandler;
use crate::proto::local::{IMsgMaker, IStorage};
use std::collections::HashSet;
use std::str::FromStr;

use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use kakyoin_base::proto::common::ICryptoOps;
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone)]
pub struct CreateOrUpdateChatHandler<S: IStorage, C: ICryptoOps, M: IMsgMaker> {
    pub storage: S,
    pub crypto: C,
    pub msg_maker: M,
}

#[derive(Deserialize)]
pub struct Body {
    chat_id: Option<String>,
    users: Vec<Uuid>,
    name: String,
}

#[derive(Serialize)]
pub struct Response {
    id: String,
}

impl<S: IStorage, C: ICryptoOps, M: IMsgMaker> CreateOrUpdateChatHandler<S, C, M> {
    async fn a_handle(self, body: Body) -> Result<Response, ApiError> {
        let users = body
            .users
            .iter()
            .map(|id| (*id).into())
            .collect::<Vec<Id>>();
        let str_id = if let Some(id) = body.chat_id {
            id
        } else {
            return self.create_chat(body.name, users).await;
        };
        let id = ChatId::from_str(&str_id).map_err(|_| ApiError::invalid_query())?;
        self.update_chat(id, users).await
    }

    async fn update_chat(self, id: ChatId, users: Vec<Id>) -> Result<Response, ApiError> {
        let participants;
        let chat;
        {
            let storage = self.storage.lock().await;
            participants = storage
                .chats()
                .get_participants(&id)
                .map_err(ApiError::internal)?;
            chat = storage
                .chats()
                .get_chat(&id)
                .map_err(|_| ApiError::not_found("Chat not found"))?;
            storage.chats().add_participants(&id, &users)?;
        }
        let mut user_set = users.into_iter().collect::<HashSet<_>>();
        user_set.extend(participants.into_iter());
        let users = user_set.into_iter().collect::<Vec<_>>();
        let s_chat_id = id.to_string();
        self.msg_maker
            .make(NewMsgToSend {
                chat: id,
                text: "".to_string(),
                attached: LocalMsgAttached::Invite {
                    name: chat.name.clone(),
                    participants: users,
                },
            })
            .await?;
        Ok(Response { id: s_chat_id })
    }

    async fn create_chat(self, name: String, users: Vec<Id>) -> Result<Response, ApiError> {
        let chat_id = ChatId::Chat(Id::new());
        let attached = LocalMsgAttached::Invite {
            name: name.clone(),
            participants: users.clone(),
        };
        let s_chat_id = chat_id.to_string();
        let chat = Chat {
            id: chat_id.clone(),
            name,
            users,
        };
        self.storage.lock().await.chats().create_chat(chat)?;
        self.msg_maker
            .make(NewMsgToSend {
                chat: chat_id,
                text: "".to_string(),
                attached,
            })
            .await?;
        Ok(Response { id: s_chat_id })
    }
}

impl<
        S: IStorage + Clone + 'static,
        C: ICryptoOps + Clone + 'static,
        M: IMsgMaker + Clone + 'static,
    > IApiHandler for CreateOrUpdateChatHandler<S, C, M>
{
    type Response = Response;
    type Body = Body;
    type Query = ();

    fn override_query() -> Option<Self::Query> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/chats/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, _: Self::Query, body: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(body))
    }
}
