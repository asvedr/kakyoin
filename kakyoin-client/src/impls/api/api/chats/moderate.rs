use crate::entities::chat::ChatId;
use crate::entities::errors::ApiError;
use crate::proto::common::IApiHandler;
use crate::proto::local::IStorage;
use hyper::Method;
use kakyoin_base::entities::common::DynFutRes;
use serde_derive::Deserialize;
use std::str::FromStr;

#[derive(Clone)]
pub struct ModerateChatHandler<S: IStorage> {
    pub storage: S,
}

#[derive(Deserialize)]
pub struct Query {
    id: String,
    action: String,
}

impl<S: IStorage> ModerateChatHandler<S> {
    async fn a_handle(self, query: Query) -> Result<(), ApiError> {
        let id = ChatId::from_str(&query.id).map_err(|_| ApiError::invalid_query())?;
        let chats = self.storage.lock().await.chats();
        match &query.action as &str {
            "ban" => chats.ban_chat(&id)?,
            "del" => chats.delete_chat(&id)?,
            _ => return Err(ApiError::invalid_query()),
        }
        Ok(())
    }
}

impl<S: IStorage + Clone + 'static> IApiHandler for ModerateChatHandler<S> {
    type Response = ();
    type Body = ();
    type Query = Query;

    fn query_fields_force_str() -> &'static [&'static str] {
        &["id", "action"]
    }

    fn override_body() -> Option<Self::Body> {
        Some(())
    }

    fn path(&self) -> &str {
        "/api/chats/moderate/"
    }

    fn method(&self) -> Method {
        Method::POST
    }

    fn handle(&self, query: Self::Query, _: Self::Body) -> DynFutRes<Self::Response, ApiError> {
        Box::pin(self.clone().a_handle(query))
    }
}
