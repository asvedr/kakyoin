use crate::proto::common::IService;
use atex::ITaskManager;
use kakyoin_base::entities::common::DynFutRes;
use std::io::{Error, ErrorKind};
use std::sync::Arc;

pub struct TaskRunner {
    pub manager: Arc<dyn ITaskManager>,
}

unsafe impl Send for TaskRunner {}
unsafe impl Sync for TaskRunner {}

impl IService for TaskRunner {
    fn run(&mut self) -> DynFutRes<(), Error> {
        let fut = self.manager.run();
        Box::pin(async move {
            fut.await.map_err(|err| {
                let msg = format!("{:?}", err);
                Error::new(ErrorKind::Other, msg)
            })
        })
    }
}
