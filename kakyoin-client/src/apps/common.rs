use crate::apps::serializers::{key_ser, server_contact_ser};
use crate::entities::config::Config;
use crate::impls::local::auth_token_manager::AuthTokenManager;
use crate::impls::local::db::storage::Storage;
use crate::impls::local::user_meta_hash_calculator::UserMetaHashCalculator;
use crate::proto::local::IStorage;
use crate::utils::time::get_now;
use kakyoin_base::impls::crypto_ops::CryptoOps;
use kakyoin_base::impls::fs::FS;
use kakyoin_base::impls::logger::Logger;
use kakyoin_base::impls::msg_packer::ApiMsgPacker;
use kakyoin_base::proto::serializers::IApiMsgPacker;
use mddd::macros::singleton;
use mddd::traits::IConfig;
use std::io::Write;

#[singleton(no_mutex)]
pub fn config() -> Config {
    match Config::build() {
        Ok(val) => val,
        Err(err) => panic!("Can not init config: {:?}", err),
    }
}

#[singleton(no_mutex)]
pub fn logger() -> Logger {
    let cnf = config();
    let writer: Box<dyn Write> = match &cnf.log_file as &str {
        "stderr" => Box::new(std::io::stderr()),
        "stdout" => Box::new(std::io::stdout()),
        other => match std::fs::File::open(other) {
            Ok(val) => Box::new(val),
            _ => panic!("Can not open log file"),
        },
    };
    let log_prefix = cnf.log_prefix.clone();
    Logger::new(
        cnf.log_level,
        Box::new(move || format!("{}ts={}", log_prefix, get_now())),
        writer,
    )
}

#[singleton(no_mutex)]
pub fn fs() -> FS {
    FS
}

#[singleton(no_mutex)]
pub fn crypto() -> CryptoOps<&'static Logger> {
    CryptoOps::new(logger())
}

pub fn user_meta_hasher() -> UserMetaHashCalculator {
    UserMetaHashCalculator
}

#[singleton(no_mutex)]
pub fn storage() -> Box<dyn IStorage> {
    let res = Storage::new(
        &config().db_file,
        key_ser(),
        server_contact_ser(),
        user_meta_hasher(),
        crypto(),
    );
    match res {
        Ok(val) => Box::new(val),
        Err(err) => panic!("Can not init storage: {:?}", err),
    }
}

#[singleton(no_mutex)]
pub fn msg_packer() -> Box<dyn IApiMsgPacker> {
    Box::new(ApiMsgPacker::new(crypto(), key_ser()))
}

#[singleton(no_mutex)]
pub fn auth_token_manager() -> AuthTokenManager {
    AuthTokenManager::new()
}
