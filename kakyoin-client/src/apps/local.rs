use crate::apps::common::{auth_token_manager, crypto, fs, logger, storage};
use kakyoin_base::impls::fs::FS;

use crate::apps::tasks::task_manager;
use crate::impls::local::initializer::Initializer;
use crate::impls::local::msg_maker::MsgMaker;
use crate::impls::local::pic_loader::PicLoader;
use crate::impls::local::task_scheduler::TaskScheduler;
use crate::proto::local::{IInitializer, IMsgMaker, ITaskScheduler};
use mddd::macros::singleton;

#[singleton(no_mutex)]
pub fn initializer() -> Box<dyn IInitializer> {
    let result = Initializer::new(&**storage(), crypto(), auth_token_manager());
    Box::new(result)
}

#[singleton(no_mutex)]
pub fn task_scheduler() -> Box<dyn ITaskScheduler> {
    Box::new(TaskScheduler {
        manager: task_manager().clone(),
    })
}

#[singleton(no_mutex)]
pub fn msg_maker() -> Box<dyn IMsgMaker> {
    Box::new(MsgMaker::new(logger(), &**storage(), &**task_scheduler()))
}

#[singleton(no_mutex)]
pub fn pic_loader() -> PicLoader<&'static FS> {
    PicLoader { fs: fs() }
}
