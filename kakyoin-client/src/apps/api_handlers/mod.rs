mod chats;
mod files;
mod init;
mod messages;
mod servers;
mod settings;
mod tasks;
mod users;

use crate::proto::common::IApiHandlerWrapped;
use mddd::macros::singleton;

#[singleton(no_mutex)]
fn _all_vec_box() -> Vec<Box<dyn IApiHandlerWrapped>> {
    let mut handlers = init::all();
    handlers.append(&mut settings::all());
    handlers.append(&mut chats::all());
    handlers.append(&mut files::all());
    handlers.append(&mut messages::all());
    handlers.append(&mut servers::all());
    handlers.append(&mut tasks::all());
    handlers.append(&mut users::all());
    handlers
}

#[singleton(no_mutex)]
pub fn all() -> Vec<&'static dyn IApiHandlerWrapped> {
    _all_vec_box().iter().map(|h| &**h).collect::<Vec<_>>()
}
