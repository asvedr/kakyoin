use crate::apps::common::{auth_token_manager, fs, storage, config};
use crate::apps::local::msg_maker;
use crate::entities::common::VecBox;
use crate::impls::api::api::messages::get_last_unread_msgs::GetLastUnreadMsgsHandler;
use crate::impls::api::api::messages::get_list::GetMessagesHandler;
use crate::impls::api::api::messages::mark_read::MarkReadMsgHandler;
use crate::impls::api::api::messages::send_file::SendFileMsgHandler;
use crate::impls::api::api::messages::send_text::SendTextMsgHandler;
use crate::impls::api_wrapper::ApiWrapper;
use crate::proto::common::IApiHandlerWrapped;

fn get_list() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetMessagesHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

fn get_last_unread_msgs() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetLastUnreadMsgsHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

fn send_file() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        SendFileMsgHandler {
            storage: &**storage(),
            msg_maker: &**msg_maker(),
            fs: fs(),
            upload_dir: config().upload_dir.clone(),
        },
        auth_token_manager(),
    )
}

fn send_text() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        SendTextMsgHandler {
            msg_maker: &**msg_maker(),
        },
        auth_token_manager(),
    )
}

fn mark_read() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        MarkReadMsgHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

pub fn all() -> VecBox<dyn IApiHandlerWrapped> {
    vec![
        get_list(),
        get_last_unread_msgs(),
        send_text(),
        send_file(),
        mark_read(),
    ]
}
