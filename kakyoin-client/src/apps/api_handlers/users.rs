use crate::apps::common::{auth_token_manager, storage};
use crate::apps::serializers::{client_contact_ser, key_ser};
use crate::entities::common::VecBox;
use crate::impls::api::api::users::connect::ConnectUserHandler;
use crate::impls::api::api::users::get_details::GetUserDetailsHandler;
use crate::impls::api::api::users::get_list::GetUserListHandler;
use crate::impls::api::api::users::moderate::ModerateUserHandler;
use crate::impls::api_wrapper::ApiWrapper;
use crate::proto::common::IApiHandlerWrapped;

fn connect() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        ConnectUserHandler {
            storage: &**storage(),
            con_ser: client_contact_ser(),
        },
        auth_token_manager(),
    )
}

fn get_details() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetUserDetailsHandler {
            storage: &**storage(),
            key_ser: key_ser(),
        },
        auth_token_manager(),
    )
}

fn get_list() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetUserListHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

fn moderate() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        ModerateUserHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

pub fn all() -> VecBox<dyn IApiHandlerWrapped> {
    vec![connect(), get_details(), get_list(), moderate()]
}
