use crate::apps::common::{auth_token_manager, storage};
use crate::apps::local::initializer;
use crate::apps::serializers::key_ser;
use crate::impls::api::api::init::check::CheckInitHandler;
use crate::impls::api::api::init::init::InitHandler;
use crate::impls::api::api::init::login::LoginHandler;
use crate::impls::api_wrapper::ApiWrapper;
use crate::proto::common::IApiHandlerWrapped;

fn h_check() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        CheckInitHandler {
            initializer: &**initializer(),
        },
        auth_token_manager(),
    )
}

fn h_init() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        InitHandler {
            initializer: &**initializer(),
            key_ser: key_ser(),
        },
        auth_token_manager(),
    )
}

fn h_login() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        LoginHandler {
            storage: &**storage(),
            token_manager: auth_token_manager(),
        },
        auth_token_manager(),
    )
}

pub fn all() -> Vec<Box<dyn IApiHandlerWrapped>> {
    vec![h_check(), h_init(), h_login()]
}
