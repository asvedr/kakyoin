use crate::apps::common::auth_token_manager;
use crate::apps::local::task_scheduler;
use crate::entities::common::VecBox;
use crate::impls::api::api::tasks::trigger_periodic::TriggerPeriodicHandler;
use crate::impls::api_wrapper::ApiWrapper;
use crate::proto::common::IApiHandlerWrapped;

fn trigger() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        TriggerPeriodicHandler {
            task_scheduler: &**task_scheduler(),
        },
        auth_token_manager(),
    )
}

pub fn all() -> VecBox<dyn IApiHandlerWrapped> {
    vec![trigger()]
}
