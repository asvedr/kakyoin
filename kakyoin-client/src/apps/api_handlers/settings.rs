use crate::apps::common::{auth_token_manager, crypto, storage};
use crate::apps::local::pic_loader;
use crate::apps::serializers::{client_contact_ser, key_ser};
use crate::impls::api::api::settings::get_me::GetMeHandler;
use crate::impls::api::api::settings::get_my_contact::GetMyContactHandler;
use crate::impls::api::api::settings::patch_me::PatchMeHandler;
use crate::impls::api::api::settings::regenerate::RegenerateHandler;
use crate::impls::api_wrapper::ApiWrapper;
use crate::proto::common::IApiHandlerWrapped;

fn get_me() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetMeHandler::new(&**storage(), key_ser(), key_ser(), crypto()),
        auth_token_manager(),
    )
}

fn get_my_contact() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetMyContactHandler {
            storage: &**storage(),
            ser: client_contact_ser(),
        },
        auth_token_manager(),
    )
}

fn patch_me() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        PatchMeHandler {
            storage: &**storage(),
            key_ser: key_ser(),
            pic_loader: pic_loader(),
        },
        auth_token_manager(),
    )
}

fn regenerate() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        RegenerateHandler {
            storage: &**storage(),
            crypto: crypto(),
        },
        auth_token_manager(),
    )
}

pub fn all() -> Vec<Box<dyn IApiHandlerWrapped>> {
    vec![get_me(), patch_me(), regenerate(), get_my_contact()]
}
