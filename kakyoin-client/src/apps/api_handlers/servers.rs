use crate::apps::common::{auth_token_manager, crypto, storage};
use crate::apps::kakyoin_server_tools::server_status_storage;
use crate::apps::local::task_scheduler;
use crate::apps::serializers::server_contact_ser;
use crate::entities::common::VecBox;
use crate::impls::api::api::servers::create::CreateServerHandler;
use crate::impls::api::api::servers::del::DelServerHandler;
use crate::impls::api::api::servers::get::GetServersHandler;
use crate::impls::api_wrapper::ApiWrapper;
use crate::proto::common::IApiHandlerWrapped;

fn create() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        CreateServerHandler {
            storage: &**storage(),
            ser: server_contact_ser(),
            scheduler: &**task_scheduler(),
        },
        auth_token_manager(),
    )
}

fn get() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetServersHandler {
            storage: &**storage(),
            server_status: server_status_storage(),
            crypto: crypto(),
        },
        auth_token_manager(),
    )
}

fn del() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        DelServerHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

pub fn all() -> VecBox<dyn IApiHandlerWrapped> {
    vec![create(), get(), del()]
}
