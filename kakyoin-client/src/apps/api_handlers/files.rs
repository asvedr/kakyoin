use crate::apps::common::{auth_token_manager, storage};
use crate::apps::kakyoin_server_tools::file_downloader;
use crate::apps::local::task_scheduler;
use crate::entities::common::VecBox;
use crate::impls::api::api::files::download::DownloadFileHandler;
use crate::impls::api::api::files::get_status::GetFileStatusHandler;
use crate::impls::api_wrapper::ApiWrapper;
use crate::proto::common::IApiHandlerWrapped;

fn download() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        DownloadFileHandler {
            storage: &**storage(),
            task_scheduler: &**task_scheduler(),
        },
        auth_token_manager(),
    )
}

fn get_status() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetFileStatusHandler {
            downloader: &**file_downloader(),
            scheduler: &**task_scheduler(),
        },
        auth_token_manager(),
    )
}

pub fn all() -> VecBox<dyn IApiHandlerWrapped> {
    vec![download(), get_status()]
}
