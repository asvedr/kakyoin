use crate::apps::common::{auth_token_manager, crypto, storage};
use crate::apps::local::msg_maker;
use crate::entities::common::VecBox;
use crate::impls::api::api::chats::create_or_update::CreateOrUpdateChatHandler;
use crate::impls::api::api::chats::get_list::GetChatListHandler;
use crate::impls::api::api::chats::get_one::GetChatDetailsHandler;
use crate::impls::api::api::chats::moderate::ModerateChatHandler;
use crate::impls::api_wrapper::ApiWrapper;
use crate::proto::common::IApiHandlerWrapped;

fn create_or_update() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        CreateOrUpdateChatHandler {
            storage: &**storage(),
            crypto: crypto(),
            msg_maker: &**msg_maker(),
        },
        auth_token_manager(),
    )
}

fn get_list() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetChatListHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

fn get_one() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        GetChatDetailsHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

fn moderate() -> Box<dyn IApiHandlerWrapped> {
    ApiWrapper::wrap(
        ModerateChatHandler {
            storage: &**storage(),
        },
        auth_token_manager(),
    )
}

pub fn all() -> VecBox<dyn IApiHandlerWrapped> {
    vec![create_or_update(), get_list(), get_one(), moderate()]
}
