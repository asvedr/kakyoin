use crate::apps::common::{config, logger, storage};
use crate::apps::kakyoin_server_tools::{
    file_downloader, msg_dispatcher, msg_fetcher, msg_sender, server_factory, server_status_storage,
};
use crate::impls::periodics::{fetch_msgs, send_msgs, update_meta, update_server_status};
use crate::impls::tasks::{download_file, send_msg};
use atex::{ITaskManager, TaskManagerBuilder};
use kakyoin_base::entities::log::Level;
use kakyoin_base::proto::common::ILogger;
use mddd::macros::singleton;
use std::sync::Arc;

#[singleton(no_mutex)]
pub fn task_manager() -> Arc<dyn ITaskManager> {
    let lg = logger();
    let cnf = config();
    let man = TaskManagerBuilder::new_mem()
        .set_log_func(Box::new(move |msg| lg.log(Level::Err, msg)))
        .set_pool_size(cnf.task_pool_size)
        .set_succ_task_lifetime(10)
        .set_err_task_lifetime(10)
        .add_executor(download_file::Executor::new(&**file_downloader()))
        .add_executor(send_msg::Executor::new(&**storage(), &**msg_sender()))
        .add_executor(fetch_msgs::Executor::new(
            &**storage(),
            &**msg_fetcher(),
            &**msg_dispatcher(),
        ))
        .add_executor(send_msgs::Executor::new(&**storage(), &**msg_sender()))
        .add_executor(update_meta::Executor::new(
            logger(),
            &**storage(),
            &**server_factory(),
        ))
        .add_executor(update_server_status::Executor::new(
            &**storage(),
            server_status_storage(),
            &**server_factory(),
        ))
        .build()
        .expect("Can not build task manager");
    Arc::from(man)
}
