use crate::apps::common::config;
use kakyoin_base::impls::http_client::HttpClient;
use mddd::macros::singleton;

#[singleton(no_mutex)]
pub fn http_client() -> HttpClient {
    HttpClient::new(config().http_timeout)
}
