use kakyoin_base::impls::serializers::{client_contact, rsa_key, server_contact};

pub fn key_ser() -> rsa_key::Serializer {
    rsa_key::Serializer
}

pub fn server_contact_ser() -> server_contact::Serializer {
    server_contact::Serializer
}

pub fn client_contact_ser() -> client_contact::Serializer {
    client_contact::Serializer
}
