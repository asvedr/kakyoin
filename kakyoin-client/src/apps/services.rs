use crate::apps::api_handlers;
use crate::apps::common::{config, logger};
use crate::apps::tasks::task_manager;
use crate::impls::server::ApiServer;
use crate::impls::task_reporter::TaskReporter;
use crate::impls::task_runner::TaskRunner;
use crate::proto::common::IService;

pub fn api_server() -> Box<dyn IService> {
    let port = config().api_server_port;
    let addr = format!("127.0.0.1:{}", port);
    Box::new(ApiServer::new(logger(), api_handlers::all(), &addr))
}

pub fn task_runner() -> Box<dyn IService> {
    Box::new(TaskRunner {
        manager: task_manager().clone(),
    })
}

pub fn task_reporter() -> Box<dyn IService> {
    Box::new(TaskReporter {
        manager: task_manager().clone(),
        logger: logger(),
        config: config(),
    })
}
