use crate::apps::common::{config, crypto, fs, logger, msg_packer, storage};
use crate::apps::serializers::key_ser;
use crate::impls::net::file_downloader::FileDownloader;
use crate::impls::net::file_uploader::FileUploader;
use crate::impls::net::msg_dispatcher::MsgDispatcher;
use crate::impls::net::msg_fetcher::MsgFetcher;
use crate::impls::net::msg_sender::MsgSender;
use crate::impls::net::server_client::ServerClientFactory;
use crate::impls::net::server_status_storage::ServerStatusStorage;
use crate::proto::net::{
    IFileDownloader, IFileUploader, IMsgDispatcher, IMsgFetcher, IMsgSender, IServerClientFactory,
};
use kakyoin_base::impls::http_client::HttpClient;
use mddd::macros::singleton;

#[singleton(no_mutex)]
fn http_client() -> HttpClient {
    HttpClient::new(config().http_timeout)
}

#[singleton(no_mutex)]
pub fn server_factory() -> Box<dyn IServerClientFactory> {
    Box::new(ServerClientFactory::new(
        http_client(),
        crypto(),
        config().max_retries,
    ))
}

#[singleton(no_mutex)]
pub fn file_downloader() -> Box<dyn IFileDownloader> {
    Box::new(FileDownloader::new(
        &**storage(),
        fs(),
        &**server_factory(),
        key_ser(),
        &**msg_packer(),
    ))
}

#[singleton(no_mutex)]
pub fn file_uploader() -> Box<dyn IFileUploader> {
    Box::new(FileUploader::new(
        &**storage(),
        &**server_factory(),
        fs(),
        &**msg_packer(),
        crypto(),
        key_ser(),
    ))
}

#[singleton(no_mutex)]
pub fn msg_fetcher() -> Box<dyn IMsgFetcher> {
    Box::new(MsgFetcher::new(
        logger(),
        &**server_factory(),
        &**msg_packer(),
    ))
}

#[singleton(no_mutex)]
pub fn msg_sender() -> Box<dyn IMsgSender> {
    Box::new(MsgSender::new(
        &**server_factory(),
        &**storage(),
        key_ser(),
        &**msg_packer(),
        crypto(),
    ))
}

#[singleton(no_mutex)]
pub fn msg_dispatcher() -> Box<dyn IMsgDispatcher> {
    Box::new(MsgDispatcher::new(
        logger(),
        &**storage(),
        key_ser(),
        &**file_downloader(),
        &**file_uploader(),
        crypto(),
    ))
}

#[singleton(no_mutex)]
pub fn server_status_storage() -> ServerStatusStorage {
    ServerStatusStorage::default()
}
