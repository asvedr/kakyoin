mod apps;
mod entities;
mod impls;
mod proto;
mod utils;

async fn a_main() {
    use crate::proto::local::IAuthTokenManager;
    use apps::common::{auth_token_manager, storage};
    use apps::services;
    use futures_util::future::join_all;

    storage().init().await.expect("Can not init database");

    let has_pwd = storage()
        .lock()
        .await
        .settings()
        .get_pwd_hash()
        .expect("Can not get hash from DB")
        .is_some();

    auth_token_manager().set_active(has_pwd).await;

    let mut api_server = services::api_server();
    let mut task_runner = services::task_runner();
    let mut reporter = services::task_reporter();

    for res in join_all([api_server.run(), task_runner.run(), reporter.run()]).await {
        if let Err(err) = res {
            panic!("Service failed: {:?}", err)
        }
    }
}

fn main() {
    use apps::common;

    tokio::runtime::Builder::new_multi_thread()
        .worker_threads(common::config().threads)
        .enable_all()
        .build()
        .expect("Can not build runtime")
        .block_on(async move { a_main().await })
}
