use sha2::{Digest, Sha256};

pub fn hash_pwd(pwd: &str) -> String {
    let mut hasher = Sha256::new();
    hasher.update(pwd);
    format!("{:X}", hasher.finalize())
}
